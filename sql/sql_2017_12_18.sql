ALTER TABLE sis_par_cotizacion ADD mensaje TEXT NOT NULL AFTER estado;


INSERT INTO sis_con_contenido (id, titulo, llave, tipo, cuerpo, inicio, peso, created_at, updated_at, deleted_at) VALUES ('0', 'Cambio de Estado de cotizacion ', 'cambio_estado_cotizacion', 'E', '<p>Se ha cambiado de estado la cotizaci&oacute;n con la siguiente informaci&oacute;n</p>\r\n<p>Cliente: {{$nombre_cliente}}</p>\r\n<p>Comercial: {{$nombre_comercial}}</p>\r\n<p>Fecha de cotizaci&oacute;n: {{$cotizacion_fecha}}</p>\r\n<p>Estado: {{$cotizacion_estado}}</p>\r\n<p>&nbsp;</p>\r\n', 'N', '0', '0000-00-00 00:00:00', '2017-02-28 13:52:18', NULL);

