CREATE TABLE sis_frm_formulario (
  id int(11) NOT NULL primary key auto_increment,
  titulo varchar(50) NOT NULL,
  llave varchar(50) NOT NULL,
  descripcion text NOT NULL,
  clase varchar(50),
  destinatarios text,
  created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE sis_frm_campo (
  id int(11) NOT NULL primary key auto_increment,
  id_form int(11) NOT NULL,
  nombre varchar(50),
  llave varchar(50),  
  tipo varchar(50),
  columnas int(1),
  obligatorio enum('S','N') default 'N',
  long_min int(3),
  orden int(2),
  parametros text,
  created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  foreign key (id_form) references sis_frm_formulario(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE sis_frm_respuesta (
  id int(11) NOT NULL primary key auto_increment,
  id_form int(11) NOT NULL,
  nombre_cliente varchar(50),
  correo_cliente varchar(50),
  fecha timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  foreign key (id_form) references sis_frm_formulario (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE sis_frm_valor (
  id int(11) NOT NULL primary key auto_increment,
  id_respuesta int(11),
  id_form int(11) NOT NULL,
  id_campo int(11) NOT NULL,
  valor text,
  created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  foreign key (id_form) references sis_frm_formulario (id),
  foreign key (id_campo) references sis_frm_campo (id),
  foreign key (id_respuesta) references sis_frm_respuesta (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table sis_frm_formulario
    add column col_email int(1) default 4 after destinatarios,
    add column col_nombre int(1) default 4 after destinatarios;
