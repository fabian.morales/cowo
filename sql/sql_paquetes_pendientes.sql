create table sis_ped_paquete_pendiente(
    id int not null primary key auto_increment,
    id_cliente int not null,
    id_paquete int not null,
    cantidad int not null,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    deleted_at timestamp NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_cliente) references sis_par_cliente(id),
    foreign key (id_paquete) references sis_cat_paquete(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

INSERT INTO sis_con_contenido (titulo, llave, tipo, cuerpo, inicio, peso) VALUES ('Bienvenido/a a Cowo!', 'compra_plan_pendiente', 'E', '<p><img src=\"http://www.cowo.com.co/template/images/logo.png\" width=\"276\" height=\"131\" /></p>\r\n<p>Hola&nbsp; {{ $nombre_cliente }},</p>\r\n<p>Has adquirido {{ $cantidad }} planes&nbsp;{{ $nombre_plan }}. Recuerda que puedes asignarlos a los clientes que desees.</p>\r\n<p>Gracias por preferirnos.</p>\r\n<p>&nbsp;</p>', 'N', '0');

INSERT INTO sis_con_contenido (titulo, llave, tipo, cuerpo, inicio, peso) VALUES ('Plan comprado', 'compra_plan_pendiente_adm', 'E', '<p>El usuario&nbsp; {{ $nombre_cliente }}, ha adquirido {{$cantidad}} planes&nbsp;{{ $nombre_plan }} por valor de $&nbsp;{{ $valor_plan }}.</p>\r\n<p>&nbsp;</p>', 'N', '0');

INSERT INTO sis_con_contenido (titulo, llave, tipo, cuerpo, inicio, peso) VALUES ('Plan Asignado Beneficiario', 'asignacion_plan_beneficiario', 'E', '<p><img src=\"http://www.cowo.com.co/template/images/logo.png\" width=\"276\" height=\"131\" /></p>\r\n<p>Hola&nbsp; {{ $nombre_beneficiario}},</p>\r\n<p>El usuario {{ $nombre_cliente }} te ha asignado un plan&nbsp;{{ $nombre_plan }}. \r\n\r\nRecuerda que tiene validez hasta la siguiente fecha: {{ $vencimiento_plan }}.</p>\r\n<p>Gracias por preferirnos.</p>\r\n<p>&nbsp;</p>', 'N', '0');



