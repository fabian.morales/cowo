create table sis_con_publicidad(
    id int not null primary key auto_increment,
    nombre varchar(255),
    fecha_inicio date,
    fecha_fin date,
    cuerpo text,
    tipo enum ('P', 'B') default 'P',
    mostrar_logueado enum('S', 'N') default 'N',
    activo enum('S', 'N') default 'N',
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
)COLLATE='utf8_general_ci' ENGINE=InnoDB;
