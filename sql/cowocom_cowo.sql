-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 13-09-2017 a las 15:28:26
-- Versión del servidor: 10.0.32-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cowocom_cowo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_05_19_151759_create_forum_table_categories', 1),
('2014_05_19_152425_create_forum_table_threads', 1),
('2014_05_19_152611_create_forum_table_posts', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_04_14_180344_create_forum_table_threads_read', 1),
('2015_07_22_181406_update_forum_table_categories', 1),
('2015_07_22_181409_update_forum_table_threads', 1),
('2015_07_22_181417_update_forum_table_posts', 1),
('2016_05_24_114302_add_defaults_to_forum_table_threads_columns', 1),
('2016_07_09_111441_add_counts_to_categories_table', 1),
('2016_07_09_122706_add_counts_to_threads_table', 1),
('2016_07_10_134700_add_sequence_to_posts_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_cat_paquete`
--

CREATE TABLE `sis_cat_paquete` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `creditos` int(11) NOT NULL DEFAULT '0',
  `valor` decimal(10,2) NOT NULL DEFAULT '0.00',
  `id_servicio` int(11) DEFAULT NULL,
  `descripcion_corta` text COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci,
  `periodicidad` enum('H','D','Q','M','T','S','A','N') COLLATE utf8_unicode_ci NOT NULL,
  `vigencia` int(11) NOT NULL,
  `orden` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sis_cat_paquete`
--

INSERT INTO `sis_cat_paquete` (`id`, `nombre`, `creditos`, `valor`, `id_servicio`, `descripcion_corta`, `descripcion`, `periodicidad`, `vigencia`, `orden`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Paquete 01', 10, '50000.00', 1, '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dapibus sit amet magna sit amet ultricies. Cras egestas scelerisque eros. Fusce pulvinar ut est ac porta.', 'H', 0, 0, '2016-12-12 16:53:10', '2017-01-30 07:12:21', '2017-01-30 07:12:21'),
(2, 'Paquete 02', 30, '10000.00', 2, '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dapibus sit amet magna sit amet ultricies. Cras egestas scelerisque eros. Fusce pulvinar ut est ac porta.', 'H', 0, 0, '2016-12-12 16:53:21', '2017-01-30 07:12:29', '2017-01-30 07:12:29'),
(3, 'Paquete 03', 50, '200000.00', NULL, '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dapibus sit amet magna sit amet ultricies. Cras egestas scelerisque eros. Fusce pulvinar ut est ac porta.', 'H', 0, 0, '2016-12-12 16:53:34', '2017-01-30 07:12:33', '2017-01-30 07:12:33'),
(4, 'Full Time ', 10, '399000.00', NULL, 'Acceso ilimitado a las instalaciones con derecho a un puesto de trabajo rotativo', '<ul>\r\n<li>Ingreso&nbsp;ilimitado a las instalaciones, con derecho a un puesto de trabajo rotativo con silla ergon&oacute;mica.</li>\r\n<li><span>Internet de alta velocidad.</span></li>\r\n<li><span>Recepci&oacute;n de correo f&iacute;sico.</span></li>\r\n<li><span>10 cr&eacute;ditos adicionales.</span></li>\r\n<li>Cr&eacute;ditos redimibles en sala de juntas, invitados, alianzas, papeler&iacute;a.</li>\r\n<li>Valor cr&eacute;dito: $2.500.</li>\r\n<li>Cuatro&nbsp;horas de consultor&iacute;a para emprendedores.</li>\r\n<li>Servicio de cafeter&iacute;a (caf&eacute;, t&eacute; y fruta ilimitado).</li>\r\n<li>Biciparqueadero.</li>\r\n<li>Zona de esparcimiento (TV, Play Station, juegos).</li>\r\n</ul>', 'M', 1, 0, '2017-01-24 19:51:43', '2017-09-09 03:02:59', NULL),
(5, 'Semi Private', 20, '550000.00', NULL, 'Acceso ilimitado', '<ul>\r\n<li>Acceso ilimitado a CoWo.</li>\r\n<li>Derecho a un puesto fijo de trabajo con silla ergon&oacute;mica.</li>\r\n<li><span>Internet de alta velocidad.</span></li>\r\n<li><span>Recepci&oacute;n de correo f&iacute;sico.</span></li>\r\n<li><span>20 cr&eacute;ditos adicionales.</span></li>\r\n<li>Cr&eacute;ditos redimibles en sala de juntas, invitados, alianzas, papeler&iacute;a.</li>\r\n<li>Valor de cr&eacute;dito adicional: $2.500.</li>\r\n<li>Cajonera, l&aacute;mpara personal y guaya.</li>\r\n<li>Servicio de cafeter&iacute;a (caf&eacute;, t&eacute; y fruta ilimitado).</li>\r\n<li>Biciparqueadero.</li>\r\n<li>Zona de esparcimiento (TV, Play Station, juegos).</li>\r\n</ul>', 'M', 1, 0, '2017-01-27 03:15:22', '2017-09-09 03:02:44', NULL),
(6, 'Private', 60, '1800000.00', NULL, 'Acceso ilimitado', '<ul>\r\n<li>Acceso ilimitado para desde 3 personas.</li>\r\n<li>Derecho a una oficina privada con sillas ergon&oacute;micas para hasta 4 personas.</li>\r\n<li><span>Internet de alta velocidad.</span></li>\r\n<li><span>Recepci&oacute;n de correo f&iacute;sico.</span></li>\r\n<li><span>60 cr&eacute;ditos adicionales.</span></li>\r\n<li>Cr&eacute;ditos redimibles en sala de juntas, invitados, alianzas, papeler&iacute;a.</li>\r\n<li>Valor cr&eacute;dito: $2.500.</li>\r\n<li>Oficina con llave, cajoneras y l&aacute;mparas.</li>\r\n<li>Servicio de cafeter&iacute;a (caf&eacute;, t&eacute; y fruta ilimitado)</li>\r\n<li>Biciparqueadero</li>\r\n<li>Zona de esparcimiento (TV, Play Station, juegos)&nbsp;</li>\r\n</ul>', 'M', 1, 0, '2017-01-28 02:41:48', '2017-09-09 02:25:09', NULL),
(7, 'Part Time', 80, '255000.00', NULL, 'Acceso ilimitado', '<ul>\r\n<li>Derecho a un puesto de trabajo con silla ergon&oacute;mica.</li>\r\n<li><span>Internet de alta velocidad.</span></li>\r\n<li><span>Recepci&oacute;n de correo f&iacute;sico.</span></li>\r\n<li><span>80 cr&eacute;ditos/horas de trabajo.</span></li>\r\n<li>Cr&eacute;ditos redimibles en sala de juntas, invitados, alianzas, papeler&iacute;a.</li>\r\n<li>Valor cr&eacute;dito: $3.188.</li>\r\n<li>Dos horas de consultor&iacute;a para emprendedores.</li>\r\n<li>Servicio de cafeter&iacute;a (caf&eacute;, t&eacute; y fruta ilimitado)</li>\r\n<li>Biciparqueadero</li>\r\n<li>Zona de esparcimiento (TV, Play Station, juegos)&nbsp;</li>\r\n</ul>', 'M', 1, -1, '2017-01-30 19:21:29', '2017-07-25 03:24:33', NULL),
(8, 'Sala de Juntas', 8, '40000.00', 4, 'Acceso limitado', '<ul>\r\n<li><span>Acceso por una hora a sala&nbsp;</span></li>\r\n<li><span>Capacidad para hasta 10 personas</span></li>\r\n<li><span>Internet de alta velocidad, equipo de proyecci&oacute;n, tableros</span></li>\r\n<li><span>Caf&eacute;, t&eacute;, arom&aacute;tica, agua y fruta ilimitada.</span></li>\r\n</ul>', 'H', 1, -3, '2017-01-30 19:23:25', '2017-09-09 02:55:38', NULL),
(9, 'Comunidad CoWo', 5, '40000.00', NULL, 'Acceso limitado.', '<ul>\r\n<li><span>Acceso a la red de networking CoWo.</span></li>\r\n<li>Ingreso a todos los eventos de la comunidad.&nbsp;</li>\r\n<li>5 cr&eacute;ditos para redimir en servicios de la comunidad.</li>\r\n</ul>', 'M', 1, 0, '2017-01-30 19:27:51', '2017-05-11 20:35:57', NULL),
(10, 'StartUp', 20, '99000.00', NULL, 'StartUp', '<ul>\r\n<li>Derecho a un puesto de trabajo con silla ergon&oacute;mica.</li>\r\n<li><span>Internet de alta velocidad.</span></li>\r\n<li><span>Recepci&oacute;n de correo f&iacute;sico.</span></li>\r\n<li><span>20 cr&eacute;ditos/horas de trabajo.</span></li>\r\n<li>Cr&eacute;ditos redimibles en sala de juntas, invitados, alianzas, papeler&iacute;a.</li>\r\n<li>Valor cr&eacute;dito: $4.950.</li>\r\n<li>Una hora de consultor&iacute;a para emprendedores.</li>\r\n<li>Servicio de cafeter&iacute;a (caf&eacute;, t&eacute; y fruta ilimitado)</li>\r\n<li>Biciparqueadero</li>\r\n<li>Zona de esparcimiento (TV, Play Station, juegos)&nbsp;</li>\r\n</ul>', 'M', 1, -2, '2017-05-11 21:04:07', '2017-07-25 03:27:35', NULL),
(11, 'Corporate ', 200, '550000.00', NULL, '', '<ul>\r\n<li>Ingreso a CoWo</li>\r\n<li>Internet de alta velocidad.</li>\r\n<li>Recepci&oacute;n de correo f&iacute;sico.</li>\r\n<li>Planes corporativos de 2 a 10 personas.</li>\r\n<li>200 cr&eacute;ditos/horas de trabajo.</li>\r\n<li>Minimo 100 cr&eacute;ditos/horas de trabajo por persona</li>\r\n<li>Valor cr&eacute;dito: $2,750</li>\r\n<li>Cr&eacute;ditos redimibles en sala de juntas, invitados, alianzas, papeler&iacute;a.</li>\r\n<li>Dos horas de consultor&iacute;a para emprendedores.</li>\r\n<li>Servicio de cafeter&iacute;a (caf&eacute;, t&eacute; y fruta ilimitado)</li>\r\n<li>Biciparqueadero</li>\r\n<li>Zona de esparcimiento (TV, Play Station, juegos)</li>\r\n<li>Planes Corporativos para uso de 2 o m&aacute;s personas</li>\r\n</ul>', 'N', 1, 1, '2017-08-02 22:16:14', '2017-08-26 01:00:27', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_cat_servicio`
--

CREATE TABLE `sis_cat_servicio` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `creditos` int(11) DEFAULT '0',
  `periodicidad` enum('H','D','Q','M','T','S','A','N') COLLATE utf8_unicode_ci NOT NULL,
  `vigencia` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sis_cat_servicio`
--

INSERT INTO `sis_cat_servicio` (`id`, `nombre`, `creditos`, `periodicidad`, `vigencia`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Alquiler Sala', 20, 'H', 0, '2016-12-12 17:06:19', '2017-01-27 20:38:44', '2017-01-27 20:38:44'),
(2, 'Alquiler video beam', 15, 'H', 0, '2016-12-12 17:06:29', '2017-01-27 20:38:21', '2017-01-27 20:38:21'),
(3, 'Impresión 20 hojas', 1, 'N', 0, '2017-01-24 19:52:53', '2017-01-27 20:38:02', NULL),
(4, '1 hora de sala de juntas', 10, 'H', 1, '2017-01-27 20:38:35', '2017-01-27 20:38:35', NULL),
(5, '1 hora de trabajo o invitado', 1, 'H', 1, '2017-01-27 20:38:56', '2017-01-27 20:38:56', NULL),
(6, '1 día de Sala de Juntas', 60, 'D', 1, '2017-01-27 20:40:14', '2017-01-27 20:40:14', NULL),
(7, '2 Cafés Gourmet', 1, 'N', 0, '2017-01-28 02:39:51', '2017-02-01 02:26:56', NULL),
(8, 'Locker Mensual', 5, 'M', 0, '2017-02-08 21:17:27', '2017-02-08 21:17:27', NULL),
(9, 'Telefonía IP', 20, 'M', 0, '2017-02-08 21:22:56', '2017-02-08 21:22:56', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_con_aliado`
--

CREATE TABLE `sis_con_aliado` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `descripcion` text,
  `url` varchar(300) DEFAULT NULL,
  `activo` enum('S','N') DEFAULT 'S',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sis_con_aliado`
--

INSERT INTO `sis_con_aliado` (`id`, `titulo`, `descripcion`, `url`, `activo`, `created_at`, `updated_at`) VALUES
(1, 'Universidad de la Sabana', '', 'unisabana.edu.co', 'S', '2017-01-24 03:07:28', '2017-06-14 23:54:00'),
(8, 'La Pola Social', '', 'www.lapolasocial.com', 'S', '2017-02-10 22:19:09', '2017-06-14 23:54:13'),
(9, 'Centro de Emprendimiento Universidad Sergio Arboleda', '', 'http://www.usergioarboleda.edu.co/events/organizer/centro-de-innovacion-y-emprendimiento/', 'S', '2017-04-04 18:06:54', '2017-06-14 23:54:29'),
(11, 'Asociación de Exalumnos Gimnasio Moderno', '', 'http://exalumnos.gimnasiomoderno.edu.co/', 'S', '2017-04-05 00:10:12', '2017-06-14 23:55:12'),
(17, 'Cabify', '', 'https://cabify.com/', 'S', '2017-04-05 01:08:53', '2017-06-14 23:55:32'),
(23, 'Fitpal', '', 'https://www.fitpal.co/', 'S', '2017-05-18 21:40:28', '2017-06-14 23:55:54'),
(26, 'NuestroMall', '', '', 'S', '2017-05-18 22:10:55', '2017-05-18 22:10:55'),
(32, 'APTS Colombia', '', '', 'S', '2017-06-14 19:19:24', '2017-06-14 19:21:42'),
(33, 'BOHNENHOF CAFÉ', '', '', 'S', '2017-06-15 00:25:10', '2017-06-15 00:25:10'),
(36, 'Encarguelo.com', '', '', 'S', '2017-06-15 00:56:59', '2017-06-15 00:56:59'),
(37, 'Biko', '', '', 'S', '2017-06-15 01:02:49', '2017-06-15 01:02:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_con_configuracion`
--

CREATE TABLE `sis_con_configuracion` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `llave` varchar(100) NOT NULL,
  `tipo` enum('G','C') DEFAULT 'G',
  `contenido` text,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sis_con_configuracion`
--

INSERT INTO `sis_con_configuracion` (`id`, `titulo`, `llave`, `tipo`, `contenido`, `created_at`, `updated_at`) VALUES
(1, 'Teléfono', 'telefono', 'C', '310-322 9778', '0000-00-00 00:00:00', '2017-09-02 01:30:17'),
(2, 'Dirección', 'direccion', 'C', 'Calle 98 #8-37 Cerca del World Trade Center', '0000-00-00 00:00:00', '2017-01-30 08:36:13'),
(3, 'Correo', 'correo', 'C', 'info@cowo.com.co', '0000-00-00 00:00:00', '2017-01-30 08:36:13'),
(4, 'Ciudad', 'ciudad', 'C', 'Bogotá, Colombia', '0000-00-00 00:00:00', '2017-01-30 08:36:13'),
(5, 'Latitud', 'latitud', 'C', '-74.04270108553526', '0000-00-00 00:00:00', '2017-01-30 08:36:13'),
(6, 'Longitud', 'longitud', 'C', '4.679398743145549', '0000-00-00 00:00:00', '2017-01-30 08:36:13'),
(7, 'Meta descripción', 'meta_descripcion', 'G', '¿Está buscando las mejores Oficinas en Bogota para trabajar y Salas de Reuniones? . Le ayudaremos a encontrar el lugar perfecto con nuestra amplia gama de propiedades en Bogotá.', '0000-00-00 00:00:00', '2017-07-10 12:23:51'),
(8, 'Palabras clave', 'meta_keywords', 'G', 'Coworking, Oficinas en Bogota, Oficinas por horas, Alquiler salas de juntas, Salas de reuniones, Oficinas compartidas, Coworking Bogota.', '0000-00-00 00:00:00', '2017-05-17 19:42:56'),
(9, 'Descripción home', 'descripcion_home', 'G', '¿Por qué Cowo?  Somos la comunidad de Coworking más colaborativa de Colombia.  Ven y trabaja en un ambiente familiar, consigue nuevos negocios y vive experiencias únicas.', '0000-00-00 00:00:00', '2017-05-23 07:55:19'),
(10, 'Titulo sitio', 'titulo_sitio', 'G', 'Coworking - Oficinas en Bogota, Alquiler salas de juntas, Oficinas compartidas ,Oficinas por horas,Salas de reuniones.', '0000-00-00 00:00:00', '2017-07-10 15:02:55'),
(11, 'Título home', 'titulo_seccion', 'G', '', '0000-00-00 00:00:00', '2017-07-10 12:24:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_con_contenido`
--

CREATE TABLE `sis_con_contenido` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `llave` varchar(100) DEFAULT NULL,
  `tipo` enum('E','S') DEFAULT NULL,
  `cuerpo` text,
  `inicio` enum('S','N') DEFAULT 'N',
  `peso` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sis_con_contenido`
--

INSERT INTO `sis_con_contenido` (`id`, `titulo`, `llave`, `tipo`, `cuerpo`, `inicio`, `peso`, `created_at`, `updated_at`) VALUES
(1, 'Cotización Cowo', 'cotizacion', 'E', '<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"color: #ff0000; font-size: 14pt;\"><strong>Hola</strong>&nbsp;{{ $nombre_cliente }}:</span></p>\r\n<p><br /><br /><span style=\"font-size: 14pt;\">En este correo te adjuntamos la cotizaci&oacute;n &nbsp;del plan {{ $nombre_plan }} el cual tiene un costo de <strong>$ {{ $valor_plan }} y lo puedes&nbsp;adquirir en el siguiente link:&nbsp;</strong></span></p>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p style=\"text-align: center;\"><strong><span style=\"font-size: 14pt;\">{!! $link_plan !!}</span></strong></p>\r\n<p><span style=\"font-size: 14pt;\">Todos nuestros planes incluyen Internet de alta velocidad, servicio de caf&eacute;, agua, te y fruta complementaria, zonas de entretenimiento, terrazas y eventos de networking. &nbsp;Adem&aacute;s contar&aacute;s con un sistema de cr&eacute;ditos con el cual puedes hacer uso de salas de juntas, ingresar invitados y utilizar los dem&aacute;s servicios de Cowo.&nbsp;</span></p>\r\n<p><span style=\"font-size: 14pt;\">&nbsp;</span></p>\r\n<p style=\"text-align: center;\"><strong><span style=\"font-size: 14pt;\">{!! $descripcion_plan !!}</span></strong></p>\r\n<p><span style=\"font-size: 14pt;\">Nos encantar&iacute;a que hicieras parte de nuestra comunidad.</span><br /><br /><span style=\"font-size: 14pt;\">Cualquier inquietud o duda puedes contactarnos en los siguientes telefonos:&nbsp;3044990158</span><br /><br /><span style=\"font-size: 14pt;\">Si deseas responder a este correo, por favor hazlo a info@cowo.com.co.</span><br /><br /><span style=\"font-size: 14pt;\">Atentamente,&nbsp;</span><br /><br /></p>\r\n<div><span style=\"font-size: 14pt;\">CoWo</span></div>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', 'N', 0, '0000-00-00 00:00:00', '2017-06-13 05:31:32'),
(2, 'Bienvenido a nuestra comunidad', 'registro', 'E', '<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Hola {{ $nombre_cliente }},&nbsp;</p>\r\n<p>Bienvenido a CoWo!</p>\r\n<p>Gracias por unirte a nuestra comunidad, si hay algo que podemos hacer para que tu experiencia sea m&aacute;s agradable, por favor no dudes en ponerte en contacto con nosotros.</p>\r\n<p>&nbsp;</p>\r\n<p>Cordialmente,</p>\r\n<p>&nbsp;</p>\r\n<p><img src=\"http://www.cowo.com.co/template/images/logo.png\" width=\"276\" height=\"131\" /></p>', 'N', 0, '0000-00-00 00:00:00', '2017-02-28 18:44:57'),
(3, 'Bienvenido/a a Cowo!', 'compra_plan', 'E', '<p><img src=\"http://www.cowo.com.co/template/images/logo.png\" width=\"276\" height=\"131\" /></p>\r\n<p>Hola&nbsp; {{ $nombre_cliente }},</p>\r\n<p>Haz adquirido un plan&nbsp;{{ $nombre_plan }}. Recuerda que tiene validez hasta la siguiente fecha: {{ $vencimiento_plan }}.</p>\r\n<p>Gracias por preferirnos.</p>\r\n<p>&nbsp;</p>', 'N', 0, '0000-00-00 00:00:00', '2017-03-09 20:08:55'),
(4, 'Créditos CoWo', 'compra_creditos', 'E', '<p>&nbsp;</p>\r\n<p>Hola&nbsp; {{ $nombre_cliente }},</p>\r\n<p>Haz adquiridos {{ $creditos}} cr&eacute;ditos por un valor de $&nbsp;{{ $valor_compra }}.&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Gracias por preferirnos.</p>\r\n<p>&nbsp;</p>\r\n<p>Atentamente,</p>\r\n<p>&nbsp;<img src=\"http://www.cowo.com.co/template/images/logo.png\" width=\"276\" height=\"131\" /></p>\r\n<p>&nbsp;</p>', 'N', 0, '0000-00-00 00:00:00', '2017-02-28 19:03:26'),
(5, 'Servicio CoWo', 'contratacion_servicio', 'E', '<p><img src=\"http://www.cowo.com.co/template/images/logo.png\" width=\"276\" height=\"131\" /></p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"color: #ff0000;\">Estimado/a {{ $nombre_cliente }}:</span></p>\r\n<p>Has adquirido el servicio de&nbsp;{{ $nombre_servicio }} por {{ $creditos }} cr&eacute;dito(s).</p>\r\n<p>{{ $id_sala }}</p>\r\n<p>{{ $fecha_inicio_servicio }}</p>\r\n<p>&nbsp;</p>\r\n<p>Gracias por escoger a <a href=\"http://cowo.com.co\">Cowo.com.co</a>.</p>', 'N', 0, '0000-00-00 00:00:00', '2017-02-28 19:11:04'),
(6, 'Tu compra está pendiente', 'compra_pendiente', 'E', '<p>&nbsp;</p>\r\n<p>Hola&nbsp; {{ $nombre_cliente }},</p>\r\n<p>Tu compra&nbsp;por valor de $&nbsp;{{ $valor_compra }} se encuentra pendiente de procesamiento por parte de las entidades bancarias implicadas.</p>\r\n<p>&nbsp;</p>\r\n<p>Gracias por preferirnos.</p>\r\n<p>&nbsp;</p>\r\n<p>Atentamente,</p>\r\n<p>&nbsp;<img src=\"http://www.cowo.com.co/template/images/logo.png\" width=\"276\" height=\"131\" /></p>', 'N', 0, '0000-00-00 00:00:00', '2017-02-28 19:04:41'),
(7, 'Solicitud de contacto', 'contacto', 'E', '<p>Lorem ipsum</p>', 'N', 0, '0000-00-00 00:00:00', '2017-02-13 08:41:04'),
(8, 'Solicitud de trabajar con nosotros', 'trabaja_con_nosotros', 'E', '<p>Lorem ipsum</p>', 'N', 0, '0000-00-00 00:00:00', '2017-02-13 08:40:57'),
(9, 'Plan comprado', 'compra_plan_adm', 'E', '<p>El usuario&nbsp; {{ $nombre_cliente }}, ha adquirido un plan&nbsp;{{ $nombre_plan }} por valor de $&nbsp;{{ $valor_plan }}. Este plan tiene una validez hasta&nbsp;{{ $vencimiento_plan }}.</p>\r\n<p>&nbsp;</p>', 'N', 0, '0000-00-00 00:00:00', '2017-02-28 18:52:18'),
(10, 'Servicio contratado', 'contratacion_servicio_adm', 'E', '<p><img src=\"http://www.cowo.com.co/template/images/logo.png\" width=\"276\" height=\"131\" /></p>\r\n<p>&nbsp;</p>\r\n<p>El usuario &nbsp; {{ $nombre_cliente }} ha adquirido el servicio de&nbsp;{{ $nombre_servicio }} por {{ $creditos }} cr&eacute;dito(s).</p>\r\n<p>{{ $id_sala }}</p>\r\n<p>{{ $fecha_inicio_servicio }}</p>\r\n<p>&nbsp;</p>\r\n<p>Gracias por escoger a <a href=\"http://cowo.com.co\">Cowo.com.co</a>.</p>', 'N', 0, '0000-00-00 00:00:00', '2017-02-24 04:03:24'),
(11, 'Tu plan está próximo a vencerse', 'recordatorio_plan', 'E', '<p><img src=\"http://www.cowo.com.co/template/images/logo.png\" width=\"276\" height=\"131\" /></p>\n<p>Hola&nbsp; {{ $nombre_cliente }}, tu plan {{ $nombre_plan }} está próximo a vencerse, el cual tiene una validez hasta&nbsp;{{ $vencimiento_plan }}.</p>\n<p>&nbsp;</p>\n<p>Gracias por preferirnos.</p>\n<p>&nbsp;</p>', 'N', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Espacios y Planes', 'espacios-y-planes', 'S', '<div class=\"container\">\r\n<div class=\"row\">\r\n<h2 class=\"titulo seccion centro\" style=\"text-align: center;\"><span>Espacios de Trabajo y Planes</span></h2>\r\n<p style=\"text-align: center;\"><span>Todos nuestros planes incluyen beneficios y accesos a nuestra comunidad virtual, en donde podr&aacute;s tener un usuario para enterarte de las necesidades de la comunidad, expresar las tuyas, y estar al tanto de todos los eventos.</span></p>\r\n<p style=\"text-align: center;\"><span><strong>Todos nuestros planes f&iacute;sicos incluyen caf&eacute;, t&eacute; y agua ilimitada.</strong>&nbsp;</span></p>\r\n<p style=\"text-align: center;\"><em><strong>*Precios no incluyen iva.</strong>&nbsp;</em></p>\r\n</div>\r\n</div>\r\n<p style=\"text-align: center;\"><strong>&nbsp;{!! $bloque_planes !!}</strong></p>\r\n<h2 style=\"text-align: center;\">Descubre nuestros espacios:</h2>\r\n<p>&nbsp;</p>\r\n<p style=\"text-align: center;\"><span>{!! $grid_fotos !!}</span></p>\r\n<div class=\"container\">&nbsp;</div>\r\n<div class=\"container\">\r\n<h2 class=\"row\" style=\"text-align: center;\">Comun&iacute;cate con nosotros:&nbsp;</h2>\r\n</div>\r\n<p style=\"text-align: center;\">{!! $form_contacto !!}</p>', 'N', 0, '2016-08-12 07:20:16', '2017-09-09 03:00:25'),
(14, 'Quiénes somos', 'quienes-somos', 'S', '<div id=\"Content\">\n<div class=\"boxed\">\n<div id=\"lipsum\">\n<p>&nbsp;</p>\n<div class=\"container\">\n<h2 class=\"row\">Qui&eacute;nes somos</h2>\n<div class=\"row\">&nbsp;</div>\n<div class=\"row\">\n<p>Cowo fue fundado con el principal objetivo de crear una comunidad m&aacute;s all&aacute; de un espacio de oficina para compartir. Creemos que existe una manera novedosa de fomentar el crecimiento de las empresas a trav&eacute;s del trabajo en comunidad promoviendo espacios de networking tanto f&iacute;sicos como virtuales para el desarrollo de diferentes proyectos laborales.&nbsp; Todo esto &nbsp;enfoc&aacute;ndonos en brindar el mayor apoyo a cada miembro de la comunidad y ofreciendo el mejor servicio buscando la manera de adecuarnos a las necesidades de nuestros clientes.&nbsp;</p>\n<p>Somos una comunidad colaborativa para independientes y peque&ntilde;os empresarios enfocada en la ayuda y acompa&ntilde;amiento integral para el crecimiento y progreso de cada miembro, bajo la filosof&iacute;a del Coworking.</p>\n<p>Todo lo anterior lo hacemos a trav&eacute;s de los siguientes pilares:</p>\n<p><strong>Espacios totalmente equipados para el trabajo eficiente y c&oacute;modo:</strong> &nbsp;Contamos con un amplio espacio con capacidad de m&aacute;s de cien puestos de trabajo, compuestos por salas de juntas, oficinas privadas, puestos fijos y espacios de coworking.</p>\n<p>&nbsp;</p>\n<p><strong>Eventos y conferencia de diferentes temas de inter&eacute;s para emprendedores y PYMES: &nbsp;</strong>Constantemente realizamos eventos, con el fin de crear una experiencia enriquecedora con temas de inter&eacute;s para los miembros de nuestra&nbsp; comunidad, y generamos espacios de networking que resulten productivos para todos.</p>\n<p>&nbsp;</p>\n<p><strong>Comunidad Virtual: &nbsp;</strong>Generamos espacios virtuales, donde buscamos la forma de difundir la informaci&oacute;n importante de nuestros miembros a trav&eacute;s de nuestras bases de datos, redes sociales y p&aacute;gina web.</p>\n<div class=\"col-sm-12\">&nbsp;</div>\n</div>\n</div>\n<div>&nbsp;{!! $grid_fotos !!}&nbsp;</div>\n</div>\n</div>\n</div>', 'N', 0, '2016-08-12 07:28:41', '2017-01-31 20:18:42'),
(15, 'Contacto', 'contacto', 'S', '<div class=\"container\">\r\n<div class=\"row\">\r\n<h2 style=\"text-align: center;\">Cont&aacute;ctanos</h2>\r\n<p style=\"text-align: center;\">Te invitamos a escribirnos en caso de que tengas comentarios, dudas o alg&uacute;n reclamo. Estaremos dispuestos a ayudarte en lo que necesites, con esto lograremos ofrecerte un mejor servicio.</p>\r\n<br />\r\n<div class=\"col-sm-12\">&nbsp;</div>\r\n</div>\r\n</div>\r\n<div>{!! $form_contacto !!}</div>\r\n<p>&nbsp;</p>\r\n<p>{!! $bloque_links !!}</p>', 'N', 0, '2016-08-17 02:25:51', '2017-04-23 01:29:42'),
(16, 'Alianzas', 'alianzas', 'S', '<h2 style=\"text-align: center;\">Nuestros Aliados</h2>\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12\">Contamos con alianzas&nbsp;con las mejores empresas de la ciudad, para brindarle a nuestros coworkers y todos los miembros de nuestra comunidad los mejores descuentos, beneficios y acceso a eventos exclusivos.</div>\r\n</div>\r\n</div>\r\n<div>&nbsp;</div>\r\n<div class=\"container no-gutter\">\r\n<div class=\"row\">{!! $grid_aliados !!}<br />\r\n<div class=\"col-sm-12\">&nbsp;</div>\r\n</div>\r\n</div>\r\n<h3 style=\"text-align: center;\">&nbsp;</h3>\r\n<h3 style=\"text-align: center;\">&iexcl;Adquiere ya uno de nuestros planes y empieza a disfrutar de estos grandes beneficios!</h3>\r\n<p>&nbsp;</p>\r\n<p>{!! $form_contacto !!}</p>\r\n<p>&nbsp;{!! $bloque_links !!}</p>', 'N', 0, '2016-08-17 23:27:33', '2017-04-23 01:28:59'),
(17, 'Preguntas frecuentes', '', 'S', '<p>{!! $acordeon_faqs !!}</p>', 'N', 0, '2016-08-18 00:02:32', '2016-08-18 00:02:32');
INSERT INTO `sis_con_contenido` (`id`, `titulo`, `llave`, `tipo`, `cuerpo`, `inicio`, `peso`, `created_at`, `updated_at`) VALUES
(23, 'Términos y Condiciones', 'terminos-y-condiciones', 'S', '<div class=\"container\">\r\n<div class=\"row\">\r\n<h2><strong>T&Eacute;RMINOS Y CONDICIONES</strong></h2>\r\n<p>Los siguientes <strong>T&Eacute;RMINOS Y CONDICIONES</strong> y <strong>POL&Iacute;TICAS DE PRIVACIDAD</strong> de Inversiones MMB SAS. aplican para el acceso y uso de la informaci&oacute;n y de los servicios ofrecidos a trav&eacute;s del portal: www.encarguelo.com y/u otros dominios relacionados, de la empresa Encarguelo.com S.A.S. NIT: 900942174-1<span>&nbsp;</span> (en adelante \"Encarguelo.com\" o \"la empresa\").</p>\r\n<div class=\"page\" title=\"Page 1\">\r\n<div class=\"section\">\r\n<div class=\"layoutArea\">\r\n<div class=\"column\">\r\n<p><span>CONTRATO DE MEMBRESÍA </span></p>\r\n<p><span>Por medio del presente Contrato, Las Partes, entendidas como </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>y </span><span> </span><span>EL MIEMBRO</span><span> </span><span>, identificadas como aparece al pie de su respectiva firma, acuerdan los términos y condiciones que regularán la Membresía y los servicios ofrecidos por </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>, que por medio del presente Contrato, </span><span> </span><span>EL MIEMBRO </span><span> </span><span>adquiere y contrata, previas las siguientes consideraciones que harán parte integral del presente Contrato: </span></p>\r\n<p><span>CONSIDERACIONES </span></p>\r\n<ol>\r\n<li>\r\n<p><span>Qué </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>ha desarrollado un espacio compartido y comunitario para personas naturales que ejerzan su actividad económica de manera independiente, buscando ofrecer espacios y servicios que le permitan al </span><span> </span><span>MIEMBRO </span><span> </span><span>ejercer dicha actividad económica de manera eficiente y rentable; </span></p>\r\n</li>\r\n<li>\r\n<p><span>Que </span><span> </span><span>EL MIEMBRO </span><span> </span><span>es una persona natural o persona jurídica, que desarrolla su actividad económica de manera independiente, y en consecuencia requiere de un espacio físico y de una serie de servicios que le permitan desarrollar dicha actividad de manera eficiente y rentable; </span></p>\r\n</li>\r\n<li>\r\n<p><span>Que en razón de lo anterior, Las Partes han acordado celebrar el presente Contrato de Membresía (en adelante &ldquo;El Contrato&rdquo;), a efecto de pactar el alcance de la Membresía y de los servicios que </span><span> </span><span>EL MIEMBRO </span><span> </span><span>adquiere y contrata respectivamente, por medio del presente Contrato, el cual se regirá por las siguientes cláusulas: </span></p>\r\n<p><span>CLÁUSULAS </span></p>\r\n</li>\r\n</ol>\r\n<p><span>1. Características de los Servicios y las Membresías: </span></p>\r\n<p><span>Las Membresías ofrecidas por </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>, se han estructurado con base en un sistema de créditos, que permite al </span><span> </span><span>MIEMBRO </span><span> </span><span>decidir libremente, y según sus necesidades específicas, el alcance que tendrá su Membresía sobre el espacio compartido ofrecido por </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>, así como decidir respecto de los servicios que utilizará durante la vigencia de la Membresía escogida por </span><span> </span><span>EL MIEMBRO. </span></p>\r\n<p><span>1.1.Sistema de créditos</span><span> </span><span>:<br /> Según el tipo de Membresía escogida por </span><span> </span><span>EL MIEMBRO, </span><span> </span><span>el mismo tendrá derecho a utilizar un número de créditos específicos que podrán ser redimidos para contratar los siguientes servicios ofrecidos por </span><span> </span><span>EL PROVEEDOR: </span></p>\r\n<ul>\r\n<li>\r\n<p><span>● &nbsp;</span><span>Ingreso de invitados</span><span> </span><span>: 8 créditos = un día en el horario establecido de un invitado. El invitado debe atenderse en zonas comunes y no utilizar un puesto de trabajo asignado al </span><span> </span><span>MIEMBRO</span><span> </span><span>. El ingreso de invitados está sujeto a la disponibilidad y estos tendrán prioridad por separación de cupos previamente; </span></p>\r\n</li>\r\n<li>\r\n<p><span>● &nbsp;</span><span>Uso por horas de sala de juntas</span><span> </span><span>:10 créditos = una hora de sala de juntas; Las salas de juntas deberían ser agendadas en el aplicativo o sistema establecido. </span></p>\r\n</li>\r\n<li>\r\n<p><span>● &nbsp;</span><span>Papelería (impresión o fotocopias)</span><span> </span><span>: 1 crédito= 18 hojas de impresión o fotocopia en blanco y negro </span><span>Parágrafo Primero: </span><span> </span><span>El tarifario actualizado de créditos, con los productos y servicios por los que se pueden </span></p>\r\n<p><span>canjear se tendrá publicado en la página oficial de la empresa. </span></p>\r\n<p><span>Los paquetes de créditos adicionales podrán ser adquiridos por </span><span> </span><span>EL MIEMBRO </span><span> </span><span>en cualquier momento con la tarifa aplicable www.cowo.com.co o en la recepción. El valor del crédito adicional será el establecido en el Anexo 2 al presente Contrato y se tendrán tarifas segmentadas por clientes con planes activos y clientes no activos. </span></p>\r\n</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"page\" title=\"Page 2\">\r\n<div class=\"section\">\r\n<div class=\"layoutArea\">\r\n<div class=\"column\">\r\n<p><span>Parágrafo Segundo: </span><span> </span><span>Los créditos adquiridos por </span><span> </span><span>EL MIEMBRO </span><span> </span><span>mediante la adquisición de la respectiva Membresía, o durante la vigencia de la misma, deben ser redimidos por </span><span> </span><span>EL MIEMBRO </span><span> </span><span>durante la vigencia establecida en la página web www.cowo.com.co www.cowo.com.co. Los créditos pierden validez mensualmente, vencido el periodo de vigencia inicialmente acordado entre Las Partes. Los créditos son personales e intransferibles. </span></p>\r\n<p><span>1.2 Tipos de Membresías</span><span> </span><span>:<br /> </span><span>EL MIEMBRO</span><span> </span><span>, a la firma del presente Contrato, deberá escoger uno (1) de los cuatro (4) tipos de Membresías que se describen a continuación, estableciendo el tipo de Membresía escogido en el Anexo 1 del presente Contrato. Los precios actualizados de los diferentes tipos de Membresías se encuentran publicados en la página web www.cowo.com.co www.cowo.com.co.: </span></p>\r\n<p><span>Full Time Community: Incluye </span></p>\r\n<ol>\r\n<li>\r\n<p><span>Puesto no fijo de trabajo sujeto a la ocupación máxima establecida; </span></p>\r\n</li>\r\n<li>\r\n<p><span>25 créditos; </span></p>\r\n</li>\r\n<li>\r\n<p><span>Tinto, frutas, té y agua; </span></p>\r\n</li>\r\n<li>\r\n<p><span>Servicio de Internet de alta velocidad inalámbrica; </span></p>\r\n</li>\r\n<li>\r\n<p><span>Recepción de correo físico. Recibo de paquetería con dimensiones superiores a treinta centímetros cuadrados (30 cm</span><span>2</span><span> </span><span> </span><span>) será cobrado según tabla establecida en la recepción de la sede por tamaño y días siguiendo el Anexo dos del presente contrato </span></p>\r\n</li>\r\n<li>\r\n<p><span>Descuentos aplicables con aliados; </span></p>\r\n</li>\r\n<li>\r\n<p><span>Ingreso a eventos de la comunidad CoWo. </span></p>\r\n</li>\r\n<li>\r\n<p><span>Puesto no fijo de trabajo sujeto a la ocupación máxima establecida; </span></p>\r\n</li>\r\n<li>\r\n<p><span>Membresía a la comunidad COWO online que permite participar en foros y ayudar o pedir ayuda </span></p>\r\n<p><span>a través del portal creado para tales efectos. </span></p>\r\n</li>\r\n<li>\r\n<p><span>Ingreso a la sede de Lunes a Domingo sin incluir festivos según el horario establecido en la </span></p>\r\n<p><span>página web www.cowo.com.co www.cowo.com.co. </span></p>\r\n</li>\r\n</ol>\r\n<p><span>Part Time:. Incluye: </span></p>\r\n<ol>\r\n<li>\r\n<p><span>Ingreso máximo 80 horas al mes. </span></p>\r\n</li>\r\n<li>\r\n<p><span>80 Cr&eacute;ditos</span></p>\r\n</li>\r\n<li>\r\n<p><span>Tinto, frutas, té y agua; </span></p>\r\n</li>\r\n<li>\r\n<p><span>Servicio de Internet de alta velocidad inalámbrica; </span></p>\r\n</li>\r\n<li>\r\n<p><span>Recepción de correo físico. Recibo de paquetería con dimensiones superiores a treinta </span></p>\r\n<p><span>centímetros cuadrados (30 cm</span><span>2</span><span> </span><span> </span><span>) será cobrado según tabla establecida en la recepción de la sede </span></p>\r\n<p><span>por tamaño y días siguiendo el Anexo dos del presente contrato </span></p>\r\n</li>\r\n<li>\r\n<p><span>Descuentos aplicables con aliados; </span></p>\r\n</li>\r\n<li>\r\n<p><span>Ingreso a eventos de la comunidad CoWo; </span></p>\r\n</li>\r\n<li>\r\n<p><span>Puesto no fijo de trabajo sujeto a la ocupación máxima establecida </span></p>\r\n</li>\r\n</ol>\r\n<p><span>Semi Private : Incluye: </span></p>\r\n<ol>\r\n<li>\r\n<p><span>Lo incluido en el plan ̈Full Time Community ̈ </span></p>\r\n</li>\r\n<li>\r\n<p><span>Puesto fijo permanente con silla, mesa y Cajonera; </span></p>\r\n</li>\r\n</ol>\r\n<p><span>Private:. Incluye: </span></p>\r\n<ol>\r\n<li>\r\n<p><span>Lo incluido en el plan ̈Full Time Community ̈ </span></p>\r\n</li>\r\n<li>\r\n<p><span>130 créditos; </span></p>\r\n</li>\r\n<li>\r\n<p><span>Oficina cerrada con posibilidad de ingreso hasta de cuatro (4) miembros de manera simultánea</span></p>\r\n<span></span><span></span></li>\r\n</ol>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"page\" title=\"Page 3\">\r\n<div class=\"section\">\r\n<div class=\"layoutArea\">\r\n<div class=\"column\">\r\n<p><span>debidamente registrados. </span></p>\r\n</div>\r\n</div>\r\n<div class=\"layoutArea\">\r\n<div class=\"column\">\r\n<p><span>Parágrafo Primero: </span><span> </span><span>La Membresía escogida por </span><span> </span><span>EL MIEMBRO </span><span> </span><span>tendrá una vigencia de un (1) mes contado a partir de la fecha de pago anticipado del precio correspondiente al tipo de Membresía escogido, salvo que Las Partes acuerden una vigencia mayor a un (1) mes, lo que deberá quedar establecido expresamente en el Anexo 1 al presente Contrato. </span></p>\r\n<p><span>Parágrafo Segundo: </span><span> </span><span>Los periodos de vigencia inicial de la Membresía escogida, y acordada por Las Partes en el Anexo 1 al presente Contrato, se renovarán automáticamente salvo que el </span><span> </span><span>EL MIEMBRO </span><span> </span><span>informe al </span><span>PROVEEDOR </span><span> </span><span>por escrito respecto de intención de no renovar la Membresía adquirida, con cinco (5) días hábiles de antelación a la fecha de vencimiento efectivo de la vigencia inicialmente pactada. Cancelada la Membresía por parte del </span><span> </span><span>MIEMBRO</span><span> </span><span>, y/o terminado el presente Contrato por cualquier causa, </span><span> </span><span>INVERSIONES MMB S.A.S </span><span>no tendrá la obligación de conceder una nueva Membresía al </span><span> </span><span>MIEMBRO</span><span> </span><span>, que corresponda al mismo tipo de Membresía inicialmente adquirida, pues la adjudicación de Membresías se encuentra sujeta a la disponibilidad de espacio y puestos de trabajo, lo que será determinado a criterio exclusivo de </span><span> </span><span>INVERSIONES MMB S.A.S </span></p>\r\n<p><span>Parágrafo Tercero: </span><span>En ningún caso procederá la devolución de dineros al </span><span> </span><span>MIEMBRO</span><span> </span><span>, por valores cancelados, relativos a la adquisición de Membresías y/o créditos no redimidos dentro de la vigencia inicial de la correspondiente Membresía. </span><span> </span><span>EL MIEMBRO </span><span> </span><span>podrá solicitar al </span><span> </span><span>PROVEEDOR </span><span> </span><span>un &ldquo;</span><span> </span><span>upgrade&rdquo; </span><span> </span><span>del tipo de Membresía inicialmente adquirido, a un tipo de Membresía con mejores derechos, lo cual deberá ser aceptado expresamente por </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>, según la disponibilidad de espacio, definido a criterio exclusivo de Inversiones MMB S.A.S , y se deberá cancelar el mayor valor correspondiente por parte del </span><span>MIEMBRO</span><span> </span><span>, según el valor liquidado para tales efectos por </span><span> </span><span>INVERSIONES MMB S.A.S . </span></p>\r\n<p><span>1.3 Horario</span><span> </span><span>:<br /> Las instalaciones y los servicios objeto del presente Contrato se encuentran disponibles de Lunes a Viernes de 7 a.m a 7 p.m, Sábados de 8a.m a 3p.m en horario continuo, y domingos y festivos según disponibilidad de los eventos.Los horarios podrán ser modificados por </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>y serán publicados en la página webwww.cowo.com.cowww.cowo.com.co .</span><span> </span><span>ELMIEMBRO</span><span> </span><span>podráhacerusodedichosespaciosyservicios, según el tipo de Membresía adquirida con la frecuencia estipulada en el Anexo 1 al presente Contrato, y previo agendamiento del correspondiente espacio a utilizar, efectuado según el sistema de agendamiento dispuesto por </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>, dentro del Manual de Convivencia que hace parte integral del presente Contrato como Anexo 2. </span></p>\r\n<p><span>1.4 Forma de Pago</span><span> </span><span>:<br /> Los créditos y las Membresías deberán ser cancelados de manera anticipada por </span><span> </span><span>EL MIEMBRO</span><span> </span><span>, en efectivo o con tarjeta débito o crédito en la recepción del espacio compartido, o por medio de pago virtual en la siguiente página web www.cowo.com.co www.cowo.com.co:www.cowo.com.co. Los pagos correspondientes a la adquisición de nuevas Membresías deberán efectuarse una vez se haya suscrito el presente Contrato entre Las Partes, a efecto de que </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>determine específicamente si cuenta con la disponibilidad requerida para adjudicar la Membresía correspondiente.</span></p>\r\n<p><span>1.5 Para cancelaci&oacute;n de alquiler de sal&oacute;n de eventos antes de 24 horas, el dinero NO ser&aacute; reembolsado.</span></p>\r\n<p><span>Parágrafo: </span><span> </span><span>Con la suscripción del presente Contrato, </span><span> </span><span>EL MIEMBRO </span><span> </span><span>declara bajo gravedad de juramento que los fondos utilizados para cancelar cualquier valor que derive de la suscripción del presente Contrato, provienen de una actividad económica lícita, y no está utilizando fondos provenientes del lavado de activos y/o de actividades enfocadas en el apoyo al terrorismo y/o actividades conexas. </span></p>\r\n<p><span>INVERSIONES MMB S.A.S </span><span> </span><span>podrá terminar de manera unilateral e inmediata este Contrato en caso de que </span><span>EL MIEMBRO </span><span>llegare a ser: incluido en listas para el control de lavado de activos y financiación del terrorismo administradas por cualquier autoridad nacional o extranjera, tales como la lista de la Oficina de Control de Activos en el Exterior &ndash; OFAC emitida por la Oficina del Tesoro de los Estados Unidos de Norteamérica, la lista de la Organización de las Naciones Unidas, así como cualquier otra lista pública </span></p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"page\" title=\"Page 4\">\r\n<div class=\"section\">\r\n<div class=\"layoutArea\">\r\n<div class=\"column\">\r\n<p><span>relacionada con el tema del lavado de activos y financiación del terrorismo, (ii) condenado por parte de las autoridades competentes en cualquier tipo de proceso judicial relacionado con la comisión de los anteriores delitos en cualquier jurisdicción, o (iii) Cuando </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>halle falsedades en la documentación e información aportada por </span><span> </span><span>EL MIEMBRO </span><span>para la celebración y/o ejecución del presente Contrato. En ese sentido, </span><span> </span><span>EL MIEMBRO </span><span>autoriza irrevocablemente a </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>para que consulte tal información en dichas listas y/o listas similares, y para que realice las confirmaciones y verificaciones que resulten necesarias, pudiendo conservar dicha información de manera indefinida. En caso de terminación unilateral del Contrato por parte de </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>, en los eventos antes descritos, </span><span>EL MIEMBRO </span><span>no tendrá derecho a reconocimiento económico alguno por concepto de indemnizaciones, penalidades, devoluciones, multas, etc. Por su parte </span><span> </span><span>INVERSIONES MMB S.A.S </span><span>podrá cobrar los perjuicios que demuestre y podrá iniciar las acciones legales que correspondan. </span></p>\r\n<p><span>2. Manual de Convivencia:<br /> </span><span>Con la suscripción del presente Contrato, </span><span> </span><span>EL MIEMBRO </span><span> </span><span>declara entender aceptar que la Membresía y los servicios, adquirida y contratados respectivamente mediante el presente Contrato, se encuentran sujetos al cumplimiento en todo momento por parte del </span><span> </span><span>MIEMBRO</span><span> </span><span>, </span><span> </span><span>de los lineamientos previstos en el Manual de Convivencia que hace parte integral del presente Contrato como Anexo 2. </span></p>\r\n<p><span>INVERSIONESMMBS.A.S </span><span> </span><span>sereservaelderechodemodificarelAnexo2alpresenteContratoencualquier tiempo, lo cual será informado al </span><span> </span><span>MIEMBRO </span><span> </span><span>por medio de publicación de la última versión del Manual de Convivencia en la siguiente página web www.cowo.com.co. Se entenderá que la última versión del Manual de Convivencia entrará en vigencia al día calendario siguiente de la fecha de la publicación de la última versión del mismo en dicha página web www.cowo.com.co. </span></p>\r\n<p><span>3. Usos ilegales y/o prohibidos: </span></p>\r\n<p><span>EL MIEMBRO </span><span> </span><span>declara entender y aceptar que como condición para el uso de los servicios ofrecidos por </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>, así como para el ejercicio de la Membresía adquirida, no utilizara los servicios de manera alguna que pueda contrariar las condiciones acordadas mediante el presente Contrato, y sus Anexos. </span><span>EL MIEMBRO </span><span> </span><span>no podrá utilizar los servicios ofrecidos por </span><span> </span><span>INVERSIONES MMB S.A.S </span><span>para dañar o deteriorar cualquier bien, derecho y/o servicio propiedad de Inversiones MMB S.A.S y/o de los demás Miembros, ni de ninguna manera que interfiera en el provecho eficiente y pacífico de los servicios ofrecidos por </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>por parte de los demás Miembros, o invitados de los mismos. Adicionalmente, </span><span>EL MIEMBRO </span><span> </span><span>se abstendrá directamente o a través de interpuesta persona de acceder sin autorización a cualquiera de los servicios o cuentas, sistemas informáticos o redes conectadas a los servicios objeto del presente Contrato, a través del &ldquo;</span><span> </span><span>hacke</span><span>o&rdquo; y/o cualquier otro medio fraudulento. </span></p>\r\n<p><span>Parágrafo: </span><span>Por medio de la suscripción del presente Contrato, </span><span> </span><span>EL MIEMBRO </span><span> </span><span>se hace entera y solidariamente responsable de cualquier acto y/o conducta desplegado por algún invitado y/o acompañante suyo, por lo que declara entender y aceptar que dicho acompañante y/o invitado suyo deberá acatar estrictamente las condiciones referidas en el presente Contrato y en sus Anexos, respecto de la utilización de los servicios objeto del presente Contrato. Cualquier incumplimiento al presente Contrato y a sus Anexos, por parte de un invitado y/o acompañante del </span><span> </span><span>MIEMBRO </span><span> </span><span>se entenderá como un incumplimiento del </span><span> </span><span>MIEMBRO </span><span> </span><span>mismo y dará derecho al </span><span>PROVEEDOR </span><span> </span><span>a tomar las medidas pertinentes, incluyendo la terminación del presente Contrato de manera inmediata, según corresponda, a criterio exclusivo de Inversiones MMB S.A.S . </span></p>\r\n<p><span>4. Acuerdos especiales sobre el uso de los Servicios: </span></p>\r\n<p><span>Además de las reglas y condiciones contenidas en el Anexo 2 del presente Contrato, con la firma del mismo, </span><span>EL MIEMBRO </span><span> </span><span>declara que se abstendrá de lo siguiente: </span></p>\r\n<p><span>a. Utilizar los servicios contratados en relación a concursos, esquemas piramidales, cartas en cadena, correo electrónico basura o cualquier mensaje duplicado o no solicitado (de carácter comercial y/o de cualquier otro carácter); </span></p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"page\" title=\"Page 5\">\r\n<div class=\"section\">\r\n<div class=\"layoutArea\">\r\n<div class=\"column\">\r\n<p><span>5. A. </span></p>\r\n</div>\r\n<div class=\"column\">\r\n<ol start=\"2\">\r\n<li>\r\n<p><span>Violar derechos fundamentales, como los derechos de privacidad y buen nombre de terceras personas; </span></p>\r\n</li>\r\n<li>\r\n<p><span>Publicar, cargar, distribuir, y/o divulgar cualquier contenido que se considere profano, inapropiado, obsceno o indecente, a través de la red de servicios propiedad de </span><span> </span><span>INVERSIONES MMB S.A.S</span><span> </span><span>; </span></p>\r\n</li>\r\n<li>\r\n<p><span>Distribuir y/o cargar material o información que sea protegido por las leyes de propiedad intelectual o industrial, se encuentre sujeto a obligaciones de confidencialidad, o sea considerado un secreto comercial y/o industrial, salvo que </span><span> </span><span>EL MIEMBRO </span><span> </span><span>sea el titular de los correspondientes derechos sobre dicho material o información, o que el respectivo titular le haya otorgado la autorización correspondiente; </span></p>\r\n</li>\r\n<li>\r\n<p><span>Cargar archivos que contengan virus, caballos de Troya, gusanos, bombas de tiempo, &ldquo;</span><span> </span><span>cancelbots</span><span>&rdquo;, archivos corruptos, o cualquier otro software similar o programas que puedan dañar el correcto funcionamiento de computadores, bienes propiedad de </span><span> </span><span>INVERSIONES MMB S.A.S</span><span> </span><span>y/o de terceros;. </span></p>\r\n</li>\r\n<li>\r\n<p><span>Descargar cualquier archivo (s) que se entiende por sentido común que no puede ser reproducido, exhibido, y/o distribuido legalmente de tal manera; </span></p>\r\n</li>\r\n<li>\r\n<p><span>Restringir o inhibir a otros Miembros de poder usar y disfrutar de los servicios ofrecido por </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>; </span></p>\r\n</li>\r\n<li>\r\n<p><span>Recolectar información acerca de terceros, incluyendo direcciones de correo electrónico, sin la autorización o consentimiento respectivo del titular de dicha información; </span></p>\r\n</li>\r\n<li>\r\n<p><span>Violar cualquier ley y/o regulación aplicable; </span></p>\r\n</li>\r\n<li>\r\n<p><span>Crear una identidad falsa con el propósito de defraudar a terceros. </span></p>\r\n</li>\r\n</ol>\r\n<p><span>Acuerdo de Confidencialidad: </span></p>\r\n<p><span> </span><span>EL MIEMBRO </span><span>reconoce y acepta que durante la ejecución de la Membresía, así como durante su participación en el uso de los servicios objeto del presente Contrato, podría tener acceso a información confidencial. Para los efectos del presente Contrato, se entenderá como &ldquo;Información Confidencial&rdquo; en parte o en su totalidad, cualquier información que se dé a conocer por </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>, sus funcionarios, empleados, representantes, y/o por parte de cualquier Miembro, información que no es pública y que debe entenderse como confidencial en su naturaleza, a pesar que no se disponga expresamente la condición de confidencialidad sobre la información revelada. </span></p>\r\n<p><span>La Información Confidencial también incluye, sin limitación, información sobre negocios, ventas, operaciones, secretos comerciales, tecnologías, productos, empleados, clientes, planes de marketing, información financiera, los servicios, los asuntos de negocios y cualquier conocimiento adquirido a través del acceso a las instalaciones, los sistemas informáticos y/o los libros y registros de Inversiones MMB S.A.S . Cualquier información derivada de estudios o compilaciones de Inversiones MMB S.A.S . </span></p>\r\n<p><span>Con la suscripción del presente Contrato, </span><span> </span><span>EL MIEMBRO</span><span> </span><span>obliga a: </span></p>\r\n</div>\r\n</div>\r\n<div class=\"layoutArea\">\r\n<div class=\"column\">\r\n<p><span>B. </span></p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"page\" title=\"Page 6\">\r\n<div class=\"section\">\r\n<div class=\"layoutArea\">\r\n<div class=\"column\">\r\n<p><span>C. </span></p>\r\n<p><span>D. </span></p>\r\n<p><span>E. </span></p>\r\n</div>\r\n<div class=\"column\">\r\n<p><span>(I) Usar la Información Confidencial únicamente para los fines autorizados por </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>y/o el Miembro que la revele; </span></p>\r\n<p><span>(II) No usar la Información Confidencial comercialmente, si no ha sido autorizado por el titular de la misma en forma previa, expresa y escrita para tales efectos; </span></p>\r\n<p><span>(III) No divulgar, no publicar, no distribuir a terceros la Información Confidencial entregada por </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>; </span></p>\r\n<p><span>(IV) No copiar, no reproducir, no hacer extractos, apuntes ni registros, parciales ni totales, idénticos o similares, ni en forma alguna de la Información Confidencial, sin que medie autorización previa y por escrito del titular de dicha información; </span></p>\r\n<ol start=\"5\">\r\n<li>\r\n<p><span>(V) &nbsp;Extender la obligación de confidencialidad a sus invitados y/o acompañantes; </span></p>\r\n</li>\r\n<li>\r\n<p><span>(VI) &nbsp;Garantizar la integridad de la Información Confidencial y mantenerla en lugares y archivos </span></p>\r\n</li>\r\n</ol>\r\n<p><span>restringidos, con las advertencias de la </span><span> </span><span>confidencialidad</span><span>; </span></p>\r\n<p><span>(VII) Devolver a </span><span> </span><span>INVERSIONES MMB S.A.S </span><span>todos los documentos que contengan Información Confidencial a la terminación por cualquier causa del presente Contrato, o dentro del término más corto posible a solicitud por parte de </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>. En caso de que no se haga la devolución por alguna razón, la misma deberá ser destruida de manera que no pueda ser copiada, reproducida o reconstruida. </span></p>\r\n<p><span>(VIII) EncasodequedentrodelaInformaciónConfidencialrecibidade</span><span> </span><span>INVERSIONESMMBS.A.S </span><span>, </span><span> </span><span>EL MIEMBRO </span><span>identifique algún dato y/o información que pueda clasificarse como personal, </span><span> </span><span>EL MIEMBRO </span><span>se obliga a dar fiel cumplimiento a lo establecido por la Ley 1581 de 2012, el Decreto 1377 de 2013 y las disposiciones concordantes. </span></p>\r\n<p><span>Toda la Información Confidencial es de única y exclusiva propiedad de Inversiones MMB S.A.S , o del respectivo Miembro que la revele. </span><span> </span><span>EL MIEMBRO </span><span>reconoce y acepta que este acuerdo y los términos de uso, no le otorgan ni le conceden de ninguna manera, permisos o autorizaciones relativos a la Información Confidencial, propiedad intelectual, patentes, o derechos de autor pertenecientes a </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>y/o a cualquier Miembro. </span></p>\r\n<p><span>Sin perjuicio de las limitaciones a la transmisión y transferencia de la Información Confidencial por parte del </span><span> </span><span>MIEMBRO</span><span> </span><span>, establecidas en el presente Acuerdo, todos los términos establecidos en este Acuerdo tienen aplicación en todo el territorio de la República de Colombia, así como en cualquier territorio al que se transmita y/o transfiera la Información Confidencial de Inversiones MMB S.A.S , o de cualquier Miembro, ya sea mediando autorización previa y expresa por parte del titular de la Información Confidencial, o en violación de lo establecido en el presente Acuerdo. </span></p>\r\n<p><span>La Membresía y Uso de Servicios:<br /> EL MIEMBRO </span><span>reconoce expresamente que adquiere la Membresía y contrata los servicios ofrecidos por</span><span> </span><span>INVERSIONESMMBS.A.S </span><span>bajosupropiavoluntadydecisión,yenconsecuenciareconoceque </span><span>INVERSIONES MMB S.A.S </span><span>no tiene responsabilidad alguna con respecto al acceso a los servicios, especialmente en lo referente a los servicios de internet, que son provistos por un tercero, o de ninguna pérdida de información resultante del uso de dichos servicios por parte del </span><span> </span><span>MIEMBRO</span><span> </span><span>. </span></p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"page\" title=\"Page 7\">\r\n<div class=\"section\">\r\n<div class=\"layoutArea\">\r\n<div class=\"column\">\r\n<ol start=\"6\">\r\n<li>\r\n<p><span>Exclusión de Garantías:<br /> EL MIEMBRO </span><span> </span><span>reconoce que los servicios ofrecidos por </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>, se proveen tal y como son, incluyendo sus posibles falencias, bajo el entendido que las obligaciones de Inversiones MMB S.A.S </span><span> </span><span>bajo el presente Contrato son de medio y no de resultado, y en consecuencia </span><span> </span><span>EL MIEMBRO </span><span> </span><span>declara entender y aceptar que </span><span> </span><span>INVERSIONES MMB S.A.S </span><span>no otorga ningún tipo de garantía expresa y/o tácita, respecto del alcance de los servicios contratados por medio del presente Contrato. </span></p>\r\n</li>\r\n<li>\r\n<p><span>Exclusión de daños incidentales, </span><span> </span><span>emergentes y o</span><span> </span><span>tros daños:<br /> </span><span>En la medida máxima permitida por la ley aplicable, bajo ninguna circunstancia </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>, sus filiales, subsidiarias, funcionarios presentes o futuros, agentes, accionistas, miembros, representantes, empleados, sucesores y cesionarios, de manera conjunta o individualmente serán responsables de cualquier daño directo, especial, incidental, emergente, indirecto, punitivo, consecuente, lucro cesante, o de otro tipo, incluyendo, pero sin limitarse a, los daños derivados de: pérdida de utilidades, ingresos, réditos, pérdida de información confidencial o de otro tipo, interrupción u obstaculización de negocios, lesiones personales, pérdida de privacidad o incumplimiento de cualquier obligación que surja o pueda surgir de la utilización o de la incapacidad para usar los servicios objeto del presente Contrato, así como de la prestación o no prestación de dichos servicios por parte de Inversiones MMB S.A.S .<br /> Así mismo, </span><span> </span><span>EL MIEMBRO </span><span> </span><span>declara entender y aceptar que </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>no asume ningún tipo de responsabilidad respecto del cuidado de los equipos y demás materiales y/o elementos propiedad de </span><span> </span><span>EL MIEMBRO</span><span> </span><span>, que el mismo utilice dentro del espacio comunitario provisto por </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>, dado que del presente Contrato no derivan obligaciones como depositario y/o custodio en cabeza de Inversiones MMB S.A.S </span><span> </span><span>respecto de bienes propiedad del </span><span>MIEMBRO</span><span> </span><span>. </span></p>\r\n</li>\r\n<li>\r\n<p><span>Causales de Terminación: </span></p>\r\n<p><span>El presente Contrato se podrá dar por terminado en los siguientes casos: </span></p>\r\n<p><span>a) Por mutuo acuerdo entre </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>y </span><span> </span><span>EL MIEMBRO</span><span> </span><span>; </span></p>\r\n<p><span>b) En caso de incumplimiento de cualquier obligación, término y/o condición derivado del presente Contrato y de sus Anexos por parte del </span><span> </span><span>MIEMBRO</span><span> </span><span>, especialmente lo concerniente al Manual de Convivencia. Dicha terminación operará de manera inmediata en el momento que </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>se entere de dicho incumplimiento por parte del </span><span> </span><span>MIEMBRO</span><span> </span><span>; decisión que será tomada a criterio exclusivo de Inversiones MMB S.A.S , y será informado por escrito al </span><span> </span><span>MIEMBRO </span><span> </span><span>incumplido; </span></p>\r\n<p><span>c) Por decisión unilateral del </span><span> </span><span>MIEMBRO</span><span> </span><span>, que deberá ser informada por escrito al </span><span> </span><span>PROVEEDOR </span><span>dentro de los tres (3) días hábiles previos a la fecha efectiva de terminación. </span></p>\r\n<p><span>Parágrafo</span><span> </span><span>: En ninguno de los anteriores eventos </span><span> </span><span>EL MIEMBRO </span><span>tendrá derecho a reclamar indemnización alguna, ni devolución de dineros relativos al término no disfrutado de la Membresía, ni de los créditos no redimidos a la fecha de terminación del Contrato. </span></p>\r\n<p><span>d) INVERSIONES MMB S.A.S </span><span> </span><span>hace constar que como persona respetuosa de las normas vigentes, conoce y acata el Código Penal Colombiano y demás normas nacionales que se relacionen con la prevención de actividades delictivas y en especial el Lavado de Activos, el Enriquecimiento Ilícito y la Financiación del Terrorismo. En tal </span></p>\r\n<p><span>sentido, </span><span> </span><span>EL MIEMBRO </span><span>podrá cruzar en cualquier momento la información de </span><span> </span><span>INVERSIONES MMB S.A.S </span><span>con las listas vinculantes para Colombia, listas públicas internacionales y locales sobre personas investigadas por Lavado de Activos o Financiación del Terrorismo, o con información relacionada con procesos de Extinción de Dominio. Cuando quiera que la investigación arroje resultados positivos o exista alguna investigación iniciada por </span></p>\r\n<p><span>las autoridades competentes por esta razón, respecto a </span><span> </span><span>INVERSIONES MMB S.A.S</span><span> </span><span>, </span><span>sus socios, sus sociedades subordinadas o vinculadas por algún motivo, sus administradores o cualquier persona natural o jurídica </span></p>\r\n</li>\r\n</ol>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"page\" title=\"Page 8\">\r\n<div class=\"section\">\r\n<div class=\"layoutArea\">\r\n<div class=\"column\">\r\n<p><span>con la cual </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>hubiere tenido relación de cualquier tipo, </span><span> </span><span>EL MIEMBRO </span><span>procederá a tomar las decisiones pertinentes teniendo en cuenta sus políticas internas, las normas vigentes e incluso podrá dar por terminado el presente contrato de manera inmediata sin lugar a indemnización alguna a favor de </span></p>\r\n<p><span>INVERSIONES MMB S.A.S</span><span> </span><span>. </span></p>\r\n<ol start=\"9\">\r\n<li>\r\n<p><span>Autorización:<br /> EL MIEMBRO</span><span> </span><span>, con la firma del presente Contrato, expresamente autoriza al </span><span> </span><span>PROVEEDOR </span><span>sus sucursales, filiales y subsidiarias, a recolectar, guardar, tener, mantener, manejar, tratar, utilizar, actualizar, transferir, transmitir, y compartir sus datos personales para los propósitos lícitos de Inversiones MMB S.A.S , a saber: ejercer el objeto social de Inversiones MMB S.A.S , establecido en sus estatutos y en la ley, desarrollar el objeto del presente Contrato, cumplir con sus obligaciones tributarias, pagar cumplidamente a sus proveedores, cumplir sus obligaciones laborales, realizar estudios de investigación y de mercados, dar a conocer información sobre los productos y servicios que comercializa, hacer vigilancia y reportes de quejas de calidad de productos y servicios, comercializar los productos y servicios, gestionar la administración de los Recursos Humanos de la Compañía, mejorar los servicios que presta, evaluar los niveles de satisfacción de sus clientes, cumplir con los compromisos adquiridos con los clientes, autoridades nacionales e internacionales sin ninguna limitación distinta a las establecidas en la Constitución Política, la Ley 1581 de 2012, el Decreto 1377 de 2013 y cualquier otra disposición legal relativa a este tema, y de acuerdo con los principios ahí establecidos. La presente autorización no incluye la recolección y trato de datos sensibles, entendiendo este tipo de información como cualquier dato que pueda afectar negativamente la intimidad de su titular o pueda generar cualquier tipo de discriminación negativa en su contra. </span><span> </span><span>EL MIEMBRO </span><span>declara que ha sido informado sobre sus derechos y obligaciones relativas a la protección de sus datos personales y que la presente autorización ha sido proveída libremente, sin perjuicio de que pueda ser revocada por </span><span> </span><span>EL MIEMBRO </span><span>por medio de una solicitud formal, cuando lo considere conveniente. </span><span> </span><span>EL MIEMBRO </span><span>declara que entiende y conoce los procedimientos y política de privacidad de Inversiones MMB S.A.S en lo que tiene que ver con el tratamiento y seguridad de los datos personales. </span></p>\r\n</li>\r\n<li>\r\n<p><span>Independencia de Las Partes:<br /> EL MIEMBRO </span><span> </span><span>se considera como un trabajador independiente con plena autonomía técnica y administrativa y no será considerado por ningún motivo como empleado de Inversiones MMB S.A.S . Así mismo, cada Parte declara con respecto de la otra que no es apoderada, representante, socia, agente, ni asociada en cuentas en participación, ni que la parte ni el personal vinculado a ésta podrá ser tenido en ningún momento como vinculado laboralmente a la otra parte. En consecuencia, cada parte asumirá como propias todas las responsabilidades que las leyes, los convenios y sus estatutos le impongan en relación consigo mismo, y con el personal propio o afiliado que utilice para desarrollar su actividad económica independiente. </span></p>\r\n</li>\r\n</ol>\r\n<p><span>11. Capacidad y No Relación de Consumo:<br /> </span><span>Con la suscripción del presente Contrato, </span><span> </span><span>EL MIEMBRO </span><span>manifiesta y garantiza que tiene plena capacidad legal y la autoridad necesaria para celebrar y cumplir a cabalidad con los términos y condiciones descritos en este Contrato, sin que ninguna otra manifestación u autorización diferente a la suya sea necesaria. Al mismo tiempo </span><span> </span><span>EL MIEMBRO </span><span>garantiza que la aceptación y celebración del presente Contrato, no interfieren de ninguna manera con alguna licencia, obligación o contrato en la que </span><span>EL MIEMBRO</span><span> </span><span>sea parte. </span></p>\r\n<p><span>Así mismo, con la suscripción del presente documento, </span><span> </span><span>EL MIEMBRO </span><span>declara que la adquisición de la correspondiente Membresía, así como la contratación de los servicios objeto del presente Contrato, son actos ligados intrínsecamente con el desarrollo de la actividad económica que de manera independiente </span></p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"page\" title=\"Page 9\">\r\n<div class=\"section\">\r\n<div class=\"layoutArea\">\r\n<div class=\"column\">\r\n<p><span>desarrolla </span><span> </span><span>EL MIEMBRO</span><span> </span><span>, por lo que el mismo declara entender y aceptar que la relación derivada de este Contrato entre Las Partes no es de ninguna manera una relación de consumo, en los términos de la Ley 1480 de 2011. </span></p>\r\n<p><span>12. Domicilio Contractual y Legislación Aplicable: </span></p>\r\n<p><span>Para todos los efectos legales, Las Partes contratantes acuerdan fijar la ciudad de Bogotá D.C. como domicilio contractual. Así mismo, declaran entender y aceptar que el presente Contrato se regulará por la legislación colombiana aplicable a la materia. </span></p>\r\n<p><span>13. Ausencia de Renuncias Implícitas: </span></p>\r\n<p><span>La omisión o demora de cualquiera de La Partes en exigir el cumplimiento de cualquier término o condición del presente Contrato o de requerir el puntual cumplimiento de las mismas, no se interpretará como renuncia a exigir el cumplimiento de dichos términos o condiciones, ni afectará la validez de tales términos y condiciones, ni el derecho que tienen Las Partes para exigir el cumplimiento en el futuro de todos y cada uno de dichos términos y condiciones. </span></p>\r\n<p><span>14. Arbitraje: </span></p>\r\n<p><span>Toda controversia o diferencia relativa al desarrollo de este Contrato que no pueda ser arreglada amistosamente entre Las Partes, se resolverá por un tribunal de arbitramento designado por la Cámara de Comercio de Bogotá, el cual una vez constituido se sujetará a lo dispuesto en la Ley 1563 de 2012 y demás normas que la reglamenten, sustituyan, deroguen, modifiquen y/o complementen, de acuerdo con las siguientes reglas: a) El Tribunal estará integrado por un (1) árbitro si la cuantía no supera una suma equivalente a cuatrocientos (400) salarios mínimos mensuales legales vigentes, o por tres (3) árbitros si la cuantía supera una suma equivalente a cuatrocientos (400) salarios mínimos mensuales legales vigentes. El (los) árbitro(s) será(n) ciudadano(s) colombiano(s) y abogado(s) en ejercicio; b) La organización interna del Tribunal se sujetará a las reglas previstas para el efecto por el Centro de Arbitraje y Conciliación de la Cámara de Comercio de Bogotá D.C.; c) El Tribunal decidirá en derecho; d) El Tribunal funcionará y sesionará en Bogotá D.C., en el Centro de Arbitraje y Conciliación de la Cámara de Comercio de esa ciudad, y e) Los costos y gastos derivados del Tribunal serán asumidos por la Parte vencida. En lo que se refiere a ejecuciones derivadas del presente Contrato se realizarán por intermedio de la justicia ordinaria colombiana. </span></p>\r\n<p><span>15. Notificaciones: </span></p>\r\n<p><span>Todas las notificaciones y avisos que deban otorgarse de conformidad con el presente Contrato deberán hacerse por escrito, debiendo obtener la parte que la realice, evidencia de que la comunicación ha sido recibida por la otra parte. Para los efectos anteriores y hasta tanto no se notifiquen nuevos domicilios en la forma antes indicada, Las Partes señalan como sus domicilios: </span></p>\r\n<p><span>EL MIEMBRO </span></p>\r\n<p><span>Ciudad: Email: Teléfono: Dirección: </span></p>\r\n<p><span>INVERSIONES MMB S.A.S </span></p>\r\n<p><span>Ciudad: Bogotá<br /> Teléfono: 3208337956 Email: </span><span> </span><span>andres@cowo.com.co </span><span>Dirección: Calle 98 #8-37 </span></p>\r\n</div>\r\n</div>\r\n<div class=\"layoutArea\">\r\n<div class=\"column\">\r\n<p><span>En caso de que cualquiera de Las Partes cambie su domicilio, deberá notificarlo por escrito a la otra, </span></p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"page\" title=\"Page 10\">\r\n<div class=\"section\">\r\n<div class=\"layoutArea\">\r\n<div class=\"column\">\r\n<p><span>dentro de las 24 (veinticuatro) horas siguientes a que esto suceda, conforme a lo establecido en esta Cláusula, de lo contrario, las notificaciones hechas a los anteriores domicilios, serán consideradas como válidas, sin la necesidad de evidencia de recibo. </span></p>\r\n<p><span>16. Indemnidades:<br /> EL MIEMBRO </span><span>deberá defender, indemnizar y mantener a salvo a </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>, sus filiales, sucesores y cesionarios y los directores, administradores, empleados y agentes de los mismos contra todas las reclamaciones, pérdidas, daños, responsabilidades y gastos, incluyendo honorarios razonables de abogados, incurridos por ellos por concepto de o como resultado de cualquier incumplimiento de este Contrato, debido a la negligencia o a actos ilícitos del </span><span> </span><span>MIEMBRO</span><span> </span><span>, sus invitados y/o acompañantes, contra todas y cada una de las reclamaciones que cualquier tercero pueda presentar contra </span><span> </span><span>INVERSIONES MMB S.A.S </span><span> </span><span>. </span></p>\r\n<p><span>17. Titulación de Cláusulas: </span></p>\r\n<p><span>Las Partes aclaran que los títulos de las cláusulas de este Contrato se han establecido con el propósito de tener puntos de referencia que no buscan definir los términos del Contrato y aceptan que no serán utilizados en la interpretación del presente Contrato. </span></p>\r\n<p><span>18. Individualidad: </span></p>\r\n<p><span>La declaración de nulidad, la ineficacia y/o la invalidez de una de las Cláusulas del Contrato no conllevará la nulidad, ineficacia y/o invalidez de las demás, que conservarán plena validez y efectividad, por lo tanto si cualquier parte o partes de este Contrato son declarada (s) invalida (s) por los tribunales competentes, las otras partes del mismo no se verán afectadas o suprimidas por ese motivo. </span></p>\r\n<p><span>19. No Cesión:<br /> </span><span>Este Contrato se celebra en consideración a la naturaleza misma del </span><span> </span><span>MIEMBRO</span><span> </span><span>, por lo tanto, no podrá ser cedido, vendido o transferido a ningún título, ni total ni parcialmente, sin la previa autorización expresa y escrita de Inversiones MMB S.A.S . </span></p>\r\n<p><span>20. Alianza Comercial: </span><span> </span><span>Si el miembro tiene una alianza comercial vigente se realizará un cruce de cuentas por los valores correspondientes acordados. </span></p>\r\n<p><span>Para constancia se extiende y firma el presente contrato en la ciudad de Bogotá, el día del mes del año 2017, en dos (2) ejemplares de igual tenor y contenidos. </span></p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"col-sm-12\">&nbsp;</div>\r\n</div>\r\n</div>\r\n<div>&nbsp;</div>', 'N', 11, '2016-11-21 22:41:28', '2017-05-23 19:45:33');
INSERT INTO `sis_con_contenido` (`id`, `titulo`, `llave`, `tipo`, `cuerpo`, `inicio`, `peso`, `created_at`, `updated_at`) VALUES
(24, 'Página inicio', 'pagina-inicio', 'S', '<div class=\"container_full\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12 col-md-3\" style=\"height: 150px !important; background: url(\'https://cowo.com.co/media//cowo-membresia.jpg\') center right; text-align: center; line-height: 150px; color: white; font-size: 50px; font-weight: bold; border-right: 2px solid white;\"><span><a style=\"color: #ffffff;\" href=\"https://cowo.com.co/planes\">PLANES</a></span></div>\r\n<div class=\"col-md-12 col-md-3\" style=\"height: 150px !important; background: url(\'https://cowo.com.co/media//cowo-cursos.jpg\') center right; text-align: center; line-height: 150px; color: white; font-size: 50px; font-weight: bold; border-right: 2px solid white;\"><span><a style=\"color: #ffffff;\" href=\"https://cowo.com.co/alianzas\">ALIANZAS</a></span></div>\r\n<div class=\"col-md-12 col-md-3\" style=\"height: 150px; background: url(\'https://cowo.com.co/media//cowo-emprende.jpg\') center center; text-align: center; line-height: 150px; color: white; font-size: 50px; font-weight: bold; border-right: 2px solid white;\"><span><span><a style=\"color: #ffffff;\" href=\"https://cowo.com.co/planes\">SALAS</a></span><br /></span></div>\r\n<div class=\"col-md-12 col-md-3\" style=\"height: 150px; background: url(\'https://cowo.com.co/media//cowo-eventos.jpg\') center center; text-align: center; line-height: 150px; color: white; font-size: 50px; font-weight: bold;\"><a style=\"color: white;\" href=\"https://cowo.com.co/calendario\">EVENTOS</a></div>\r\n</div>\r\n</div>\r\n<div>&nbsp;</div>\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<h2 style=\"text-align: center;\">En CoWo tendr&aacute;s todos los beneficios que necesitas <br /><strong>para emprender y hacer crecer tu negocio. &nbsp;Abrimos de lunes a viernes de 7 a 7 y los s&aacute;bados de 7 a 12.</strong></h2>\r\n<br /><br />\r\n<div class=\"col-6 col-sm-3\" style=\"background-position: center;\"><img style=\"vertical-align: middle;\" src=\"https://cowo.com.co/media//cowo-1.png\" alt=\"\" width=\"100%\" /><br /><br />\r\n<p style=\"color: black; text-align: center;\">Espacio de 300 m&sup2;<br /> <strong>totalmente equipado para trabajar.</strong></p>\r\n</div>\r\n<div class=\"col-6 col-sm-3\" style=\"background-position: center;\"><img style=\"vertical-align: middle;\" src=\"https://cowo.com.co/media//cowo-2.png\" alt=\"\" width=\"100%\" /><br /><br />\r\n<p style=\"color: black; text-align: center;\">Servicio de tel&eacute;fono <br /><strong>y recepci&oacute;n de correo.</strong></p>\r\n</div>\r\n<div class=\"col-6 col-sm-3\" style=\"background-position: center;\"><a href=\"https://cowo.com.co/calendario\"><img style=\"vertical-align: middle;\" src=\"https://cowo.com.co/media//cowo-3.png\" alt=\"\" width=\"100%\" /></a><br /><br />\r\n<p style=\"color: black; text-align: center;\">Eventos de networking <br /><strong>y acceso a conferencias.</strong></p>\r\n</div>\r\n<div class=\"col-6 col-sm-3\" style=\"background-position: center;\"><img style=\"vertical-align: middle;\" src=\"https://cowo.com.co/media//cowo-4.png\" alt=\"\" width=\"100%\" /><br /><br />\r\n<p style=\"color: black; text-align: center;\">Amplia red de trabajadores: <strong>Participa en foros, ofrece tus servicios, comparte o pide ayuda.</strong></p>\r\n</div>\r\n</div>\r\n</div>\r\n<div>&nbsp;</div>\r\n<div class=\"container\">\r\n<div class=\"row\"><br /><br />\r\n<div class=\"col-6 col-sm-3\" style=\"background-position: center;\"><img style=\"vertical-align: middle;\" src=\"https://cowo.com.co/media//cowo-5.png\" alt=\"\" width=\"100%\" /><br /><br />\r\n<p style=\"color: black; text-align: center;\">Zonas de esparcimiento, <br /><strong>TV y juegos.</strong></p>\r\n</div>\r\n<div class=\"col-6 col-sm-3\" style=\"background-position: center;\"><img style=\"vertical-align: middle;\" src=\"https://cowo.com.co/media//cowo-6.png\" alt=\"\" width=\"100%\" /><br /><br />\r\n<p style=\"color: black; text-align: center;\">Cafeter&iacute;a <br /><strong>y papeler&iacute;a.</strong></p>\r\n</div>\r\n<div class=\"col-6 col-sm-3\" style=\"background-position: center;\"><a href=\"https://cowo.com.co/saladejuntas\"><img style=\"vertical-align: middle;\" src=\"https://cowo.com.co/media//cowo-7.png\" alt=\"\" width=\"100%\" /></a><br /><br />\r\n<p style=\"color: black; text-align: center;\">Salas de juntas equipadas, <br /><strong>internet de alta velocidad.</strong></p>\r\n</div>\r\n<div class=\"col-6 col-sm-3\" style=\"background-position: center;\"><a href=\"https://cowo.com.co/alianzas\"><img style=\"vertical-align: middle;\" src=\"https://cowo.com.co/media//cowo-8.png\" alt=\"\" width=\"100%\" /></a><br /><br />\r\n<p style=\"color: black; text-align: center;\">Aliados para transporte, eventos, parqueaderos, gimnasios,&nbsp;<strong>descuentos y m&aacute;s</strong>.</p>\r\n</div>\r\n</div>\r\n</div>\r\n<div>&nbsp;</div>\r\n<p>&nbsp;</p>\r\n<p><span>{!! $grid_fotos !!}<br /></span></p>\r\n<div class=\"fondo03\">\r\n<div class=\"container no-gutter\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12\">\r\n<h2><br />Comun&iacute;cate con nosotros:</h2>\r\n<div class=\"col-sm-12\">{!! $form_contacto !!}</div>\r\n<div class=\"col-sm-12\">&nbsp;</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<p>&nbsp;</p>\r\n<div class=\"container no-gutter\">\r\n<div class=\"row\">\r\n<p>{!! $bloque_links !!}</p>\r\n</div>\r\n</div>', 'S', 0, '2017-01-28 02:23:51', '2017-09-09 03:18:11'),
(25, 'Trabaja con nosotros', 'trabaja-con-nosotros', 'S', '<div class=\"container\">\r\n<h2 class=\"row\" style=\"text-align: center;\">Trabaja con nosotros</h2>\r\n<div class=\"row\">&nbsp;</div>\r\n<div class=\"row\"><span>Si quieres ser nuestro aliado o te interesa trabajar con nosotros de cualquier forma, por favor llena los siguientes datos.</span><br />\r\n<div class=\"col-sm-12\">&nbsp;</div>\r\n</div>\r\n</div>\r\n<div>&nbsp;</div>\r\n<div>{!! $form_trabaja !!}</div>', 'N', 0, '2017-01-30 07:35:16', '2017-02-28 02:05:47'),
(26, 'Políticas de privacidad', 'politicas-de-privacidad', 'S', '<div class=\"container\">\r\n<h2 class=\"row\" style=\"text-align: center;\">Pol&iacute;ticas de privacidad</h2>\r\n<div class=\"row\">&nbsp;</div>\r\n<div class=\"row\">\r\n<div class=\"col-sm-12\">\r\n<p><strong>CONTENIDO Y ACEPTACI&Oacute;N DE LA POL&Iacute;TICA DE PRIVACIDAD</strong></p>\r\n<p>La presente Pol&iacute;tica de Privacidad tiene por objeto dar a conocer el modo en que se recaban, tratan y protegen los datos de car&aacute;cter personal a los que se accede por los servicios asociados con este Sitio Web, a fin de que determines libre y voluntariamente si deseas facilitar tus datos personales a trav&eacute;s de los formularios habilitados al efecto. Asimismo, la Pol&iacute;tica de Privacidad regula el tratamiento de los datos personales facilitados en el pasado por ti a trav&eacute;s de otras v&iacute;as ajenas al Sitio Web, o los que puedas facilitar en el futuro con remisi&oacute;n a la presente Pol&iacute;tica, siempre en la medida en que no contradiga lo expresamente aceptado por ti en el momento de facilitar los datos, que ser&aacute; respetado en todo caso.</p>\r\n<p>Adem&aacute;s, se te proporcionar&aacute;n los recursos t&eacute;cnicos adecuados para que, con car&aacute;cter previo a la cumplimentaci&oacute;n de los datos personales, pod&aacute;is acceder a esta Pol&iacute;tica de Privacidad y a cualquier otra informaci&oacute;n relevante en materia de Protecci&oacute;n de Datos de Car&aacute;cter Personal.</p>\r\n<p>El acceso y uso del Sitio Web implica que aceptas en su totalidad y te obligas a cumplir por completo los t&eacute;rminos y condiciones recogidos en la presente Pol&iacute;tica de Privacidad, as&iacute; como en las disposiciones contenidas en el Aviso Legal y Condiciones de Uso, la <a href=\"http://vocento.com/politica-cookies/\" target=\"_blank\">Pol&iacute;tica de Cookies</a> y, en su caso, las Condiciones Generales o Particulares de Contrataci&oacute;n de cualesquiera otros servicios; configur&aacute;ndose los citados documentos en su conjunto como los Textos Legales aplicables. Puedes encontrar los enlaces correspondientes al pie de la p&aacute;gina principal. La prestaci&oacute;n del servicio del Sitio Web tiene una duraci&oacute;n limitada al momento en el que te encuentres conectado en su caso al Sitio Web o alguno de los servicios que a trav&eacute;s del mismo se facilitan; por tanto, debes leer atentamente dichos Textos Legales en cada una de las ocasiones en que te propongas utilizar el Sitio Web, ya que &eacute;stos pueden sufrir modificaciones en funci&oacute;n de novedades o exigencias legislativas y jurisprudenciales o por necesidades de negocio.</p>\r\n<p>La presente Pol&iacute;tica de Privacidad ser&aacute; v&aacute;lida para los datos de car&aacute;cter personal obtenidos para este Sitio Web u otros servicios de la SOCIEDAD. No obstante, en el supuesto de que formalices tu registro o asociaci&oacute;n con el Sitio Web a trav&eacute;s de alguna red social, aplicaci&oacute;n de mensajer&iacute;a instant&aacute;nea, red o sistema similar, estar&aacute;s autorizando a que dicha red o sistema entregue datos a la SOCIEDAD y a COMERESA en los t&eacute;rminos previstos en la Pol&iacute;tica de Privacidad de la red o sistema de que se trate en cada caso, sin perjuicio de que ser&aacute; de tu responsabilidad el consultar dicha Pol&iacute;tica con car&aacute;cter previo a conceder tu autorizaci&oacute;n.</p>\r\n</div>\r\n</div>\r\n</div>\r\n<div>&nbsp;</div>', 'N', 0, '2017-01-30 07:36:17', '2017-02-28 02:27:07'),
(27, 'Reglamento', 'reglamento', 'S', '<div class=\"container\">\r\n<div class=\"row\" style=\"text-align: left;\"><span></span>\r\n<p>&nbsp;</p>\r\n<p><span>Reglamento CoWo</span></p>\r\n<p><span>Para tener una experiencia amigable y &oacute;ptima en nuestro trabajo, por favor sigue las siguientes normas de convivencia:</span></p>\r\n<ol>\r\n<li><span>Todo es de todos, cu&iacute;dalo.</span><span></span></li>\r\n<li><span>Si llegas, saluda.</span></li>\r\n<li><span>Si desordenaste, ordena.</span></li>\r\n<li><span>Si vas a usar algo que no te pertenece, pide permiso. </span></li>\r\n<li><span>Si te prestaron, devuelve.</span></li>\r\n<li><span>Si est&aacute;s utilizando las herramientas que tenemos para ti, cu&iacute;dalas y devu&eacute;lvelas a su lugar.</span></li>\r\n<li><span>Si traes a tu perro, enc&aacute;rgate de &eacute;l.</span></li>\r\n<li>No dejes a tu perro subir a sof&aacute;s, sillas o mesas.</li>\r\n<li>Cualquier da&ntilde;o causado por un perro debe ser asumido por su due&ntilde;o.</li>\r\n<li><span>Si ensucias, limpia.</span></li>\r\n<li><span>Apaga luces de ba&ntilde;os y salas de juntas cuando termines de usarlas.</span></li>\r\n<li><span>Respeta el horario de reserva de salas de juntas.</span></li>\r\n<li><span>Las salas de juntas se cobran a partir de la hora que se reservaron y cualquier fracci&oacute;n superior a 15 minutos se cobrar&aacute; como hora adicional. &nbsp;El alquiler de sala de juntas incluye m&aacute;ximo 7 asistentes adicionales a la reuni&oacute;n.Los asistentes a reuni&oacute;n en la sala de juntas deben permanecer en la misma. Si usan alguna de las otras instalaciones se les cobrar&aacute; un cr&eacute;dito por hora. &nbsp;</span></li>\r\n<li><span>Las salas de juntas deben ser utilizadas por el miembro titular de CoWo y no pueden ser reservadas para alguien m&aacute;s.</span></li>\r\n<li><span>Borra los tableros de salas de juntas al salir.</span></li>\r\n<li><span>Todo pago de cualquier servicio debe hacerse anticipado. &nbsp;Si tienes cr&eacute;ditos, red&iacute;melos. &nbsp;Si se te acaban, compra m&aacute;s antes de acceder a servicios.</span></li>\r\n<li><span>Si quieres o&iacute;r tu m&uacute;sica o cualquier contenido de audio, utiliza aud&iacute;fonos.</span></li>\r\n<li><span>Si vas a hablar en voz&nbsp;alta, recibir o hacer llamadas, utiliza los espacios adecuados como booths traseros o terrazas. </span></li>\r\n<li><span>Atiende a tus invitados en booths, terrazas o en el s&oacute;tano.</span></li>\r\n<li><span>Si tienes un invitado, debes ingresarlo y notificar al personal. &nbsp;Recuerda que cada invitado tiene un costo de 1 cr&eacute;dito por hora.</span></li>\r\n<li><span>Debes &nbsp;marcar cualquier ingreso o salida de la casa en el biom&eacute;trico.</span></li>\r\n<li><span>En caso de no marcar ingreso y permanecer adentro o salir y no marcar salida, se te descontar&aacute;n 4 cr&eacute;ditos o 4 horas en caso de ser Part Time.</span></li>\r\n<li><span>No est&aacute; permitido fumar en la casa ni en las terrazas.</span></li>\r\n<li><span>Las horas de servicio son de 7:00 am a 7:00 pm.</span></li>\r\n<li><span>No estaciones motos ni carros en el deck exterior de la casa. &nbsp;Si tienes bicicleta, utiliza el rack con candado. &nbsp;</span></li>\r\n<li><span>En caso de que el rack est&eacute; lleno, utiliza el parqueadero Citi Parking que cuenta con parqueadero de bicicletas gratis.</span></li>\r\n<li><span>No consumas alimentos que puedan causar olores desagradables adentro de la casa. </span></li>\r\n<li><span>Las neveras son de uso comunal y ser&aacute;n desocupadas todos los fines de semana.</span></li>\r\n<li><span>El consumo de alcohol es solo permitido en eventos autorizados por el personal de CoWo.</span></li>\r\n<li><span>Las zonas designadas para almuerzo son las terrazas.</span></li>\r\n<li><span>No dejes objetos personales en puestos comunales. Cualquier objeto personal dejado en puestos comunes, ser&aacute; retirado para liberar el espacio a otros coworkers.</span></li>\r\n<li><span>El caf&eacute;, t&eacute;, agua y fruta son ilimitados para los miembros de CoWo. &nbsp;Sin embargo, ser&aacute;n servidos&nbsp;</span><span>a los miembros entre 8:00 y 9:00 am y entre 2:00 y 3:00 pm. &nbsp;En cualquier horario adicional, es autoservicio.</span></li>\r\n<li style=\"font-weight: 400;\"><span>Las comidas y bebidas complementarias son para consumir dentro de CoWo.</span></li>\r\n<li style=\"font-weight: 400;\"><span>CoWo no se hace responsable por la p&eacute;rdida de objetos personales.</span></li>\r\n<li style=\"font-weight: 400;\"><span>Acu&eacute;rdate que el ba&ntilde;o lo va &nbsp;usar otro coworker despu&eacute;s de ti. </span></li>\r\n<li style=\"font-weight: 400;\"><span>Usa los Booths de&nbsp; para reuniones de dos o m&aacute;s personas no m&aacute;s de 60 minutos consecutivos.</span></li>\r\n</ol>\r\n&nbsp;</div>\r\n</div>\r\n<div>&nbsp;{!! $grid_fotos !!}</div>', 'N', 0, '0000-00-00 00:00:00', '2017-02-27 23:07:56'),
(28, 'Calendario de eventos', 'calendario-de-eventos', 'S', '<div class=\"container\">\r\n<h1 class=\"row\" style=\"text-align: center;\">Eventos, Conferencias y Networking</h1>\r\n<div class=\"row\">&nbsp;</div>\r\n<div class=\"row\">En CoWo<span>&nbsp;realizamos eventos y conferencias con expertos en diferentes tem&aacute;ticas, con el fin de crear una experiencia enriquecedora con temas de inter&eacute;s para los miembros de nuestra comunidad, generando espacios de networking que resulten productivos para todos.&nbsp;</span><span>En este calendario, puedes revisar nuestros eventos y eventos de empresas, entidades del gobierno y organizaciones de tu inter&eacute;s:</span></div>\r\n<div class=\"row\">&nbsp;</div>\r\n<div class=\"row\"><iframe src=\"https://calendar.google.com/calendar/embed?src=ca1edhpj30ovaoh72fe2lpihgs%40group.calendar.google.com&amp;ctz=America/Bogota\" frameborder=\"0\" scrolling=\"no\" width=\"100%\" height=\"600\"></iframe><br />\r\n<div class=\"col-sm-12\">&nbsp;</div>\r\n<div class=\"col-sm-12\" style=\"text-align: center;\">Si quieres participar en nuestros eventos, organizar un evento de tu empresa, o te interesa dar una conferencia en nuestras instalaciones, escr<span>&iacute;</span>benos al correo: <a href=\"mailto:info@cowo.com.co\">info@cowo.com.co</a></div>\r\n<div class=\"col-sm-12\" style=\"text-align: center;\">&nbsp;</div>\r\n</div>\r\n</div>\r\n<div>&nbsp;</div>', 'N', 0, '0000-00-00 00:00:00', '2017-09-09 03:19:42'),
(29, 'Programa de Emprendimiento', 'programa-de-emprendimiento', 'S', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12\">\r\n<h2 style=\"text-align: center;\"><span><strong>&iquest;Est&aacute;s listo para emprender?</strong></span></h2>\r\n<p>&iexcl;No lo pienses m&aacute;s! En <a href=\"http://www.cowo.com.co/\" target=\"_blank\">COWO</a> tenemos un programa de emprendimiento&nbsp;perfecto para ti! En nuestra primera fase del programa:&nbsp;<strong>Down 2 Earth</strong>, te asesoramos en el desarrollo e implementaci<span>&oacute;n de tu idea de negocio, trabajando en equipo, cada uno de los puntos necesarios para aterrizar, construir y evolucionar y tu idea a un modelo de negocio sostenible.</span></p>\r\n<p class=\"col-sm-12\" style=\"text-align: center;\"><span><strong>&iexcl;Agenda tu primera asesor&iacute;a de emprendimiento totalmente gratis!</strong></span></p>\r\n<p style=\"text-align: center;\"><span>Ll&aacute;manos a los tel&eacute;fonos: 3108061776 / 3174396049</span></p>\r\n<h2 class=\"col-sm-12\" style=\"text-align: center;\"><strong>&iquest;En qu&eacute; consiste DOWN 2 EARTH?</strong></h2>\r\n<p><strong>DOWN 2 EARTH: Programa de emprendimiento en donde vamos a asesorarte para definir:</strong></p>\r\n<ol>\r\n<li>La definici&oacute;n de tu producto o servicio.</li>\r\n<li>Validaci&oacute;n del diferenciador.</li>\r\n<li>Mercado Objetivo.</li>\r\n<li>Pricing.</li>\r\n<li>Competencia y d&oacute;nde est&aacute;s parado.</li>\r\n<li>&iquest;C&oacute;mo lo vas a vender?</li>\r\n<li>Capital humano necesario.</li>\r\n<li>Aporte personal.</li>\r\n<li>Tipo de negocio y financiaci&oacute;n.</li>\r\n</ol>\r\n<h2 style=\"text-align: center;\"><span><a style=\"color: #99cc00;\" title=\"Formulario emprendimiento\" href=\"https://docs.google.com/forms/d/e/1FAIpQLSdcWAQ4KwLA4D-bAXtYOSLdvW0G0mKD-Ialu9TBGXRpiiKjUg/viewform?usp=sf_link\" target=\"_blank\">&iexcl;Inscr&iacute;bete aqu&iacute;!</a></span></h2>\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12\">\r\n<h2 class=\"col-sm-12\" style=\"text-align: center;\"><iframe src=\"https://docs.google.com/forms/d/e/1FAIpQLSdcWAQ4KwLA4D-bAXtYOSLdvW0G0mKD-Ialu9TBGXRpiiKjUg/viewform?embedded=true\" frameborder=\"0\" width=\"100%\" height=\"500\">Cargando&hellip;</iframe></h2>\r\n</div>\r\n</div>\r\n</div>\r\n<div>&nbsp;</div>\r\n<h2 style=\"text-align: center;\"><a title=\"Inscr&iacute;bete\" href=\"https://docs.google.com/forms/d/e/1FAIpQLSdcWAQ4KwLA4D-bAXtYOSLdvW0G0mKD-Ialu9TBGXRpiiKjUg/viewform?usp=sf_link\" target=\"_blank\"><strong><img src=\"https://gallery.mailchimp.com/420590498bc242d2214f017fd/images/7e858fbd-f740-4c06-a4ed-c4488415a428.png\" alt=\"Programa de Emprendimiento\" width=\"80%\" height=\"80%\" /></strong></a></h2>\r\n<h2 style=\"text-align: center;\"><span><a style=\"color: #99cc00;\" title=\"Formulario emprendimiento\" href=\"https://docs.google.com/forms/d/e/1FAIpQLSdcWAQ4KwLA4D-bAXtYOSLdvW0G0mKD-Ialu9TBGXRpiiKjUg/viewform?usp=sf_link\" target=\"_blank\">&iexcl;Inscr&iacute;bete aqu&iacute;!</a></span></h2>\r\n</div>\r\n</div>\r\n</div>', 'N', 0, '2017-03-22 01:06:10', '2017-04-23 04:24:39'),
(30, 'Cursos', 'cursos', 'S', '<h1 style=\"text-align: center;\">Cursos y Seminarios</h1>\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<h2>Curso de&nbsp;Excel</h2>\r\n<div>\r\n<p>En Cowo creamos espacios para promover tu crecimiento profesional y acad&eacute;mico. En esta oportunidad, abrimos&nbsp;el Curso de Excel para que fortalezcas&nbsp;los conocimientos y habilidades de este programa y los lleves a la pr&aacute;ctica en tu trabajo.</p>\r\n<h3>Horario:</h3>\r\n<div id=\"nn_sliders_slider_1____546___-3\" class=\"nn_sliders_slider nn_sliders_count_3 noindent\">\r\n<div class=\"nn_sliders_slider nn_sliders_count_4 noindent\"><span>Duraci&oacute;n : 15 horas.</span></div>\r\n<div class=\"nn_sliders_slider nn_sliders_count_4 noindent\"><span>Lunes, martes y mi&eacute;rcoles.</span></div>\r\n<div class=\"nn_sliders_slider nn_sliders_count_4 noindent\"><span>Inicia el 8 de mayo&nbsp;y finaliza el 10 de Mayo de 2017.</span></div>\r\n</div>\r\n<h3>Valor y Forma de Pago:</h3>\r\n<div>Precio por persona: $60.000 COP.</div>\r\n<h2 style=\"text-align: center;\"><span><a style=\"color: #ff6600;\" title=\"Formulario curso excel\" href=\"https://goo.gl/forms/RPAKZiif1GsqSxlM2\" target=\"_blank\">&iexcl;Inscr&iacute;bete aqu&iacute;!</a>&nbsp;</span></h2>\r\n<p><iframe src=\"https://docs.google.com/forms/d/e/1FAIpQLSeTZ0iXI85CtBb_9zObNMjEhN2ewK0Dg-1P5PMKj4vnILn1gQ/viewform?embedded=true\" frameborder=\"0\" width=\"100%\" height=\"500\">Cargando...</iframe></p>\r\n<h2>&nbsp;Contenido del Curso:</h2>\r\n<table style=\"width: 1065px;\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 568px;\">\r\n<h3>Introducci&oacute;n:</h3>\r\n<div id=\"nn_sliders_slider_1____546___-3\" class=\"nn_sliders_slider nn_sliders_count_3 noindent\">\r\n<ul>\r\n<li>Concepto de hoja de c&aacute;lculo.</li>\r\n<li>Estructura b&aacute;sica de una hoja de c&aacute;lculo.</li>\r\n</ul>\r\n<h3>Coceptos b&aacute;sicos:</h3>\r\n<ul>\r\n<li>Libros y hojas.</li>\r\n<li>Celdas, filas, columnas y rangos.</li>\r\n</ul>\r\n<h3>Operaciones b&aacute;sicas con texto:</h3>\r\n<ul>\r\n<li>Mover el puntero.</li>\r\n<li>Seleccionar varias celdas.</li>\r\n<li>Introducir texto y n&uacute;meros en una sola celda.</li>\r\n<li>Formato de celda.</li>\r\n<li>Insertar hiperv&iacute;nculos.</li>\r\n<li>Cortar, copiar y pegar (Incluir formato).</li>\r\n</ul>\r\n<h3>Operaciones b&aacute;sicas con celdas y hojas:</h3>\r\n<ul>\r\n<li>Ajustar ancho de la columna y alto de las filas.</li>\r\n<li>Insertary eliminar filas y columnas.</li>\r\n<li>Insertar y eliminar celdas.</li>\r\n<li>Seleccionar rangos no adyacentes.</li>\r\n<li>Mover y copiar los contenidos de las celdas.</li>\r\n<li>Ordenar celdas.</li>\r\n<li>Combinar celdas.</li>\r\n<li>Vista preliminar e impresi&oacute;n.</li>\r\n<li>Configuraci&oacute;n de la p&aacute;gina.</li>\r\n<li>M&aacute;rgenes.</li>\r\n<li>Encabezados y pies de p&aacute;gina.</li>\r\n<li>Modificar las configuraciones del men&uacute;.</li>\r\n<li>Inmovilizar y movilizar paneles.</li>\r\n</ul>\r\n<h3>Autoformatos y estilos:</h3>\r\n<ul>\r\n<li>Crear estilo.</li>\r\n<li>Modificar y eliminar estilo.</li>\r\n</ul>\r\n</div>\r\n</td>\r\n<td style=\"width: 511px;\">\r\n<h3>Trabajar con m&uacute;ltiples hojas de c&aacute;lculo:</h3>\r\n<ul>\r\n<li>Seleccionar hojas de c&aacute;lculo.</li>\r\n<li>Manejar hojas de c&aacute;lculo agrupadas.</li>\r\n<li>Desagrupar hojas de c&aacute;lculo.</li>\r\n<li>Copiar y mover hojas de c&aacute;lculo.</li>\r\n<li>Insertar y eliminar hojas de c&aacute;lculo.</li>\r\n</ul>\r\n<h3>Referencias a celdas:</h3>\r\n<ul>\r\n<li>Nombrar rasgos.</li>\r\n<li>Utilizar cuadro de nombres.</li>\r\n<li>Utilizar cuadro de di&aacute;logo.</li>\r\n<li>Definir nombre.</li>\r\n<li>Modificar nombre de rangos.</li>\r\n</ul>\r\n<h3>Usar F&oacute;rmulas:</h3>\r\n<ul>\r\n<li>Crear f&oacute;rmulas.</li>\r\n<li>Barra de f&oacute;rmulas.</li>\r\n<li>Mostrar f&oacute;rmulas en una hoja de trabajo.</li>\r\n<li>Operadores de referencia.</li>\r\n<li>Mover y copiar f&oacute;rmulas y referencias.</li>\r\n<li>Eliminar f&oacute;rmulas.</li>\r\n</ul>\r\n<h3>Utilidades:</h3>\r\n<ul>\r\n<li>Revisi&oacute;n ortogr&aacute;fica.</li>\r\n<li>Proteger hojas de c&aacute;lculo.</li>\r\n<li>Conexi&oacute;n excel.</li>\r\n</ul>\r\n<h3>Trabajo con varias hojas y varios libros:</h3>\r\n<ul>\r\n<li>Visualizaci&oacute;n de tablas dentro de la hoja de 10.2.</li>\r\n<li>Trabajo con varias hojas.</li>\r\n</ul>\r\n<h3>Gr&aacute;ficos:</h3>\r\n<ul>\r\n<li>Creaci&oacute;n y modificaci&oacute;n de gr&aacute;ficos.</li>\r\n<li>Personalizaci&oacute;n de gr&aacute;ficos.</li>\r\n<li>Edici&oacute;n de datos en gr&aacute;ficos.</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3>&nbsp;</h3>\r\n<div>&nbsp;</div>\r\n<div>\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12\">\r\n<div style=\"text-align: center;\"><form id=\"frm_botonePayco\" action=\"https://secure.payco.co/checkout.php\" method=\"post\" name=\"frm_botonePayco\"></form></div>\r\n</div>\r\n</div>\r\n</div>\r\n<div>&nbsp;</div>\r\n</div>\r\n</div>\r\n<br />\r\n<div class=\"col-sm-12\">&nbsp;</div>\r\n</div>\r\n</div>\r\n<div>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n</div>\r\n</div>\r\n</div>', 'N', 0, '2017-04-01 05:55:42', '2017-04-26 03:16:00'),
(31, 'BLOG', 'blog', 'S', '<div class=\"container\">\r\n<div class=\"row\">&nbsp;</div>\r\n<div class=\"row\">\r\n<div class=\"col-sm-12\">\r\n<h1 class=\"entry-title cb-entry-title cb-title\" style=\"text-align: center;\">Emprender en Colombia&nbsp;</h1>\r\n<table style=\"width: 1102px;\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 26px;\">&nbsp;</td>\r\n<td style=\"width: 533px;\">\r\n<h2 class=\"entry-title cb-entry-title cb-title\">Las 7 claves para crear un negocio mientras eres empleado</h2>\r\n<p><img src=\"http://www.emprendiendohistorias.com/wp-content/uploads/2017/03/crear-un-negocio-mientras-eres-un-empleado-1200x600.jpg\" alt=\"Las 7 claves para crear un negocio mientras eres empleado\" width=\"482\" height=\"243\" /></p>\r\n<p>Fuente:&nbsp;<a title=\"Las 7 claves para crear un negocio mientras eres empleado\" href=\"http://www.emprendiendohistorias.com/crear-un-negocio-mientras-eres-empleado/\" target=\"_blank\">Emprendiendo Historias</a></p>\r\n<p>Mucha gente sue&ntilde;a con dejar su trabajo para comenzar con su propio negocio.Varios empresarios exitosos tuvieron que quedarse en sus&nbsp;empleos hasta que lleg&oacute; el punto en que, gracias a sus negocios, les era factible renunciar. No obstante, este tiempo de transici&oacute;n puede llegar a ser estresante y dif&iacute;cil, especialmente cuando no disfrutas tu trabajo,La buena noticia es no tiene que ser&nbsp;as&iacute;.Aunque ser ambicioso es importante, a veces no es suficiente para poder avanzar; mantenerte motivado cuando se pasa de ser empleado a empresario es crucial, es com&uacute;n dudar y sentirse desanimado durante esta fase.A continuaci&oacute;n, podr&aacute;s ver algunas acciones con las que puedes comenzar inmediatamente mientras est&aacute;s en tu jornada de trabajo para mantenerte encaminado y siempre dirigi&eacute;ndote hacia delante.</p>\r\n<h3 class=\"p1\">Aclara bien la raz&oacute;n por la cual vas a comenzar tu negocio</h3>\r\n<p>Conectarte con el &ldquo;porqu&eacute;&rdquo; es el primer paso que debes tomar.Tal vez te sientes lo suficientemente motivado para ser&nbsp;tu propio jefe, tener independencia financiera, libertad de tiempo, estar con tus hijos en casa o ayudar a los otros.Cualquiera que sea la raz&oacute;n, conocer lo que te motiva e impulsa te mantendr&aacute; enfocado y comprometido con tu sue&ntilde;o cuando todo se torna dif&iacute;cil.</p>\r\n<h3 class=\"p1\">Comprom&eacute;tete con tu sue&ntilde;o</h3>\r\n<p>Tener el anhelo para hacer algo y comprometerse con ello es completamente diferente. Cuando te comprometes con lo que sue&ntilde;as, creas la obligaci&oacute;n de verlo prosperar; esto significa que pones todo de ti mismo, encuentras soluciones cuando se te presentan obst&aacute;culos y no te rindes sin importar lo que pase.</p>\r\n<h3 class=\"p1\">Crea una visi&oacute;n personal de tu negocio</h3>\r\n<p>Piensa qu&eacute; quieres hacer para que tu negocio prospere desde hoy en un a&ntilde;o y escr&iacute;belo, s&eacute; espec&iacute;fico y detallado. Describe en presente tu d&iacute;a ideal, con qu&eacute; actividades te enganchas, quienes son tus clientes, cu&aacute;ntos tienes y tus ingresos mensuales.Lee tu misi&oacute;n en voz alta todas las ma&ntilde;anas y noches, concentr&aacute;ndote especialmente en el resultado (tu pr&oacute;spero y apasionante negocio). Al enfocarte en el resultado que deseas ser&aacute;s capaz de tomar decisiones d&iacute;a a d&iacute;a y as&iacute; ignorar cualquier pensamiento negativo que surja.</p>\r\n<h3 class=\"p1\">&Uacute;nete a un grupo \"cerebor maestro\"</h3>\r\n<p>Un &ldquo;cerebro maestro&rdquo; es un grupo de mentores que trabajan de igual-a-igual con regularidad y se apoyan el uno al otro, generando ideas creativas y en conjunto para solucionar los retos que se interpongan (sirve como ayuda para llevar cuenta de lo que haces y sentirte motivado).&Eacute;ste tipo de grupos pueden trabajar en persona, en l&iacute;nea o por tel&eacute;fono. Si no encuentras uno del cual puedas hacer parte, puedes crear uno propio con personas que tambi&eacute;n est&eacute;n empezando su negocio.</p>\r\n<h3 class=\"p1\">Sum&eacute;rgete en el aprendizaje</h3>\r\n<p>Investiga y estudia cualquier cosa que encuentres y que sea pertinente para tu negocio. Internet provee abundante y valiosa informaci&oacute;n sobre c&oacute;mo construir un negocio exitoso.Toma cursos, contrata un profesor o un mentor, escucha podcasts o audiolibros mientras haces tu rutina cada ma&ntilde;ana; entre m&aacute;s aprendas y te conviertas en un experto en tu &aacute;rea, estar&aacute;s m&aacute;s seguro y motivado y ser&aacute;s m&aacute;s exitoso.</p>\r\n<h3 class=\"p1\">Mantente positivo con tu trabajo actual</h3>\r\n<p>Todos los d&iacute;as reconoce algo que aprecias de tu trabajo, tal vez tus compa&ntilde;eros de trabajo o tener vacaciones, o simplemente el caf&eacute; y que tu silla es c&oacute;moda.El punto es encontrar el lado bueno de las cosas, entre m&aacute;s te concentres en qu&eacute; es lo positivo en tu situaci&oacute;n actual m&aacute;s &eacute;xito traer&aacute;s a todas las situaciones de tu vida, incluyendo tu negocio.</p>\r\n<blockquote>\r\n<p><em>Los pensamientos negativos agotan tu energ&iacute;a, haciendo mucho m&aacute;s dif&iacute;cil avanzar con lo que sue&ntilde;as.</em></p>\r\n</blockquote>\r\n<h3 class=\"p1\">Fija metas diarias</h3>\r\n<p>Toma acciones peque&ntilde;as cada d&iacute;a para la creaci&oacute;n de tu negocio, entendiendo que habr&aacute; momentos en los cuales no sientas que est&aacute;s progresando. La mayor&iacute;a del trabajo al principio ser&aacute;n tareas tediosas as&iacute; que tienes que tener en cuenta que eso hace parte de la creaci&oacute;n de tu negocio.Al dar peque&ntilde;os pasos cada d&iacute;a (lo que incluye hacer lo cotidiano) comenzar&aacute;s a ver los resultados, y entre m&aacute;s progreso hagas mayor va a ser la confianza y determinaci&oacute;n para continuar construyendo tu negocio.Si tomas en cuenta estas acciones mientras sigues en tu trabajo del d&iacute;a a d&iacute;a te mantendr&aacute;s concentrado, motivado y comprometido a convertir tu sue&ntilde;o en realidad, y te har&aacute; sentir que tu trabajo se va m&aacute;s r&aacute;pido. Hazlo.</p>\r\n<p>Fuente: <a title=\"Las 7 claves para crear un negocio mientras eres empleado\" href=\"http://www.emprendiendohistorias.com/crear-un-negocio-mientras-eres-empleado/\" target=\"_blank\">Emprendiendo Historias</a></p>\r\n</td>\r\n<td style=\"width: 22px;\">&nbsp;</td>\r\n<td style=\"width: 21px;\">&nbsp;</td>\r\n<td style=\"width: 492px;\">\r\n<h2 class=\"entry-title cb-entry-title cb-title\">Y Combinator lanza curso gratuito para crear tu empresa</h2>\r\n<p><img src=\"http://www.emprendiendohistorias.com/wp-content/uploads/2017/03/Y-Combinator-lanza-curso-gratuito-1200x600.jpg\" alt=\"Y Combinator lanza curso gratuito para crear tu empresa\" width=\"484\" height=\"242\" /></p>\r\n<p>Fuente:&nbsp;<a title=\"Y Combinator lanza curso gratuito para crear tu empresa\" href=\"http://www.emprendiendohistorias.com/y-combinator-curso-gratuito-crear-empresa/\" target=\"_blank\">Emprendiendo Historias</a></p>\r\n<div class=\"cb-breadcrumbs\">\r\n<p>Y Combinator es una de las aceleradoras de startups m&aacute;s importantes del mundo. Por su programa han pasado empresas como&nbsp;Dropbox, Airbnb, Stripe, Reddit, entre otras.</p>\r\n<p>Recientemente anunciaron&nbsp;el lanzamiento de su nueva escuela online <strong>&ldquo;Startup School&rdquo;</strong>, un curso abierto de 10 semanas gratuito donde aprender&aacute;s a crear tu negocio y encontrar&aacute;s apoyo de personas que ya han creado&nbsp;sus propias empresas.</p>\r\n<p>Las inscripciones ir&aacute;n hasta el 5 de abril, fecha en la que inicia el curso gratuito. Para inscribirte puedes hacerlo en <strong><a href=\"http://startupschool.org/\" target=\"_blank\">Startup School</a></strong>.&nbsp;</p>\r\n</div>\r\n<h3 class=\"p1\">&iquest;Para qui&eacute;n est&aacute; dirigido este curso?</h3>\r\n<p class=\"p1\">Este curso esta dise&ntilde;ado para aquellas personas que est&aacute;n comenzando su negocio. Es un curso pr&aacute;ctico que puedes ir aplicando en tu negocio a medida que avanzas en el contenido. De igual manera puedes acceder a los videos y contenido as&iacute; no est&eacute;s emprendiendo.</p>\r\n<blockquote>\r\n<p class=\"p1\"><span><em>El objetivo es replicar la experiencia de Y Combinator en m&aacute;s de 950 inversiones.</em></span></p>\r\n</blockquote>\r\n<h3 class=\"p1\">&iquest;Qu&eacute; incluye el curso?</h3>\r\n<p class=\"p1\">1. El curso tendr&aacute; conferencias hablando de diversos temas como:</p>\r\n<ul>\r\n<li class=\"p1\">C&oacute;mo obtener nuevas ideas</li>\r\n<li class=\"p1\">C&oacute;mo construir un gran producto</li>\r\n<li class=\"p1\">C&oacute;mo crecer tu negocio y crear una cultura</li>\r\n<li class=\"p1\">C&oacute;mo levantar dinero para tu negocio</li>\r\n</ul>\r\n<p class=\"p1\">Algunos de los invitados ser&aacute;n <strong>Alan Kay</strong>, <strong>Jan Koum</strong> de Whatsapp, <strong>Patrcik Collison</strong> de Stripe, <strong>Steve Huffman</strong> de Reddit, <strong>Adam D&rsquo;Angelo</strong> de Quora, <strong>Alex Schultz</strong> de Facebook y muchos otros m&aacute;s.</p>\r\n<p class=\"p1\">2. Para un grupo limitado de personas, semanalmente se har&aacute;n sesiones de mentor&iacute;a donde puedes recibir consejos espec&iacute;ficos para tu negocio.</p>\r\n<p class=\"p1\"><span>3. Se crear&aacute; una comunidad en l&iacute;nea v&iacute;a Slack y correo electr&oacute;nico donde te puedes conectar con otros emprendedores de tu clase.&nbsp;</span></p>\r\n<p class=\"p1\">4. Al final de la clase en l&iacute;nea, los participantes tendr&aacute;n la oportunidad de compartir lo que han construido con una amplia audiencia.</p>\r\n<p class=\"p2\">Si quieres aprender c&oacute;mo crear tu negocio de manera gratuita y en tu propio tiempo, basado en casos de &eacute;xito como Dropbox, Airbnb y Reddit puedes inscribirte en <strong><a href=\"http://www.startupschool.org/\" target=\"_blank\">este enlace</a></strong>.</p>\r\n<p class=\"p2\">Fuente: <a title=\"Y Combinator lanza curso gratuito para crear tu empresa\" href=\"http://www.emprendiendohistorias.com/y-combinator-curso-gratuito-crear-empresa/\" target=\"_blank\">Emprendiendo Historias</a></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n</div>\r\n<div class=\"col-sm-12\">&nbsp;</div>\r\n<div class=\"col-sm-12\">&nbsp;</div>\r\n</div>\r\n</div>\r\n<div>&nbsp;</div>', 'N', 0, '2017-04-03 22:18:06', '2017-04-03 22:58:53'),
(32, 'Gracias Inscripción', 'gracias-inscripcion', 'S', '<h1 style=\"text-align: center;\">&iexcl;Gracias por tu inscripci&oacute;n!</h1>\r\n<p>&nbsp;</p>\r\n<div class=\"container\">\r\n<div class=\"row\">En breve uno de nuestros coworkers se comunicar&aacute; contigo. Si tienes alguna inquietud, no dudes en escribirnos al correo: <a href=\"mailto:info@cowo.com.co\">info@cowo.com.co</a>&nbsp;o ll&aacute;manos a los n&uacute;meros&nbsp;<span>3108061776 - 3174396049</span></div>\r\n<div class=\"row\">&nbsp;</div>\r\n<div class=\"row\"><span>{!! $form_contacto !!}</span></div>\r\n</div>\r\n<div>&nbsp;</div>', 'N', 0, '2017-04-11 08:53:35', '2017-04-11 09:29:55'),
(33, 'sala de juntas', 'sala-de-juntas', 'S', '<h1 style=\"text-align: center;\">Salas de Juntas</h1>\r\n<div class=\"container\">\r\n<h2 style=\"text-align: center;\"><span><strong>&iexcl;DESCUENTO POR TIEMPO LIMITADO!</strong></span></h2>\r\n<p class=\"row\" style=\"text-align: center;\"><span></span><span>Alquila una Sala de Juntas por un d&iacute;a completo a tan solo <strong>$250.000 COP</strong>.&nbsp;</span></p>\r\n<div class=\"row\">&nbsp;</div>\r\n<h2 class=\"row\" style=\"text-align: center;\"><a href=\"http://www.cowo.com.co/contacto\" target=\"_blank\">&iexcl;Res&eacute;rvala aqu&iacute;!</a></h2>\r\n<p>&nbsp;</p>\r\n<div class=\"row\"><span></span><span>Alquila una de nuestras salas de juntas por horas, en uno de los sectores m&aacute;s exclusivos al norte de Bogot&aacute;, cerca al World Trade Center. Por solo $50.000 por hora disfrutar&aacute;s de un espacio adecuado con todo lo necesario para tus reuniones y conferencias con:</span></div>\r\n<div class=\"row\">&nbsp;</div>\r\n<ol>\r\n<li><span>Acceso por una hora a sala de juntas totalmente equipada.</span></li>\r\n<li><span>Internet de alta velocidad, televisor, equipo de teleconferencia.</span></li>\r\n<li><span>Caf&eacute;, te, arom&aacute;tica y agua.</span><span></span></li>\r\n</ol>\r\n<p>Comun&iacute;cate con nosotros a los tel&eacute;fonos:&nbsp;3108061776 - 3174396049 o escr&iacute;benos <a title=\"reservar\" href=\"http://cowo.com.co/contacto\" target=\"_blank\">aqu&iacute;</a> y nos comunicaremos contigo para asesorarte&nbsp;personalizadamente.&nbsp;</p>\r\n</div>\r\n<div>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div>\r\n<div class=\"container no-gutter\">\r\n<h2 class=\"row\"><span>Descubre nuestros espacios:</span></h2>\r\n<div class=\"row\">&nbsp;</div>\r\n<div class=\"row\"><span>{!! $grid_fotos|salajuntas !!}</span>&nbsp;<br />\r\n<div class=\"col-sm-12\">&nbsp;</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"fondo03\">\r\n<div class=\"container no-gutter\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12\">\r\n<h2>Reservalo aqu&iacute;:</h2>\r\n<div class=\"col-sm-12\">{!! $form_contacto !!}</div>\r\n<div class=\"col-sm-12\">&nbsp;</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', 'N', 0, '2017-04-23 01:52:05', '2017-07-04 22:18:46'),
(34, 'Home-Montaje', 'home-montaje', 'S', '<div class=\"container_full\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12 col-md-3\" style=\"height: 150px !important; background: url(\'https://cowo.com.co/media//cowo-cursos.jpg\') center right; text-align: center; line-height: 150px; color: white; font-size: 50px; font-weight: bold;\"><a style=\"color: white;\" href=\"https://cowo.com.co/cursos\">CURSOS</a></div>\r\n<div class=\"col-md-12 col-md-3\" style=\"height: 150px; background: url(\'https://cowo.com.co/media//cowo-membresia.jpg\') center center; text-align: center; line-height: 150px; color: white; font-size: 50px; font-weight: bold;\"><a style=\"color: white;\" href=\"https://cowo.com.co/planes\">MEMBRESIAS</a></div>\r\n<div class=\"col-md-12 col-md-3\" style=\"height: 150px; background: url(\'https://cowo.com.co/media//cowo-emprende.jpg\') center center; text-align: center; line-height: 150px; color: white; font-size: 50px; font-weight: bold;\"><a style=\"color: white;\" href=\"https://cowo.com.co/emprende\">EMPRENDE</a></div>\r\n<div class=\"col-md-12 col-md-3\" style=\"height: 150px; background: url(\'https://cowo.com.co/media//cowo-eventos.jpg\') center center; text-align: center; line-height: 150px; color: white; font-size: 50px; font-weight: bold;\"><a style=\"color: white;\" href=\"https://cowo.com.co/calendario\">EVENTOS</a></div>\r\n</div>\r\n</div>\r\n<div>&nbsp;</div>\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<h2 style=\"text-align: center;\">En Cowo tendr&aacute;s todos los beneficios que necesitas <br /><strong>para emprender y hacer crecer tu negocio:</strong></h2>\r\n<br /><br />\r\n<div class=\"col-xs-6 col-sm-3\" style=\"background-position: center;\"><img style=\"vertical-align: middle;\" src=\"https://cowo.com.co/media//cowo-1.png\" alt=\"\" /><br /><br />\r\n<p style=\"color: black; text-align: center;\">Espacio de 300 m&sup2;<br /> <strong>totalmente equipado para trabajar.</strong></p>\r\n</div>\r\n<div class=\"col-xs-6 col-sm-3\" style=\"background-position: center;\"><img style=\"vertical-align: middle;\" src=\"https://cowo.com.co/media//cowo-2.png\" alt=\"\" /><br /><br />\r\n<p style=\"color: black; text-align: center;\">Servicio de tel&eacute;fono <br /><strong>y recepci&oacute;n de correo.</strong></p>\r\n</div>\r\n<div class=\"col-xs-6 col-sm-3\" style=\"background-position: center;\"><img style=\"vertical-align: middle;\" src=\"https://cowo.com.co/media//cowo-3.png\" alt=\"\" /><br /><br />\r\n<p style=\"color: black; text-align: center;\">Eventos de networking <br /><strong>y acceso a conferencias.</strong></p>\r\n</div>\r\n<div class=\"col-xs-6 col-sm-3\" style=\"background-position: center;\"><img style=\"vertical-align: middle;\" src=\"https://cowo.com.co/media//cowo-4.png\" alt=\"\" /><br /><br />\r\n<p style=\"color: black; text-align: center;\">Amplia red de trabajadores: <strong>Participa en foros, ofrece tus servicios, comparte o pide ayuda.</strong></p>\r\n</div>\r\n<div class=\"col-xs-6 col-sm-3\" style=\"background-position: center;\"><img style=\"vertical-align: middle;\" src=\"https://cowo.com.co/media//cowo-5.png\" alt=\"\" /><br /><br />\r\n<p style=\"color: black; text-align: center;\">Zonas de esparcimiento, <br /><strong>TV y juegos.</strong></p>\r\n</div>\r\n<div class=\"col-xs-6 col-sm-3\" style=\"background-position: center;\"><img style=\"vertical-align: middle;\" src=\"https://cowo.com.co/media//cowo-6.png\" alt=\"\" /><br /><br />\r\n<p style=\"color: black; text-align: center;\">Cafeter&iacute;a <br /><strong>y papeler&iacute;a.</strong></p>\r\n</div>\r\n<div class=\"col-xs-6 col-sm-3\" style=\"background-position: center;\"><img style=\"vertical-align: middle;\" src=\"https://cowo.com.co/media//cowo-7.png\" alt=\"\" /><br /><br />\r\n<p style=\"color: black; text-align: center;\">Salas de Juntas equipadas, <br /><strong>internet de alta velocidad.</strong></p>\r\n</div>\r\n<div class=\"col-xs-6 col-sm-3\" style=\"background-position: center;\"><img style=\"vertical-align: middle;\" src=\"https://cowo.com.co/media//cowo-8.png\" alt=\"\" /><br /><br />\r\n<p style=\"color: black; text-align: center;\">aliados para mensajer&iacute;a, eventos, <strong>descuentos y m&aacute;s</strong>.</p>\r\n</div>\r\n</div>\r\n</div>\r\n<div>&nbsp;</div>\r\n<p>&nbsp;Galer&iacute;a 1</p>\r\n<p><span>{!! $grid_fotos !!}<br /></span></p>\r\n<p><span>Galer&iacute;a 2</span></p>\r\n<p><span>{!! $grid_fotos|segunda !!}</span></p>\r\n<div class=\"fondo03\">\r\n<div class=\"container no-gutter\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12\">\r\n<h2><br />Comun&iacute;cate con nosotros:</h2>\r\n<div class=\"col-sm-12\">{!! $form_contacto !!}</div>\r\n<div class=\"col-sm-12\">&nbsp;</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<p>&nbsp;</p>\r\n<div class=\"container no-gutter\">\r\n<div class=\"row\">\r\n<p>{!! $bloque_links !!}</p>\r\n</div>\r\n</div>', 'N', 0, '2017-05-19 02:27:13', '2017-06-01 00:20:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_con_faq`
--

CREATE TABLE `sis_con_faq` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `descripcion` text,
  `activo` enum('S','N') DEFAULT 'S',
  `peso` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sis_con_faq`
--

INSERT INTO `sis_con_faq` (`id`, `titulo`, `descripcion`, `activo`, `peso`, `created_at`, `updated_at`) VALUES
(1, '¿Cuánto se demora en llegar mi compra?', 'El proceso de compra y envío dura 10-15 días hábiles sujetos a los tiempos de la tienda.', 'S', 0, '2016-08-17 23:59:09', '2016-09-13 00:18:27'),
(2, '¿Cómo se paga?', 'Puedes hacer el pago directamente desde la cotización o tu carrito de compras con tarjeta de crédito o por PSE.  También puedes pagar en Bancolombia a la cuenta de ahorros número 304.576.943.57 a nombre de Encarguelo.com SAS con NIT 900.942.174.  O puedes pagar en puntos Efecty en todo el país.\r\n', 'S', 1, '2016-08-17 23:59:41', '2016-11-12 02:41:41'),
(3, '¿El precio de la cotización es el precio final?', 'Sí.  Ese es el precio final que tendrá que pagar con envío a Bogotá.  A otras ciudades se hace envío contraentrega desde Bogotá', 'S', 2, '2016-08-17 23:59:52', '2016-08-22 18:17:31'),
(4, '¿Cuánto es la comisión que cobra Encarguelo.com?', 'Encarguelo.com cobra entre un 10-15% del costo del producto más envío, impuestos y seguros, dependiendo del monto cotizado', 'S', 3, '2016-08-18 00:00:12', '2016-08-22 18:18:19'),
(5, '¿Cómo me aseguro que la compra fue hecha?', 'Te enviaremos la factura de tu compra apenas hagas el pago de mínimo el 50%.', 'S', 5, '2016-08-22 18:18:55', '2016-09-13 00:19:23'),
(6, '¿Qué garantías tengo?', 'Encarguelo.com te garantiza que tu encargo llegará a satisfacción o te devolveremos el dinero.   Además, cuentas con el respaldo de garantía de pagos PSE y de Paypal en caso de hacer tu pago por esos medios.', 'S', 6, '2016-08-22 18:20:52', '2016-09-13 00:19:54'),
(7, '¿Qué debo hacer cuando haga el pago?', 'Una vez realizado el pago, envía el comprobante a info@encarguelo.com y procederemos con la compra y el envío de la factura', 'S', 7, '2016-09-13 00:20:46', '2016-09-13 00:20:56'),
(8, '¿Cómo se hacen los envíos nacionales?', 'Todos los envíos nacionales se hacen por Coordinadora o Interrapidisimo dependiendo de la población.  El envío dentro de Bogotá está incluido en el precio.  A otras ciudades se realiza contraentrega desde Bogotá.', 'S', 8, '2016-09-13 00:22:01', '2016-09-13 00:22:01'),
(9, '¿Que productos no se pueden importar?', 'No podemos importar pistolas, rifles, escopetas o cualquier arma de fuego, así sea de juguete.', 'S', 0, '2016-10-09 19:38:17', '2016-10-09 19:38:17'),
(10, '¿Puedo recoger mi encargo en Bogotá?', 'Sí puedes recoger tu encargo en la calle 65 #10-43 los lunes, miércoles y viernes entre 8 AM y 4 PM, y pagar tu saldo en efectivo.', 'S', 0, '2016-10-13 18:55:03', '2016-10-13 18:55:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_con_foto`
--

CREATE TABLE `sis_con_foto` (
  `id` int(11) NOT NULL,
  `id_galeria` int(11) DEFAULT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `meta` varchar(500) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sis_con_foto`
--

INSERT INTO `sis_con_foto` (`id`, `id_galeria`, `titulo`, `descripcion`, `meta`, `created_at`, `updated_at`) VALUES
(1, 1, 'Instalaciones Cowo', 'Espacio coworking', 'Cowo, coworking, coworking bogota, oficinas bogota, oficinas compartidas, cowo bogota, Coworking space, oficinas en Bogota, networking, oficinas virtuales,  workingspace, oficinas temporales, oficinas amobladas, oficinas compartidas, espacios de trabajo Bogotá, emprendedores, startup', '2017-01-24 00:00:20', '2017-03-15 03:28:08'),
(2, 1, 'Cowo 2', 'Espacio coworking', 'coworking, coworking bogota, oficinas bogota', '2017-01-24 00:01:41', '2017-02-01 01:20:03'),
(3, 1, 'Cowo 4', 'Coworking space', 'Coworking, coworking bogota, espacios de trabajo, oficinas bogota, world trade center', '2017-01-24 00:02:06', '2017-02-01 01:30:12'),
(4, 1, 'Cowo 3', 'coworking', 'coworking, espacios de trabajo, oficinas compartidas', '2017-01-24 00:02:31', '2017-02-01 01:32:18'),
(5, 1, 'sala de juntas', '', 'coworking, oficina coworking, oficinas compartidas', '2017-01-24 00:02:54', '2017-04-03 21:42:49'),
(6, 1, 'Cowo', 'Coworking', 'coworking bogota, coworking colombia, oficinas world trade center', '2017-01-24 00:03:23', '2017-02-01 01:44:17'),
(7, 1, 'Cowo 9', 'cowo', 'cowo, coworking, coworking bogota, oficinas por horas', '2017-01-24 00:03:48', '2017-02-01 01:45:10'),
(8, 1, 'Café', 'Coworking', 'coworking, coworking bogota, oficinas bogota', '2017-01-24 00:04:11', '2017-05-27 02:06:47'),
(9, 2, 'Imagen 1', '', '', '2017-06-01 00:19:23', '2017-06-01 00:19:23'),
(10, 2, 'Imagen 2', '', '', '2017-06-01 00:19:34', '2017-06-01 00:19:34'),
(11, 2, 'Imagen 3', '', '', '2017-06-01 00:19:49', '2017-06-01 00:19:49'),
(12, 3, 'Sala de Juntas', 'Alquila una de nuestras salas de juntas por horas, en uno de los sectores más exclusivos al norte de Bogotá, cerca al World Trade Center. Por solo $50.000 por hora disfrutarás de un espacio adecuado con todo lo necesario para tus reuniones y conferencias con:\r\n \r\nAcceso por una hora a sala de juntas totalmente equipada.\r\nInternet de alta velocidad, televisor, equipo de teleconferencia.\r\nCafé, te, aromática y agua.\r\nComunícate con nosotros a los teléfonos: 3108061776 - 3174396049 o escríbenos aqu', 'sala de juntas, sala de reuniones, sala de conferencias, salas de reuniones, salasjuntas, reuniones,', '2017-06-06 20:49:57', '2017-06-06 20:49:57'),
(13, 3, 'Sala de juntas 1', 'Alquila una de nuestras salas de juntas por horas, en uno de los sectores más exclusivos al norte de Bogotá, cerca al World Trade Center. Por solo $50.000 por hora disfrutarás de un espacio adecuado con todo lo necesario para tus reuniones y conferencias con:\r\n \r\nAcceso por una hora a sala de juntas totalmente equipada.\r\nInternet de alta velocidad, televisor, equipo de teleconferencia.\r\nCafé, te, aromática y agua.\r\nComunícate con nosotros a los teléfonos: 3108061776 - 3174396049 o escríbenos aqu', 'sala de juntas, salas de juntas, salas reuniones, sala capacitaciones, salon reuniones', '2017-06-06 20:51:27', '2017-06-06 20:51:27'),
(14, 3, 'Sala de Juntas Cowo', 'Alquila una de nuestras salas de juntas por horas, en uno de los sectores más exclusivos al norte de Bogotá, cerca al World Trade Center. Por solo $50.000 por hora disfrutarás de un espacio adecuado con todo lo necesario para tus reuniones y conferencias con:\r\n \r\nAcceso por una hora a sala de juntas totalmente equipada.\r\nInternet de alta velocidad, televisor, equipo de teleconferencia.\r\nCafé, te, aromática y agua.\r\nComunícate con nosotros a los teléfonos: 3108061776 - 3174396049 o escríbenos aqu', 'sala de juntas, sala de reuniones, sala conferencias, salon reuniones, salas de reuniones, salas de juntas bogota', '2017-06-06 20:52:43', '2017-06-06 20:52:43'),
(17, 3, 'Sala de Juntas Cowo 2', 'Alquila una de nuestras salas de juntas por horas, en uno de los sectores más exclusivos al norte de Bogotá, cerca al World Trade Center. Por solo $50.000 por hora disfrutarás de un espacio adecuado con todo lo necesario para tus reuniones y conferencias con:\r\n \r\nAcceso por una hora a sala de juntas totalmente equipada.\r\nInternet de alta velocidad, televisor, equipo de teleconferencia.\r\nCafé, te, aromática y agua.\r\nComunícate con nosotros a los teléfonos: 3108061776 - 3174396049 o escríbenos aqu', 'sala de juntas, sala de reuniones, salas de juntas. sala de juntas bogota, sala de conferencias', '2017-06-06 20:58:13', '2017-06-06 20:58:13'),
(18, 4, 'Evento Uniandinos', 'Evento Coworking Experience en Uniandinos', 'eventos coworking, coworking, eventos, cowo', '2017-06-06 21:08:00', '2017-06-06 21:08:00'),
(19, 4, 'Eventos COWO', '', '', '2017-06-06 21:11:04', '2017-06-06 21:11:04'),
(21, 4, 'Evento WIX en COWO', '', '', '2017-06-06 21:11:53', '2017-06-06 21:11:53'),
(22, 4, 'Evento Facebook Developers en COWO', '', '', '2017-06-06 21:12:32', '2017-06-06 21:12:32'),
(23, 4, 'Evento FUNDACIÓN LAS PULGAS en COWO', '', '', '2017-06-06 21:13:23', '2017-06-06 21:13:23'),
(24, 4, 'Evento WIX en COWO 2', '', '', '2017-06-06 21:14:11', '2017-06-06 21:14:11'),
(25, 4, 'Evento Alianza Cabify y COWO', '', '', '2017-06-06 21:19:07', '2017-06-06 21:19:07'),
(26, 4, 'Evento La Pola Social en COWO', '', '', '2017-06-06 21:19:36', '2017-06-06 21:19:36'),
(27, 4, 'Evento Tarde de Tacos en COWO', '', '', '2017-06-06 21:20:00', '2017-06-06 21:20:00'),
(28, 4, 'Evento COWO en el GStartUp 2 del Gimnasio Moderno', '', '', '2017-06-06 21:29:31', '2017-06-06 21:29:31'),
(29, 4, 'Evento Lanzamiento de Player One en Bogotá', '', '', '2017-06-06 21:30:10', '2017-06-06 21:30:10'),
(30, 4, 'Lanzamiento WEBCONGRESS en COWO', '', '', '2017-06-06 21:30:43', '2017-06-06 21:30:43'),
(32, 4, 'Tarde de Play Station en COWO', '', '', '2017-06-06 21:31:52', '2017-06-06 21:31:52'),
(33, 4, 'Lanzamiento COWO', '', '', '2017-06-06 21:32:19', '2017-06-06 21:32:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_con_galeria`
--

CREATE TABLE `sis_con_galeria` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `llave` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sis_con_galeria`
--

INSERT INTO `sis_con_galeria` (`id`, `titulo`, `llave`, `created_at`, `updated_at`) VALUES
(1, 'Galeria principal', 'principal', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Galería 2', 'segunda', '2017-06-01 00:16:17', '2017-06-01 00:16:17'),
(3, 'Galería Sala de Juntas', 'salajuntas', '2017-06-06 20:43:46', '2017-06-09 21:11:11'),
(4, 'Galería Eventos', 'galeriaeventos', '2017-06-06 21:01:21', '2017-06-12 22:36:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_con_menu`
--

CREATE TABLE `sis_con_menu` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `llave` varchar(100) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `peso` int(11) DEFAULT '0',
  `descripcion` varchar(500) NOT NULL,
  `meta` varchar(500) NOT NULL,
  `id_seccion` int(11) DEFAULT NULL,
  `mostrar` enum('S','N') NOT NULL DEFAULT 'S',
  `usar_en_bloque` enum('S','N') NOT NULL DEFAULT 'N',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sis_con_menu`
--

INSERT INTO `sis_con_menu` (`id`, `titulo`, `llave`, `url`, `peso`, `descripcion`, `meta`, `id_seccion`, `mostrar`, `usar_en_bloque`, `created_at`, `updated_at`) VALUES
(1, 'Espacios y Planes', 'planes', '', 1, 'Estamos ofreciendo todos nuestros planes físicos incluyen café, té y agua ilimitada en diferentes categorías. Usted puede tener un usuario para descubrir sobre las necesidades de la comunidad, expresar la suya.', 'Coworking, Oficinas en Bogota, Oficinas por horas, Alquiler salas de juntas, Salas de reuniones, Oficinas compartidas, Coworking Bogota.', 13, 'S', 'N', '2016-08-12 01:50:20', '2017-05-19 05:07:09'),
(2, 'Quiénes Somos', 'quienes-somos', '', 3, 'Quienes componen nuestra empresa', 'COWO, nuestra empresa, coworking bogota, coworking space, espacios de coworking, comunidad colaborativa, emprendimiento, espacios en coworking, espacios de trabajo colaborativo,  co working, trabajo colaborativo, coworkers, emprendedores, independientes, pymes, emprendimiento, freelancer, empresarios, colaboradores, ', 14, 'N', 'N', '2016-08-12 01:51:37', '2017-04-17 22:32:18'),
(3, 'Alianzas', 'alianzas', '', 5, 'Alianzas estratégicas para beneficio del coworking', 'alianzas, COWO, alianzas estrategicas, emprendimientos,  beneficios coworking,  alianzas coworking, ', 16, 'S', 'N', '2016-08-12 01:51:46', '2017-04-10 20:46:13'),
(4, 'Foro', 'foro', '/forum', 4, 'Foro para emprendedores, empresarios, coworkers, independientes y freelancer', 'foro, discucion, debate, charla emprendimiento, comentarios emprendimiento, comentarios coworking, debate emprendimiento, debate coworking, foro coworking, foro emprender, foro pymes, foro empresa, ', 16, 'N', 'N', '2016-08-17 23:28:13', '2017-04-11 03:01:20'),
(6, 'Contacto', 'contacto', '', 7, 'Formulario de contacto con COWO', 'contacto cowo, formulario de contacto cowo, datos de contacto cowo, comunicación cowo, ', 15, 'S', 'N', '2016-08-31 19:57:43', '2017-04-11 03:05:43'),
(7, 'Términos y condiciones', 'terminos-y-condiciones', '', 0, 'Términos y condiciones de uso - Cowo', 'terminos,condiciones', 23, 'N', 'S', '2017-01-27 03:41:09', '2017-02-12 07:42:17'),
(8, 'Trabaja con nosotros', 'trabaja-con-nosotros', '', 0, 'Trabajar en Cowo', 'trabajo en coworking, trabajar en coworking, trabajar con emprendedores, trabajar en espacio de coworking', 25, 'N', 'S', '2017-01-28 03:04:08', '2017-04-11 04:41:13'),
(9, 'Políticas de privacidad', 'politicas-de-privacidad', '', 0, '', '', 26, 'N', 'S', '2017-01-30 07:37:08', '2017-02-12 07:32:13'),
(10, 'Reglamento', 'reglamento', '', 15, 'Reglamento, manual de convivencia coworking', 'coworking, manual de convivencia, reglamento, convivencia en coworking, manuales de comportamiento en coworking, comcortavientos en coworking, Mañuela etiqueta coworking', 27, 'N', 'S', '2017-02-17 00:02:10', '2017-04-12 00:21:29'),
(11, 'Eventos', 'calendario', '', 6, 'Realizamos eventos y conferencias con especialistas en diferentes temas, con el fin de producir una experiencia enriquecedora con temas de atención a los miembros de nuestra comunidad.', 'Coworking, Oficinas en Bogota, Oficinas por horas, Alquiler salas de juntas, Salas de reuniones, Oficinas compartidas, Coworking Bogota.', 28, 'S', 'N', '2017-02-21 03:28:15', '2017-05-17 19:41:44'),
(12, 'Emprende', 'emprende', '/emprende', 0, 'Desarrollar la capacidad de su organización para crear valor económico y social con programas diseñados y entregados específicamente para desarrollar programas de emprendimiento', 'Coworking, Oficinas en Bogota, Oficinas por horas, Alquiler salas de juntas, Salas de reuniones, Oficinas compartidas, Coworking Bogota.', 29, 'N', 'N', '2017-03-22 01:33:58', '2017-06-06 20:29:47'),
(13, 'Cursos', 'cursos', '/cursos', 0, 'Estamos compartiendo muchos tipos de cursos de Excel Program aquí. Consulte nuestros Cursos de Excel. Cowok creamos espacios para alentar su expansión profesional y académica.', 'Coworking, Oficinas en Bogota, Oficinas por horas, Alquiler salas de juntas, Salas de reuniones, Oficinas compartidas, Coworking Bogota.', 30, 'N', 'N', '2017-04-01 06:03:17', '2017-06-06 20:29:59'),
(14, 'Emprender en Colombia', 'blog', 'blog', 0, 'aprende sobre emprendimiento', 'blog emprendimiento, aprender sobre emprendimiento, noticias emprendimiento, emprendimientos colombia, casos de exito emprendimientos, ', 31, 'N', 'S', '2017-04-03 22:22:34', '2017-04-12 19:55:35'),
(15, 'Gracias Inscripción', 'inscripcion', 'www.cowo.com/inscripcion', 0, 'Inscripción Exitosa', 'inscripción curso, inscripción en capacitacion, inscripción exitosa, ', 32, 'N', 'N', '2017-04-11 08:55:15', '2017-04-12 00:22:36'),
(16, 'sala de juntas', 'saladejuntas', '/saladejuntas', 0, 'Sala de Juntas en Bogotá por horas', 'sala de juntas, alquiler sala de juntas, alquiler sala de reuniones, arriendo sala de juntas, sala de conferencias, ', 33, 'N', 'N', '2017-04-23 01:53:13', '2017-04-23 01:53:13'),
(17, 'home-montaje', 'home-montaje', 'home-montaje', 0, '', '', 34, 'N', 'N', '2017-05-19 02:28:20', '2017-05-19 02:28:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_con_testimonio`
--

CREATE TABLE `sis_con_testimonio` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `descripcion` text,
  `activo` enum('S','N') DEFAULT 'S',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sis_con_testimonio`
--

INSERT INTO `sis_con_testimonio` (`id`, `titulo`, `descripcion`, `activo`, `created_at`, `updated_at`) VALUES
(1, 'Guillermo Eduardo Díaz', 'Trajeron mi encargo según lo pactado, serios y responsables con los tiempos, los recomiendo si necesitan traer algo del exterior que este publicado en eBay o Amazon.', 'S', '2016-09-28 18:23:57', '2016-09-28 18:23:57'),
(2, 'Laura Gutierrez', 'CON ESTE MENSAJE QUIERO DARLE LAS GRACIAS YA QUE ES LA PRIMERA VEZ QUE PIDO EN ENCARGELO Y ME HA PARECIDO MUY BUEN SERVICIO PENSE QUE IBA ESPERAR MAS EL PEDIDO COMO OTRAS TIENDAS ME HAN HECHO ESPERAR BASTANTE, ESTOY SATISFECHA CON ESTA PAGINA Y LA SEGUIRE UTILIZANDO PORQUE ME ENCANTA COMPRAR EN INTERNET', 'S', '2016-10-26 01:21:29', '2016-10-26 01:21:29'),
(3, 'Nathaly Cortez', 'Recibimos el encargo tal como lo solicitamos.  Muchas gracias por el buen servicio.', 'S', '2016-11-01 21:22:59', '2016-11-01 21:22:59'),
(4, 'Nobaira Londoño', 'LES RECOMIENDO EL SEVICIO DE .ENCARGUELO .COM.ES EXCELENTE.EN TODO EL SENTIDO DE LA PALABRA.QUEDE MAS QUE SATISFECHO . CON LA COMPRA QUE HICE ATRAVEZ DE .ENCARGUELO.COM', 'S', '2016-11-02 18:12:13', '2016-11-02 18:12:13'),
(5, 'Jonathan Garzón', 'Gracias totales por hacer lo complicado tan simple.', 'S', '2016-11-11 08:22:27', '2016-11-11 08:22:27'),
(6, 'Felix Moreno', 'Los mejores 100% recomendados', 'S', '2016-11-11 08:23:24', '2016-11-11 08:23:24'),
(7, 'William Córdoba', 'Estoy satisfecho con el pedido llegó a tiempo sin complicaciones recomiendo este servicio. Es la primera vez que compro en internet y estoy satisfecho ,muchas gracias encarguelo.com', 'S', '2016-11-18 02:48:37', '2016-11-18 02:48:37'),
(8, 'Carlos Rivero', 'Ante todo Gracias por su excelencia y seriedad en el trabajo, la mercancía llegó perfectamente, espero seguir con el servicio que prestan ustedes por mucho tiempo.', 'N', '2016-12-01 03:30:10', '2016-12-01 03:30:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_forum_categories`
--

CREATE TABLE `sis_forum_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` int(11) NOT NULL DEFAULT '0',
  `enable_threads` tinyint(1) NOT NULL DEFAULT '0',
  `thread_count` int(11) NOT NULL DEFAULT '0',
  `post_count` int(11) NOT NULL DEFAULT '0',
  `private` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sis_forum_categories`
--

INSERT INTO `sis_forum_categories` (`id`, `category_id`, `title`, `description`, `weight`, `enable_threads`, `thread_count`, `post_count`, `private`, `created_at`, `updated_at`) VALUES
(1, 0, 'Categoria 01', 'Probando', 0, 1, 1, 2, 1, NULL, NULL),
(2, 1, 'Contabilidad', 'preguntas de contabilidad', 0, 1, 1, 2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_forum_posts`
--

CREATE TABLE `sis_forum_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `thread_id` int(10) UNSIGNED NOT NULL,
  `author_id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `post_id` int(10) UNSIGNED DEFAULT NULL,
  `sequence` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sis_forum_posts`
--

INSERT INTO `sis_forum_posts` (`id`, `thread_id`, `author_id`, `content`, `post_id`, `sequence`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 660, 'Probando hilos', NULL, 1, '2016-12-02 22:46:45', '2016-12-02 22:46:45', NULL),
(2, 2, 1, '¿cuanto es el iva?', NULL, 1, '2017-01-30 20:43:44', '2017-01-30 20:43:44', NULL),
(3, 2, 9, 'Yo tambien quiero saberlo', 0, 2, '2017-02-16 09:02:33', '2017-02-16 09:02:33', NULL),
(4, 1, 6, 'Probando', 0, 2, '2017-02-24 03:39:25', '2017-02-24 03:39:25', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_forum_threads`
--

CREATE TABLE `sis_forum_threads` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `author_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pinned` tinyint(1) DEFAULT '0',
  `locked` tinyint(1) DEFAULT '0',
  `reply_count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sis_forum_threads`
--

INSERT INTO `sis_forum_threads` (`id`, `category_id`, `author_id`, `title`, `pinned`, `locked`, `reply_count`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 660, 'Hilo 01', 0, 0, 1, '2016-12-02 22:46:44', '2017-02-24 03:39:26', NULL),
(2, 2, 1, 'Reforma tributaria', 0, 0, 1, '2017-01-30 20:43:44', '2017-02-16 09:02:33', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_forum_threads_read`
--

CREATE TABLE `sis_forum_threads_read` (
  `thread_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sis_forum_threads_read`
--

INSERT INTO `sis_forum_threads_read` (`thread_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 660, '2016-12-02 22:46:45', '2016-12-02 22:46:45'),
(1, 1, '2016-12-02 23:03:06', '2016-12-02 23:03:06'),
(2, 1, '2017-01-30 20:43:44', '2017-01-30 20:43:44'),
(2, 9, '2017-02-16 09:02:33', '2017-02-16 09:02:33'),
(1, 6, '2017-02-24 03:39:26', '2017-02-24 03:39:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_par_cliente`
--

CREATE TABLE `sis_par_cliente` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `apellido` varchar(200) DEFAULT NULL,
  `tipo_documento` enum('CC','NIT','CE') NOT NULL,
  `documento_identidad` varchar(50) NOT NULL,
  `telefono` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `empresa` varchar(100) NOT NULL,
  `direccion` varchar(500) NOT NULL,
  `ciudad` varchar(100) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `creditos` int(11) DEFAULT '0',
  `publico` enum('S','N') NOT NULL DEFAULT 'S',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sis_par_cliente`
--

INSERT INTO `sis_par_cliente` (`id`, `nombre`, `apellido`, `tipo_documento`, `documento_identidad`, `telefono`, `email`, `empresa`, `direccion`, `ciudad`, `id_usuario`, `creditos`, `publico`, `created_at`, `updated_at`) VALUES
(1, 'Alejandro', 'Muller', 'CC', '80665236', NULL, 'info@encarguelo.com', 'Encarguelo.com', 'calle 128 #7A 24', 'Bogota', 1, 798, 'N', '2017-02-08 03:10:29', '2017-05-11 20:09:19'),
(2, 'Eileen', 'Silva', 'CC', '1', NULL, 'eileens@hollyhock.com', 'Hollyhock', 'Calle 103a #11b 18', 'Bogota', 5, 12, 'N', '2017-02-23 23:00:02', '2017-03-18 00:58:06'),
(3, 'Andres Felipe', 'Velez Restrepo', 'CC', '94479237', '300345677', 'anfevelez83@gmail.com', 'Cowo', 'Calle 4A # 3/32', '', 6, 20, 'S', '2017-02-24 03:27:41', '2017-02-24 03:43:41'),
(4, 'Giovana', 'Martínez', 'CC', '52052626', NULL, 'gmartinez@americanentrepreneurgroup.com', 'American Entrepreneur Group', 'calle 127d 71-66', 'Bogota', 7, 0, 'N', '2017-02-24 21:33:23', '2017-03-22 00:20:31'),
(5, 'Andrés ', 'Tobón', 'CC', '91495374', NULL, 'planner@ene.com', 'Ene', 'Calle 65#10-43', 'Bogotá', 8, 10, 'N', '2017-02-25 00:33:56', '2017-03-15 21:17:23'),
(7, 'Fabian', 'Morales', 'CC', '94064368', NULL, 'fabian.morales@outlook.com', 'Home', 'Calle con carrera', 'Cali', 4, 0, '', '2017-03-01 00:26:38', '2017-03-01 00:26:38'),
(8, 'Juan Manuel', 'Quintero', 'CC', '900409363-0', NULL, 'jquintero@infovalmer.com.co', 'Infovalmer', 'Carrera 7 # 71 - 21 torre B Of 403', 'Bogotá', 9, 253, 'N', '2017-03-01 19:05:33', '2017-04-06 22:21:16'),
(9, 'Andres', 'Jaramillo', 'CC', '80872420', NULL, 'jgpr_coordinacion@mail.com', 'JGPR', 'Calle 234 # 70-50', 'Bogotá', 10, 11, 'N', '2017-03-03 01:45:48', '2017-04-01 00:26:30'),
(10, 'Katiana', 'Jolimeau', 'CE', 'B69693218 US', '7036721528', 'katiana@tripstravelagency.com', 'Trips Travel Agency', 'Carrera 7 # 64 - 37', 'Bogotá', 11, 5, 'N', '2017-03-05 04:16:36', '2017-03-07 18:05:09'),
(11, 'Camilo', 'Naranjo', 'CC', '80195244', NULL, 'camilo.naranjo@gmail.com', 'Puertas en vidrio', 'Carrera 16#109-55', 'Bogotá', 12, 45, '', '2017-03-06 22:20:06', '2017-03-22 01:59:53'),
(12, 'Pablo Andrés ', 'Arguello', 'CC', '80772596', NULL, 'paacsistem@gmail.com', 'Paac Sistem', 'Calle 160 # 58 - 50', 'Bogotá', 13, 80, 'N', '2017-03-07 18:07:27', '2017-05-25 19:32:02'),
(13, 'Catalina ', 'Acosta', 'CC', '52864544', NULL, 'catalina.acosta@gmail.com', '..', 'Cra 6 # 91-35', 'Bogota', 17, 17, 'N', '2017-03-08 02:43:23', '2017-03-09 20:50:02'),
(14, 'Camila', 'Mariño', 'CC', '1020726631', NULL, 'cmarino@hof.com.co', 'Hof', 'Diagonal 74# 6 - 85', 'Bogotá', 18, 9, 'N', '2017-03-08 19:57:20', '2017-03-14 22:08:17'),
(15, 'Juan ', 'Dorado', 'CC', '80873644', NULL, 'juan.dorado@nuestromall.com', 'Nuestro Mall', 'Calle 128b #19 55 ', 'Bogota', 19, 0, '', '2017-03-08 20:10:21', '2017-03-08 20:10:21'),
(16, 'Jose', 'Payares', 'CC', '0000', NULL, 'arq.josepayares@gmail.com', 'Arquitectura e ingeniera', 'xxx', 'Bogota', 20, 0, '', '2017-03-08 20:16:49', '2017-03-08 20:16:49'),
(17, 'Juan carlos', 'Gallon', 'CC', '0000', NULL, 'jcgallon@hseqcamero.com', 'HSEQ CAMERO', 'xxx', 'Bogotá', 21, 18, 'N', '2017-03-08 20:19:27', '2017-03-08 20:20:53'),
(18, 'Andrés', 'Tobon', 'CC', '0000', NULL, 'planner@ene.com.co', 'ENE', 'xxx', 'Bogotá', 22, 0, 'N', '2017-03-08 20:25:11', '2017-03-14 22:07:48'),
(19, 'Jonathan', 'Voris', 'CC', '0000', NULL, 'jb@prentissinternationalinvestments.co', 'prentissinternationalinvestments', 'xxx', 'Bogotá', 23, 130, 'N', '2017-03-08 20:27:14', '2017-05-25 20:32:14'),
(20, 'Charlotte', 'Gitenay', 'CE', '0000', NULL, 'gitenay.ch@gmail.com', 'Multiplica', 'xxx', 'Bogotá', 24, 50, '', '2017-03-08 20:54:45', '2017-03-16 03:00:25'),
(21, 'Oswaldo ', 'Rodriguez', 'CC', '0000', NULL, 'inversionesrs2017@gmail.com', 'Inversiones RS', 'xxx', 'Bogotá', 25, 0, '', '2017-03-09 19:25:54', '2017-03-09 19:25:54'),
(22, 'Hotel', 'Revenue', 'NIT', '900752242-7', NULL, 'ncontreras@hotelrevenue.com', 'RMC COLOMBIA SAS', 'calle 98 #8 37', 'Bogotá', 27, 109, '', '2017-03-09 20:31:02', '2017-03-25 02:43:44'),
(23, 'Daniel', 'Forero', 'CC', '0000', NULL, 'daniel@alojapp.com', 'Alojapp', 'xxx', 'Bogotá', 30, 0, 'N', '2017-03-16 00:27:20', '2017-05-05 20:41:28'),
(24, 'Roger', 'Moreno', 'CC', '111', NULL, 'rogmoreno@lingenos.com', 'Lingenos', 'xxx', 'Bogota', 32, 10, 'N', '2017-03-16 01:29:25', '2017-03-16 01:40:14'),
(25, 'Enrique', 'Gonzalez', 'CC', '80094644', NULL, 'enrique.gonzalez.wilches@outlook.com', 'Independiente', 'xxx', 'Bogotá', 34, 0, '', '2017-03-16 01:51:31', '2017-03-16 01:51:31'),
(26, 'Leslie', 'Lindarte', 'CC', '3192020850', NULL, 'certificacion@gerenciaparalavida.com', 'Gerencia para la vida', 'xxx', 'Bogota', 35, 41, 'N', '2017-03-16 02:05:30', '2017-03-24 05:28:11'),
(27, 'Valentine', 'Busnengo', 'CC', '111', NULL, 'valentinebus@gmail.com', 'xxx', '111', 'Bogota', 36, 10, 'N', '2017-03-16 20:48:55', '2017-03-16 22:22:26'),
(28, 'edinson  alexander ', 'romero  gutierrez', 'CC', '1233898306', '3105785534', 'tahwil.helcias1@gmail.com', 'tahwil helcias', 'carrera  120 d 127 -61', '', 39, 0, 'S', '2017-03-18 06:01:22', '2017-03-18 06:01:22'),
(29, 'Camila', 'Epalza', 'CC', '1022421341', '3192294611', 'camilaepalzatamara@gmail.com', 'Cascos.co', 'Calle 8 sur #41-28', '', 40, 0, 'S', '2017-03-19 07:04:26', '2017-03-19 07:04:26'),
(30, 'Ana Maria', 'Suarez', 'CC', '0', NULL, 'anamaria@cowo.com.co', 'Cowo', 'calle 98 #8-37', 'Bogota', 41, 0, 'S', '2017-03-22 00:53:07', '2017-03-22 00:53:39'),
(31, 'Felipe', 'Ramirez', 'CC', '111', NULL, 'felipe@feliperamirez.com', 'xxx', 'xxx', 'Bogota', 42, 30, '', '2017-03-22 04:52:49', '2017-03-22 04:53:50'),
(32, 'Esteban', 'Marin', 'CC', '1035419125', '3226346441', 'em@treevolt.com', 'Treevolt', 'Stree 50, 22', '', 43, 0, 'S', '2017-03-27 23:25:52', '2017-03-27 23:25:52'),
(33, 'Alberto ', 'Figueroa', 'CC', '691010', NULL, 'al.figueroa@gmail.com', 'xxx', 'Cra 18 #93b 52', 'Bogota', 44, 19, '', '2017-03-28 20:52:49', '2017-03-28 20:54:13'),
(34, 'Mark', 'firth', 'CE', '404466', '3164539809', 'mkrfirth@gmail.com', 'ABC', 'cra 4 59-41', 'Bogotá', 45, 0, 'N', '2017-04-06 05:17:24', '2017-04-11 17:11:45'),
(35, 'Catalina ', 'Pizarro', 'CC', '53001934', NULL, 'pizarropcatalina@gmail.com', 'Pizarro arquitectos', 'Carrera 7 #106 37', 'Bogota', 46, 20, 'N', '2017-04-19 21:25:09', '2017-05-25 20:33:17'),
(36, 'Juan David', 'Barrero', 'CC', '1001083338', '3115211759', 'juandavid@cowo.com.co', 'COWO', 'Cra 79 #127c-40', '', 48, 0, 'S', '2017-05-03 22:22:48', '2017-05-03 22:22:48'),
(37, 'Joel ', 'Zambrano', 'CC', '1007020863', '584140799822', 'joelalfredoz@gmail.com', 'Webland CA', 'Venezuela', '', 49, 0, 'S', '2017-05-15 19:14:01', '2017-05-15 19:14:01'),
(38, 'Alexandra', 'Ortiz Herrera', 'CC', '52281083', '3158923300', 'heclobar_77@hotmail.com', 'Importaciones jn sas', 'Calle 64 a # 57 23', '', 50, 0, 'S', '2017-05-17 12:06:20', '2017-05-17 12:06:20'),
(39, 'Ximena Sabogal', 'Sabogal ', 'CC', '52415707', '3138682071', 'ximenasabogal@gmail.com', 'Urban lab', 'Calle 72_ 29_45', '', 51, 0, 'S', '2017-05-24 02:35:11', '2017-05-24 02:35:11'),
(40, 'Sergio', 'Navarrete', 'CC', '1018444964', '3106782138', 'julianaje26@gmail.com', 'Independiente', 'Calle  146 # 15 - 92', '', 52, 20, 'S', '2017-06-02 03:18:11', '2017-06-01 22:36:13'),
(41, 'María Camila ', 'Henao', 'CC', ',,.', NULL, 'henaor.maria@gmail.com', 'Efex ', '...', 'Bogotá', 53, 0, '', '2017-06-12 18:19:43', '2017-06-12 18:19:43'),
(42, 'Fernando ', 'Otálora', 'CC', '...', NULL, 'fotalora@otaloraconsultores.com', 'Otálora Consultores', '...', 'Bogotá', 54, 0, '', '2017-06-12 18:30:06', '2017-06-12 18:30:06'),
(43, 'juan felipe ', 'Van strahlen ', 'CC', '1020747945', '3204954269', 'vanss111@gmail.com', 'Jfvo', 'Colombia', '', 55, 0, 'S', '2017-06-13 08:39:13', '2017-06-13 08:39:13'),
(44, 'Sebastián ', 'Herrera', 'CC', '1143834663', NULL, '', 'Independiente', '...', 'Bogotá', 56, 0, '', '2017-06-15 19:39:32', '2017-06-15 19:39:32'),
(45, 'Sandra', 'Mancebo', 'CC', 'Pasaporte FK300990', '5548999683376', 'sandra.mancebo@touricoholidays.com', 'Tourico Holidays', 'Rua Acary Margarida, 182 - 201 - Florianopolis - Brasil', '', 57, 0, 'S', '2017-06-20 05:36:55', '2017-06-20 05:36:55'),
(46, 'Andres Felipe', 'Velez Restrepo', 'CC', '94479237', '3108493685', 'encubo.proyectos@gmail.com', 'Encubo', 'admin', '', 59, 0, 'S', '2017-06-29 19:15:08', '2017-06-29 19:15:08'),
(47, 'Pablo', 'Zambrano', 'CE', '396786', '3177851971', 'pablo@zambranodigital.com', 'ZambranoGroup sas', 'cll 89a #116a-35', '', 64, 0, 'S', '2017-07-06 02:24:14', '2017-07-06 02:24:14'),
(48, 'Juan Carlos', 'Gonzalez', 'CC', '80091864', NULL, 'juanpulga@gmail.com', 'x', 'x', 'Bogotá', 65, 0, '', '2017-07-11 20:35:11', '2017-07-11 20:35:11'),
(49, 'Paola', 'Mendoza', 'CC', '52438111', '3176444538', 'Aloapm@yahoo.com', 'Independiente', 'Cra 13a # 88 47', '', 66, 0, 'S', '2017-07-20 00:36:57', '2017-07-20 00:36:57'),
(50, 'JAIME', 'Prieto', 'CC', '1020736092', '', 'jaimeandresp@hotmail.com', 'AAA', 'Calle 97 21 50 Apt 412', '', 67, 0, 'S', '2017-07-25 05:48:35', '2017-07-25 05:48:35'),
(51, 'Luisa ', 'Rivera', 'CC', '1088030491', '3104147616', 'luisa.but@hotmail.com', 'Independent', 'Pereira', '', 68, 0, 'S', '2017-07-25 20:12:15', '2017-07-25 20:12:15'),
(52, 'maria alexandra ', 'alba', 'CC', '39774936', '3163834910', 'marialexa555@gmail.com', 'Enlace publicidad', 'k 6 B No149–72', '', 69, 0, 'S', '2017-07-26 09:59:21', '2017-07-26 09:59:21'),
(53, 'Laura Maria ', 'Nuñez ', 'CC', '1018432742', '3102353034', 'laura.nunez@heladospopsy.com', 'Helados Popsy - Comercial Allan ', 'Autopista Medellin Km. 1.8 Costado Sur Parque Soko Industrial Bodega 4', '', 70, 0, 'S', '2017-07-31 19:20:01', '2017-07-31 19:20:01'),
(54, 'JUAN DAVID', 'NUNEZ ALJURE', 'CC', '1075249802', '3156701402', 'juan.d.nunez.aljure@gmail.com', 'INDEPENDIENTE', 'Calle 95b 11a 46 e', '', 71, 0, 'S', '2017-08-01 16:58:43', '2017-08-01 16:58:43'),
(55, 'Andrea', 'Gomez', 'CC', '1127602277', '3108868628', '5andrea25@gmail.com', 'Soloinnove', 'Bogota', '', 72, 0, 'S', '2017-08-04 09:17:53', '2017-08-04 09:17:53'),
(56, 'Santiago', 'Bedoya', 'CC', '1020811804', '3183589587', 'santiago@bedoba.com', 'Sinapsis', 'Calle 152a #16a-60', '', 73, 0, 'S', '2017-08-16 07:43:19', '2017-08-16 07:43:19'),
(57, 'William', 'Morales', 'CC', '80109812', '3132111273', 'williamamoralesr@gmail.com', 'CRM Force', 'williamamoralesr@gmail.com', '', 74, 0, 'S', '2017-08-17 02:54:06', '2017-08-17 02:54:06'),
(58, 'Erika Marcela', 'Torres Pelaez', 'CC', '1032375941', '3153023072', 'akane999@hotmail.com', '-', 'Calle 50 15-45 apto 610', '', 75, 0, 'S', '2017-08-25 02:48:49', '2017-08-25 02:48:49'),
(59, 'Juan', 'Real', 'CC', '103241314o', '2774614', 'Juan. ReAl@avianca.com ', 'Avianca', 'Cll 2#25 a28', '', 76, 0, 'S', '2017-08-26 21:44:26', '2017-08-26 21:44:26'),
(60, 'juan pablo', 'herrera guevara', 'CC', '9870070', '3138951402', 'kunderjuan@gmail.com', 'publicaciones gubernamentales', 'cll 10 sur # 37b 22', '', 77, 0, 'S', '2017-08-30 00:38:40', '2017-08-30 00:38:40'),
(61, 'Carmenza', 'Grisales', 'CC', '51578993', '3153588863', 'caroaristizabal@hotmail.com', 'Grupogrande', 'carrera 2 # 116 20', '', 78, 0, 'S', '2017-09-07 01:30:59', '2017-09-07 01:30:59'),
(62, 'Kevin', 'Álvarez', 'NIT', '9011117668', '3194894755', 'Kevin.bonvoyagecolombia@gmail.com', 'BONVOYAGE SAS', 'CRA 93a # 76 43', '', 79, 0, 'S', '2017-09-07 19:02:35', '2017-09-07 19:02:35'),
(63, 'Germán', 'Rivera', 'CC', '79791524', '3004245608', 'gerrv@hotmail.com', 'PlusIntel', 'Av Jiménez # 8a - 77 Oficina 805', '', 80, 0, 'S', '2017-09-09 18:51:33', '2017-09-09 18:51:33'),
(64, 'Germán', 'Rivera', 'CC', '79791524', '3004245608', 'german.rivera@plusintel.com', 'PlusIntel', 'Av Jiménez # 8a - 77 Oficina 805', '', 81, 0, 'S', '2017-09-09 18:52:46', '2017-09-09 18:52:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_par_token_clave`
--

CREATE TABLE `sis_par_token_clave` (
  `email` varchar(100) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sis_par_token_clave`
--

INSERT INTO `sis_par_token_clave` (`email`, `token`, `created_at`, `updated_at`) VALUES
('Jrpp1991@hotmail.com', '94516bd3c90432e7112530b807ea7000e87f2fd65ec7ca883ad477b211cfe3c2', '2016-09-22 23:52:14', '0000-00-00 00:00:00'),
('nannysalazar@gmail.com', 'f4045ab89145c6e79dc51769398c91fb8902b254cf49f255f98d8a907499b425', '2016-09-23 00:28:45', '0000-00-00 00:00:00'),
('javiermmontoya@hotmail.com', 'd603f1cb28c7a5bdc89fea9ffbe6171debe1b29b8fd7ff3e41c585d650ba6a70', '2016-09-23 03:14:50', '0000-00-00 00:00:00'),
('williamvalencia@hotmail.es', '522d155823d62ab3e33df4fef36adaad849b97bd533b5198bc3f4f25bdd99d40', '2016-09-23 16:53:21', '0000-00-00 00:00:00'),
('mvaleganem@hotmail.com', '270e14d0d080d2060f7e0bca48306091e34a8162957f58c0517ecc48a0d36cec', '2016-09-24 20:41:52', '0000-00-00 00:00:00'),
('grossvater1977@gmail.com', '9c26a44102569a39c1bd44360bd6a12fd6727518a1f8ad220c5f1a781aa1778d', '2016-09-25 06:05:30', '0000-00-00 00:00:00'),
('ratonchuki@hotmail.com', '9b3c954b1c34ed15c2f555c7c675b628371b92735c14acbc2be5b318a40a6d30', '2016-09-25 20:51:11', '0000-00-00 00:00:00'),
('Hybt1988@hotmail.com', '26db57c0623c44152af92d819cea48f3d2cc73a55a9f961485db5b4892021345', '2016-09-26 18:29:43', '0000-00-00 00:00:00'),
('alexandra11091@hotmail.com', 'ecbbd4e083c520264c42691882c60f76716b8a412b7a4aca096d5840fc456dd2', '2016-09-27 02:51:06', '0000-00-00 00:00:00'),
('dagibu212@hotmail.com', '2ad10f15acb6c80ae8ba4eb98c156385c0654cb7a7e1e264bbb9f1529b25c904', '2016-10-04 22:17:22', '0000-00-00 00:00:00'),
('nanidecorreal@gmail.com', '5695fe79c94eb171baee910e03a00914d97f94deb93531471638aecd140ef23b', '2016-10-08 19:12:17', '0000-00-00 00:00:00'),
('sebasrestrepo96@gmail.com', '4f645bd4e7b8271c903e6b49b2df43081c99be38a79565fdd64a5341ae3dff39', '2016-10-10 19:39:01', '0000-00-00 00:00:00'),
('Leidyorozco772@gmail.com', '3777557c0c42dc895dccd26c6a8490daca2a235f9fb76ef8b31add6d4606cb1a', '2016-10-11 00:36:28', '0000-00-00 00:00:00'),
('andres.aguirre00@gmail.com', 'aad2e2afb0f22d6dd7b79fff5c6af43b01a35eecfcad5f7132e7425363764832', '2016-10-13 00:54:16', '0000-00-00 00:00:00'),
('brigitegaravito11@gmail.com', '4b1552a82a6db660cab13878d5b62bbad542326fec9aec76ed81fd5a842f0abd', '2016-10-18 19:19:52', '0000-00-00 00:00:00'),
('lilia.alvarez.ar@gmail.com', 'c6bca8551cd6886e00e13b0f362d9cdb024a91dd4d20d7c32184549cb7aab037', '2016-10-20 22:08:25', '0000-00-00 00:00:00'),
('ghos1948@Outlook.com', '412dc7e0c1dda9153ef9128042f4d9774e2735be50c1e7ba816e16c08c930d9c', '2016-10-24 18:38:25', '0000-00-00 00:00:00'),
('julian_u@hotmail.com', 'a2e160e085608b0a4278adf472a2b6c3e0aac5e5f8ff3250504d0c582e240373', '2016-10-30 02:45:07', '0000-00-00 00:00:00'),
('Jp_mesa15@hotmail.es', '2ed6c6b6ddc21894f33db663ac704b4436a1cf353b37c0d1eccbfe61655e39fe', '2016-11-03 00:28:59', '0000-00-00 00:00:00'),
('emersonibarra@yahoo.es', '12400d19767e6f0a915329eaeff1ba611e2f188d03a048307fee1f565bba6d7e', '2016-11-06 20:07:39', '0000-00-00 00:00:00'),
('Alejandromartinezbeltran@gmail.com', 'a960d8fa001a42cc75c182096acb7e43d5a70fa8b8bd9f8bc7da68ac7199924c', '2016-11-11 03:14:18', '0000-00-00 00:00:00'),
('jorgematias10@icloud.com', '88d39002fb9f6a09b2d3e802bebc6436702139b87698e67aaf0c7f3fd7acfca7', '2016-11-15 03:19:18', '0000-00-00 00:00:00'),
('johnedilor@gmail.com', '0caa92c774d58816d54f57390891966bf99430e26c1074048da72466ea1e4ba9', '2016-11-15 23:56:13', '0000-00-00 00:00:00'),
('oswaldinsua@hotmail.com', 'd860bd07ae43fa134fcfe0196e89d1a10115ab6b5f61a6b20b9db8ca855fb582', '2016-11-18 21:34:36', '0000-00-00 00:00:00'),
('alexco27_1@hotmail.com', '62c82cce104c08de5fd11ebeb9e7a2f90356adc63a74b2b3a0d23ca0b174611b', '2016-11-23 07:20:16', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_par_usuario`
--

CREATE TABLE `sis_par_usuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `login` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `admin` char(1) DEFAULT 'N',
  `activo` char(1) DEFAULT 'Y',
  `alerta_creacion` char(1) DEFAULT 'N',
  `alerta_edicion` char(1) DEFAULT 'N',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sis_par_usuario`
--

INSERT INTO `sis_par_usuario` (`id`, `nombre`, `login`, `email`, `password`, `admin`, `activo`, `alerta_creacion`, `alerta_edicion`, `created_at`, `updated_at`, `deleted_at`, `remember_token`) VALUES
(1, 'Alejandro', 'admin', 'info@encarguelo.com', '$2y$10$AJfL/.F5yeLOCUUXTwB/hejOW1daprS55LnJatB/47dlzgIz19M3e', 'Y', 'Y', 'N', 'N', '2016-03-04 02:04:18', '2017-07-10 15:04:07', NULL, '0000-00-00 00:00:00'),
(2, 'Cowo', 'Cowo', 'info@cowo.com.co', '$2y$10$PgE5rDBb12SkE8lcAkq30OrqYmLTB6fJlxYlb1V874NKHPyC.w4V2', 'Y', 'Y', 'N', 'N', '2017-02-08 20:02:36', '2017-02-24 21:35:18', NULL, '0000-00-00 00:00:00'),
(3, 'Andres Mozo', 'Andrés CoWo', 'andres@cowo.com.co', '$2y$10$ZfPAVptsttY.Brpd8NIzReZpP0.l17IbtArtpZp19xYo4P.evoo5.', 'Y', 'Y', 'N', 'N', '2017-02-08 20:05:24', '2017-07-25 03:24:00', NULL, '0000-00-00 00:00:00'),
(4, 'Fabian', 'fabian.morales@outlook.com', 'fabian.morales@outlook.com', '$2y$10$7T4GPUEMvls62ye8d4OTjONPxOfktw2oj4.7wz/XOQWLmR0BwNWaK', 'N', 'Y', 'N', 'N', '2017-02-13 06:40:31', '2017-06-29 18:49:31', '2017-06-29 18:49:31', '0000-00-00 00:00:00'),
(5, 'Eileen', 'eileens@hollyhock.com', 'eileens@hollyhock.com', '$2y$10$c4VoUALwQOwo/dHfF6ESY.XPsqwGZzrNr0mrcXPDxFeNDPi3Z9.CW', 'N', 'Y', 'N', 'N', '2017-02-23 23:00:02', '2017-02-25 01:04:47', NULL, '0000-00-00 00:00:00'),
(6, 'Andres Felipe', 'anfevelez83@gmail.com', 'anfevelez83@gmail.com', '$2y$10$awp4z7thEG/mcrJMKtzGd.98bTi4SImXt6iqc/j1CQMqd42aHTsfW', 'N', 'Y', 'N', 'N', '2017-02-24 03:27:41', '2017-06-29 18:49:27', '2017-06-29 18:49:27', '0000-00-00 00:00:00'),
(7, 'Giovana', 'gmartinez@americanentrepreneurgroup.com', 'gmartinez@americanentrepreneurgroup.com', '$2y$10$or7qY6DfWJzoPWxk20U.wekBdfbQ2CD8jR6bs4MLCUzIo/GoyZW6S', 'N', 'Y', 'N', 'N', '2017-02-24 21:33:23', '2017-02-24 23:03:27', NULL, '0000-00-00 00:00:00'),
(8, 'Andrés ', 'planner@ene.com', 'planner@ene.com', '$2y$10$0GiUlpPGYR5yPBBJizLsB.86TFhM7nUcdwgh8mZ3qEv8CXnzYa56C', 'N', 'Y', 'N', 'N', '2017-02-25 00:33:56', '2017-06-29 18:49:22', '2017-06-29 18:49:22', '0000-00-00 00:00:00'),
(9, 'Juan Manuel', 'jquintero@infovalmer.com.co', 'jquintero@infovalmer.com.co', '$2y$10$5W6VTG3gHUmO8J6bclmM2utJC9GeOeTFQawiBntWxXIizGOPCnr6u', 'N', 'Y', 'N', 'N', '2017-03-01 19:05:33', '2017-06-01 23:59:03', '2017-06-01 23:59:03', '0000-00-00 00:00:00'),
(10, 'Andres', 'jgpr_coordinacion@mail.com', 'jgpr_coordinacion@mail.com', '$2y$10$utXvkqnvQk6vdRTSY7kt..4jdBq.r8QAJSiWBK/GaJng91oa.ek3K', 'N', 'Y', 'N', 'N', '2017-03-03 01:45:48', '2017-06-02 00:01:29', '2017-06-02 00:01:29', '0000-00-00 00:00:00'),
(11, 'Katiana', 'katiana@tripstravelagency.com ', 'katiana@tripstravelagency.com', '$2y$10$yVRRIJstRPaBxrTKzGh4zeecKfPWsebFx1PewYHOnLp.g.w9BDnDe', 'N', 'Y', 'N', 'N', '2017-03-05 04:16:36', '2017-06-01 23:59:54', '2017-06-01 23:59:54', '0000-00-00 00:00:00'),
(12, 'Camilo', 'camilo.naranjo@gmail.com', 'camilo.naranjo@gmail.com', '$2y$10$38NKQ0wwN.SytqDsD0DjaetAtYp.rMCSior0o9UqHB4giDCjQluwG', 'N', 'Y', 'N', 'N', '2017-03-06 22:20:06', '2017-03-22 01:50:20', NULL, '0000-00-00 00:00:00'),
(13, 'Pablo Andrés ', 'paacsistem@gmail.com', 'paacsistem@gmail.com', '$2y$10$Hy0ExnoIOO4n/WllOqlnQONmvkFlc09JMfnoArgI77lqbFTq7GPv.', 'N', 'Y', 'N', 'N', '2017-03-07 18:07:27', '2017-06-01 23:59:10', '2017-06-01 23:59:10', '0000-00-00 00:00:00'),
(17, 'Catalina ', 'catalina.acosta@gmail.com', 'catalina.acosta@gmail.com', '$2y$10$zd/hEWCous9TrY15.HG6Bu5CgJzF4LoeYHXUkMQ14Z3IYGsP2tWM2', 'N', 'Y', 'N', 'N', '2017-03-08 02:43:23', '2017-03-09 20:49:37', NULL, '0000-00-00 00:00:00'),
(18, 'Camila', 'cmarino@hof.com.co', 'cmarino@hof.com.co', '$2y$10$vnoAsdc58pz.eSuxCAfvVeI48jwJcYsg.K7mzysaDEzJyy055aM2G', 'N', 'Y', 'N', 'N', '2017-03-08 19:57:20', '2017-03-08 20:01:30', NULL, '0000-00-00 00:00:00'),
(19, 'Juan ', 'juan.dorado@nuestromall.com', 'juan.dorado@nuestromall.com', '$2y$10$1YS.CBH1CLWZeDYHpI913uyLusZBpXfnr9s6V6S7Q7jXRUvPiJ9W6', 'N', 'Y', 'N', 'N', '2017-03-08 20:10:21', '2017-06-29 18:49:57', '2017-06-29 18:49:57', '0000-00-00 00:00:00'),
(20, 'Jose', 'arq.josepayares@gmail.com', 'arq.josepayares@gmail.com', '$2y$10$tf4I28R/3H2AqXumjrNVquZVZGA0NWJaV7fDNM7Byy8FXy6zcre8m', 'N', 'Y', 'N', 'N', '2017-03-08 20:16:49', '2017-03-08 20:16:49', NULL, '0000-00-00 00:00:00'),
(21, 'Juan carlos', 'jcgallon@hseqcamero.com', 'jcgallon@hseqcamero.com', '$2y$10$Q69MmSTD9WR4VxgNQxiP9uNu3MZWRdsryma/yeaKds.Zb0EY/HZwi', 'N', 'Y', 'N', 'N', '2017-03-08 20:19:27', '2017-06-29 18:50:06', '2017-06-29 18:50:06', '0000-00-00 00:00:00'),
(22, 'Andrés', 'planner@ene.com.co', 'planner@ene.com.co', '$2y$10$W6WmF8jGa4pqpyJfagk/1eP/gGnaqvKk3DDz5HDHdu/xdHXgX2hRa', 'N', 'Y', 'N', 'N', '2017-03-08 20:25:11', '2017-06-29 18:50:14', '2017-06-29 18:50:14', '0000-00-00 00:00:00'),
(23, 'Jonathan', 'jb@prentissinternationalinvestments.co', 'jb@prentissinternationalinvestments.co', '$2y$10$ZQnuAIYWStamKQucQ55kt.24uCBiGpQV6K2v/9IS8fLBUi/CuuE0W', 'N', 'Y', 'N', 'N', '2017-03-08 20:27:14', '2017-06-01 19:06:32', '2017-06-01 19:06:32', '0000-00-00 00:00:00'),
(24, 'Charlotte', 'gitenay.ch@gmail.com', 'gitenay.ch@gmail.com', '$2y$10$T0XQliSxmmZuJbs2E5YfQeULbhHJ90WwFCc.IWK8tiU/nPNGrrgTu', 'N', 'Y', 'N', 'N', '2017-03-08 20:54:45', '2017-03-08 20:54:45', NULL, '0000-00-00 00:00:00'),
(25, 'Oswaldo ', 'inversionesrs2017@gmail.com', 'inversionesrs2017@gmail.com', '$2y$10$xTV4ZD2JX7qQsyv.FvWrmObVaGEWMfkI0VDqGf.HyNBEY4dP6SvpC', 'N', 'Y', 'N', 'N', '2017-03-09 19:25:54', '2017-03-09 19:25:54', NULL, '0000-00-00 00:00:00'),
(27, 'Hotel', 'ncontreras@hotelrevenue.com', 'ncontreras@hotelrevenue.com', '$2y$10$MmwP5Bk8qQfwrSfaQdhXM.n.Vyjk80RgdyepNkL0VmIKGj9Gqjjhm', 'N', 'Y', 'N', 'N', '2017-03-09 20:31:02', '2017-03-09 20:31:02', NULL, '0000-00-00 00:00:00'),
(30, 'Daniel', 'daniel@alojapp.com', 'daniel@alojapp.com', '$2y$10$GOsOGi.vNEVrKL/P5cK4d.1fQySks5Krgv43xoi5JcFn9nhVfWqGO', 'N', 'Y', 'N', 'N', '2017-03-16 00:27:20', '2017-06-01 23:59:27', '2017-06-01 23:59:27', '0000-00-00 00:00:00'),
(32, 'Roger', 'rogmoreno@lingenos.com', 'rogmoreno@lingenos.com', '$2y$10$cd0vey7c8xaLT6pLGgPXpOo/VId5BWdGhz1Ic8Y.EdR/7Affb0VQa', 'N', 'Y', 'N', 'N', '2017-03-16 01:29:25', '2017-06-02 00:00:30', '2017-06-02 00:00:30', '0000-00-00 00:00:00'),
(34, 'Enrique', 'enrique.gonzalez.wilches@outlook.com', 'enrique.gonzalez.wilches@outlook.com', '$2y$10$mQqSdwpqwQRtLiMgZZiZPe7NvnQsccYMzkIwhOBcQoNz1qY4hImAe', 'N', 'Y', 'N', 'N', '2017-03-16 01:51:31', '2017-06-29 18:52:19', '2017-06-29 18:52:19', '0000-00-00 00:00:00'),
(35, 'Leslie', 'certificacion@gerenciaparalavida.com', 'certificacion@gerenciaparalavida.com', '$2y$10$dbe7ExO3CTeuFWUOs2DQpebCHCNc.fWmYyxWL9QGwS/5vg5kYJuWm', 'N', 'Y', 'N', 'N', '2017-03-16 02:05:30', '2017-06-29 18:49:14', '2017-06-29 18:49:14', '0000-00-00 00:00:00'),
(36, 'Valentine', 'valentinebus@gmail.com', 'valentinebus@gmail.com', '$2y$10$saRXxgSv0AO6nO/UjuMaDuHPUGbW.rN58SmPNnrIl/YW1QLX5l2IC', 'N', 'Y', 'N', 'N', '2017-03-16 20:48:55', '2017-03-16 20:48:55', NULL, '0000-00-00 00:00:00'),
(39, 'edinson  alexander ', 'tahwil.helcias1@gmail.com', 'tahwil.helcias1@gmail.com', '$2y$10$O/xmUu984RraehbrTtLjmeewhBqMAHVb5hdpIG5hq50h.mvfowIJO', 'N', 'Y', 'N', 'N', '2017-03-18 06:01:22', '2017-06-12 18:20:54', '2017-06-12 18:20:54', '0000-00-00 00:00:00'),
(40, 'Camila', 'camilaepalzatamara@gmail.com', 'camilaepalzatamara@gmail.com', '$2y$10$ZcNhaibDewW/e6sMWeGKPOLrc6MqgErSQzODC/5yjl.AepLZBdnq.', 'N', 'Y', 'N', 'N', '2017-03-19 07:04:26', '2017-06-29 18:50:35', '2017-06-29 18:50:35', '0000-00-00 00:00:00'),
(41, 'Ana Maria', 'anamaria@cowo.com.co', 'anamaria@cowo.com.co', '$2y$10$9dxw4bfzVVeeNMCYpPg8mOT20QJf/1lKt.eaQW1fkR8iM7cty1s8O', 'Y', 'Y', 'N', 'N', '2017-03-22 00:53:07', '2017-06-29 18:49:47', '2017-06-29 18:49:47', '0000-00-00 00:00:00'),
(42, 'Felipe', 'felipe@feliperamirez.com', 'felipe@feliperamirez.com', '$2y$10$QoW9O6IR8.NwvJaih8eCuu/QSpIq6eac7ko37zNjWrIw3s2QvCOku', 'N', 'Y', 'N', 'N', '2017-03-22 04:52:49', '2017-06-29 18:49:41', '2017-06-29 18:49:41', '0000-00-00 00:00:00'),
(43, 'Esteban', 'em@treevolt.com', 'em@treevolt.com', '$2y$10$yiyZSdY6kqBp06JczsKVz.1PIf4wBrudtKhsKRS2bgTb2BmZXpmfq', 'N', 'Y', 'N', 'N', '2017-03-27 23:25:52', '2017-06-12 18:20:49', '2017-06-12 18:20:49', '0000-00-00 00:00:00'),
(44, 'Alberto ', 'al.figueroa@gmail.com', 'al.figueroa@gmail.com', '$2y$10$6crg0MDtXdgjNcw8xe06..dk6UqHWFQxvBz4UkpAFbSYKvPMVY9tq', 'N', 'Y', 'N', 'N', '2017-03-28 20:52:49', '2017-06-29 18:48:06', '2017-06-29 18:48:06', '0000-00-00 00:00:00'),
(45, 'Mark', 'mkrfirth@gmail.com', 'mkrfirth@gmail.com', '$2y$10$szacoXUnWfWIfskIgxiY7u/Z8yPAATwogGMnrs9.Ewt5Fg6ZGeHFW', 'N', 'Y', 'N', 'N', '2017-04-06 05:17:24', '2017-06-02 00:01:08', '2017-06-02 00:01:08', '0000-00-00 00:00:00'),
(46, 'Catalina ', 'pizarropcatalina@gmail.com', 'pizarropcatalina@gmail.com', '$2y$10$jP7QyHwS8LSLPQaQLoUuG.eN5ZFZ7JsowOrLIdpVgH2vjrI5EyNL2', 'N', 'Y', 'N', 'N', '2017-04-19 21:25:09', '2017-04-19 21:25:09', NULL, '0000-00-00 00:00:00'),
(48, 'Juan David', 'juandavid@cowo.com.co', 'juandavid@cowo.com.co', '$2y$10$LOXIDU26c1cPl0K6Kbud8OEdQkDDPFOgK7ndvCo/FQcwwYLsD8asO', 'N', 'Y', 'N', 'N', '2017-05-03 22:22:48', '2017-05-03 22:22:48', NULL, '0000-00-00 00:00:00'),
(49, 'Joel ', 'joelalfredoz@gmail.com', 'joelalfredoz@gmail.com', '$2y$10$TQ9jAiJEI5rlnJKtXxemOOSUZEUume4Yjn.akgL9CsOPOVoJcRWxu', 'N', 'Y', 'N', 'N', '2017-05-15 19:14:01', '2017-06-12 18:20:04', '2017-06-12 18:20:04', '0000-00-00 00:00:00'),
(50, 'Alexandra', 'heclobar_77@hotmail.com', 'heclobar_77@hotmail.com', '$2y$10$dctLI2xHZ1ppckB4xDr3ju.JoIl0MlImxmshvRZ.HckKClfq1GI6q', 'N', 'Y', 'N', 'N', '2017-05-17 12:06:20', '2017-06-12 18:20:36', '2017-06-12 18:20:36', '0000-00-00 00:00:00'),
(51, 'Ximena Sabogal', 'ximenasabogal@gmail.com', 'ximenasabogal@gmail.com', '$2y$10$b8HO7Gq61aESlyytIN2yeOHMF0wd/3h5Ke7spH5lI/t8wtvUtGYrO', 'N', 'Y', 'N', 'N', '2017-05-24 02:35:11', '2017-06-12 18:20:31', '2017-06-12 18:20:31', '0000-00-00 00:00:00'),
(52, 'Sergio', 'julianaje26@gmail.com', 'julianaje26@gmail.com', '$2y$10$D74ZQTPH6IcTWBKwpB/UMuLM8dj6JvWVJ0imWyzhiDiuwW9vIyqIm', 'N', 'Y', 'N', 'N', '2017-06-02 03:18:11', '2017-06-29 18:50:47', '2017-06-29 18:50:47', '0000-00-00 00:00:00'),
(53, 'María Camila ', 'cowo', 'henaor.maria@gmail.com', '$2y$10$uBZwn3aGo3Iw0WKsCSQlne3c7bA6u2i1bG6igXBuimjB2SD8rK1e.', 'N', 'Y', 'N', 'N', '2017-06-12 18:19:43', '2017-06-12 18:19:43', NULL, '0000-00-00 00:00:00'),
(54, 'Fernando ', 'cowo', 'fotalora@otaloraconsultores.com', '$2y$10$gz7NnrVMaedGDA5.7xSsdOkxpg5oeYRvaGcJYJUYvccRbImrnI986', 'N', 'Y', 'N', 'N', '2017-06-12 18:30:06', '2017-06-12 18:30:06', NULL, '0000-00-00 00:00:00'),
(55, 'juan felipe ', 'vanss111@gmail.com', 'vanss111@gmail.com', '$2y$10$dX6kpntCCIB4WiqirOmeQuKlty8Xg58Gt3qe9ESsB6HDKg8fLYn7u', 'N', 'Y', 'N', 'N', '2017-06-13 08:39:13', '2017-06-13 08:39:13', NULL, '0000-00-00 00:00:00'),
(56, 'Sebastián ', 'cowo', '', '$2y$10$jv5asZPK7OHDUnN36pnxW.aqyClleVpUc2n.d5V4/xaJQvBMcIokW', 'N', 'Y', 'N', 'N', '2017-06-15 19:39:32', '2017-07-11 20:30:53', '2017-07-11 20:30:53', '0000-00-00 00:00:00'),
(57, 'Sandra', 'sandra.mancebo@touricoholidays.com', 'sandra.mancebo@touricoholidays.com', '$2y$10$iVxBlOtRQD.WGNJak1/AHuDwQhwgnXmqKpRehoaA4rTv5ZcvYp0OC', 'N', 'Y', 'N', 'N', '2017-06-20 05:36:55', '2017-07-11 20:30:42', '2017-07-11 20:30:42', '0000-00-00 00:00:00'),
(59, 'Andres Felipe', 'encubo.proyectos@gmail.com', 'encubo.proyectos@gmail.com', '$2y$10$ad8g75uplI7/Ty5d3AYZiuDuLTjW.Y0pi1jJyj5SoQf0DPtAkUPLW', 'N', 'Y', 'N', 'N', '2017-06-29 19:15:08', '2017-06-29 19:15:08', NULL, '0000-00-00 00:00:00'),
(64, 'Pablo', 'pablo@zambranodigital.com', 'pablo@zambranodigital.com', '$2y$10$/OlFVjuJxvtsfqnT5jsFPOyvFDEgU8X0R77NwP/KHcmM77/Ogyi0q', 'N', 'Y', 'N', 'N', '2017-07-06 02:24:14', '2017-07-11 20:30:23', '2017-07-11 20:30:23', '0000-00-00 00:00:00'),
(65, 'Juan Carlos', 'juanpulga@gmail.com', 'juanpulga@gmail.com', '$2y$10$c3FETTWhz6Rrrg.U5hZf6.eMfO2TKUY9y6rZL0qL4oGoPMLQUoJiq', 'N', 'Y', 'N', 'N', '2017-07-11 20:35:11', '2017-07-11 20:35:11', NULL, '0000-00-00 00:00:00'),
(66, 'Paola', 'Aloapm@yahoo.com', 'Aloapm@yahoo.com', '$2y$10$ys1Iv8qCzDWFty81xQ6PSe60fHux0pIVQ/b4xeK8EVowlHr/evyGG', 'N', 'Y', 'N', 'N', '2017-07-20 00:36:57', '2017-07-20 00:36:57', NULL, '0000-00-00 00:00:00'),
(67, 'JAIME', 'jaimeandresp@hotmail.com', 'jaimeandresp@hotmail.com', '$2y$10$hGh40HwOlwRKl4IaT//XleZL9dHvh0r6.y6u1EdNoQEqNmSE5nQgq', 'N', 'Y', 'N', 'N', '2017-07-25 05:48:35', '2017-07-25 05:48:35', NULL, '0000-00-00 00:00:00'),
(68, 'Luisa ', 'luisa.but@hotmail.com', 'luisa.but@hotmail.com', '$2y$10$sEfIpRnXeDJDQhJ2VThJRePOuY2HW1jQ0fI0DHPXclRl0uExZ4Dei', 'N', 'Y', 'N', 'N', '2017-07-25 20:12:15', '2017-07-25 20:12:15', NULL, '0000-00-00 00:00:00'),
(69, 'maria alexandra ', 'marialexa555@gmail.com', 'marialexa555@gmail.com', '$2y$10$74Ia8TUfk4wtxqKimjmzoOm5SugfUocKBK4gmkNwVnptSGVRWk9G.', 'N', 'Y', 'N', 'N', '2017-07-26 09:59:21', '2017-07-26 09:59:21', NULL, '0000-00-00 00:00:00'),
(70, 'Laura Maria ', 'laura.nunez@heladospopsy.com', 'laura.nunez@heladospopsy.com', '$2y$10$Jju2T4Ol2Sr9bhierOglhuEvQWe67R0FtsNNdffSwBHxi8zf9TKuq', 'N', 'Y', 'N', 'N', '2017-07-31 19:20:01', '2017-07-31 19:20:01', NULL, '0000-00-00 00:00:00'),
(71, 'JUAN DAVID', 'juan.d.nunez.aljure@gmail.com', 'juan.d.nunez.aljure@gmail.com', '$2y$10$0vBSKpxnnpiXLa1YNqEBzuHbnxRpw9uIUnB.rV6ZNqX/kmniO1dmW', 'N', 'Y', 'N', 'N', '2017-08-01 16:58:43', '2017-08-01 16:58:43', NULL, '0000-00-00 00:00:00'),
(72, 'Andrea', '5andrea25@gmail.com', '5andrea25@gmail.com', '$2y$10$QigzXS9D/lYmPVMbAr0bXudB8stK1apPVuPBY4pxLj9egiPgU3j62', 'N', 'Y', 'N', 'N', '2017-08-04 09:17:53', '2017-08-04 09:17:53', NULL, '0000-00-00 00:00:00'),
(73, 'Santiago', 'santiago@bedoba.com', 'santiago@bedoba.com', '$2y$10$hJCYK5sYwusen63.Cw55ZuvjFBN929Boj2ml.ehU3hV1NWzWLy/S6', 'N', 'Y', 'N', 'N', '2017-08-16 07:43:19', '2017-08-16 07:43:19', NULL, '0000-00-00 00:00:00'),
(74, 'William', 'williamamoralesr@gmail.com', 'williamamoralesr@gmail.com', '$2y$10$jhcTKjJTyrqv.WMUVHpvqO0iMyHGKRzHkW.OTW5WJIXci6p5BtdB2', 'N', 'Y', 'N', 'N', '2017-08-17 02:54:06', '2017-08-17 02:54:06', NULL, '0000-00-00 00:00:00'),
(75, 'Erika Marcela', 'akane999@hotmail.com', 'akane999@hotmail.com', '$2y$10$EQA7NsShRm3E1iJb21Kxd.T8AUl8WU7B5m6apFWcc/d/Ucf8CCkT.', 'N', 'Y', 'N', 'N', '2017-08-25 02:48:49', '2017-08-25 02:48:49', NULL, '0000-00-00 00:00:00'),
(76, 'Juan', 'Juan. ReAl@avianca.com ', 'Juan. ReAl@avianca.com ', '$2y$10$ZgUqSVNOYPC/4U52izJLxuG3zy3ZBNc.FiDMDkDt7fZEw.FgqK/Om', 'N', 'Y', 'N', 'N', '2017-08-26 21:44:26', '2017-08-26 21:44:26', NULL, '0000-00-00 00:00:00'),
(77, 'juan pablo', 'kunderjuan@gmail.com', 'kunderjuan@gmail.com', '$2y$10$7qkGLtxitKhS7qBMRb1JiOCDLyPDcGxD0oYrUA/OAYdoWC5VK1Sga', 'N', 'Y', 'N', 'N', '2017-08-30 00:38:40', '2017-08-30 00:38:40', NULL, '0000-00-00 00:00:00'),
(78, 'Carmenza', 'caroaristizabal@hotmail.com', 'caroaristizabal@hotmail.com', '$2y$10$FnFeIVKWujWbLcIw9ZjD0Oz8AEpzipLScUxFYSDvfCVCggoeNegpC', 'N', 'Y', 'N', 'N', '2017-09-07 01:30:59', '2017-09-07 01:30:59', NULL, '0000-00-00 00:00:00'),
(79, 'Kevin', 'Kevin.bonvoyagecolombia@gmail.com', 'Kevin.bonvoyagecolombia@gmail.com', '$2y$10$ingDfkjyWTjUKx7vq52cVuAvftcdflnvM9Y6ssFUU2hcs3aNgkkl2', 'N', 'Y', 'N', 'N', '2017-09-07 19:02:35', '2017-09-07 19:02:35', NULL, '0000-00-00 00:00:00'),
(80, 'Germán', 'gerrv@hotmail.com', 'gerrv@hotmail.com', '$2y$10$tBfif03OgFgkEc/uvB6WmOl49IcAA4.YZbh6.HsVML0Q3CAiG2/9y', 'N', 'Y', 'N', 'N', '2017-09-09 18:51:33', '2017-09-09 18:51:33', NULL, '0000-00-00 00:00:00'),
(81, 'Germán', 'german.rivera@plusintel.com', 'german.rivera@plusintel.com', '$2y$10$EvvIEhgS1rExpWk03a2/P.gEw2RM7O1OynhaqJTbt3kTy1cLlXs.O', 'N', 'Y', 'N', 'N', '2017-09-09 18:52:46', '2017-09-09 18:52:46', NULL, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_password_resets`
--

CREATE TABLE `sis_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sis_password_resets`
--

INSERT INTO `sis_password_resets` (`email`, `token`, `created_at`) VALUES
('katiana@tripstravelagency.com', 'a496b24d12f8c79e392575c76da96e0b1dea7cd0ea4f247bb7c82e2db186e247', '2017-03-09 08:43:59'),
('tahwil.helcias1@gmail.com', 'e196cbbdafc31bb5850c68b8cc3425a698ab11355261cc4771ecf3ed6c8eece2', '2017-03-18 06:12:09'),
('andres@cowo.com.co', '33292204ab6226c90d1684ad23ea90074e9fd9acaeb47b84de85b07974273d2f', '2017-05-10 21:10:55'),
('marialexa555@gmail.com', 'd4be21c52c44408455e5d274ef56a7d42bf11736916f4e025d2a7b91eefe199d', '2017-07-26 10:00:10'),
('cmarino@hof.com.co', '3733d6d3a60f097f14ecef7595e653fd9dfb198985f2b3c42ba175527da9f7d9', '2017-09-09 03:21:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_ped_compra`
--

CREATE TABLE `sis_ped_compra` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_paquete` int(11) DEFAULT NULL,
  `creditos` int(11) NOT NULL DEFAULT '0',
  `fecha_pago` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `valor` decimal(10,2) NOT NULL DEFAULT '0.00',
  `estado` enum('P','E') COLLATE utf8_unicode_ci DEFAULT 'E',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sis_ped_compra`
--

INSERT INTO `sis_ped_compra` (`id`, `id_cliente`, `id_paquete`, `creditos`, `fecha_pago`, `valor`, `estado`, `created_at`, `updated_at`) VALUES
(1, 40, 10, 0, '2017-06-01 22:36:13', '117810.00', 'P', '2017-06-01 22:36:13', '2017-06-01 22:36:13'),
(2, 62, NULL, 0, '2017-09-07 14:08:01', '47600.00', 'E', '2017-09-07 14:08:01', '2017-09-07 14:08:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_ped_contratacion`
--

CREATE TABLE `sis_ped_contratacion` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_servicio` int(11) NOT NULL,
  `activo` enum('S','N') COLLATE utf8_unicode_ci DEFAULT 'S',
  `fecha_vence` date DEFAULT NULL,
  `id_sala` int(11) NOT NULL,
  `fecha_inicio` timestamp NULL DEFAULT NULL,
  `fecha_fin` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sis_ped_contratacion`
--

INSERT INTO `sis_ped_contratacion` (`id`, `id_cliente`, `id_servicio`, `activo`, `fecha_vence`, `id_sala`, `fecha_inicio`, `fecha_fin`, `created_at`, `updated_at`) VALUES
(1, 3, 4, 'S', '2017-02-23', 1, '2017-02-25 00:23:38', '2017-02-25 01:23:38', '2017-02-24 03:43:41', '2017-02-24 03:45:52'),
(2, 1, 4, 'S', '2017-03-02', 1, '2017-03-09 13:30:19', '2017-03-09 14:30:19', '2017-03-02 21:39:08', '2017-03-02 21:39:30'),
(3, 14, 8, 'S', '2017-04-08', 0, NULL, NULL, '2017-03-08 20:15:58', '2017-03-08 20:15:58'),
(4, 1, 4, 'S', '2017-03-10', 0, NULL, NULL, '2017-03-10 22:41:28', '2017-03-10 22:41:28'),
(5, 1, 4, 'S', '2017-03-10', 0, NULL, NULL, '2017-03-10 22:41:55', '2017-03-10 22:41:55'),
(6, 11, 4, 'S', '2017-03-21', 0, NULL, NULL, '2017-03-22 01:52:55', '2017-03-22 01:52:55'),
(10, 1, 4, 'S', '2017-05-10', 1, '2017-05-12 14:00:00', '2017-05-12 15:00:00', '2017-05-11 02:46:21', '2017-05-11 02:46:42'),
(11, 1, 4, 'S', '2017-05-11', 1, '2017-05-15 19:00:00', '2017-05-15 20:00:00', '2017-05-11 20:09:19', '2017-05-11 20:11:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_ped_intento_pago`
--

CREATE TABLE `sis_ped_intento_pago` (
  `id` int(11) NOT NULL,
  `id_compra` int(11) DEFAULT NULL,
  `id_token` int(11) DEFAULT NULL,
  `fecha` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fecha_transaccion` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `respuesta` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `franquicia` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `num_transaccion` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `cod_aprobacion` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `ref_payco` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `mensaje` text CHARACTER SET latin1,
  `estado` enum('P','A','R','V') CHARACTER SET latin1 DEFAULT 'P',
  `monto` decimal(10,2) NOT NULL DEFAULT '0.00',
  `moneda` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sis_ped_intento_pago`
--

INSERT INTO `sis_ped_intento_pago` (`id`, `id_compra`, `id_token`, `fecha`, `fecha_transaccion`, `respuesta`, `franquicia`, `num_transaccion`, `cod_aprobacion`, `ref_payco`, `mensaje`, `estado`, `monto`, `moneda`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, '0000-00-00 00:00:00', '2017-06-01 22:35:46', '1', 'Visa', '48771496356640', '173600', '308378', '00-Aprobada', 'A', '117810.00', 'COP', '2017-06-01 22:36:13', '2017-06-01 22:36:13'),
(2, NULL, 2, '0000-00-00 00:00:00', '2017-09-07 14:07:18', '3', 'EF', '48771505188066', '000000', '394827', 'P004-Esperando pago del cliente en punto de servicio Efecty', 'V', '47600.00', 'COP', '2017-09-07 14:08:01', '2017-09-07 14:08:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_ped_membresia`
--

CREATE TABLE `sis_ped_membresia` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_paquete` int(11) NOT NULL,
  `activo` enum('S','N') COLLATE utf8_unicode_ci DEFAULT 'S',
  `fecha_inicio` date NOT NULL DEFAULT '0000-00-00',
  `fecha_vence` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sis_ped_membresia`
--

INSERT INTO `sis_ped_membresia` (`id`, `id_cliente`, `id_paquete`, `activo`, `fecha_inicio`, `fecha_vence`, `created_at`, `updated_at`) VALUES
(1, 2, 4, 'S', '2017-02-23', '2017-03-23', '2017-02-23 18:00:15', '2017-02-23 18:00:15'),
(2, 4, 7, 'N', '2017-02-24', '2017-03-24', '2017-02-24 16:33:47', '2017-03-21 15:24:23'),
(3, 8, 5, 'S', '2017-03-02', '2017-04-02', '2017-03-02 12:47:00', '2017-03-02 12:47:00'),
(4, 10, 7, 'S', '2017-03-06', '2017-04-06', '2017-03-06 17:39:01', '2017-03-06 17:39:01'),
(5, 12, 7, 'S', '2017-03-07', '2017-04-07', '2017-03-07 13:07:51', '2017-03-07 13:07:51'),
(6, 14, 7, 'S', '2017-03-08', '2017-04-08', '2017-03-08 15:02:25', '2017-03-08 15:02:25'),
(7, 22, 6, 'S', '2017-03-09', '2017-04-09', '2017-03-09 15:37:45', '2017-03-09 15:37:45'),
(8, 5, 7, 'S', '2017-02-15', '2017-03-15', '2017-03-14 17:05:49', '2017-03-14 17:05:49'),
(9, 18, 7, 'S', '2017-02-16', '2017-03-16', '2017-03-14 17:07:43', '2017-03-14 17:07:43'),
(10, 11, 7, 'S', '2017-02-26', '2017-03-26', '2017-03-14 17:09:30', '2017-03-14 17:09:30'),
(11, 24, 7, 'S', '2017-03-10', '2017-04-10', '2017-03-15 20:40:01', '2017-03-15 20:40:01'),
(12, 27, 7, 'S', '2017-03-10', '2017-04-10', '2017-03-16 17:22:18', '2017-03-16 17:22:18'),
(13, 15, 7, 'S', '2017-03-27', '2017-04-27', '2017-03-21 15:23:31', '2017-03-21 15:23:31'),
(14, 4, 7, 'S', '2017-02-24', '2017-03-24', '2017-03-21 15:26:53', '2017-03-21 15:26:53'),
(15, 40, 10, 'S', '2017-06-01', '2017-07-01', '2017-06-01 22:36:13', '2017-06-01 22:36:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_ped_token`
--

CREATE TABLE `sis_ped_token` (
  `id` int(11) NOT NULL,
  `id_compra` int(11) DEFAULT NULL,
  `token` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `fecha_vencimiento` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sis_ped_token`
--

INSERT INTO `sis_ped_token` (`id`, `id_compra`, `token`, `fecha_vencimiento`, `created_at`, `updated_at`) VALUES
(1, 1, 'kboR8tfcAm9k2OLN1YVY', NULL, '2017-06-01 22:36:13', '2017-06-01 22:36:13'),
(2, 2, 'HOPBkH4IzHk4IjYkQYLE', NULL, '2017-09-07 14:08:01', '2017-09-07 14:08:01');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `sis_cat_paquete`
--
ALTER TABLE `sis_cat_paquete`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_sis_cat_paquete_sis_cat_servicio` (`id_servicio`);

--
-- Indices de la tabla `sis_cat_servicio`
--
ALTER TABLE `sis_cat_servicio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sis_con_aliado`
--
ALTER TABLE `sis_con_aliado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sis_con_configuracion`
--
ALTER TABLE `sis_con_configuracion`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `llave` (`llave`);

--
-- Indices de la tabla `sis_con_contenido`
--
ALTER TABLE `sis_con_contenido`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sis_con_faq`
--
ALTER TABLE `sis_con_faq`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sis_con_foto`
--
ALTER TABLE `sis_con_foto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_galeria` (`id_galeria`);

--
-- Indices de la tabla `sis_con_galeria`
--
ALTER TABLE `sis_con_galeria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sis_con_menu`
--
ALTER TABLE `sis_con_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_seccion` (`id_seccion`);

--
-- Indices de la tabla `sis_con_testimonio`
--
ALTER TABLE `sis_con_testimonio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sis_forum_categories`
--
ALTER TABLE `sis_forum_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sis_forum_posts`
--
ALTER TABLE `sis_forum_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sis_forum_threads`
--
ALTER TABLE `sis_forum_threads`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sis_par_cliente`
--
ALTER TABLE `sis_par_cliente`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `idx_id_usuario_cliente_fk` (`id_usuario`);

--
-- Indices de la tabla `sis_par_usuario`
--
ALTER TABLE `sis_par_usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indices de la tabla `sis_password_resets`
--
ALTER TABLE `sis_password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `sis_ped_compra`
--
ALTER TABLE `sis_ped_compra`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cliente` (`id_cliente`),
  ADD KEY `id_paquete` (`id_paquete`);

--
-- Indices de la tabla `sis_ped_contratacion`
--
ALTER TABLE `sis_ped_contratacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cliente` (`id_cliente`),
  ADD KEY `id_servicio` (`id_servicio`);

--
-- Indices de la tabla `sis_ped_intento_pago`
--
ALTER TABLE `sis_ped_intento_pago`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_compra` (`id_compra`);

--
-- Indices de la tabla `sis_ped_membresia`
--
ALTER TABLE `sis_ped_membresia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cliente` (`id_cliente`),
  ADD KEY `id_paquete` (`id_paquete`);

--
-- Indices de la tabla `sis_ped_token`
--
ALTER TABLE `sis_ped_token`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_compra` (`id_compra`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `sis_cat_paquete`
--
ALTER TABLE `sis_cat_paquete`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `sis_cat_servicio`
--
ALTER TABLE `sis_cat_servicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `sis_con_aliado`
--
ALTER TABLE `sis_con_aliado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT de la tabla `sis_con_configuracion`
--
ALTER TABLE `sis_con_configuracion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `sis_con_contenido`
--
ALTER TABLE `sis_con_contenido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT de la tabla `sis_con_faq`
--
ALTER TABLE `sis_con_faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `sis_con_foto`
--
ALTER TABLE `sis_con_foto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `sis_con_galeria`
--
ALTER TABLE `sis_con_galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `sis_con_menu`
--
ALTER TABLE `sis_con_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `sis_con_testimonio`
--
ALTER TABLE `sis_con_testimonio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `sis_forum_categories`
--
ALTER TABLE `sis_forum_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `sis_forum_posts`
--
ALTER TABLE `sis_forum_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `sis_forum_threads`
--
ALTER TABLE `sis_forum_threads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `sis_par_cliente`
--
ALTER TABLE `sis_par_cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT de la tabla `sis_par_usuario`
--
ALTER TABLE `sis_par_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT de la tabla `sis_ped_compra`
--
ALTER TABLE `sis_ped_compra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `sis_ped_contratacion`
--
ALTER TABLE `sis_ped_contratacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `sis_ped_intento_pago`
--
ALTER TABLE `sis_ped_intento_pago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `sis_ped_membresia`
--
ALTER TABLE `sis_ped_membresia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `sis_ped_token`
--
ALTER TABLE `sis_ped_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `sis_cat_paquete`
--
ALTER TABLE `sis_cat_paquete`
  ADD CONSTRAINT `FK_sis_cat_paquete_sis_cat_servicio` FOREIGN KEY (`id_servicio`) REFERENCES `sis_cat_servicio` (`id`);

--
-- Filtros para la tabla `sis_con_menu`
--
ALTER TABLE `sis_con_menu`
  ADD CONSTRAINT `sis_con_menu_ibfk_1` FOREIGN KEY (`id_seccion`) REFERENCES `sis_con_contenido` (`id`);

--
-- Filtros para la tabla `sis_par_cliente`
--
ALTER TABLE `sis_par_cliente`
  ADD CONSTRAINT `idx_id_usuario_cliente_fk` FOREIGN KEY (`id_usuario`) REFERENCES `sis_par_usuario` (`id`);

--
-- Filtros para la tabla `sis_ped_compra`
--
ALTER TABLE `sis_ped_compra`
  ADD CONSTRAINT `sis_ped_compra_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `sis_par_cliente` (`id`),
  ADD CONSTRAINT `sis_ped_compra_ibfk_2` FOREIGN KEY (`id_paquete`) REFERENCES `sis_cat_paquete` (`id`);

--
-- Filtros para la tabla `sis_ped_contratacion`
--
ALTER TABLE `sis_ped_contratacion`
  ADD CONSTRAINT `sis_ped_contratacion_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `sis_par_cliente` (`id`),
  ADD CONSTRAINT `sis_ped_contratacion_ibfk_2` FOREIGN KEY (`id_servicio`) REFERENCES `sis_cat_servicio` (`id`);

--
-- Filtros para la tabla `sis_ped_intento_pago`
--
ALTER TABLE `sis_ped_intento_pago`
  ADD CONSTRAINT `sis_ped_intento_pago_ibfk_1` FOREIGN KEY (`id_compra`) REFERENCES `sis_ped_compra` (`id`);

--
-- Filtros para la tabla `sis_ped_membresia`
--
ALTER TABLE `sis_ped_membresia`
  ADD CONSTRAINT `sis_ped_membresia_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `sis_par_cliente` (`id`),
  ADD CONSTRAINT `sis_ped_membresia_ibfk_2` FOREIGN KEY (`id_paquete`) REFERENCES `sis_cat_paquete` (`id`);

--
-- Filtros para la tabla `sis_ped_token`
--
ALTER TABLE `sis_ped_token`
  ADD CONSTRAINT `sis_ped_token_ibfk_1` FOREIGN KEY (`id_compra`) REFERENCES `sis_ped_compra` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
