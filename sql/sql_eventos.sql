create table sis_eve_eventos(
    id int not null primary key auto_increment,
    evento varchar(100) NOT NULL,
	descripcion varchar(150) NOT NULL,
    id_creador int not null,
    fecha date not null,
    lugar varchar(150) not null,
    imagen varchar(150) not null,
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    deleted_at timestamp NULL
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_eve_registro(
    id int not null primary key auto_increment,
    id_evento int NOT NULL,
	nombre varchar(200) NOT NULL,
    email varchar(200) not null,
    telefono int not null,
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    deleted_at timestamp NULL
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

ALTER TABLE sis_eve_registro ADD FOREIGN KEY (id_evento) REFERENCES sis_eve_eventos(id) ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO sis_con_contenido (id, titulo, llave, tipo, cuerpo, inicio, peso, created_at, updated_at, deleted_at) VALUES 
('0', 'Registro en evento Cowo', 'registro-en-evento-cowo', 'E', '<p><img src=\"http://www.cowo.com.co/template/images/logo.png\" width=\"276\" height=\"131\" /></p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"color: #ff0000;\">Estimado/a {{ $nombre_cliente }}:</span></p>\r\n<p>Tu registro al evento {{ $nombre_evento }} se ha realizado de forma exitosa.</p>\r\n<p>Descripci&oacute;n: {{ $descripcion_evento }}</p>\r\n<p>Fecha: {{ $fecha_evento }}</p>\r\n<p>Lugar: {{ $lugar_evento }}</p>\r\n<p>Gracias por escoger a <a href=\"http://cowo.com.co\">Cowo.com.co</a>.</p>', 'N', '0', '0000-00-00 00:00:00', '2017-10-23 20:33:42', NULL), 
('0', 'Registro en evento', 'registro-en-evento', 'E', '<p><img src=\"http://www.cowo.com.co/template/images/logo.png\" width=\"276\" height=\"131\" /></p>\r\n<p>&nbsp;</p>\r\n<p>El usuario &nbsp; {{ $nombre_cliente }} se ha registrado al evento {{ $nombre_evento }}.</p>\r\n<p>Descripci&oacute;n: {{ $descripcion_evento }}</p>\r\n<p>Fecha: {{ $fecha_evento }}</p>\r\n<p>Lugar: {{ $lugar_evento }}</p>\r\n<p></p>\r\n<p>Datos del usuario:</p>\r\n<p>Nombre: {{ $nombre_cliente }}</p>\r\n<p>Correo: {{ $correo_cliente }}</p>\r\n<p>Tel&eacute;fono: {{ $telefono_cliente }}</p>\r\n\r\n<p>Gracias por escoger a <a href=\"http://cowo.com.co\">Cowo.com.co</a>.</p>', 'N', '0', '0000-00-00 00:00:00', '2017-10-23 20:37:25', NULL);


UPDATE sis_con_contenido SET cuerpo = '<img src=\"{{ asset(\'/template/images/logo.png\') }}\"/>\r\n<br />\r\n<p><span style=\"color: #ff0000; font-size: 14pt;\"><strong>Hola</strong>&nbsp;{{ $nombre_cliente }}:</span></p>\r\n<p><br /><span style=\"font-size: 14pt;\">De acuerdo a tu solicitud, a continuaci&oacute;n encontrar&aacute;s la cotizaci&oacute;n requerida.</span></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 14pt;\"><strong>{!! $info_planes !!}</strong></span></p>\r\n<p><span style=\"font-size: 14pt;\">Todos nuestros planes incluyen Internet de alta velocidad, servicio de caf&eacute;, agua, te y fruta complementaria, zonas de entretenimiento, terrazas y eventos de networking. &nbsp;Adem&aacute;s contar&aacute;s con un sistema de cr&eacute;ditos con el cual puedes hacer uso de salas de juntas, ingresar invitados y utilizar los dem&aacute;s servicios de Cowo.&nbsp;</span></p>\r\n<p><span style=\"font-size: 14pt;\">&nbsp;</span></p>\r\n<p><span style=\"font-size: 14pt;\">Nos encantar&iacute;a que hicieras parte de nuestra comunidad.</span></p>\r\n<p><span style=\"font-size: 14pt;\">Cualquier duda o inquietud al respecto, no dudes en contactarnos&nbsp;</span><span style=\"font-size: 14pt;\">en los siguientes telefonos:&nbsp;3044990158</span><br /><br /><span style=\"font-size: 14pt;\">Si deseas responder a este correo, por favor hazlo a info@cowo.com.co.</span><br /><br /><span style=\"font-size: 14pt;\">Atentamente,&nbsp;</span><br /><br /></p>\r\n<div><span style=\"font-size: 14pt;\">CoWo</span></div>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', deleted_at = NULL WHERE sis_con_contenido.id = 1;