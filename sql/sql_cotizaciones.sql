ALTER TABLE sis_ped_compra DROP foreign key FK_sis_ped_compra_sis_par_descuento;

alter table sis_ped_compra 
    drop id_descuento;
	
drop table sis_par_descuento;

create table sis_par_cotizacion(
    id int not null primary key auto_increment,
    fecha_creacion timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    id_creador int not null,
    correo varchar(200),
    estado char(1),
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    deleted_at timestamp NULL,
    foreign key (id_creador) references sis_par_usuario(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_par_descuento(
    id int not null primary key auto_increment,
    id_cotizacion int not null,
    id_paquete int not null,
    precio INT NOT NULL,
    horas INT NOT NULL,
    cantidad INT NOT NULL,
    meses INT NOT NULL,
    valor decimal(10, 2),
    fecha_vencimiento date,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_cotizacion) references sis_par_cotizacion(id),
    foreign key (id_paquete) references sis_cat_paquete(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

alter table sis_ped_compra 
   add column id_descuento int default null after valor;
   
ALTER TABLE sis_ped_compra
	ADD CONSTRAINT FK_sis_ped_compra_sis_par_descuento FOREIGN KEY (id_descuento) REFERENCES sis_par_descuento (id);

