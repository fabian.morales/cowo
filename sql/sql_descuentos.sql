create table sis_par_descuento(
    id int not null primary key auto_increment,
    fecha_creacion timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    id_creador int not null,
    id_paquete int not null,
    correo varchar(200),
    valor decimal(10, 2),
    estado enum('A', 'C'),
    fecha_vencimiento date,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_creador) references sis_par_usuario(id),
    foreign key (id_paquete) references sis_cat_paquete(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

alter table sis_ped_compra 
    add column descuento decimal(10, 2) default 0 after valor,
    add column id_descuento int default null after valor,
    add column iva decimal(10, 2) default 0 after valor;

ALTER TABLE sis_ped_compra
	ADD CONSTRAINT FK_sis_ped_compra_sis_par_descuento FOREIGN KEY (id_descuento) REFERENCES sis_par_descuento (id);
	
ALTER TABLE `sis_par_usuario`
 ADD COLUMN `comercial` ENUM ('S', 'N') DEFAULT 'N' after admin;
 
ALTER TABLE `sis_cat_paquete`
 ADD COLUMN `tipo` ENUM ('T', 'P') DEFAULT 'T' after id;
 
 ALTER TABLE `sis_cat_paquete`
 ADD COLUMN `imagen` varchar(200) after id_servicio;
 
 UPDATE sis_con_contenido SET cuerpo = '<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style="color: #ff0000; font-size: 14pt;"><strong>Hola</strong>&nbsp;{{ $nombre_cliente }}:</span></p>
<p><br /><span style="font-size: 14pt;">De acuerdo a tu solicitud, a continuaci&oacute;n encontrar&aacute;s la cotizaci&oacute;n requerida.</span></p>
<p style="text-align: center;"><span style="font-size: 14pt;"><strong>{!! $info_planes !!}</strong></span></p>
<p><span style="font-size: 14pt;">Todos nuestros planes incluyen Internet de alta velocidad, servicio de caf&eacute;, agua, te y fruta complementaria, zonas de entretenimiento, terrazas y eventos de networking. &nbsp;Adem&aacute;s contar&aacute;s con un sistema de cr&eacute;ditos con el cual puedes hacer uso de salas de juntas, ingresar invitados y utilizar los dem&aacute;s servicios de Cowo.&nbsp;</span></p>
<p><span style="font-size: 14pt;">&nbsp;</span></p>
<p><span style="font-size: 14pt;">Nos encantar&iacute;a que hicieras parte de nuestra comunidad.</span></p>
<p><span style="font-size: 14pt;">Cualquier duda o inquietud al respecto, no dudes en contactarnos&nbsp;</span><span style="font-size: 14pt;">en los siguientes telefonos:&nbsp;3044990158</span><br /><br /><span style="font-size: 14pt;">Si deseas responder a este correo, por favor hazlo a info@cowo.com.co.</span><br /><br /><span style="font-size: 14pt;">Atentamente,&nbsp;</span><br /><br /></p>
<div><span style="font-size: 14pt;">CoWo</span></div>
<p>&nbsp;</p>
<p>&nbsp;</p>' WHERE id = 1;

ALTER TABLE `sis_cat_paquete` ADD `credito_adicional` INT NOT NULL AFTER `valor`;

update sis_con_contenido set  cuerpo = '<p style="text-align: center;"><strong>{!! $detalle_plan !!}</strong></p>
<h2 style="text-align: center;">Descubre nuestros espacios:</h2>
<p>&nbsp;</p>
<p style="text-align: center;"><span>{!! $grid_fotos !!}</span></p>
<div class="fondo03">
<div class="container no-gutter">
<div class="row">
<div class="col-sm-12">
<h2><br />Comun&iacute;cate con nosotros:</h2>
<div class="col-sm-12">{!! $form_contacto !!}</div>
<div class="col-sm-12">&nbsp;</div>
</div>
</div>
</div>
</div>' where id = 35;


update sis_con_contenido set  cuerpo = '<div class="container">
<div class="row">
<h2 class="titulo seccion centro" style="text-align: center;"><span>Espacios de Trabajo y Planes</span></h2>
<p style="text-align: center;"><span>Todos nuestros planes incluyen beneficios y accesos a nuestra comunidad virtual, en donde podr&aacute;s tener un usuario para enterarte de las necesidades de la comunidad, expresar las tuyas, y estar al tanto de todos los eventos.</span></p>
<p style="text-align: center;"><span><strong>Todos nuestros planes f&iacute;sicos incluyen caf&eacute;, t&eacute; y agua ilimitada.</strong>&nbsp;</span></p>
<p style="text-align: center;"><em><strong>*Precios no incluyen iva.</strong>&nbsp;</em></p>
</div>
</div>
<p style="text-align: center;"><strong>&nbsp;{!! $bloque_planes !!}</strong></p>
<h2 style="text-align: center;">Descubre nuestros espacios:</h2>
<p>&nbsp;</p>
<p style="text-align: center;"><span>{!! $grid_fotos !!}</span></p>
<div class="fondo03">
<div class="container no-gutter">
<div class="row">
<div class="col-sm-12">
<h2><br />Comun&iacute;cate con nosotros:</h2>
<div class="col-sm-12">{!! $form_contacto !!}</div>
<div class="col-sm-12">&nbsp;</div>
</div>
</div>
</div>
</div>' where id = 13;

create table sis_log_credito(
    id int not null primary key auto_increment,
    id_creador int not null,
    id_cliente int not null,
    motivo text,
    cantidad int,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_creador) references sis_par_usuario(id),
    foreign key (id_cliente) references sis_par_cliente(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

ALTER TABLE sis_cat_paquete ADD desde CHAR(1) NOT NULL default 0 AFTER valor;

ALTER TABLE `sis_con_contenido` ADD `deleted_at` TIMESTAMP NULL DEFAULT NULL AFTER `updated_at`;

ALTER TABLE `sis_con_faq` ADD `deleted_at` TIMESTAMP NULL DEFAULT NULL AFTER `updated_at`;
