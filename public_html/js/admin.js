function calcularTotal(elem){
    fila = $(elem).parent().parent();
    descuento     = $(fila).find($(".descuento")).val() === "" ? 0 : $(fila).find($(".descuento")).val();
    valor         = $(fila).find($(".valor")).val() === "" ? 1 : $(fila).find($(".valor")).val();
    cantidad      = $(fila).find($(".cantidad")).val() === "" ? 1 : $(fila).find($(".cantidad")).val();
    meses         = $(fila).find($(".meses")).val()    === "" ? 1 : $(fila).find($(".meses")).val();
    horas         = $(fila).find($(".horas")).val()    === "" ? 1 : $(fila).find($(".horas")).val();
    
    if(valor === ''){
        $(fila).find($(".total")).val(0);
    }else{
        subtotal= ( valor - ( (valor * descuento) / 100 ) ) * cantidad * meses * horas;
        iva     = ( subtotal * 19 / 100 );
        total   = ( iva + subtotal );
        
        $(fila).find($(".subtotal")).val(subtotal);
        $(fila).find($(".iva")).val(Math.round(iva));
        $(fila).find($(".total")).val(Math.round(total));
    }
}

function agregarPlan(elem){
    var $uuid = uuid();
    var $fila = $("tr[rel='tr_planes']").filter(":first").clone(true, true);
    $fila.find("input, select").each(function(i, o){
        $(o).attr("name", $(o).attr("name").replace("[]", "[" + $uuid + "]"));
    });
    
    $fila.appendTo($(".planes"));
    
    $fila.find($("input")).val('');
    $fila.find(".suprimir").show();
}

function removerPlan(elem){
    $(elem).parent().parent().remove();
}

function buscarPlan(elem){
    campo = $(elem);
    $(campo).parent().parent().find($("input")).val('');
    if(campo.val() === 8){
        $(campo).parent().parent().find( $(".horas") ).removeAttr("readonly");
        $(campo).parent().parent().find( $(".cantidad, .meses") ).attr("readonly", "readonly");
    }else{
        $(campo).parent().parent().find( $(".cantidad, .meses") ).removeAttr("readonly");
        $(campo).parent().parent().find( $(".horas") ).attr("readonly", "readonly");
    }
    
    $.post("paquetes/postInfo", {"_token": $("input[name='_token']").val() , id:campo.val()}, function(data){
        subtotal= data.valor;
        iva     = subtotal * 19 / 100;
        total   = ( parseFloat(iva) + parseFloat(subtotal) );
        
        campo.parent().next().find($(".valor")).val( Math.round(subtotal) );
        campo.parent().parent().find($(".subtotal")).val( Math.round(subtotal) );
        campo.parent().parent().find($(".iva")).val( Math.round(iva) );
        campo.parent().parent().find($(".total")).val( Math.round(total) );
    });
}

function uuid() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

// (function (window, $) {

    /*var admin = {

    };
    
    window.admin = amdin;*/
    
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
    
    $(function() {
        
		$(".descuento, .valor, .cantidad, .meses, .horas").on("change keyup", function(){
			calcularTotal(this);
		});
		
        // $(".cantidad, .meses, .horas").attr("readonly", "readonly")
        
        $(".suprimir").filter(":first").hide();
        
        $('.tooltip-x').tooltipster({
            theme: 'tooltipster-borderless'
        });
        
        $("a[rel='borrar-usuario']").click(function(e) {
            if (!confirm('¿Está seguro de borrar este usuario?')){
                e.preventDefault();
            }
        });
        
        $("a[rel='borrar-paquete']").click(function(e) {
            if (!confirm('¿Está seguro de borrar este paquete?')){
                e.preventDefault();
            }
        });
        
        $("a[rel='borrar-galeria']").click(function(e) {
            if (!confirm('¿Está seguro de borrar esta galería?')){
                e.preventDefault();
            }
        });
        
        $("a[rel='agregar-foto']").click(function(e) {
            e.preventDefault();
            $("#id_foto").val('');
            $("#titulo_foto").val('');
            $("#descripcion_foto").val('');
            $("#meta_foto").val('');
            $.featherlight("#div_form_foto");
        });
        
        $("a[rel='borrar-foto']").click(function(e) {
            if (!confirm('¿Está seguro de borrar esta foto?')){
                e.preventDefault();
            }
        });
        
        $("a[rel='editar-foto']").click(function(e) {
            e.preventDefault();
            
            $.ajax({
                url: $(this).attr('href'),
                method: 'get',
                dataType: 'json'
            })
            .done(function(res){
                $("#id_foto").val(res.id);
                $("#titulo_foto").val(res.titulo);
                $("#descripcion_foto").val(res.descripcion);
                $("#meta_foto").val(res.meta);
                $.featherlight("#div_form_foto");
            });
        });
        
    });
// })(window, jQuery);