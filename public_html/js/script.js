(function (window, $) {
    
    var cotizador = {
        /*cargarForm: function($target) {
            $.ajax({
                url: '/cotizador/light',
                method: 'get',
                success: function(res){
                    $($target).html(res);
                    hookForm();
                }
            });
        },*/
        
        isEmail: function(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
    };
    
    window.cotizador = cotizador;
    
    function scrollTo(target) {
        var ooo = $(target).offset().top;
        console.log($(target));
        $('html, body').animate({scrollTop: ooo}, 600);
    }
    
    $(document).ready(function() {
        $(window).resize(function() {
            $(".menu-resp > a").each(function(i, o) {
                var $target = $(o).attr("href");
                if (!$(o).is(":visible")){
                    $($target).show();
                }
                else{
                    $($target).hide();
                }
            });
        });
        
        $("#telefono").keypress(function(e) {
            if (e.which < 48 || e.which > 57){
                e.preventDefault();
            }
        });        
        
        $("a[rel='scroll']").click(function(e){
            e.preventDefault();
            var $target = $(this).attr("href");            
            scrollTo($target);
        });
        
        $(".menu-resp > a").click(function(e) {
            e.preventDefault();
            var $target = $(this).attr("href");
            $($target).toggle("slow");
            console.log($($target));
        });
        
        var $datePickerOpc = {
            dateFormat: "yy-mm-dd",
            dayNames: [ "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado" ],
            dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
            dayNamesShort: [ "Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb" ],
            monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
            monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ],
            currentText: "Hoy",
            closeText: "Aceptar",
            showButtonPanel: true
        };
        
        $("input[rel='date-picker']").datepicker($datePickerOpc);
        
        $("form[rel='form-contacto-zoho']").submit(function(e) {
            e.preventDefault();
            $("#$loader").addClass("loading");
            var $form = $(this);
            $.ajax({
                url: $form.attr('action'),
                method: 'post',
                data: $form.serialize(),
                dataType: 'json'
            }).done(function(res){
                if (res.ok === 1){
                    window.location.href = '/gracias';
                }
                else{
                    alert(res.response);
                    grecaptcha.reset();
                }
            }).always(function() {
                $("#$loader").removeClass("loading");
            });
        });
    });
})(window, jQuery);
