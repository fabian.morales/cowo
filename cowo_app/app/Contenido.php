<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contenido extends \Illuminate\Database\Eloquent\Model
{
	use SoftDeletes;
	
    protected $table = 'con_contenido';
    protected $fillable = [
        'llave', 'titulo', 'tipo', 'cuerpo', 'peso', 'inicio'
    ];
}
