<?php

namespace App;

class Config extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'con_configuracion';
    protected $fillable = [
        'titulo', 'llave', 'contenido'
    ];
}
