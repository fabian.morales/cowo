<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faq extends \Illuminate\Database\Eloquent\Model
{
	use SoftDeletes;
	
    protected $table = 'con_faq';
    protected $fillable = [
        'titulo', 'descripcion', 'peso', 'activo'
    ];
}
