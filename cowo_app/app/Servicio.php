<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class Servicio extends \Illuminate\Database\Eloquent\Model
{
    use SoftDeletes;
    protected $table = 'cat_servicio';
    
    protected $fillable = [
        'nombre', 'creditos', 'periodicidad', 'vigencia'
    ];
}
