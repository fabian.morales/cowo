<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class Paquete extends \Illuminate\Database\Eloquent\Model
{
    use SoftDeletes;
    protected $table = 'cat_paquete';
    
    protected $fillable = [
        'tipo', 'nombre','creditos', 'valor', 'desde', 'credito_adicional', 'id_servicio', 'imagen', 'descripcion_corta', 'descripcion', 'periodicidad', 'vigencia', 'orden'
    ];
    
    public function servicio(){
        return $this->hasOne('\App\Servicio', 'id', 'id_servicio');
    }
    
    public function descuentos(){
        return $this->hasMany('\App\Descuento', 'id_paquete');
    }
    
    public function paquetePendiente(){
        return $this->hasMany('\App\PaquetePendiente', 'id_paquete');
    }
    
    public static function obtenerVigencias($periodo = 1){
        if($periodo < 1)
            $periodo = 1;
        return [
            'H' => 'PT'.($periodo).'H',
            'D' => 'P'.($periodo).'D',
            'Q' => 'P'.($periodo * 15).'D',
            'M' => 'P'.($periodo).'M',
            'T' => 'P'.($periodo * 3).'M',
            'S' => 'P'.($periodo * 6).'M',
            'A' => 'P'.($periodo).'Y'
        ];
    }
}
