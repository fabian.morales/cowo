<?php

namespace App;

class Testimonio extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'con_testimonio';
    protected $fillable = [
        'titulo', 'descripcion', 'activo'
    ];
}
