<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class Contratacion extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'ped_contratacion';
    
    public function cliente(){
        return $this->belongsTo("\App\Cliente", "id_cliente");
    }
    
    public function servicio(){
        return $this->belongsTo("\App\Servicio", "id_servicio");
    }
}
