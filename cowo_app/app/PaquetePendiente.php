<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class PaquetePendiente extends \Illuminate\Database\Eloquent\Model
{
    use SoftDeletes;
    protected $table = 'ped_paquete_pendiente';
    
    protected $fillable = [
        'id_cliente', 'id_paquete','cantidad'
    ];
    
    public function cliente(){
        return $this->hasOne('\App\Cliente', 'id', 'id_cliente');
    }
    
    public function paquete(){
        return $this->hasOne('\App\Paquete', 'id', 'id_paquete');
    }
}
