<?php

namespace App;

use \Illuminate\Support\Facades\Mail;

trait MailTrait{
    
    //var $destino1 = "cowocom@exodo.colombiahosting.com.co";
    var $destino1 = "info@cowo.com.co";
    //var $destino1 = "soporte.myc@gmail.com";
    var $destino2 = "soporte.myc@gmail.com";
    
    public function enviarMensaje($vars, $key, $emailDestino, $nombreDestino){
        $email = $this->obtenerMensaje($vars, $key);
        
        $files = [];
        if (key_exists('archivos', $vars) && sizeof($vars['archivos'])){
            foreach ($vars['archivos'] as $f){
                if (is_file($f)){
                    $files[] = $f;
                }
            }
        }
        
        /*Mail::send('emails.general', ["contenido" => $email["cuerpo"]], function($message) use($email, $emailDestino, $nombreDestino, $files) {
            $message->from($this->destino1, 'Cowo.com.co');
            $message->subject($email["titulo"]);
            $message->to($emailDestino, $nombreDestino);
            $message->bcc($this->destino2);
            
            if (sizeof($files)){
                foreach ($files as $f){
                    $message->attach($f);
                }
            }
        });*/
        
        $mail = new \PHPMailer\PHPMailer\PHPMailer(true);
        try {
            
            $mail->isSMTP();
            $mail->Host = env('MAIL_HOST');
            $mail->SMTPAuth = true;
            $mail->Username = env('MAIL_USERNAME');
            $mail->Password = env('MAIL_PASSWORD');
            $mail->SMTPSecure = env('MAIL_ENCRYPTION');
            $mail->Port = env('MAIL_PORT');

            $mail->setFrom($this->destino1, 'Cowo.com.co');
            $mail->addAddress($emailDestino, $nombreDestino);
            $mail->addBCC($this->destino2);
            
            if (sizeof($files)){
                foreach ($files as $f){
                    $mail->addAttachment($f);
                }
            }

            $mail->isHTML(true);
            $mail->Subject = $email["titulo"];
            $mail->Body    = $email["cuerpo"];
            //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();
            
        } catch (Exception $e) {
            //echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }
    
    public function obtenerMensaje($varsEmail, $keyCorreo){
        
        $vars = [
            "nombre_beneficiario" => '',
            "direccion_beneficiario" => '',
            "ciudad_beneficiario" => '',
            "correo_beneficiario" => '',
            "empresa_beneficiario" => '',
            "telefono_beneficiario" => '',
            
            "nombre_cliente" => '',
            "direccion_cliente" => '',
            "ciudad_cliente" => '',
            "correo_cliente" => '',
            "empresa_cliente" => '',
            "telefono_cliente" => '',
                        
            "nombre_comercial" => '',
            "cotizacion_fecha" => '',
            "cotizacion_estado" => '',
            
            "clave" => '',
            "nombre_plan" => '',
            "valor_plan" => '',
            "valor_compra" => '',
            "info_planes" => '',
            "cantidad" => '',
            "creditos" => '',
            "vencimiento_plan" => '',
            "nombre_servicio" => '',
            "id_sala" => '',
            "fecha_inicio_servicio" => '',
            "fecha_fin_servicio" => '',
            "link_plan" => '',
            "observaciones" => '',
            "estado_compra" => '',        
            
            "nombre_evento" => '',
            "descripcion_evento" => '',
            "fecha_evento" => '',
            "lugar_evento" => '',
            
            
        ];
        
        foreach ($varsEmail as $key => $v){
            if (key_exists($key, $vars)){
                $vars[$key] = $v;
            }
        }
        
        try{
            $contenido = Contenido::where("llave", $keyCorreo)->where("tipo", "E")->first();
            $html = \Blade::compileString($contenido->cuerpo);

            return ["titulo" => $contenido->titulo, "cuerpo" => $this->render($html, $vars)];
        } 
        catch (\Exception $e) {
            $file = public_path('BLOL.txt');
            file_put_contents($file, $e->getLine().' - '.$e->getMessage());
            die();
        }
        
    }
    
    function render($__php, $__data)
    {
        $obLevel = ob_get_level();
        ob_start();
        extract($__data, EXTR_SKIP);
        try {
            eval('?' . '>' . $__php);
        } catch (Exception $e) {
            while (ob_get_level() > $obLevel) ob_end_clean();
            throw $e;
        } catch (Throwable $e) {
            while (ob_get_level() > $obLevel) ob_end_clean();
            throw new FatalThrowableError($e);
        }
        return ob_get_clean();
    }
    
    
}