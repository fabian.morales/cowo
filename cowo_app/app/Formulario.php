<?php

namespace App;

class Formulario extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'frm_formulario';
    protected $fillable = [
        'titulo', 'descripcion', 'clase', 'destinatarios', 'col_nombre', 'col_email'
    ];
    
    public function campos(){
        return $this->hasMany('\App\CampoForm', 'id_form');
    }
    
    public function respuestas(){
        return $this->hasMany('\App\RespuestaForm', 'id_form');
    }
}
