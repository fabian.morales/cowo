<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventoRegistro extends \Illuminate\Database\Eloquent\Model
{
    use SoftDeletes;
    
    protected $table = 'eve_registro';
    
    
    function evento(){
        return $this->hasOne('\App\Evento', 'id', 'id_evento');
    }
}
