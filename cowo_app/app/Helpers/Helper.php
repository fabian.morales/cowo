<?php

namespace App\Helpers;

class Helper
{
    public static function cargueInicial($seccion=""){
        $menu = \App\Menu::orderBy("peso")->get();
        \View::share('main_menu', $menu);
        \View::share('inicio', 0);
        \View::share('meta_descripcion', '');
        \View::share('meta_keywords', '');
        \View::share('descripcion_home', '');
        //\View::share('titulo_sitio', 'Cowo');
        \View::share('seccion_sitio', $seccion);
        
        $contacto = \App\Config::where("tipo", "C")->get();
        $tmp = [];
        foreach ($contacto as $c){
            $tmp[$c->llave] = $c->contenido;
        }
        
        \View::share('cfg_contacto', $tmp);
        
        $config = \App\Config::where("tipo", "G")->get();
        foreach ($config as $c){
            \View::share($c->llave, $c->contenido);
        }
    }
    
    public static function number_format($value, $dcto = 0)
    {
        if($dcto != 0)
            return number_format((float)($value - ( ($value * $dcto) / 100 )), 0, ",", ".");
        else
            return number_format((float)$value, 0, ",", ".");
    }
    
    public static function generar_token(){
        return str_random(20);
    }
    
    public static function periodicidad($valor){
        $periodos = [
            'N' => 'No aplica',
            'H' => 'Hora',
            'D' => 'Diarios',
            'Q' => 'Quincenales',
            'M' => 'Mensuales',
            'T' => 'Trimestrales',
            'S' => 'Semestrales',
            'A' => 'Anuales',
        ];
        
        return $periodos[$valor];
    }
    
    public static function calcularConIva($valor, $descuento = 0){
        $subtotal= $valor - ( ($valor * $descuento) / 100 );
        $iva     = $subtotal * 19 / 100;
        $total   = $iva + $subtotal;
        
        return $total;
    }
    
    public static function calcularIva($valor, $descuento = 0){
        $subtotal= $valor - ( ($valor * $descuento) / 100 );
        $iva     = $subtotal * 19 / 100;
        return $iva;
    }
    
    
    public static function estados(){
        return array(
            'C' => 'Cotizado',
            'S' => 'Segundo contacto',
            'T' => 'Tercer contacto',
            'P' => 'Perdido',
            'G' => 'Ganado',
        );
    }
    
    public static function validarCaptcha($response, $remote){
        //$secret = "6Lec9RgUAAAAAMY9VuUJgRc51dvh7PErw5_aAuDH";
        
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $fields = [
            'secret' => '6Lec9RgUAAAAAMY9VuUJgRc51dvh7PErw5_aAuDH',
            'response' => $response,
            'remoteip' => $remote
        ];

        $fields_string = '';
        foreach($fields as $key=>$value) {
            $fields_string .= $key.'='.$value.'&'; 
        }

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = json_decode(curl_exec($ch));
        curl_close($ch);
        
        $retorno = false;
        if (isset($result->success) && $result->success){
            $retorno = true;
        }

        return $retorno;
    }
}