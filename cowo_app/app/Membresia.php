<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class Membresia extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'ped_membresia';
    
    public function cliente(){
        return $this->belongsTo("\App\Cliente", "id_cliente");
    }
    
    public function paquete(){
        return $this->belongsTo("\App\Paquete", "id_paquete");
    }
}
