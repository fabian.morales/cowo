<?php

namespace App;

class Cliente extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'par_cliente';
    
    protected $fillable = [
        'nombre', 'apellido', 'tipo_documento', 'documento_identidad', 'telefono', 'email', 'empresa', 'direccion', 'ciudad', 'publico'
    ];
    
    public function cotizaciones(){
        return $this->hasMany('\App\Cotizacion', 'email', 'correo');
    } 
    
    
    public function usuario(){
        return $this->belongsTo("\App\User", "id_usuario");
    }
    
    public function logCreditos(){
        return $this->hasMany("\App\LogCredito", "id_cliente");
    }
    
    public function compras(){
        return $this->hasMany('\App\Compra', 'id_cliente');
    }
    
    function contrataciones(){
        return $this->hasMany('App\Contratacion', 'id_cliente');
    }
    
    function membresias(){
        return $this->hasMany('App\Membresia', 'id_cliente');
    }
    
    function paquetePendiente(){
        return $this->hasMany('App\PaquetePendiente', 'id_cliente');
    }
    
    function membresiaActual(){
        return $this->hasMany('App\Membresia', 'id_cliente')->where('activo', 'S')->where('fecha_vence', '>=', date('Y-m-d'))->orderBy("fecha_vence")->take(1);
    }
    
    function adquirirMembresia($plan, $periodicidad = 1){
        date_default_timezone_set('America/Bogota');
        /*
        if (!sizeof($plan)){
            $plan = \App\Paquete::find($idPlan);
            if (!sizeof($plan)){
                throw new \Exception('Plan no válido');
            }
        }
        */
        $fecha = new \DateTime();
        $membresiaActual = $this->membresiaActual()->first();
        $membresia = new \App\Membresia();

        if (sizeof($membresiaActual)){
            $planActual = $membresiaActual->paquete()->first();
            
            if ($planActual->id == $plan->id){
                $fecha = new \DateTime($membresiaActual->fecha_vence);
                $fecha->add(new \DateInterval('P1D'));
            }
            elseif ($planActual->valor >= $plan->valor || in_array($plan->periodicidad, ['N', 'H', 'D'])){
                throw new \Exception('No se puede activar el plan porque ya posee uno de mayor valor');
            }
            else{
                $membresiaActual->activo = 'N';
                if (!$membresiaActual->save()) {
                    throw new \Exception('No se pudo desactivar el plan actual');
                }
            }
        }

        $vigencias = Paquete::obtenerVigencias($periodicidad);

        if (!in_array($plan->periodicidad, ['N', 'H', 'D'])){
            $membresia->fecha_inicio = date('Y-m-d H:i:s');
            $membresia->id_cliente = $this->id;
            $membresia->id_paquete = $plan->id;
            $membresia->activo = 'S';    

            $membresia->fecha_vence = $fecha->add(new \DateInterval($vigencias[$plan->periodicidad]));
            if (!$membresia->save()) {
                throw new \Exception('No se pudo activar el nuevo plan');
            }
        }
        
        return $membresia;
    }
    
    function contratarServicio($servicio, $idCliente = NULL){
        date_default_timezone_set('America/Bogota');
        /*
        if (!sizeof($servicio)){
            $servicio = \App\Servicio::find($idServicio);
            if (!sizeof($servicio)){
                throw new \Exception('Servicio no válido');
            }
        }
        */
        
        $fecha = new \DateTime();
        $vigencias = Paquete::obtenerVigencias();
        
        $con = new \App\Contratacion();
        $con->id_cliente = $idCliente !== NULL ? $idCliente : $this->id_cliente;
        $con->id_servicio = $servicio->id;
        $con->fecha_vence = $fecha->add(new \DateInterval($vigencias[$servicio->periodicidad]));
        $con->fecha_inicio = null;
        $con->fecha_fin = null;
        $con->activo = 'S';

        if (!$con->save()){
            throw new \Exception('No se pudo contratar el servicio');
        }
        
        return $con;
    }
    
    
    function guardarPaquetesPendientes($idPaquete, $cantidad){
        $paq = new \App\PaquetePendiente();
        $paq->id_cliente = $this->id;
        $paq->id_paquete = $idPaquete;
        $paq->cantidad = $cantidad;

        if (!$paq->save()){
            throw new \Exception('No se pudo contratar el servicio');
        }
        
        return $paq;
    }
}
