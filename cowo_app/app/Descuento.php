<?php

namespace App;

class Descuento extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'par_descuento';
    protected $fillable = [
        'id_creador', 'id_paquete', 'correo', 'valor', 'estado', 'fecha_creacion', 'fecha_vencimiento'
    ];
    
    public function paquete(){
        return $this->hasOne("\App\Paquete", "id", "id_paquete");
    }
    
    public function cotizacion(){
        return $this->hasOne("\App\Cotizacion", "id", "id_cotizacion");
    }
    
    public function compra(){
        return $this->hasOne('\App\Compra', 'id', 'id_descuento');
    }
    
}
