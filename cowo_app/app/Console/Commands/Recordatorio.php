<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Recordatorio extends Command {
    use \App\MailTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cowo:recordatorio';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envía recordatorio a los usuarios que tienen planes próximos a vencerse';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        date_default_timezone_set('America/Bogota');
        $fecha = date('Y-m-d');
        $membresias = \App\Membresia::with(["cliente", "paquete"])->where('activo', 'S')->whereRaw('datediff(fecha_vence, \''.$fecha.'\') = 5')->get();
        foreach ($membresias as $m){
            $vars = [
                "nombre_cliente" => $m->cliente->nombre,
                "direccion_cliente" => $m->cliente->direccion,
                "ciudad_cliente" => $m->cliente->ciudad,
                "correo_cliente" => $m->cliente->email,
                "empresa_cliente" => $m->cliente->empresa,
                "nombre_plan" => $m->paquete->nombre,
                "valor_plan" => $m->paquete->valor,
                "vencimiento_plan" => $m->fecha_vence
            ];

            $this->enviarMensaje($vars, 'recordatorio_plan', $m->cliente->email, $m->cliente->nombre);
        }
    }
}
