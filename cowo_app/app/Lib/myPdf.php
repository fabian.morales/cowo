<?php

namespace App\Lib;

use DOMPDF;

define('DOMPDF_ENABLE_REMOTE', true);
require_once("dompdf/dompdf_config.inc.php");

class myPdf{
    public function render($html, $nombre){
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->render();
        $dompdf->stream($nombre, array('compress'=>1, 'Attachment' => 0));
    }
}