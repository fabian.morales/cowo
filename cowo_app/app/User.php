<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes;
    
    protected $table = 'par_usuario';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'login', 'email', 'admin', 'comercial', 'activo'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    
    public function cliente(){
        return $this->hasOne("\App\Cliente", "id_usuario");
    }
    
    public function logCreditos(){
        return $this->hasMany("\App\LogCredito", "id_creador");
    }
    
    public function obtenerImagen(){
        $file = public_path('storage/imagenes/usuario/'.md5($this->login).'.jpg');
        if (is_file($file)){
            $imagen_perfil = asset('storage/imagenes/usuario/'.md5($this->login).'.jpg');
        }
        else{
            $imagen_perfil = asset('imagenes/usuario.png');
        }
        
        return $imagen_perfil;
    }
}
