<?php

namespace App;

class TokenCompra extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'ped_token';
    
    public function compra(){
        return $this->belongsTo('\App\Compra', 'id_compra', 'id');
    }
    
    public function intentoPago(){
        return $this->hasMany('\App\IntentoPago', 'id_token');
    }
}
