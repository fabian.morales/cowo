<?php

namespace App;

class IntentoPago extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'ped_intento_pago';
    
    public function compra(){
        return $this->belongsTo('\App\Compra', 'id_compra', 'id');
    }
    
    public function token(){
        return $this->belongsTo('\App\TokenPedido', 'id_token', 'id');
    }
}
