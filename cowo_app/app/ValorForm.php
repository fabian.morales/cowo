<?php

namespace App;

class ValorForm extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'frm_valor';
    protected $fillable = [];
    
    public function formulario(){
        return $this->belongsTo('\App\Formulario', 'id_form', 'id');
    }
    
    public function campo(){
        return $this->belongsTo('\App\CampoForm', 'id_campo', 'id');
    }
    
    public function respuesta(){
        return $this->belongsTo('\App\RespuestaForm', 'id_respuesta', 'id');
    }
}
