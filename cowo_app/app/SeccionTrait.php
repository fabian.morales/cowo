<?php

namespace App;

trait SeccionTrait{
        
    public function obtenerSeccionesInicio(){
        $secciones = Contenido::where("tipo", "S")->where("inicio", "S")->orderBy("peso")->get();
        $ret = [];
        foreach ($secciones as $s){
            $ret[] = $this->obtenerSeccion($s);
        }
        
        return $ret;
    }
    
    public function obtenerPopup(){
        $popupHtml = null;
        
        $popupMostrado = \Session::get("popup_mostrado");
               
        if ($popupMostrado != 'S'){
            $popup = \App\Publicidad::where("activo", "S")->where("tipo", "P")->orderBy("created_at", "desc")->take(1)->first();
            if (sizeof($popup)){
                $popupHtml = $this->obtenerPublicidad($popup);    
            }
            \Session::put("popup_mostrado", "S");
        }
        
        return $popupHtml;
    }
    
    public function procesarVariables($contenido){
        $url = url('/cotizacion/enviar');
        $formContacto = "";
        $sliderTestimonios = "";
        $gridAliados = "";
        $acordeonFaqs = "";
        $bloqueLinks = "";
        $bloquePlanes = "";
        $detalle_plan = "";
        $bloqueServicios = "";
        $gridFotos = "";
        $formTrabaja = "";
        $eventos_list = "";
        $galerias = [];
        $patron_gal = "/\\\$grid_fotos\\|(.[^\\s\\!]*)/";
        $formularios = [];
        $patron_form = "/\\\$formulario\\|(.[^\\s\\!]*)/";
        
        if (strpos($contenido, "{!! \$eventos_list !!}") !== false){
            $eventos = \App\Evento::orderBy("id", "DESC")->take(20)->get();
            $eventos_list = \View::make("sesion.eventos", ["eventos" => $eventos])->render();
        }
        
        if (strpos($contenido, "{!! \$form_contacto !!}") !== false){
            $formContacto = \View::make('contenido.contacto')->render();
        }
        
        if (strpos($contenido, "{!! \$slider_testimonios !!}") !== false){
            $testimonios = Testimonio::where('activo', 'S')->get();
            $sliderTestimonios = \View::make('contenido.testimonios', ["testimonios" => $testimonios])->render();
        }
        
        if (strpos($contenido, "{!! \$grid_aliados !!}") !== false){
            $aliados = Aliado::where('activo', 'S')->orderBy("titulo")->get();
            $gridAliados = \View::make('contenido.grid_aliados', ["aliados" => $aliados])->render();
        }
        
        if (strpos($contenido, "{!! \$acordeon_faqs !!}") !== false){
            $faqs = Faq::where('activo', 'S')->orderBy("peso")->orderBy("titulo")->get();
            $acordeonFaqs = \View::make('contenido.faqs', ["faqs" => $faqs])->render();
        }
        
        if (strpos($contenido, "{!! \$bloque_links !!}") !== false){
            $menus = Menu::where("usar_en_bloque", "S")->orderBy("created_at", "desc")->take(3)->get();
            $bloqueLinks = \View::make('seccion.bloque_links', ["menus" => $menus])->render();
        }
        
        if (strpos($contenido, "{!! \$bloque_planes !!}") !== false){
            if(\Auth::id() != NULL){
                $planes = Paquete::where("tipo", "T")->orderBY("orden", "asc")->get();
            }
            else{
                $planes = Paquete::where("tipo", "T")->orderBY("orden", "asc")->get();
            }
                        
            $usuario = null;
            if (\Auth::check()){
                $usuario = User::where('id', \Auth::user()->id)->with('cliente.membresiaActual.paquete')->first();
                $planes = \App\Paquete::with(['descuentos' => function($q){
                            $q->orderBy('par_descuento.id', 'DESC')
                            ->get();
                        }, 'descuentos.cotizacion' => function($q){
                            $q->where('par_cotizacion.correo', \Auth::user()->email)
                            ->get();
                        }])->orderBY("orden", "asc")->get();    
                        
            }
            
            $bloquePlanes = \View::make('contenido.bloque_planes', ["planes" => $planes, "usuario" => $usuario])->render();
        }
        
        if (strpos($contenido, "{!! \$bloque_servicios !!}") !== false){
            $servicios = \App\Servicio::take(6)->get();
            $bloqueServicios = \View::make('contenido.bloque_servicios', ["servicios" => $servicios])->render();
        }
        
        if (strpos($contenido, "{!! \$grid_fotos !!}") !== false && strpos($contenido, "{!! \$grid_fotos| !!}") === false){
            $galeria = GaleriaFoto::with("fotos")->first();
            $gridFotos = \View::make('contenido.grid_fotos', ["fotos" => $galeria->fotos])->render();
        }
        
        if (strpos($contenido, "{!! \$grid_fotos|") !== false){
            $salida = [];
            preg_match_all($patron_gal, $contenido, $salida);
            
            if (count($salida) >= 2){
                foreach($salida[1] as $s){
                    $galerias["grid_fotos_".$s] = '';
                }
                
                $gals = GaleriaFoto::with("fotos")->whereIn("llave", $salida[1])->get();
                
                foreach ($gals as $g){
                    $tmpFotos = \View::make('contenido.grid_fotos', ["fotos" => $g->fotos])->render();
                    $galerias["grid_fotos_".$g->llave] = $tmpFotos;
                }
            }
        }
        
        if (strpos($contenido, "{!! \$detalle_plan") !== false){
            $plan =  \App\Paquete::find($idSeccion);
            
            $usuario = null;
            if (\Auth::check()){
                $usuario = User::where('id', \Auth::user()->id)->with('cliente.membresiaActual.paquete')->first();
            }
            
            $detalle_plan = \View::make('contenido.detalle_plan', ["plan" => $plan, "usuario" => $usuario])->render();
            
        }
        
        if (strpos($contenido, "{!! \$form_trabaja !!}") !== false){
            $formTrabaja = \View::make('contenido.form_trabaja_nosotros')->render();
        }
        
       if (strpos($contenido, "{!! \$formulario !!}") !== false && strpos($contenido, "{!! \$grid_fotos| !!}") === false){
            //$galeria = GaleriaFoto::with("fotos")->first();
            //$gridFotos = \View::make('contenido.grid_fotos', ["fotos" => $galeria->fotos])->render();
        }
        
        if (strpos($contenido, "{!! \$formulario|") !== false){
            $salida = [];
            preg_match_all($patron_form, $contenido, $salida);
            
            if (count($salida) >= 2){
                foreach($salida[1] as $s){
                    $formularios["formulario_".$s] = '';
                }
                
                $forms = \App\Formulario::whereIn("llave", $salida[1])->with(["campos" => function($q) {
                    $q->orderBy("orden");
                }])->get();
                
                foreach ($forms as $f){
                    $tmpForm = \View::make("contenido.formulario", ["form" => $f])->render();
                    $formularios["formulario_".$f->llave] = $tmpForm;
                }
            }
        }
        
        $vars = [
            "form_contacto" => $formContacto,
            "slider_testimonios" => $sliderTestimonios,
            "grid_aliados" => $gridAliados,
            "acordeon_faqs" => $acordeonFaqs,
            "bloque_links" => $bloqueLinks,
            "bloque_planes" => $bloquePlanes,
            "detalle_plan" => $detalle_plan,
            "bloque_servicios" => $bloqueServicios,
            "grid_fotos" => $gridFotos,
            "form_trabaja" => $formTrabaja,
            "eventos_list" => $eventos_list
        ];
        
        $cuerpo = $contenido;
        if (sizeof($galerias)){
            $vars = array_merge($vars, $galerias);
            $cuerpo = preg_replace($patron_gal, "\$grid_fotos_$1", $cuerpo);    
        }
        
        if (sizeof($formularios)){
            $vars = array_merge($vars, $formularios);
            $cuerpo = preg_replace($patron_form, "\$formulario_$1", $cuerpo);    
        }
        
        $html = \Blade::compileString($cuerpo);
        
        return $this->renderSeccion($html, $vars);
    }
    
    public function obtenerSeccion($seccion, $idSeccion=null){
                
        if (!sizeof($seccion)){
            $seccion = Contenido::find($idSeccion);
        }
        
        return $this->procesarVariables($seccion->cuerpo);
    }
    
    public function obtenerPublicidad($publicidad, $id=null){
                
        if (!sizeof($publicidad)){
            $publicidad = Publicidad::find($id);
        }
        
        return $this->procesarVariables($publicidad->cuerpo);
    }
    
    function renderSeccion($__php, $__data)
    {
        $obLevel = ob_get_level();
        ob_start();
        extract($__data, EXTR_SKIP);
        try {
            eval('?' . '>' . $__php);
        } catch (Exception $e) {
            while (ob_get_level() > $obLevel) ob_end_clean();
            throw $e;
        } catch (Throwable $e) {
            while (ob_get_level() > $obLevel) ob_end_clean();
            throw new FatalThrowableError($e);
        }
        return ob_get_clean();
    }
}