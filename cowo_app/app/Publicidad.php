<?php

namespace App;

class Publicidad extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'con_publicidad';
    
    protected $fillable = [
        'nombre', 'fecha_inicio', 'fecha_fin', 'cuerpo', 'mostrar_logueado', 'activo'
    ];
}
