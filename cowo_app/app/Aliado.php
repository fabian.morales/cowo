<?php

namespace App;

class Aliado extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'con_aliado';
    protected $fillable = [
        'titulo', 'descripcion', 'url', 'activo'
    ];
}
