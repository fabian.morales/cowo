<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cotizacion extends \Illuminate\Database\Eloquent\Model
{
    use SoftDeletes;
    
    protected $table = 'par_cotizacion';
    protected $fillable = [
        'id_creador', 'correo', 'estado', 'fecha_creacion'
    ];
    
    public function cliente(){
        return $this->hasOne('\App\Cliente', 'email', 'correo');
    } 
    
    public function usuario(){
        return $this->hasOne("\App\User", "id", "id_creador");
    }
    
    public function descuentos(){
        return $this->hasMany("\App\Descuento", "id_cotizacion", "id");
    }
}
