<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;

class Evento extends \Illuminate\Database\Eloquent\Model
{
    use SoftDeletes;
    
    protected $table = 'eve_eventos';
    
    function registros(){
        return $this->hasMany('\App\EventoRegistro', 'id_evento', 'id');
    }
}
