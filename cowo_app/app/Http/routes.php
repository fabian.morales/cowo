<?php

/*
  |--------------------------------------------------------------------------
  | Routes File
  |--------------------------------------------------------------------------
  |
  | Here is where you will register all of the routes in an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
//Route::pattern('id', '[0-9]+');

Route::group(array('middleware' => ['https']), function() {
    Route::get('/crearAdmin', function() {
        $usuario = App\User::where("email", "fabian.morales@outlook.com")->first();
        //$usuario->nombre = "Admin";
        //$usuario->login = "admin";
        $usuario->password = \Hash::make("Ev4nerv");
        $usuario->admin = "Y";
        print_r($usuario);
        $usuario->save();
    });

    Route::post('/buscador', 'BuscadorController@realizarBusqueda');

    /*
      |--------------------------------------------------------------------------
      | Application Routes
      |--------------------------------------------------------------------------
      |
      | This route group applies the "web" middleware group to every route
      | it contains. The "web" middleware group is defined in your HTTP
      | kernel and includes session state, CSRF protection, and more.
      |
     */
    
    Route::get('/', 'SeccionController@mostrarIndex');
    
    Route::get('/gracias', 'SeccionController@mostrarGracias');

    Route::get('/login', 'SesionController@mostrarIndex');
    Route::post('/login', 'SesionController@hacerLogin');
    Route::get('/logout', 'SesionController@hacerLogout');
    
    Route::post('/formularios/enviar', 'SeccionController@recibirFormulario');

    Route::group(array('prefix' => 'compra'), function() {
        
        Route::get('/paquete/{idCotizacion}/{idDescuento}', 'CompraController@mostrarBotonCompraCotizacion');
        
        Route::group(array('middleware' => ['auth']), function() {
            Route::post('/paquete/', 'CompraController@mostrarBotonCompraPlan');
            Route::get('/paquete/{id}', 'CompraController@mostrarBotonCompraPlan');
            Route::post('/creditos', 'CompraController@mostrarBotonCompraCreditos');
            Route::any('/respuesta', 'CompraController@mostrarRespuesta');            
        });
        
        Route::any('/confirmar', 'CompraController@procesarCompra');
    });

    Route::any('/seccion/{key}', 'SeccionController@mostrarSeccionMenu');
    Route::any('/contenido/{key}', 'SeccionController@mostrarSeccion');
    Route::any('/contacto/enviar', 'SeccionController@enviarContacto');
    Route::any('/contacto/zoho/enviar', 'SeccionController@recibirFormularioContactoZoho');

    Route::group(['prefix' => 'cliente', 'as' => 'cliente::'], function() {
        Route::post('/obtener/{email}', 'SesionController@obtenerCliente');
        Route::get('/registro', 'SesionController@crearUsuario');
        Route::post('/registro', 'SesionController@registrarUsuario');
        Route::any('/reservas', 'SesionController@obtenerReservas');
        Route::any('/reservas/{idSala}', 'SesionController@obtenerReservas');

        Route::get('/clave/email', 'Auth\PasswordController@getEmail');
        Route::post('/clave/email', 'Auth\PasswordController@postEmail');

        Route::get('/clave/reset/{token}', 'Auth\PasswordController@getReset');
        Route::post('/clave/reset', 'Auth\PasswordController@postReset');

        Route::group(array('middleware' => ['auth']), function() {
            Route::get('cotizacion/ver/{id}', 'CotizacionController@ver');
            
            Route::get('/perfil', 'SesionController@mostrarPerfil');
            Route::post('/contratar', 'SesionController@contratarServicio');
            Route::get('/asignar/{id}', 'SesionController@asignarPaquete');  
            Route::post('/guardar_asignacion', 'SesionController@guardarAsignacion');  
            Route::get('/reservar/{id}', 'SesionController@mostrarFormReserva');  
            Route::post('/reservar', 'SesionController@realizarReserva');  
            Route::get('/comunidad/{id}', 'SesionController@mostrarUsuario');  
            Route::post('/buscar', 'SesionController@buscarUsuario');  
            
            Route::get('/misreservas', 'SesionController@misReservas');  
            Route::post('/inforeserva', 'SesionController@infoReserva');  
            Route::post('/editar_reserva', 'SesionController@editarReserva');  
            Route::get('/eliminar_reserva/{id}', 'SesionController@eliminarReserva');  
        });
    });

    Route::group(array('prefix' => 'comercial', 'middleware' => ['comercial']), function() {
        Route::group(array('prefix' => 'cotizaciones'), function() {
            Route::get('/', 'Admin\CotizacionController@mostrarIndex');
            Route::get('/cotizar', 'Admin\CotizacionController@mostrarCotizador');
            Route::post('/enviar', 'Admin\CotizacionController@enviarCotizacion');
            Route::post('/paquetes/postInfo', 'Admin\PaqueteController@postInfo');
            Route::get('/ver/{id}', 'Admin\CotizacionController@verCotizacion');
            Route::get('/estados/{id}', 'Admin\CotizacionController@estadosCotizacion');
            Route::post('/estados/cambiar/{id}', 'Admin\CotizacionController@cambiarEstadoCotizacion');
            Route::get('/eliminar/{id}', 'Admin\CotizacionController@eliminarCotizacion');
        });
    });
    
    Route::group(array('prefix' => 'administrador', 'middleware' => ['permisos']), function() {
        Route::get('/', 'Admin\AdminController@mostrarIndex');
        Route::get('/contacto', 'Admin\ConfigController@editarContacto');
        Route::post('/contacto/guardar', 'Admin\ConfigController@guardarContacto');

        Route::get('/configuracion', 'Admin\ConfigController@editarConfig');
        Route::post('/configuracion/guardar', 'Admin\ConfigController@guardarConfig');

        Route::group(array('prefix' => 'eventos'), function() {
            Route::get('/', 'Admin\EventosController@index');
            Route::get('/crear', 'Admin\EventosController@create');
            Route::post('/guardar', 'Admin\EventosController@store');
            Route::get('/editar/{id}', 'Admin\EventosController@edit');
            Route::post('/modificar/{id}', 'Admin\EventosController@update');
            Route::get('/eliminar/{id}', 'Admin\EventosController@delete');
            
            Route::group(array('prefix' => 'registro'), function() {
                Route::get('/{id}', 'Admin\EventosRegistroController@index');
                Route::get('/eliminar/{id}', 'Admin\EventosRegistroController@delete');
            });
        
        });
        
        Route::group(array('prefix' => 'cliente'), function() {
            Route::post('/contratar/{id}', 'Admin\SesionController@contratarServicio');
            Route::get('/reservar/{id}', 'Admin\SesionController@mostrarFormReserva');
            Route::post('/reservar', 'Admin\SesionController@realizarReserva');
        });
        
        Route::group(array('prefix' => 'contenido'), function() {
            Route::get('/', 'Admin\ContenidoController@mostrarIndex');
            Route::get('/crear', 'Admin\ContenidoController@formCrearContenidoEmail');
            Route::get('/editar/{key}', 'Admin\ContenidoController@mostrarFormContenidoEmail');
            Route::post('/guardar', 'Admin\ContenidoController@guardarContenido');
            Route::get('/eliminar/{id}', 'Admin\ContenidoController@eliminarContenido');

            Route::group(array('prefix' => 'seccion'), function() {
                Route::get('/', 'Admin\ContenidoController@mostrarListaSecciones');
                Route::get('/crear', 'Admin\ContenidoController@crearSeccion');
                Route::get('/editar/{id}', 'Admin\ContenidoController@editarSeccion');
                Route::get('/borrar/{id}', 'Admin\ContenidoController@eliminarSeccion');
            });
        });
        
        Route::group(array('prefix' => 'formularios'), function() {
            Route::any('/', 'Admin\FormularioController@mostrarIndex');
            Route::get('/crear', 'Admin\FormularioController@crearForm');
            Route::get('/editar/{id}', 'Admin\FormularioController@editarForm');
            Route::post('/guardar', 'Admin\FormularioController@guardarForm');
            Route::get('/borrar/{id}', 'Admin\FormularioController@borrarForm');
            Route::get('/visualizar/{id}', 'Admin\FormularioController@visualizarForm');
            Route::post('/organizar', 'Admin\FormularioController@organizarCampos');
            Route::get('/respuestas', 'Admin\FormularioController@mostrarIndexRespuestas');
            Route::get('/respuestas/{id}', 'Admin\FormularioController@mostrarIndexRespuestas');
            Route::get('/respuestas/detalle/{id}', 'Admin\FormularioController@mostrarDetalleRespuesta');
            
            Route::group(array('prefix' => 'campos'), function() {
                Route::any('/', 'Admin\FormularioController@mostrarCampos');
                Route::get('/crear/{idForm}', 'Admin\FormularioController@crearFormCampo');
                Route::get('/editar/{id}', 'Admin\FormularioController@editarFormCampo');
                Route::post('/guardar', 'Admin\FormularioController@guardarFormCampo');
                Route::get('/borrar/{id}', 'Admin\FormularioController@borrarFormCampo');
            });
        });

        Route::group(array('prefix' => 'cotizaciones'), function() {
            Route::get('/', 'Admin\CotizacionController@mostrarIndex');
            Route::get('/cotizar', 'Admin\CotizacionController@mostrarCotizador');
            Route::post('/enviar', 'Admin\CotizacionController@enviarCotizacion');
            Route::post('/paquetes/postInfo', 'Admin\PaqueteController@postInfo');
            Route::get('/ver/{id}', 'Admin\CotizacionController@verCotizacion');
            Route::get('/estados/{id}', 'Admin\CotizacionController@estadosCotizacion');
            Route::post('/estados/cambiar/{id}', 'Admin\CotizacionController@cambiarEstadoCotizacion');
            Route::get('/eliminar/{id}', 'Admin\CotizacionController@eliminarCotizacion');
        });

        Route::group(array('prefix' => 'menus'), function() {
            Route::get('/', 'Admin\MenuController@mostrarIndex');
            Route::get('/crear', 'Admin\MenuController@crearMenu');
            Route::get('/editar/{id}', 'Admin\MenuController@editarMenu');
            Route::post('/guardar', 'Admin\MenuController@guardarMenu');
            Route::get('/borrar/{id}', 'Admin\MenuController@borrarMenu');
        });

        Route::group(array('prefix' => 'testimonios'), function() {
            Route::get('/', 'Admin\TestimonioController@mostrarIndex');
            Route::get('/crear', 'Admin\TestimonioController@crearTest');
            Route::get('/editar/{id}', 'Admin\TestimonioController@editarTest');
            Route::post('/guardar', 'Admin\TestimonioController@guardarTest');
        });

        Route::group(array('prefix' => 'faqs'), function() {
            Route::get('/', 'Admin\FaqController@mostrarIndex');
            Route::get('/crear', 'Admin\FaqController@crearFaq');
            Route::get('/editar/{id}', 'Admin\FaqController@editarFaq');
            Route::get('/eliminar/{id}', 'Admin\FaqController@eliminarFaq');
            Route::post('/guardar', 'Admin\FaqController@guardarFaq');
        });
        
        Route::group(array('prefix' => 'galerias'), function() {
            Route::get('/', 'Admin\GaleriaController@mostrarIndex');
            Route::get('/crear', 'Admin\GaleriaController@crearGaleria');
            Route::get('/editar/{id}', 'Admin\GaleriaController@editarGaleria');
            Route::get('/borrar/{id}', 'Admin\GaleriaController@borrarGaleria');
            Route::post('/guardar', 'Admin\GaleriaController@guardarGaleria');
        });

        Route::group(array('prefix' => 'fotos'), function() {
            Route::get('/', 'Admin\GaleriaController@mostrarIndex');
            Route::get('/editar/{id}', 'Admin\GaleriaController@editarFoto');
            Route::get('/borrar/{id}', 'Admin\GaleriaController@borrarFoto');
            Route::post('/guardar', 'Admin\GaleriaController@guardarFoto');
        });

        Route::group(array('prefix' => 'usuarios'), function() {
            Route::get('/', 'Admin\UsuarioController@mostrarIndex');
            Route::get('/crear', 'Admin\UsuarioController@crearUsuario');
            Route::get('/editar/{id}', 'Admin\UsuarioController@editarUsuario');
            Route::post('/guardar', 'Admin\UsuarioController@guardarUsuario');
            Route::post('/creditos/modificar', 'Admin\UsuarioController@modificarCreditos');
            Route::post('/membresia', 'Admin\UsuarioController@asignarPlan');
            Route::get('/borrar/{id}', 'Admin\UsuarioController@borrarUsuario');
            
            Route::get('/perfil/{id}', 'Admin\UsuarioController@mostrarPerfil');
        });

        Route::group(array('prefix' => 'paquetes'), function() {
            Route::any('/', 'Admin\PaqueteController@mostrarIndex');
            Route::get('/crear', 'Admin\PaqueteController@crearPaquete');
            Route::get('/editar/{id}', 'Admin\PaqueteController@editarPaquete');
            Route::post('/guardar', 'Admin\PaqueteController@guardarPaquete');
            Route::get('/borrar/{id}', 'Admin\PaqueteController@borrarPaquete');
            
            Route::post('/postInfo', 'Admin\PaqueteController@postInfo');
        });

        Route::group(array('prefix' => 'aliados'), function() {
            Route::any('/', 'Admin\AliadoController@mostrarIndex');
            Route::get('/crear', 'Admin\AliadoController@crearAliado');
            Route::get('/editar/{id}', 'Admin\AliadoController@editarAliado');
            Route::post('/guardar', 'Admin\AliadoController@guardarAliado');
            Route::get('/borrar/{id}', 'Admin\AliadoController@borrarAliado');
        });

        Route::group(array('prefix' => 'servicios'), function() {
            Route::any('/', 'Admin\ServicioController@mostrarIndex');
            Route::get('/crear', 'Admin\ServicioController@crearServicio');
            Route::get('/editar/{id}', 'Admin\ServicioController@editarServicio');
            Route::post('/guardar', 'Admin\ServicioController@guardarServicio');
            Route::get('/borrar/{id}', 'Admin\ServicioController@borrarServicio');
            Route::get('/reservas', 'Admin\ServicioController@mostrarReservas');
            Route::get('/get_reservas', 'Admin\ServicioController@todasLasReservas');
            Route::get('/crear_reserva', 'Admin\ServicioController@crearReserva');
            Route::post('/editar_reserva', 'Admin\ServicioController@editarReserva');
            Route::get('/cancelar/{id}', 'Admin\ServicioController@cancelarReserva');
            Route::post('/inforeserva', 'Admin\ServicioController@infoReserva');  
        });

        Route::group(array('prefix' => 'buscador'), function() {
            Route::group(array('prefix' => 'categorias'), function() {
                Route::get('/', 'Admin\BuscadorController@mostrarIndex');
                Route::get('/crear', 'Admin\BuscadorController@crearCategoria');
                Route::get('/editar/{id}', 'Admin\BuscadorController@editarCategoria');
                Route::post('/guardar', 'Admin\BuscadorController@guardarCategoria');
            });
        });
        
        Route::group(array('prefix' => 'publicidad'), function() {
            Route::any('/', 'Admin\PublicidadController@mostrarIndex');
            Route::get('/crear', 'Admin\PublicidadController@crearPublicidad');
            Route::get('/editar/{id}', 'Admin\PublicidadController@editarPublicidad');
            Route::post('/guardar', 'Admin\PublicidadController@guardarPublicidad');
            Route::get('/borrar/{id}', 'Admin\PublicidadController@borrarPublicidad');
        });
    });

    Route::get('/test', function() {
        $planes = App\Paquete::all();
        return View::make('compra.test', ["planes" => $planes, "menu" => null, "meta_descripcion" => "", "meta_keywords" => "", "titulo_seccion" => "Cowo"]);
        //\Illuminate\Support\Facades\Artisan::call('cowo:recordatorio');
    });

    Route::post('calendario/registro', 'SeccionController@registrarEnevento');
    
    if (!Request::is("forum")){
        Route::any('/{key}', 'SeccionController@mostrarSeccionMenu');        
    }
    
    Route::get('planes/{key}/{id}', 'SeccionController@mostrarSeccion');

});

