<?php 

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Comercial {

    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            }
            else {
                return redirect()->guest('/login');
            }
        }
        else{
            if (Auth::user()->comercial != "S"){
                return \Redirect::action('SesionController@mostrarIndex')->with("mensajeError", "No tiene permisos para acceder a esta sección");
            }
        }

        return $next($request);
    }

}
