<?php

namespace App\Http\Controllers;

use \Illuminate\Support\Facades\Input;
use \Illuminate\Support\Facades\Mail;
use \Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use \Riari\Forum\Models as Foro;

class SesionController extends Controller {
    use \App\MailTrait;
    
    public function infoReserva(){
        $info = \App\Contratacion::with('servicio')->find(Input::get('id'))->toJson();
        return $info;
    }
       
    public function mostrarIndex(){
        if (\Auth::check()){
            return $this->mostrarPerfil();
        }
        return \View::make('sesion.login');
    }
       
    public function hacerLogin(){
        $login = Input::get("login");
        $clave = Input::get("password");
        if (\Auth::attempt(array('login' => $login, 'password' => $clave))){
            if (\Auth::user()->activo != 'Y'){
                \Auth::logout();
                return \Redirect::action("SesionController@mostrarIndex")->with("mensajeError", "Usuario no activo");
            }
            
            if (\Auth::user()->admin == 'Y'){
                return \Redirect::action("Admin\AdminController@mostrarIndex")->with("mensaje", "Logueado");
            }
            else{
                return \Redirect::action("SesionController@mostrarPerfil")->with("mensaje", "Logueado");
            }
        }
        else{
            return \Redirect::action("SesionController@mostrarIndex")->with("mensajeError", "Usuario o clave incorrecta");
        }
    }
    
    public function hacerLogout(){
        \Auth::logout();
        return \Redirect::action("SesionController@mostrarIndex");
    }
    
    public function obtenerCliente($email){
        $cliente = \App\Cliente::where("email", $email)->first();
        
        if (sizeof($cliente)){
            return $cliente->toJson();
        }
        else
        {
            return json_encode([]);
        }
        
    }
    
    public function mostrarFormUsuario($usuario) {
        if (!sizeof($usuario)) {
            $usuario = new \App\User();
        }
        
        if (!sizeof($usuario->cliente)){
            $usuario->cliente = new \App\Cliente();
        }
        
        $tipos = ['CC', 'NIT', 'CE'];

        return \View::make("sesion.form_registro", array("usuario" => $usuario, "tipos" => $tipos));
    }

    public function crearUsuario() {
        return $this->mostrarFormUsuario(new \App\User());
    }

    public function editarUsuario($id) {
        $usuario = \App\User::where("id", $id)->with("cliente")->first();
        if (!sizeof($usuario)) {
            return \Redirect::action('SesionController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el usuario");
        }

        return $this->mostrarFormUsuario($usuario);
    }
    
    public function registrarUsuario(){
        try{
            $id = Input::get("id");
            $clave = Input::get("password");
            $nuevo = false;

            $usuario = \App\User::find($id);
            if (!sizeof($usuario)) {
                $usuario = new \App\User();
                $nuevo = true;
            }

            $cliente = $usuario->cliente;

            if (!sizeof($cliente)){
                $email = Input::get("email");
                $cliente = \App\Cliente::where("email", $email)->first();

                if (!sizeof($cliente)){
                    $cliente = new \App\Cliente();
                }
            }

            $usuario->fill(Input::all());

            $cliente->fill(Input::all());
            if (empty($cliente->publico)){
                $cliente->publico = 'S';
            }
            
            $imagen = Input::file("imagen");

            if (empty($cliente->nombre)) {
                throw new \Exception('Debe ingresar su nombre');
            }

            if (empty($cliente->apellido)) {
                throw new \Exception('Debe ingresar su apellido');
            }

            if (empty($cliente->email)) {
                throw new \Exception('Debe ingresar su dirección de correo');
            }

            if (empty($id) && empty($clave)) {
                throw new \Exception('Debe ingresar su clave');
            } 
            else if (!empty($clave)) {
                $clave = \Illuminate\Support\Facades\Hash::make($clave);
            } 
            else {
                $clave = $usuario->password;
            }

            $usuario->password = $clave;

            $cntEmail = \App\User::where("email", $usuario->email)->where("id", "<>", (int)$usuario->id)->count();
            if ($cntEmail > 0) {
                throw new \Exception('Ya existe un usuario con el correo ingresado');
            }

            $cntLogin = \App\User::where("login", $usuario->login)->where("id", "<>", (int)$usuario->id)->count();
            if ($cntLogin > 0) {
                throw new \Exception('Ya existe un usuario con el nombre de usuario ingresado');
            }
            
            if (empty($usuario->login)){
                $usuario->login = $usuario->email;
            }

            if ($usuario->save()) {
                $cliente->id_usuario = $usuario->id;

                if ($cliente->save()){
                    if ($nuevo){
                        $vars = [
                            "nombre_cliente" => $cliente->nombre,
                            "direccion_cliente" => $cliente->direccion,
                            "ciudad_cliente" => $cliente->ciudad,
                            "correo_cliente" => $cliente->email,
                            "empresa_cliente" => $cliente->empresa
                        ];
                        
                        $this->enviarMensaje($vars, 'registro', $cliente->email, $cliente->nombre);
                    }
                    
                    if ((sizeof($imagen) && $imagen->isValid())) {
                        if ($imagen->getClientSize() <= 1048576){
                            $path = public_path('storage/imagenes/usuario/');

                            if (!is_dir($path)) {
                                $partesPath = ['storage', 'imagenes', 'usuario'];
                                $subpath = public_path() . '/';
                                foreach ($partesPath as $p) {
                                    $subpath .= $p . '/';

                                    if (!is_dir($subpath)) {
                                        echo $subpath;
                                        mkdir($subpath);
                                    }
                                }
                            }

                            $file = md5($usuario->login).'.jpg';
                            $imagen->move($path, $file);                             
                        }
                    }

                    return \Redirect::action('SesionController@mostrarPerfil')->with('mensaje', ($nuevo ? 'Registro realizado exitosamente' : 'Perfil actualizado exitosamente'));
                }
                else{
                    throw new \Exception('No se pudo realizar el registro');
                }
            }
            else {
                throw new \Exception('No se pudo realizar el registro');
            }
        }
        catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
            return \Redirect::action('SesionController@mostrarPerfil')->with('mensajeError', $ex->getMessage());
        }
    }
    
    public function mostrarPerfil(){
        
        $usuario = \App\User::where("id", \Auth::user()->id)->with([
            "cliente.compras.paquete",
            "cliente.compras" => function($q) {
                $q->orderBy('created_at', 'desc')->take(25);
            }, 
            "cliente.logCreditos",
            "cliente.contrataciones.servicio",
            "cliente.contrataciones" => function($q) {
                $q->orderBy('created_at', 'desc')->take(25);
            }, 
            "cliente.membresiaActual.paquete"
        ])->first();
        
        $imagen_perfil = "";
        $file = public_path('storage/imagenes/usuario/'.md5($usuario->login).'.jpg');
        if (is_file($file)){
            $imagen_perfil = asset('storage/imagenes/usuario/'.md5($usuario->login).'.jpg');
        }
        else{
            $imagen_perfil = asset('imagenes/usuario.png');
        }
        
        $tipos = ['CC', 'NIT', 'CE'];
        $servicios = [];
        $pendientes = [];
        $planes = [];
        $planes_pendientes = [];
        $temas = [];
        $mostrarForo = false;
        
        if (sizeof($usuario->cliente)){
            $servicios = \App\Servicio::where("creditos", "<=", $usuario->cliente->creditos)->get();
            $pendientes = \App\Contratacion::with('servicio')
                    ->where('id_cliente', $usuario->cliente->id)
                    ->whereHas("servicio", function($q) {
                        $q->whereRaw("periodicidad in ('D', 'H')");
                    })
                    ->whereNull('fecha_inicio')
                    ->orderBy('created_at', 'desc')->get();
                    
            $planes_pendientes = \App\PaquetePendiente::with(['paquete', 'cliente'])
                    ->where('id_cliente', $usuario->cliente->id)
                    ->where("cantidad", ">", 0)
                    ->orderBy('created_at', 'desc')->get();
                    
            if (sizeof($usuario->cliente->membresiaActual)){
                $planes = \App\Paquete::where("valor", ">", $usuario->cliente->membresiaActual[0]->paquete->valor)
                        ->whereRaw("periodicidad not in ('H', 'D', 'N')")
                        ->orderBy("valor")
                        ->get();
                $mostrarForo = true;
            }
            
            if ($mostrarForo || $usuario->cliente->creditos > 0){
                $temas = Foro\Thread::with("category")->orderBy("created_at", "desc")->take(10)->get();
            }
        }
        
        $cntClientes = \App\Cliente::count();
        $clientes = \App\Cliente::with('usuario')->whereHas('usuario', function($q){
            $q->whereNull('deleted_at');
        })->where('publico', 'S');
        
        if ($cntClientes > 16){
            $ids = [];
            while(count($ids) < 16){
                $i = rand(1, $cntClientes);
                if (!in_array($i, $ids)){
                    $ids[] = $i;
                }
            }
            
            $clientes = $clientes->whereIn('id', $ids);
        }
        
        $clientes = $clientes->get();
        $cotizaciones = \App\Cotizacion::where("correo", \Auth::user()->email)->orderBy("fecha_creacion", "DESC")->get();
        
        return \View::make('sesion.perfil', ["usuario" => $usuario, "imagen_perfil" => $imagen_perfil, "tipos" => $tipos, "cotizaciones" => $cotizaciones, "servicios" => $servicios, "pendientes" => $pendientes, "planes_pendientes" => $planes_pendientes, "planes" => $planes, "temas" => $temas, "clientes" => $clientes]);
    }
    
    public function asignarPaquete($id){
        $plan = \App\PaquetePendiente::with(['paquete', 'cliente'])
                ->where('id_cliente', \Auth::user()->cliente->id)
                ->where("cantidad", ">", 0)
                ->where("id", $id)
                ->orderBy('created_at', 'desc')->first();
        
        $clientes = \App\User::with("cliente")->get();    
        return \View::make('sesion.asignar', ["plan" => $plan, "clientes" => $clientes]);
    }
    
    
    public function guardarAsignacion(){
        $cliente = \App\Cliente::where( "id", Input::get('id_cliente') )->first();
        $plan = \App\Paquete::with(['descuentos.cotizacion' => function($query) use($cliente){
                    $query->where("par_cotizacion.correo", $cliente->email);
                }])->find( Input::get('plan') );

        $plan_pendiente = \App\PaquetePendiente::with(['paquete', 'cliente'])
                ->where('id_cliente', \Auth::user()->cliente->id)
                ->where('id_paquete', Input::get('plan') )
                ->first();
        
        $membresia = $cliente->adquirirMembresia($plan, $plan->descuentos[0]->meses);
        $servicio = $plan->servicio()->first();
        //dd($plan->servicio());
        if (sizeof($servicio)){
            $cliente->contratarServicio($servicio, $cliente->id);
        }

        $cliente->creditos += $plan->creditos;
        if (!$cliente->save()) {
            throw new \Exception('No se pudo abonar los créditos a la cuenta');
        }
        
        $vars = [
            "nombre_beneficiario" => $cliente->nombre,
            "direccion_beneficiario" => $cliente->direccion,
            "ciudad_beneficiario" => $cliente->ciudad,
            "correo_beneficiario" => $cliente->email,
            "empresa_beneficiario" => $cliente->empresa,
            
            "vencimiento_plan" => $membresia->fecha_vence->format('Y-m-d H:i:s'),
            
            "nombre_cliente" => \Auth::user()->cliente->nombre,
            "direccion_cliente" => \Auth::user()->cliente->direccion,
            "ciudad_cliente" => \Auth::user()->cliente->ciudad,
            "correo_cliente" => \Auth::user()->cliente->email,
            "empresa_cliente" => \Auth::user()->cliente->empresa,            
            
            "nombre_plan" => $plan->nombre
        ];

        $this->enviarMensaje($vars, 'asignacion_plan_beneficiario', $cliente->email, $cliente->nombre);
        $this->enviarMensaje($vars, 'asignacion_plan_asignador', \Auth::user()->cliente->email, \Auth::user()->cliente->nombre);
        
        $plan_pendiente->cantidad -= 1;
        $plan_pendiente->save();
        
        return \Redirect::action('SesionController@mostrarPerfil')->with('mensaje', 'Servicio asignado exitosamente');
        
    }
    
    
    
    public function contratarServicio() {

        try {
            \Illuminate\Support\Facades\DB::beginTransaction();
            $cliente = \App\Cliente::where("id_usuario", \Auth::user()->id)->first();
            $servicio = \App\Servicio::find(Input::get("id_servicio"));
            $con = new \App\Contratacion();
            
            if (sizeof($servicio)){
                $vigencias = \App\Paquete::obtenerVigencias();

                $fecha = new \DateTime();
                $con->id_cliente = $cliente->id;
                $con->id_servicio = $servicio->id;
                $con->fecha_vence = $fecha->add(new \DateInterval($vigencias[$servicio->periodicidad]));
                $con->fecha_inicio = null;
                $con->fecha_fin = null;
                $con->activo = 'S';

                if (!$con->save()){
                    throw new \Exception('No se pudo contratar el servicio');
                }
            }

            $cliente->creditos -= $servicio->creditos;
            if (!$cliente->save()) {
                throw new \Exception('No se pudo restar los créditos a su cuenta');
            }
            
            if (!in_array($servicio->periodicidad, ['H', 'D'])){
                $vars = [
                    "nombre_cliente" => $cliente->nombre,
                    "direccion_cliente" => $cliente->direccion,
                    "ciudad_cliente" => $cliente->ciudad,
                    "correo_cliente" => $cliente->email,
                    "empresa_cliente" => $cliente->empresa,
                    "creditos" => $servicio->creditos,
                    "nombre_servicio" => $servicio->nombre
                ];
                
                $this->enviarMensaje($vars, 'contratacion_servicio', $cliente->email, $cliente->nombre);
                $this->enviarMensaje($vars, 'contratacion_servicio_adm', $this->destino1, 'Cowo.com.co');
            }

            \Illuminate\Support\Facades\DB::commit();

            return \Redirect::action('SesionController@mostrarPerfil')->with('mensaje', 'Servicio contratado exitosamente');
        } catch (\Exception $e) {
            \Illuminate\Support\Facades\DB::rollback();
            echo $e->getMessage(); die();
            return \Redirect::action('SesionController@mostrarPerfil')->with('mensajeError', $e->getMessage());
        }
    }
    
    public function obtenerReservas($idSala = ''){
        $fechaInicio = Input::get("start", date('Y-m-01 00:00:00'));
        $fechaFin = Input::get("end", date('Y-m-d 23:59:59'));
       
        if($idSala != NULL){
            $reservas = \App\Contratacion::with("cliente")
                    ->where("id_sala", $idSala)
                    ->where("fecha_inicio", ">=", $fechaInicio)
                    ->where("fecha_inicio", "<=", $fechaFin)
                    ->get();
        }else{
            $reservas = \App\Contratacion::with("cliente")
                    ->where("fecha_inicio", ">=", $fechaInicio)
                    ->where("fecha_inicio", "<=", $fechaFin)
                    ->get();
        }
        
        $_reservas = [];
        foreach ($reservas as $r){
            $reserva = [
                'title' => $r->cliente->nombre.", Sala ".$r->id_sala,
                'start' => $r->fecha_inicio,
                'end' => $r->fecha_fin
            ];
            
            $_reservas[] = $reserva;
        }
        
        return \Psy\Util\Json::encode($_reservas);
    }
    
    public function obtenerMisReservas(){
        $fechaInicio = Input::get("start", date('Y-m-d H:i:s'));
        
        $cliente = \App\User::with("cliente")->where("id", \Auth::user()->id)->first();
        
        $reservas = \App\Contratacion::with("cliente")
                ->where("id_cliente", "=", $cliente->cliente->id)
                ->where("fecha_inicio", ">=", $fechaInicio)
                ->get();
        return $reservas;
    }
    
    public function mostrarFormReserva($id){
        $con = \App\Contratacion::with('servicio')->where('id', $id)->first();
        return \View::make('sesion.reserva', ["usuario" => \Auth::user(), "con" => $con]);
    }
    
    public function misReservas(){
        $reservas = $this->obtenerMisReservas();        
        return \View::make('sesion.misreservas', ["usuario" => \Auth::user(), "reservas" => $reservas]);
    }
    
    public function realizarReserva(){
        try{
            $id = Input::get("id_contratacion");
            $con = \App\Contratacion::with(["servicio", "cliente"])->where("id", $id)->first();
            
            if (!sizeof($con)){
                return json_encode(["ok" => 0, "msg" => 'Contratación de servicio no encontrada']);
            }
            
            $cliente = \App\Cliente::where("id_usuario", \Auth::user()->id)->first();
            
            if (!sizeof($cliente)){
                return json_encode(["ok" => 0, "msg" => 'Complete su perfil']);
            }
            
            if ($con->id_cliente != $cliente->id){
                return json_encode(["ok" => 0, "msg" => 'La contratación no corresponde con el cliente']);
            }
            
            $fInicio = Input::get("fecha");
            $hInicio = Input::get("hora", "00");
            $mInicio = Input::get("minuto", "00");
            $inicio = $fInicio." ".$hInicio.":".$mInicio.":00";
            
            $fechaInicio = new \DateTime($inicio);
            $fechaFin = new \DateTime($inicio);
            $idSala = Input::get("id_sala");
            
            $hFin = $fechaFin->format("H");

            $vigencias = [
                'H' => 'PT1H',
                'D' => 'P1D',
                'Q' => 'P15D',
                'M' => 'P1M',
                'T' => 'P3M',
                'S' => 'P6M',
                'A' => 'P1Y'
            ];
            
            if ($vigencias[$con->servicio->periodicidad] == 'H' && ($hInicio < 7 || $hInicio > 7 || $hFin < 7 || $hFin > 7)){
                return json_encode(["ok" => 0, "msg" => 'Debe seleccionar una hora entre 7AM y 7PM']);
            }
            
            $dw = $fechaInicio->format("w");
            if ($dw == 6 || $dw == 0){
                return json_encode(["ok" => 0, "msg" => 'Las reservas solo se pueden hacer para días de lunes a viernes '.$dw]);
            }

            $fechaFin->add(new \DateInterval($vigencias[$con->servicio->periodicidad]));
            $cnt = \App\Contratacion::where(function($q) use($fechaInicio, $fechaFin){
                $q->where("fecha_inicio", ">", $fechaInicio)->where("fecha_inicio", "<", $fechaFin);
            })->orWhere(function($q) use($fechaInicio, $fechaFin){
                $q->where("fecha_fin", ">", $fechaInicio)->where("fecha_fin", "<", $fechaFin);
            })->orWhere(function($q) use($fechaInicio, $fechaFin){
                $q->where("fecha_inicio", "=", $fechaInicio)->where("fecha_fin", "=", $fechaFin);
            })->where("id_sala", $idSala)->count();

            if ($cnt > 0){
                return json_encode(["ok" => 0, "msg" => 'La sala '.$idSala.' ya está separada para el periodo seleccionado']);
            }

            $con->id_sala = $idSala;
            $con->fecha_inicio = $fechaInicio;
            $con->fecha_fin = $fechaFin;
            $con->save();
            
            $vars = [
                "nombre_cliente" => $con->cliente->nombre,
                "direccion_cliente" => $con->cliente->direccion,
                "ciudad_cliente" => $con->cliente->ciudad,
                "correo_cliente" => $con->cliente->email,
                "empresa_cliente" => $con->cliente->empresa,
                "creditos" => $con->servicio->creditos,
                "nombre_servicio" => $con->servicio->nombre,
                "id_sala" => 'Sala '.$con->id_sala.' reservada',
                "fecha_inicio_servicio" => 'Fecha de inicio: '.$con->fecha_inicio->format('Y-m-d H:i:s'),
                "fecha_fin_servicio" => 'Fecha de fin: '.$con->fecha_fin->format('Y-m-d H:i:s')
            ];
                
            $this->enviarMensaje($vars, 'contratacion_servicio', $con->cliente->email, $con->cliente->nombre);
            $this->enviarMensaje($vars, 'contratacion_servicio_adm', $this->destino1, 'Cowo.com.co');

            //return \Redirect::action('SesionController@mostrarPerfil')->with('mensaje', 'Reserva realizada exitosamente');
            return json_encode(["ok" => 1]);
        } catch (Exception $e) {
            return $this->retornarError($e);
            //return \Redirect::action('SesionController@mostrarPerfil')->with('mensajeError', 'No se pudo realizar la reserva');
        }
    }
    
    public function editarReserva(){
        try{
            $id = Input::get("id");
            $con = \App\Contratacion::with("cliente")->where("id", $id)->first();
            
            if (!sizeof($con)){
                return json_encode(["ok" => 0, "msg" => 'Contratación de servicio no encontrada']);
            }
            
            $cliente = \App\Cliente::where("id_usuario", \Auth::user()->id)->first();
            
            if (!sizeof($cliente)){
                return json_encode(["ok" => 0, "msg" => 'Complete su perfil']);
            }
            
            if ($con->id_cliente != $cliente->id){
                return json_encode(["ok" => 0, "msg" => 'La contratación no corresponde con el cliente']);
            }
            
            $fInicio = Input::get("fecha");
            $hInicio = Input::get("hora", "00");
            $mInicio = Input::get("minuto", "00");
            $inicio = $fInicio." ".$hInicio.":".$mInicio.":00";
            
            $fechaInicio = new \DateTime($inicio);
            $fechaFin = new \DateTime($inicio);
            $idSala = Input::get("id_sala");
            
            $hFin = $fechaFin->format("H");

            $vigencias = [
                'H' => 'PT1H',
                'D' => 'P1D',
                'Q' => 'P15D',
                'M' => 'P1M',
                'T' => 'P3M',
                'S' => 'P6M',
                'A' => 'P1Y'
            ];
            
            if ($vigencias[$con->servicio->periodicidad] == 'H' && ($hInicio < 7 || $hInicio > 7 || $hFin < 7 || $hFin > 7)){
                return json_encode(["ok" => 0, "msg" => 'Debe seleccionar una hora entre 7AM y 7PM']);
            }
            
            $dw = $fechaInicio->format("w");
            if ($dw == 6 || $dw == 0){
                return json_encode(["ok" => 0, "msg" => 'Las reservas solo se pueden hacer para días de lunes a viernes '.$dw]);
            }

            $fechaFin->add(new \DateInterval($vigencias[$con->servicio->periodicidad]));
            $cnt = \App\Contratacion::where(function($q) use($fechaInicio, $fechaFin){
                $q->where("fecha_inicio", ">", $fechaInicio)->where("fecha_inicio", "<", $fechaFin);
            })->orWhere(function($q) use($fechaInicio, $fechaFin){
                $q->where("fecha_fin", ">", $fechaInicio)->where("fecha_fin", "<", $fechaFin);
            })->orWhere(function($q) use($fechaInicio, $fechaFin){
                $q->where("fecha_inicio", "=", $fechaInicio)->where("fecha_fin", "=", $fechaFin);
            })->where("id_sala", $idSala)->count();

            if ($cnt > 0){
                return json_encode(["ok" => 0, "msg" => 'La sala '.$idSala.' ya está separada para el periodo seleccionado']);
            }

            $con->id_sala = $idSala;
            $con->fecha_inicio = $fechaInicio;
            $con->fecha_fin = $fechaFin;
            $con->save();
            
            $vars = [
                "nombre_cliente" => $con->cliente->nombre,
                "direccion_cliente" => $con->cliente->direccion,
                "ciudad_cliente" => $con->cliente->ciudad,
                "correo_cliente" => $con->cliente->email,
                "empresa_cliente" => $con->cliente->empresa,
                "creditos" => $con->servicio->creditos,
                "nombre_servicio" => $con->servicio->nombre,
                "id_sala" => 'Sala '.$con->id_sala.' reservada',
                "fecha_inicio_servicio" => 'Fecha de inicio: '.$con->fecha_inicio->format('Y-m-d H:i:s'),
                "fecha_fin_servicio" => 'Fecha de fin: '.$con->fecha_fin->format('Y-m-d H:i:s')
            ];
                
            $this->enviarMensaje($vars, 'servicio-cowo-editado', $con->cliente->email, $con->cliente->nombre);
            $this->enviarMensaje($vars, 'edicion-de-servicio-contratado', $this->destino1, 'Cowo.com.co');

            //return \Redirect::action('SesionController@mostrarPerfil')->with('mensaje', 'Reserva eliminada exitosamente');
            return json_encode(["ok" => 1]);
        } catch (Exception $e) {
            return $this->retornarError($e);
            //return \Redirect::action('SesionController@mostrarPerfil')->with('mensajeError', 'No se pudo eliminar la reserva');
        }
    }
    
    public function eliminarReserva($id){
        try{
            $con = \App\Contratacion::with(["cliente", "servicio"])->where("id", $id)->first();
            
            if (!sizeof($con)){
                return \Redirect::action('SesionController@misReservas')->with('mensaje', 'Contratación de servicio no encontrada');
            }
            
            $cliente = \App\Cliente::where("id_usuario", \Auth::user()->id)->first();
            
            if (!sizeof($cliente)){
                return \Redirect::action('SesionController@misReservas')->with('mensaje', 'Complete su perfil');
            }
            
            if ($con->id_cliente != $cliente->id){
                return json_encode(["ok" => 0, "msg" => 'La contratación no corresponde con el cliente']);
                return \Redirect::action('SesionController@misReservas')->with('mensaje', 'La contratación no corresponde con el cliente');
            }
            
            $vars = [
                "nombre_cliente" => $con->cliente->nombre,
                "direccion_cliente" => $con->cliente->direccion,
                "ciudad_cliente" => $con->cliente->ciudad,
                "correo_cliente" => $con->cliente->email,
                "empresa_cliente" => $con->cliente->empresa,
                "creditos" => $con->servicio->creditos,
                "nombre_servicio" => $con->servicio->nombre,
                "id_sala" => 'Sala '.$con->id_sala.' reservada',
                "fecha_inicio_servicio" => 'Fecha de inicio: '.$con->fecha_inicio,
                "fecha_fin_servicio" => 'Fecha de fin: '.$con->fecha_fin
            ];
            
            $this->enviarMensaje($vars, 'servicio-cowo-eliminado', $con->cliente->email, $con->cliente->nombre);
            $this->enviarMensaje($vars, 'servicio-eliminado', $this->destino1, 'Cowo.com.co');
            
            $cliente->creditos += $con->servicio->creditos;
            $cliente->save();
            
            $con->fecha_inicio = null;
            $con->fecha_fin = null;            
            $con->save();
            
            return \Redirect::action('SesionController@misReservas')->with('mensaje', 'Reserva eliminada exitosamente');
            //return json_encode(["ok" => 1]);
        } catch (Exception $e) {
            //return $this->retornarError($e);
            return \Redirect::action('SesionController@misReservas')->with('mensajeError', 'No se pudo eliminar la reserva');
        }
    }
    
    public function buscarUsuario(){
        $crit = '%'.Input::get('criterio').'%';
        $clientes = \App\Cliente::with('usuario')
                ->where('publico', 'S')
                ->where('nombre', 'like', $crit)
                ->orWhere('apellido', 'like', $crit)
                ->orWhere('empresa', 'like', $crit)
                ->orWhere('ciudad', 'like', $crit)                
                ->get();
        return \View::make('sesion.comunidad', ['clientes' => $clientes]);
    }
    
    public function mostrarUsuario($id){
        $cliente = \App\Cliente::with('usuario')->where('id', $id)->where('publico', 'S')->first();
        if (!sizeof($cliente)){
            return "Cliente no encontrado";
        }
        
        return \View::make('sesion.datos_usuario', ['cliente' => $cliente]);
    }
}