<?php

namespace App\Http\Controllers\Admin;

use \Illuminate\Support\Facades\Input;

class ConfigController extends AdminController {

    public function mostrarIndex() {
        return \View::make('admin.servicio.index', array("servicios" => $servicios));
    }

    public function editarContacto() {
        $config = \App\Config::where("tipo", "C")->get();

        return \View::make("admin.config.form_contacto", array("config" => $config));
    }

    public function guardarContacto() {
        $cfg = Input::get("cfg");
        foreach($cfg as $id => $c){
            $config = \App\Config::find($id);
            $config->contenido = $c;
            $config->save();
        }
        
        return \Redirect::action('Admin\ConfigController@editarContacto')->with("mensaje", "Datos guardados exitosamente");
    }
    
    public function editarConfig() {
        $config = \App\Config::where("tipo", "G")->get();

        return \View::make("admin.config.form_config", array("config" => $config));
    }

    public function guardarConfig() {
        $cfg = Input::get("cfg");
        foreach($cfg as $id => $c){
            $config = \App\Config::find($id);
            $config->contenido = $c;
            $config->save();
        }
        
        return \Redirect::action('Admin\ConfigController@editarConfig')->with("mensaje", "Datos guardados exitosamente");
    }
}
