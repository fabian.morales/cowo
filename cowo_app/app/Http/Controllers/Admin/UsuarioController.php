<?php

namespace App\Http\Controllers\Admin;

use \Illuminate\Support\Facades\Input;
use DB;

class UsuarioController extends AdminController {
    use \App\MailTrait;
    
    public function mostrarPerfil($id){
        
        
        $usuario = \App\User::where("id", $id)->with([
            "cliente.compras.paquete",
            "cliente.compras" => function($q) {
                $q->orderBy('created_at', 'desc')->take(25);
            }, 
            "cliente.logCreditos",
            "cliente.contrataciones.servicio",
            "cliente.contrataciones" => function($q) {
                $q->orderBy('created_at', 'desc')->take(25);
            }, 
            "cliente.membresiaActual.paquete"
        ])->first();
        
        $imagen_perfil = "";
        $file = public_path('storage/imagenes/usuario/'.md5($usuario->login).'.jpg');
        if (is_file($file)){
            $imagen_perfil = asset('storage/imagenes/usuario/'.md5($usuario->login).'.jpg');
        }
        else{
            $imagen_perfil = asset('imagenes/usuario.png');
        }
        
        $tipos = ['CC', 'NIT', 'CE'];
        $servicios = [];
        $pendientes = [];
        $planes = [];
        $planes_pendientes = [];
        $temas = [];
        $mostrarForo = false;
        
        if (sizeof($usuario->cliente)){
            $servicios = \App\Servicio::where("creditos", "<=", $usuario->cliente->creditos)->get();
            $pendientes = \App\Contratacion::with('servicio')
                    ->where('id_cliente', $usuario->cliente->id)
                    ->whereHas("servicio", function($q) {
                        $q->whereRaw("periodicidad in ('D', 'H')");
                    })
                    ->whereNull('fecha_inicio')
                    ->orderBy('created_at', 'desc')->get();
                    
            $planes_pendientes = \App\PaquetePendiente::with(['paquete', 'cliente'])
                    ->where('id_cliente', $usuario->cliente->id)
                    ->where("cantidad", ">", 0)
                    ->orderBy('created_at', 'desc')->get();
                    
            if (sizeof($usuario->cliente->membresiaActual)){
                $planes = \App\Paquete::where("valor", ">", $usuario->cliente->membresiaActual[0]->paquete->valor)
                        ->whereRaw("periodicidad not in ('H', 'D', 'N')")
                        ->orderBy("valor")
                        ->get();
                $mostrarForo = true;
            }
            
        }
        
        $cntClientes = \App\Cliente::count();
        $clientes = \App\Cliente::with('usuario')->whereHas('usuario', function($q){
            $q->whereNull('deleted_at');
        })->where('publico', 'S');
        
        if ($cntClientes > 16){
            $ids = [];
            while(count($ids) < 16){
                $i = rand(1, $cntClientes);
                if (!in_array($i, $ids)){
                    $ids[] = $i;
                }
            }
            
            $clientes = $clientes->whereIn('id', $ids);
        }
        
        $clientes = $clientes->get();
                
        return \View::make('admin.usuario.perfil', ["usuario" => $usuario, "imagen_perfil" => $imagen_perfil, "tipos" => $tipos, "servicios" => $servicios, "pendientes" => $pendientes, "planes_pendientes" => $planes_pendientes, "planes" => $planes, "temas" => $temas, "clientes" => $clientes]);
    }

    
    public function mostrarIndex() {
        $filtro = Input::get("filtro");
        
        if(!empty($filtro)) {
            $usuarios = \App\User::with('cliente.membresiaActual')
                ->whereHas("cliente", function($q) use ($filtro) {
                    $q->where('nombre', 'LIKE', '%'.$filtro.'%')
                        ->orWhere('apellido', 'LIKE', '%'.$filtro.'%')
                        ->orWhere('email', 'LIKE', '%'.$filtro.'%')
                        ->orWhere(DB::raw('concat(nombre," ",apellido)') , 'LIKE' , "%".$filtro."%")
                        ->take(20);
                })
            ->paginate(20);
        }else{
            $usuarios = \App\User::with('cliente.membresiaActual')->paginate(20);
        }
        return \View::make('admin.usuario.index', array("filtro" => $filtro, "usuarios" => $usuarios));
    }

    public function mostrarFormUsuario($usuario) {
        if (!sizeof($usuario)) {
            $usuario = new \App\User();
        }
        
        $tipos = ['CC', 'NIT', 'CE'];
        $planes = \App\Paquete::whereNotIn("periodicidad", ['N','H','D'])->orderBy("valor")->get();

        return \View::make("admin.usuario.form", array("usuario" => $usuario, "tipos" => $tipos, "planes" => $planes));
    }

    public function crearUsuario() {
        return $this->mostrarFormUsuario(new \App\User());
    }

    public function editarUsuario($id) {
        $usuario = \App\User::with([
            "cliente.membresiaActual.paquete",
            "cliente.compras.paquete",
            "cliente.compras" => function($q) {
                $q->orderBy('created_at', 'desc')->take(25);
            }, 
            "cliente.logCreditos",
            "cliente.contrataciones.servicio",
            "cliente.contrataciones" => function($q) {
                $q->orderBy('created_at', 'desc')->take(25);
            }
        ])->where("id", $id)->first();
        if (!sizeof($usuario)) {
            return \Redirect::action('Admin\UsuarioController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el usuario");
        }

        return $this->mostrarFormUsuario($usuario);
    }

    public function guardarUsuario() {
        $id = Input::get("id");
        $clave = Input::get("password");
        $nuevo = false;

        $usuario = \App\User::find($id);
        if (!sizeof($usuario)) {
            $usuario = new \App\User();
            $nuevo = true;
        }

        if (empty($id) && empty($clave)) {
            \Illuminate\Support\Facades\Session::flash("mensajeError", "Debe ingresar la clave para el usuario");
            return $this->mostrarFormUsuario($usuario);
        } else if (!empty($clave)) {
            
            if(!$nuevo){                
                    
                    $vars = [
                        "nombre_cliente" => $usuario->nombre,
                        "clave" => $clave
                    ];

                    $this->enviarMensaje($vars, 'clave-modificada', $usuario->email, $usuario->nombre);                
            }            
            $clave = \Illuminate\Support\Facades\Hash::make($clave);
            
        } else {
            $clave = $usuario->password;
        }
        
        $cliente = $usuario->cliente;
        if (!sizeof($cliente)){
            $email = Input::get("email");
            $cliente = \App\Cliente::where("email", $email)->first();

            if (!sizeof($cliente)){
                $cliente = new \App\Cliente();
            }
        }

        $usuario->fill(Input::all());
        $cliente->fill(Input::all());
        
        $usuario->admin = Input::get("tipo") == "A" ? "Y":"N";        
        $usuario->comercial = Input::get("tipo") == "C" ? "S":"N";
        $usuario->password = $clave;

        if (!empty($usuario->id)) {
            $cntLogin = \App\User::where("login", $usuario->login)->where("id", "<>", $usuario->id)->count();
            if ($cntLogin > 0) {
                \Illuminate\Support\Facades\Session::flash("mensajeError", "Ya existe un usuario con el login ingresado");
                return $this->mostrarFormUsuario($usuario);
            }
            
            $cntEmail = \App\User::where("email", $usuario->email)->where("id", "<>", $usuario->id)->count();
            if ($cntEmail > 0) {
                \Illuminate\Support\Facades\Session::flash("mensajeError", "Ya existe un usuario con el correo ingresado");
                return $this->mostrarFormUsuario($usuario);
            }
        }
        else{
            $cntLogin = \App\User::where("login", $usuario->login)->count();
            if ($cntLogin > 0) {
                \Illuminate\Support\Facades\Session::flash("mensajeError", "Ya existe un usuario con el login ingresado");
                return $this->mostrarFormUsuario($usuario);
            }
            
            $cntEmail = \App\User::where("email", $usuario->email)->count();
            if ($cntEmail > 0) {
                \Illuminate\Support\Facades\Session::flash("mensajeError", "Ya existe un usuario con el correo ingresado");
                return $this->mostrarFormUsuario($usuario);
            }
        }
        
        if (empty($cliente->publico)){
            $cliente->publico = 'S';
        }

        if ($usuario->save()) {
            if (empty($cliente->id_usuario)){
                $cliente->id_usuario = $usuario->id;
            }
            
            if ($cliente->save()){
                if ($nuevo){
                    $vars = [
                        "nombre_cliente" => $cliente->nombre,
                        "direccion_cliente" => $cliente->direccion,
                        "ciudad_cliente" => $cliente->ciudad,
                        "correo_cliente" => $cliente->email,
                        "empresa_cliente" => $cliente->empresa
                    ];

                    $this->enviarMensaje($vars, 'registro', $cliente->email, $cliente->nombre);
                }
                return \Redirect::action('Admin\UsuarioController@mostrarIndex')->with("mensaje", "Usuario guardado exitosamente");
            }
            else{
                return \Redirect::action('Admin\UsuarioController@mostrarIndex')->with("mensajeError", "No se pudo guardar los datos de cliente");
            }
        } else {
            return \Redirect::action('Admin\UsuarioController@mostrarIndex')->with("mensajeError", "No se pudo guardar el usuario");
        }
    }
    
    public function modificarCreditos(){
        $id = Input::get("id_usuario");
        $creditos = (int)Input::get("cantidad");
        $usuario = \App\User::where("id", $id)->with("cliente")->first();
        
        if (sizeof($usuario) && sizeof($usuario->cliente)){
            $usuario->cliente->creditos += $creditos;
            $usuario->cliente->save();
            
            $log = new \App\LogCredito;
            $log->id_creador = \Auth::user()->id;
            $log->id_cliente = $usuario->cliente->id;
            $log->motivo     = Input::get("motivo");
            $log->cantidad   = $creditos;
            $log->save();
        }
        
        //return $creditos;
    }
    
    public function asignarPlan(){
        try{
            date_default_timezone_set('America/Bogota');
            
            $id = Input::get("id_usuario");
            $idPlan = Input::get("id_plan");
            $fechaStr = Input::get("fecha");
            $fecha = new \DateTime($fechaStr);
            $plan = \App\Paquete::find($idPlan);
            $usuario = \App\User::where("id", $id)->with("cliente.membresiaActual")->first();
            
            if(sizeof($plan)){
                if (sizeof($usuario) && sizeof($usuario->cliente)){
                                    
                    $membresia = new \App\Membresia();
                    $nueva = true;
                    if (sizeof($usuario->cliente->membresiaActual)){
                        $membresiaActual = $usuario->cliente->membresiaActual[0];
                        
                        if ($idPlan == $membresiaActual->id_paquete){
                            $membresia = $membresiaActual;
                            $nueva = false;
                        }
                        else{
                            $membresiaActual->activo = 'N';
                            if (!$membresiaActual->save()) {
                                throw new \Exception('No se pudo desactivar el plan actual');
                            }
                        }
                    }
                    
                    if ($nueva){
                        $membresia->id_cliente = $usuario->cliente->id;
                        $membresia->id_paquete = $idPlan;
                        $membresia->activo = 'S'; 
                    }
                    
                    $vigencias = \App\Paquete::obtenerVigencias();
                    $fechaVence = new \DateTime();
                    $fechaVence->modify($fechaStr);
                    
                    $membresia->fecha_inicio = $fecha;
                    $membresia->fecha_vence = $fechaVence->add(new \DateInterval($vigencias[$plan->periodicidad]));
                    
                    $membresia->save();
                    
                    return json_encode([$fecha, $fecha->add(new \DateInterval($vigencias[$plan->periodicidad]))]);
                    
                    if ($nueva){
                        //$membresia = $usuario->cliente->adquirirMembresia(null, $idPlan);
                        $vars = [
                            "nombre_cliente" => $usuario->cliente->nombre,
                            "direccion_cliente" => $usuario->cliente->direccion,
                            "ciudad_cliente" => $usuario->cliente->ciudad,
                            "correo_cliente" => $usuario->cliente->email,
                            "empresa_cliente" => $usuario->cliente->empresa,
                            "nombre_plan" => $plan->nombre,
                            "valor_plan" => $plan->valor,
                            "vencimiento_plan" => $membresia->fecha_vence->format('Y-m-d H:i:s'),
                            "estado_compra" => 'Aprobada'
                        ];

                        $this->enviarMensaje($vars, 'compra_plan', $usuario->cliente->email, $usuario->cliente->nombre);
                    }
                    
                    return \Redirect::action('Admin\UsuarioController@mostrarIndex')->with("mensaje", "Membresía asignada exitosamente");
                }
                else{
                    return \Redirect::action('Admin\UsuarioController@mostrarIndex')->with("mensajeError", "Debe completarse el perfil del usuario");
                }
            }else{
                return back()->with("mensajeError", "Este plan no se encuentra registrado")->withInput();
            }
        } catch (Exception $ex) {
            return \Redirect::action('Admin\UsuarioController@mostrarIndex')->with("mensajeError", $ex->getMessage());
        }
    }
    
    function borrarUsuario($id){
        $usuario = \App\User::find($id);
        
        if (!sizeof($usuario)){
            return \Redirect::action('Admin\UsuarioController@mostrarIndex')->with("mensajeError", "Usuario no encontrado");
        }
        
        if ($usuario->delete()){
            return \Redirect::action('Admin\UsuarioController@mostrarIndex')->with("mensaje", "Usuario borrado exitosamente");
        }
        else{
            return \Redirect::action('Admin\UsuarioController@mostrarIndex')->with("mensajeError", "No se pudo borrar el usuario");
        }
    }

}
