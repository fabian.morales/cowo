<?php

namespace App\Http\Controllers\Admin;

use \Illuminate\Support\Facades\Input;

class PaqueteController extends AdminController {

    public function mostrarIndex() {
        //$productos = \App\Producto::orderBy("id");
        
        /*$nombre = Input::get("nombre");
        $codigo = Input::get("codigo");
        $destacado = Input::get("destacado");
        $busqueda = [];
        
        if (!empty($nombre)){
            $productos = $productos->where("nombre", "like", "%".$nombre."%");
            $busqueda["nombre"] = $nombre;
        }
        
        if (!empty($destacado)){
            $productos = $productos->where("destacado", 'S');
            $busqueda["destacado"] = $destacado;
        }
        
        if (!empty($codigo)){
            $productos = $productos->where("codigo_unico", "like", "%".$cliente."%");
            $busqueda["codigo"] = $codigo;
        }*/
        
        //$productos = $productos->paginate(20)->appends($busqueda);
        $paquetes = \App\Paquete::orderBy("id")->paginate(20);
        
        return \View::make('admin.paquete.index', array("paquetes" => $paquetes));
    }

    public function mostrarFormPaquete($paquete) {
        if (!sizeof($paquete)) {
            $paquete = new \App\Producto();
        }

        $servicios = \App\Servicio::orderBy("nombre")->get();
        $periodos = [
            'N' => 'No aplica',
            'H' => 'Hora',
            'D' => 'Día',
            'Q' => 'Quincena',
            'M' => 'Mes',
            'T' => 'Trimestre',
            'S' => 'Semestre',
            'A' => 'Año',
        ];
        
        return \View::make("admin.paquete.form", array("paquete" => $paquete, "servicios" => $servicios, "periodos" => $periodos));
    }

    public function crearPaquete() {
        return $this->mostrarFormPaquete(new \App\Paquete());
    }

    public function editarPaquete($id) {
        $paquete = \App\Paquete::find($id);
        if (!sizeof($paquete)) {
            return \Redirect::action('Admin\PaqueteController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el paquete");
        }

        return $this->mostrarFormPaquete($paquete);
    }

    public function guardarPaquete() {
        $id = Input::get("id");

        $paquete = \App\Paquete::find($id);
        if (!sizeof($paquete)) {
            $paquete = new \App\Paquete();
        }

        $paquete->fill(Input::all());
        
        if(Input::file("imagen") != NULL){
            $imagen = Input::file("imagen")->getClientOriginalName();
            $paquete->imagen = $imagen;
            
            $imageSize = getimagesize( Input::file('imagen')->getRealPath() );
        
        
            if(sizeof($imagen) && ($imageSize[0] < 500 || $imageSize[1] < 500 ) ){
                 \Illuminate\Support\Facades\Session::flash("mensajeError", "La imagen debe tener un alto y un ancho m&iacute;nimo de 500px ");
                return $this->mostrarFormPaquete($paquete);
            }else if (sizeof($imagen) && Input::file("imagen")->isValid()){
                $path = public_path('storage/imagenes/planes/'.$paquete->id.'/');

                if (!is_dir($path)) {
                    $partesPath = ['storage', 'imagenes', 'planes', $paquete->id];
                    $subpath = public_path() . '/';
                    foreach ($partesPath as $p) {
                        $subpath .= $p . '/';

                        if (!is_dir($subpath)) {
                            mkdir($subpath);
                        }
                    }
                }

                $file = $imagen;
                Input::file("imagen")->move($path, $file);                
            }else{
                \Illuminate\Support\Facades\Session::flash("mensajeError", "Debe ingresar la imagen del paquete");
                return $this->mostrarFormPaquete($paquete);
            }
            
            
        }
        
        if (empty($paquete->id_servicio)){
            $paquete->id_servicio = null;
        }
        
        if (empty($paquete->nombre)){
            \Illuminate\Support\Facades\Session::flash("mensajeError", "Debe ingresar el nombre del paquete");
            return $this->mostrarFormPaquete($paquete);
        }
        
        if (empty($paquete->valor)){
            \Illuminate\Support\Facades\Session::flash("mensajeError", "Debe ingresar el valor del paquete");
            return $this->mostrarFormPaquete($paquete);
        }
        
        if (empty($paquete->creditos)){
            \Illuminate\Support\Facades\Session::flash("mensajeError", "Debe ingresar el número de créditos del paquete");
            return $this->mostrarFormPaquete($paquete);
        }
        

        if ($paquete->save()) {
            return \Redirect::action('Admin\PaqueteController@mostrarIndex')->with("mensaje", "Paquete guardado exitosamente");
        } else {
            return \Redirect::action('Admin\PaqueteController@mostrarIndex')->with("mensajeError", "No se pudo guardar el paquete");
        }
    }
    
    function borrarPaquete($id){
        $paquete = \App\Paquete::find($id);
        
        if (!sizeof($paquete)){
            return \Redirect::action('Admin\PaqueteController@mostrarIndex')->with("mensajeError", "Paquete no encontrado");
        }
        
        $cnt = \App\Compra::where("id_paquete", $id)->count();
        if ($cnt > 0){
            return \Redirect::action('Admin\PaqueteController@mostrarIndex')->with("mensajeError", "El paquete no puede ser borrado porque tiene compras asociadas");
        }        
        
        if ($paquete->delete()){
            return \Redirect::action('Admin\PaqueteController@mostrarIndex')->with("mensaje", "Paquete borrado exitosamente");
        }
        else{
            return \Redirect::action('Admin\PaqueteController@mostrarIndex')->with("mensajeError", "No se pudo borrar el paquete");
        }
    }
    
    function postInfo(){
        $id = Input::get("id");
        $paquete = \App\Paquete::find($id);
        return response()->json($paquete);         
    }

}
