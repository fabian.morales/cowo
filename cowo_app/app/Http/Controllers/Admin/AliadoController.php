<?php

namespace App\Http\Controllers\Admin;

use \Illuminate\Support\Facades\Input;

class AliadoController extends AdminController {

    public function mostrarIndex() {
        $aliados = \App\Aliado::paginate(20);
        return \View::make('admin.aliado.index', array("aliados" => $aliados));
    }

    public function mostrarFormAliado($aliado) {
        if (!sizeof($aliado)) {
            $aliado = new \App\Aliado();
        }
        
        return \View::make("admin.aliado.form", array("aliado" => $aliado));
    }

    public function crearAliado() {
        return $this->mostrarFormAliado(new \App\Aliado());
    }

    public function editarAliado($id) {
        $aliado = \App\Aliado::find($id);
        if (!sizeof($aliado)) {
            return \Redirect::action('Admin\AliadoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar al aliado");
        }

        return $this->mostrarFormAliado($aliado);
    }

    public function guardarAliado() {
        $id = Input::get("id");

        $aliado = \App\Aliado::find($id);
        if (!sizeof($aliado)) {
            $aliado = new \App\Aliado();
        }

        $aliado->fill(Input::all());
        if (empty($aliado->activo)){
            $aliado->activo = 'N';
        }
        
        if (empty($aliado->titulo)){
            \Illuminate\Support\Facades\Session::flash("mensajeError", "Debe ingresar el nombre del aliado");
            return $this->mostrarFormAliado($aliado);
        }

        if ($aliado->save()) {
            $imageSize = getimagesize( Input::file('imagen')->getRealPath() );
        
        
            if(Input::hasFile('imagen') && ($imageSize[0] < 500 || $imageSize[1] < 500 ) ){
				return \Redirect::action('Admin\AliadoController@mostrarIndex')->with("mensajeError", "La imagen debe tener un alto y un ancho m&iacute;nimo de 500px ");
            }else if (Input::hasFile('imagen') && Input::file('imagen')->isValid()) {
                Input::file('imagen')->move(public_path('imagenes/aliados'), $aliado->id.'.png');
            }
            
            return \Redirect::action('Admin\AliadoController@mostrarIndex')->with("mensaje", "Aliado guardado exitosamente");
        } else {
            return \Redirect::action('Admin\AliadoController@mostrarIndex')->with("mensajeError", "No se pudo guardar el aliado");
        }
    }
    
    public function borrarAliado($id){
        $aliado = \App\Aliado::find($id);
        if (!sizeof($aliado)) {
            return \Redirect::action('Admin\AliadoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el aliado");
        }
        
        if ($aliado->delete()){
            @unlink(public_path('imagenes/aliados'), $id.'.png');
            return \Redirect::action('Admin\AliadoController@mostrarIndex')->with("mensaje", "Aliado borrado exitosamente");
        } else {
            return \Redirect::action('Admin\AliadoController@mostrarIndex')->with("mensajeError", "No se pudo borrar el aliado");
        }
    }
}
