<?php

namespace App\Http\Controllers\Admin;

use \Illuminate\Support\Facades\Input;

class GaleriaController extends AdminController {

    public function mostrarIndex() {
        $galerias = \App\GaleriaFoto::orderBy("id")->get();
        
        return \View::make('admin.galeria.index', array("galerias" => $galerias));
    }

    public function mostrarFormGaleria($galeria) {
        if (!sizeof($galeria)) {
            $galeria = new \App\GaleriaFoto();
        }

        return \View::make("admin.galeria.form", array("galeria" => $galeria));
    }

    public function crearGaleria() {
        return $this->mostrarFormGaleria(new \App\GaleriaFoto());
    }

    public function editarGaleria($id) {
        $galeria = \App\GaleriaFoto::with("fotos")->where("id", $id)->first();
        if (!sizeof($galeria)) {
            return \Redirect::action('Admin\GaleriaController@mostrarIndex')->with("mensajeError", "No se pudo encontrar la galería");
        }

        return $this->mostrarFormGaleria($galeria);
    }
    
    public function guardarGaleria() {
        $id = Input::get("id");

        $galeria = \App\GaleriaFoto::find($id);
        if (!sizeof($galeria)) {
            $galeria = new \App\GaleriaFoto();
        }

        $galeria->fill(Input::all());
        
        if (empty($galeria->titulo)){
            \Illuminate\Support\Facades\Session::flash("mensajeError", "Debe ingresar el nombre de la galería");
            return $this->mostrarFormGaleria($galeria);
        }

        if ($galeria->save()) {
            return \Redirect::action('Admin\GaleriaController@editarGaleria', ["id" => $galeria->id])->with("mensaje", "Galería guardada exitosamente");
        } else {
            return \Redirect::action('Admin\GaleriaController@mostrarIndex')->with("mensajeError", "No se pudo guardar la galería");
        }
    }
    
    function borrarGaleria($id){
        $galeria = \App\GaleriaFoto::with("fotos")->where("id", $id)->first();
        
        if (!sizeof($galeria)){
            return \Redirect::action('Admin\GaleriaController@mostrarIndex')->with("mensajeError", "Galería no encontrada");
        }
        
        if (!$galeria->fotos()->detach()){
            return \Redirect::action('Admin\GaleriaController@mostrarIndex')->with("mensajeError", "No se pudo borrar las fotos de la galería");
        }
        
        if ($galeria->delete()){
            foreach($fotos as $f){
                $path = public_path('storage/imagenes/galeria/'.$f->id_galeria.'/'.$f->id.'.jpg');
                @unlink($path);
            }
            
            @rmdir(public_path('storage/imagenes/galeria/'.$galeria->id));
            
            return \Redirect::action('Admin\GaleriaController@mostrarIndex')->with("mensaje", "Galería borrada exitosamente");
        }
        else{
            return \Redirect::action('Admin\GaleriaController@mostrarIndex')->with("mensajeError", "No se pudo borrar la galería");
        }
    }
    
    public function editarFoto($id){
        $foto = \App\Foto::find($id);
        if (!sizeof($foto)) {
            $foto = new \App\Foto();
        }
        
        return $foto->toJson();
    }

    public function guardarFoto() {
        $id = Input::get("id");

        $foto = \App\Foto::find($id);
        if (!sizeof($foto)) {
            $foto = new \App\Foto();
        }

        $foto->fill(Input::all());
        $galeria = $foto->galeria()->with("fotos")->first();
        
        if (empty($foto->titulo)){
            return \Redirect::action('Admin\GaleriaController@editarGaleria', ["id" => $galeria->id])->with("mensajeError", "Debe ingresar el nombre de la foto");
        }
        
        $imagen = Input::file("imagen");
    
        if (!$foto->id && !(sizeof($imagen) && $imagen->isValid())) {
            return \Redirect::action('Admin\GaleriaController@editarGaleria', ["id" => $galeria->id])->with("mensajeError", "Debe ingresar la imagen");
        }

        if ($foto->save()) {
            
            if (sizeof($imagen) && $imagen->isValid()){
                $path = public_path('storage/imagenes/galeria/'.$foto->id_galeria.'/');

                if (!is_dir($path)) {
                    $partesPath = ['storage', 'imagenes', 'galeria', $foto->id_galeria];
                    $subpath = public_path() . '/';
                    foreach ($partesPath as $p) {
                        $subpath .= $p . '/';

                        if (!is_dir($subpath)) {
                            mkdir($subpath);
                        }
                    }
                }

                $file = $foto->id.".jpg";
                $imagen->move($path, $file);                
            }

            return \Redirect::action('Admin\GaleriaController@editarGaleria', ["id" => $galeria->id])->with("mensaje", "Galería borrada exitosamente");
        } else {
            return \Redirect::action('Admin\GaleriaController@editarGaleria', ["id" => $galeria->id])->with("mensajeError", "No se pudo guardar la foto");
        }
    }
    
    function borrarFoto($id){
        $foto = \App\Foto::find($id);
        $galeria = $foto->galeria()->with("fotos")->first();
        
        if (!sizeof($foto)){
            return \Redirect::action('Admin\GaleriaController@editarGaleria', ["id" => $galeria->id])->with("mensajeError", "Foto no encontrada");
        }
        
        $path = public_path('storage/imagenes/galeria/'.$foto->id_galeria.'/'.$foto->id.'.jpg');
        
        if ($foto->delete()){
            @unlink($path);
            return \Redirect::action('Admin\GaleriaController@editarGaleria', ["id" => $galeria->id])->with("mensaje", "Foto borrada exitosamente");
        }
        else{
            return \Redirect::action('Admin\GaleriaController@editarGaleria', ["id" => $galeria->id])->with("mensajeError", "No se pudo borrar la foto");
        }
    }

}
