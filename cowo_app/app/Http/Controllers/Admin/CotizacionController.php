<?php

namespace App\Http\Controllers\Admin;

use \Illuminate\Support\Facades\Input;

class CotizacionController extends AdminController {
    use \App\MailTrait;
    
    public function mostrarIndex() {
        
        $filtro = Input::get("filtro");
        
        if(\Auth::user()->admin == 'Y'){
            
            if( empty($filtro) ){
                $lista = \App\Cotizacion::with(["usuario", "cliente"])->groupBy("correo", "fecha_creacion")->orderBy("fecha_creacion", "DESC")->paginate(10);
            }else{
                
                $lista = \App\Cotizacion::whereHas("usuario", function($q) use ($filtro) {
                                    $q->where('nombre', 'LIKE', '%'.$filtro.'%')
                                        ->orWhere('email', 'LIKE', '%'.$filtro.'%');
                                    })
                                ->orWhereHas("cliente", function($q) use ($filtro) {
                                        $q->where('nombre', 'LIKE', '%'.$filtro.'%')
                                            ->orWhere('apellido', 'LIKE', '%'.$filtro.'%')
                                            ->orWhere('email', 'LIKE', '%'.$filtro.'%')
                                            ->orWhere(\DB::raw('concat(nombre," ",apellido)') , 'LIKE' , "%".$filtro."%");
                                    })
                                ->orWhereHas("descuentos.paquete", function($q) use ($filtro) {
                                        $q->where('nombre', 'LIKE', '%'.$filtro.'%');
                                    })
                                ->orWhere('correo', 'LIKE', '%'.$filtro.'%')
                                ->orWhere('fecha_creacion', 'LIKE', '%'.$filtro.'%')
                                ->groupBy("correo", "fecha_creacion")
                                ->orderBy("fecha_creacion", "DESC")
                                ->paginate(10)->appends($filtro);
            }
        
            return \View::make("admin.cotizacion.lista", ["lista" => $lista, "filtro" => $filtro]);
        }else{
            if( empty($filtro) ){
                $lista = \App\Cotizacion::with(["usuario", "cliente"])
                        ->where("id_creador", \Auth::user()->id)
                        ->groupBy("correo", "fecha_creacion")
                        ->paginate(10);
            }else{
                
                $lista = \App\Cotizacion::where(function($q) use($filtro){
                                    $q->whereHas("usuario", function($q) use ($filtro) {
                                        $q->where('nombre', 'LIKE', '%'.$filtro.'%')
                                            ->orWhere('email', 'LIKE', '%'.$filtro.'%');
                                    })
                                    ->orWhereHas("cliente", function($q) use ($filtro) {
                                        $q->where('nombre', 'LIKE', '%'.$filtro.'%')
                                            ->orWhere('apellido', 'LIKE', '%'.$filtro.'%')
                                            ->orWhere('email', 'LIKE', '%'.$filtro.'%')
                                            ->orWhere(\DB::raw('concat(nombre," ",apellido)') , 'LIKE' , "%".$filtro."%");
                                    })
                                    ->orWhereHas("descuentos.paquete", function($q) use ($filtro) {
                                        $q->where('nombre', 'LIKE', '%'.$filtro.'%');
                                    })
                                    ->orWhere('correo', 'LIKE', '%'.$filtro.'%')
                                    ->orWhere('fecha_creacion', 'LIKE', '%'.$filtro.'%');
                                })
                                ->where("id_creador", \Auth::user()->id)
                                ->paginate(10)->appends($filtro);
            }
            
            return \View::make("comercial.cotizacion.lista", ["lista" => $lista, "filtro" => $filtro]);
        }
    }
    
    public function mostrarCotizador() {
        $planes = \App\Paquete::all();
        
        if(\Auth::user()->admin == 'Y'){
            $comerciales = \App\User::where("comercial", "S")->get();
            return \View::make("admin.cotizacion.index", ["comerciales" => $comerciales, "planes" => $planes]);
        }else{
            return \View::make("comercial.cotizacion.index", ["planes" => $planes]);            
        }
    }
    
    public function enviarCotizacion(){
        
        $nombreCliente = Input::get("nombre");
        $ciudadCliente = Input::get("ciudad");
        $correoCliente = Input::get("correo");
        $empresaCliente = Input::get("empresa");
        $idPlanes  = Input::get("id_plan");
        $valores   = Input::get("valor");
        $meses     = Input::get("meses");
        $horas     = Input::get("horas");
        $cantidad  = Input::get("cantidad");
        $descuento = Input::get("descuento");
        $subtotal  = Input::get("subtotal");
        $iva       = Input::get("iva");
        $total     = Input::get("total");
        $vence     = Input::get("vence");
        $mensaje   = Input::get("mensaje");
        $comercial = Input::get("comercial") != "" ? Input::get("comercial") : \Auth::id();
        
        $archivos = Input::file("archivos");
        
        $files = [];
        
        if ((sizeof($archivos))) {
            
            $path = public_path('storage/correos/tmp/');

            if (!is_dir($path)) {
                $partesPath = ['storage', 'correos', 'tmp'];
                $subpath = public_path() . '/';
                foreach ($partesPath as $p) {
                    $subpath .= $p . '/';

                    if (!is_dir($subpath)) {
                        mkdir($subpath);
                    }
                }
            }

            foreach($archivos as $a){
                if (sizeof($a) && $a->isValid()){
                    $a->move($path, $a->getClientOriginalName());
                    $files[] = public_path('storage/correos/tmp/'.$a->getClientOriginalName());
                }
            }
        }
        
        $cotizacion = new \App\Cotizacion();
        $cotizacion->id_creador = $comercial;
        $cotizacion->correo     = $correoCliente;
        $cotizacion->nombre_cliente = $nombreCliente;
        $cotizacion->fecha_creacion = date("Y-m-d H:i:s");                    
        $cotizacion->mensaje    = $mensaje;
        $cotizacion->estado     = 'C';
        $cotizacion->save();
        
        if(sizeof($idPlanes)){
            $dataSet = [];
            
            foreach($idPlanes as $key => $plan){
                if($plan == "") continue;
                $dataSet[] = [
                    'id_cotizacion' => $cotizacion->id,
                    'id_paquete' => $plan,                   
                    'precio'   => $valores[$key] != NULL ? $valores[$key] : \App\Paquete::find($plan)->valor,
                    'horas'    => $horas[$key] != NULL ? $horas[$key] : 0,
                    'cantidad' => $cantidad[$key] != NULL ? $cantidad[$key] : 1,
                    'meses'    => $meses[$key] != NULL ? $meses[$key] : 1,
                    'valor'    => $descuento[$key] != NULL ? $descuento[$key] : 0,
                    'fecha_vencimiento' => $vence[$key],
                ];                
            }
            
            \DB::table("par_descuento")->insert($dataSet);
        }
        
        $vars = [
            "nombre_cliente" => $nombreCliente,
            "ciudad_cliente" => $ciudadCliente,
            "correo_cliente" => $correoCliente,
            "empresa_cliente" => $empresaCliente,
            "info_planes"    => $this->buildInfoPlanes($cotizacion->id, $idPlanes, $valores, $descuento, $cantidad, $meses, $horas, $subtotal, $iva, $total, $vence, $mensaje),            
            "archivos" => $files
        ];
        
        $this->enviarMensaje($vars, 'cotizacion', $correoCliente, $nombreCliente);
        
        if(\Auth::user()->admin == 'Y'){
            return \Redirect::action('Admin\CotizacionController@mostrarIndex')->with("mensaje", "Cotización enviada.");
        }else{            
            return \Redirect::to('comercial/cotizaciones')->with("mensaje", "Cotización enviada.");
        }
    }
    
    
    function buildInfoPlanes($idCotizacion, $planes, $valores, $descuento, $cantidad, $meses, $horas, $subtotal, $iva, $total, $vence, $mensaje){
        $lista = \App\Paquete::whereIn("id", $planes)->with([
            "descuentos" => function ($q) use ($idCotizacion){
                $q->where("id_cotizacion", $idCotizacion);
            }
        ])->orderByRaw(\DB::raw("FIELD(id, ".( implode( ",", $planes ) ).")"))->get();
        
        //$lista = \App\Descuento::where("id_cotizacion", $idCotizacion)->with(["cotizacion", "paquete"])->orderByRaw(\DB::raw("FIELD(id_paquete, ".( implode( ",", $planes ) ).")"))->get();
        return \View::make("admin.cotizacion.info_planes", ["idCotizacion" => $idCotizacion, "paquetes" => $lista, "valores" => $valores, "descuento" => $descuento, "cantidad" => $cantidad, "meses" => $meses, "horas" => $horas, "subtotal" => $subtotal, "iva" => $iva, "total" => $total, "vence" => $vence, "mensaje" => $mensaje])->render();
    }
    
    
    function verCotizacion($id){
        $cotizacion = \App\Cotizacion::with(["usuario", "cliente", "descuentos.paquete"])
                        ->where("id", $id)
                        ->first();
        if(\Auth::user()->admin == 'Y'){
            return \View::make("admin.cotizacion.ver", ["cotizacion" => $cotizacion]);        
        }else{
            return \View::make("comercial.cotizacion.ver", ["cotizacion" => $cotizacion]);        
        }
    }
    
    function estadosCotizacion($id){
        $cotizacion = \App\Cotizacion::with(["usuario", "cliente", "descuentos.paquete"])
                        ->where("id", $id)
                        ->first();    
        $estados = \App\Helpers\Helper::estados();
        
        if(\Auth::user()->admin == 'Y'){
            return \View::make("admin.cotizacion.estados", ["cotizacion" => $cotizacion, "estados" => $estados]);        
        }else{            
            return \View::make("comercial.cotizacion.estados", ["cotizacion" => $cotizacion, "estados" => $estados]);        
        }
    }
    
    function cambiarEstadoCotizacion($id){
        $cotizacion = \App\Cotizacion::with(["usuario", "cliente"])->find($id);
        $cotizacion->estado = Input::get("estado");
        $cotizacion->save();
        
        $vars = [
            "nombre_cliente" => $cotizacion->cliente !== NULL ? $cotizacion->cliente->nombre .' '. $cotizacion->cliente->apellido : $cotizacion->nombre_cliente,
            "nombre_comercial" => $cotizacion->usuario->nombre,
            "cotizacion_fecha" => $cotizacion->fecha_creacion,
            "cotizacion_estado" => \App\Helpers\Helper::estados()[$cotizacion->estado],
        ];
            
        $this->enviarMensaje($vars, 'cambio_estado_cotizacion', $cotizacion->usuario->email, $cotizacion->usuario->nombre);
		
		$cliente = $cotizacion->cliente !== NULL ? $cotizacion->cliente->nombre .' '. $cotizacion->cliente->apellido : $cotizacion->nombre_cliente ;
        
		$this->enviarMensaje($vars, 'cambio_estado_cotizacion_'.$cotizacion->estado, $cotizacion->correo, $cliente);
		
        if(\Auth::user()->admin == 'Y'){
            return \Redirect::action('Admin\CotizacionController@mostrarIndex')->with("mensaje", "Estado de la Cotizaci&oacute;n modificado exitosamente.");
        }else{
            return \Redirect::to('comercial/cotizaciones')->with("mensaje", "Estado de la Cotizaci&oacute;n modificado exitosamente.");
        }            
    }
    
    function eliminarCotizacion($id){
        $cotizacion = \App\Cotizacion::find($id)->delete();
        if(\Auth::user()->admin == 'Y'){
            return \Redirect::action('Admin\CotizacionController@mostrarIndex')->with("mensaje", "Cotizaci&oacute;n eliminada exitosamente.");        
        }else{
            return \Redirect::to('comercial/cotizaciones')->with("mensaje", "Cotizaci&oacute;n eliminada exitosamente.");        
        }
    }
}
