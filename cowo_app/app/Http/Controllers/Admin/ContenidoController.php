<?php

namespace App\Http\Controllers\Admin;

use \Illuminate\Support\Facades\Input;

class ContenidoController extends AdminController {
    
    var $destino1 = "desarrollo@encubo.ws"; //"info@encarguelo.com";
    var $destino2 = "desarrollo@encubo.ws";

    public function mostrarIndex() {
        $emails = \App\Contenido::where("tipo", "E")->get();
        return \View::make("admin.contenido.index", ["emails" => $emails]);
    }
    
    public function mostrarFormContenidoEmail($key){
        $contenido = \App\Contenido::where("llave", $key)->first();
        return \View::make('admin.contenido.form_email', ["contenido" => $contenido]);
    } 
	
    public function formCrearContenidoEmail(){
        return \View::make('admin.contenido.form_crear_email');
    }
    
    public function guardarContenido(){
        $id = Input::get("id");
        $contenido = \App\Contenido::find($id);
        
        if (!sizeof($contenido)){
            $contenido = new \App\Contenido();
        }
        
        $contenido->fill(Input::all());
        
        if ($contenido->tipo == "S"){
            $car = ["á", "é", "í", "ó", "ú", "ï", "ü", " ", ",", "/", "?", "¿", "!", "¡"];
            $rep = ["a", "e", "i", "o", "u", "i", "u", "-", "-", "", "", "", "", ""];

            $contenido->llave = str_replace($car, $rep, strtolower($contenido->titulo));
        }
        
        if (empty($contenido->inicio)){
            $contenido->inicio = 'N';
        }
        
        if($contenido->save()){
            $action = '';
            if (Input::has('guardar_permancer')){
                if ($contenido->tipo == "E"){
                    $action = \Redirect::action("Admin\ContenidoController@mostrarFormContenidoEmail", ["key" => $contenido->llave]);
                }
                else{
                    $action = \Redirect::action("Admin\ContenidoController@editarSeccion", ["id" => $contenido->id]);
                }
            }
            else{
                if ($contenido->tipo == "E"){
                    $action = \Redirect::action("Admin\ContenidoController@mostrarIndex");
                }
                else{
                    $action = \Redirect::action("Admin\ContenidoController@mostrarListaSecciones");
                }
            }
            
            return $action->with("mensaje", "Contenido guardado exitosamente");
        }
        else{
            if ($contenido->tipo == "E"){
                return \Redirect::action("Admin\ContenidoController@mostrarIndex");
            }
            else{
                return \Redirect::action("Admin\ContenidoController@mostrarListaSecciones");
            }
        }
    }
    
	public function eliminarContenido($id){
        $contenido = \App\Contenido::find($id);
        
        if (!sizeof($contenido)){
            return \Redirect::action('Admin\ContenidoController@mostrarIndex')->with("mensajeError", "Contenido no encontrado");
        }
        
        if ($contenido->delete()){
            return \Redirect::action('Admin\ContenidoController@mostrarIndex')->with("mensaje", "Contenido borrado exitosamente");
        }
        else{
            return \Redirect::action('Admin\ContenidoController@mostrarIndex')->with("mensajeError", "No se pudo borrar el Contenido");
        }
    }
	
    public function mostrarListaSecciones(){
		$secciones = \App\Contenido::where("tipo", "S")->orderBy("inicio")->orderBy("peso")->get();
        return \View::make('admin.contenido.lista_secciones', ["secciones" => $secciones]);
    }
    
    public function crearSeccion(){
        return $this->mostrarFormContenidoSeccion(new \App\Contenido());
    }
    
    public function editarSeccion($id){
        $contenido = \App\Contenido::find($id);
        
        if (!sizeof($contenido)){
            return \Redirect::action('Admin\ContenidoController@mostrarListaSecciones')->with("mensajeError", "No se pudo encontrar la sección");
        }
        
        return $this->mostrarFormContenidoSeccion($contenido);
    }

    public function mostrarFormContenidoSeccion($contenido){
        return \View::make('admin.contenido.form_seccion', ["contenido" => $contenido]);
    }
	
    public function eliminarSeccion($id){
        $contenido = \App\Contenido::find($id);
        
        if (!sizeof($contenido)){
            return \Redirect::action('Admin\ContenidoController@mostrarListaSecciones')->with("mensajeError", "Secci&oacute;n no encontrada");
        }
        
        if ($contenido->delete()){
            return \Redirect::action('Admin\ContenidoController@mostrarListaSecciones')->with("mensaje", "Secci&oacute;n borrada exitosamente");
        }
        else{
            return \Redirect::action('Admin\ContenidoController@mostrarListaSecciones')->with("mensajeError", "No se pudo borrar la Secci&oacute;n");
        }
    }

}
