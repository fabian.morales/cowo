<?php

namespace App\Http\Controllers\Admin;

use \Illuminate\Support\Facades\Input;

class FaqController extends AdminController {

    public function mostrarIndex() {
        $faqs = \App\Faq::paginate(20);
        return \View::make('admin.faq.index', array("faqs" => $faqs));
    }

    public function mostrarFormFaq($faq) {
        if (!sizeof($faq)) {
            $faq = new \App\Faq();
        }
        
        return \View::make("admin.faq.form", array("faq" => $faq));
    }

    public function crearFaq() {
        return $this->mostrarFormFaq(new \App\Faq());
    }

    public function editarFaq($id) {
        $faq = \App\Faq::find($id);
        if (!sizeof($faq)) {
            return \Redirect::action('Admin\FaqController@mostrarIndex')->with("mensajeError", "No se pudo encontrar la faq");
        }

        return $this->mostrarFormFaq($faq);
    }

    public function guardarFaq() {
        $id = Input::get("id");

        $faq = \App\Faq::find($id);
        if (!sizeof($faq)) {
            $faq = new \App\Faq();
        }

        $faq->fill(Input::all());
        if (empty($faq->activo)){
            $faq->activo = 'N';
        }
        
       if (empty($faq->peso)){
            $faq->peso = 0;
        }
        
        if (empty($faq->titulo)){
            \Illuminate\Support\Facades\Session::flash("mensajeError", "Debe ingresar el nombre de la faq");
            return $this->mostrarFormFaq($faq);
        }

        if ($faq->save()) {           
            return \Redirect::action('Admin\FaqController@mostrarIndex')->with("mensaje", "Faq guardada exitosamente");
        } else {
            return \Redirect::action('Admin\FaqController@mostrarIndex')->with("mensajeError", "No se pudo guardar la faq");
        }
    }
	
	
    public function eliminarFaq($id) {
        $faq = \App\Faq::find($id);

        if ($faq->delete()) {           
            return \Redirect::action('Admin\FaqController@mostrarIndex')->with("mensaje", "Faq eliminada exitosamente");
        } else {
            return \Redirect::action('Admin\FaqController@mostrarIndex')->with("mensajeError", "No se pudo eliminar la faq");
        }
    }
}
