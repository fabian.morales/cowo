<?php

namespace App\Http\Controllers\Admin;

use \Illuminate\Support\Facades\Input;

class ServicioController extends AdminController {
    
    use \App\MailTrait;
    
    public function infoReserva(){
        $info = \App\Contratacion::with('servicio')->find(Input::get('id'))->toJson();
        return $info;
    }
      
    public function mostrarIndex() {
        //$productos = \App\Producto::orderBy("id");
        
        /*$nombre = Input::get("nombre");
        $codigo = Input::get("codigo");
        $destacado = Input::get("destacado");
        $busqueda = [];
        
        if (!empty($nombre)){
            $productos = $productos->where("nombre", "like", "%".$nombre."%");
            $busqueda["nombre"] = $nombre;
        }
        
        if (!empty($destacado)){
            $productos = $productos->where("destacado", 'S');
            $busqueda["destacado"] = $destacado;
        }
        
        if (!empty($codigo)){
            $productos = $productos->where("codigo_unico", "like", "%".$cliente."%");
            $busqueda["codigo"] = $codigo;
        }*/
        
        //$productos = $productos->paginate(20)->appends($busqueda);
        $servicios = \App\Servicio::orderBy("id")->paginate(20);
        
        return \View::make('admin.servicio.index', array("servicios" => $servicios));
    }

    public function mostrarFormServicio($servicio) {
        if (!sizeof($servicio)) {
            $servicio = new \App\Servicio();
        }
        
        $periodos = [
            'N' => 'No aplica',
            'H' => 'Hora',
            'D' => 'Día',
            'Q' => 'Quincena',
            'M' => 'Mes',
            'T' => 'Trimestre',
            'S' => 'Semestre',
            'A' => 'Año',
        ];

        return \View::make("admin.servicio.form", array("servicio" => $servicio, "periodos" => $periodos));
    }

    public function crearServicio() {
        return $this->mostrarFormServicio(new \App\Servicio());
    }

    public function editarServicio($id) {
        $servicio = \App\Servicio::find($id);
        if (!sizeof($servicio)) {
            return \Redirect::action('Admin\ServicioController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el servicio");
        }

        return $this->mostrarFormServicio($servicio);
    }

    public function guardarServicio() {
        $id = Input::get("id");

        $servicio = \App\Servicio::find($id);
        if (!sizeof($servicio)) {
            $servicio = new \App\Servicio();
        }

        $servicio->fill(Input::all());
        
        if (empty($servicio->nombre)){
            \Illuminate\Support\Facades\Session::flash("mensajeError", "Debe ingresar el nombre del servicio");
            return $this->mostrarFormServicio($producto);
        }
        
        if (empty($servicio->creditos)){
            \Illuminate\Support\Facades\Session::flash("mensajeError", "Debe ingresar el número de créditos del servicio");
            return $this->mostrarFormServicio($servicio);
        }
        

        if ($servicio->save()) {
            return \Redirect::action('Admin\ServicioController@mostrarIndex')->with("mensaje", "Servicio guardado exitosamente");
        } else {
            return \Redirect::action('Admin\ServicioController@mostrarIndex')->with("mensajeError", "No se pudo guardar el servicio");
        }
    }
    
    function borrarServicio($id){
        $servicio = \App\Servicio::find($id);
        
        if (!sizeof($servicio)){
            return \Redirect::action('Admin\ServicioController@mostrarIndex')->with("mensajeError", "Servicio no encontrado");
        }
        
        /*$cnt = \App\Compra::where("id_paquete", $id)->count();
        if ($cnt > 0){
            return \Redirect::action('Admin\PaqueteController@mostrarIndex')->with("mensajeError", "El paquete no puede ser borrado porque tiene compras asociadas");
        }*/        
        
        if ($servicio->delete()){
            return \Redirect::action('Admin\ServicioController@mostrarIndex')->with("mensaje", "Servicio borrado exitosamente");
        }
        else{
            return \Redirect::action('Admin\ServicioController@mostrarIndex')->with("mensajeError", "No se pudo borrar el servicio");
        }
    }
    
    public function todasLasReservas(){
        $reservas = \App\Contratacion::with(['servicio', 'cliente'])
        ->whereHas("servicio", function($q) {
            $q->whereRaw("periodicidad in ('D', 'H')");
        })
        ->whereNotNull('fecha_inicio')
        ->where('fecha_inicio', '>', date('Y-m-d H:i:s'))
        ->orderBy('created_at', 'desc')->get();
        $_reservas = [];
        
        foreach ($reservas as $r){
            $reserva = [
                'title' => $r->cliente->nombre.", Sala ".$r->id_sala,
                'start' => $r->fecha_inicio,
                'end' => $r->fecha_fin
            ];
            
            $_reservas[] = $reserva;
        }
        
        return \Psy\Util\Json::encode($_reservas);
    }
    
    public function mostrarReservas(){
        $reservas = \App\Contratacion::with(['servicio', 'cliente'])
        ->whereHas("servicio", function($q) {
            $q->whereRaw("periodicidad in ('D', 'H')");
        })
        ->whereNotNull('fecha_inicio')
        ->where('fecha_inicio', '>', date('Y-m-d H:i:s'))
        ->orderBy('created_at', 'desc')->get();
        
        return \View::make("admin.servicio.reservas", array("reservas" => $reservas));
    }
    
    public function crearReserva(){
        $filtro = Input::get("filtro");
        
        if(!empty($filtro)) {
            $usuarios = \App\User::with('cliente.membresiaActual')
                ->whereHas("cliente", function($q) use ($filtro) {
                    $q->where('nombre', 'LIKE', '%'.$filtro.'%')
                        ->orWhere('apellido', 'LIKE', '%'.$filtro.'%')
                        ->orWhere('email', 'LIKE', '%'.$filtro.'%')
                        ->orWhere(\DB::raw('concat(nombre," ",apellido)') , 'LIKE' , "%".$filtro."%");
                })
            ->paginate(20)->appends($filtro);
        }else{
            $usuarios = \App\User::with('cliente.membresiaActual')->paginate(20);
        }
        return \View::make('admin.servicio.crear_reserva', ["filtro" => $filtro, "usuarios" => $usuarios]);
    }
    
    public function editarReserva(){
        try{
            $id = Input::get("id");
            $con = \App\Contratacion::find($id);
            
            $fInicio = Input::get("fecha");
            $hInicio = Input::get("hora", "00");
            $mInicio = Input::get("minuto", "00");
            $inicio = $fInicio." ".$hInicio.":".$mInicio.":00";
            $fechaInicio = new \DateTime($inicio);
            $fechaFin = new \DateTime($inicio);
            $idSala = Input::get("id_sala");
            
            $hFin = $fechaFin->format("H");

            $vigencias = [
                'H' => 'PT1H',
                'D' => 'P1D',
                'Q' => 'P15D',
                'M' => 'P1M',
                'T' => 'P3M',
                'S' => 'P6M',
                'A' => 'P1Y'
            ];
            
            if ($vigencias[$con->servicio->periodicidad] == 'H' && ($hInicio < 7 || $hInicio > 7 || $hFin < 7 || $hFin > 7)){
                return json_encode(["ok" => 0, "msg" => 'Debe seleccionar una hora entre 7AM y 7PM']);
            }
            
            $dw = $fechaInicio->format("w");
            if ($dw == 6 || $dw == 0){
                return json_encode(["ok" => 0, "msg" => 'Las reservas solo se pueden hacer para días de lunes a viernes '.$dw]);
            }

            $fechaFin->add(new \DateInterval($vigencias[$con->servicio->periodicidad]));
            $cnt = \App\Contratacion::where(function($q) use($fechaInicio, $fechaFin){
                $q->where("fecha_inicio", ">", $fechaInicio)->where("fecha_inicio", "<", $fechaFin);
            })->orWhere(function($q) use($fechaInicio, $fechaFin){
                $q->where("fecha_fin", ">", $fechaInicio)->where("fecha_fin", "<", $fechaFin);
            })->orWhere(function($q) use($fechaInicio, $fechaFin){
                $q->where("fecha_inicio", "=", $fechaInicio)->where("fecha_fin", "=", $fechaFin);
            })->where("id_sala", $idSala)->count();

            if ($cnt > 0){
                return json_encode(["ok" => 0, "msg" => 'La sala '.$idSala.' ya está separada para el periodo seleccionado']);
            }

            $con->id_sala = $idSala;
            $con->fecha_inicio = $fechaInicio;
            $con->fecha_fin = $fechaFin;
            $con->save();
            
            $vars = [
                "nombre_cliente" => $con->cliente->nombre,
                "direccion_cliente" => $con->cliente->direccion,
                "ciudad_cliente" => $con->cliente->ciudad,
                "correo_cliente" => $con->cliente->email,
                "empresa_cliente" => $con->cliente->empresa,
                "creditos" => $con->servicio->creditos,
                "nombre_servicio" => $con->servicio->nombre,
                "id_sala" => 'Sala '.$con->id_sala.' reservada',
                "fecha_inicio_servicio" => 'Fecha de inicio: '.$con->fecha_inicio->format('Y-m-d H:i:s'),
                "fecha_fin_servicio" => 'Fecha de fin: '.$con->fecha_fin->format('Y-m-d H:i:s')
            ];
                
            $this->enviarMensaje($vars, 'servicio-cowo-editado', $con->cliente->email, $con->cliente->nombre);
            $this->enviarMensaje($vars, 'edicion-de-servicio-contratado', $this->destino1, 'Cowo.com.co');

            //return \Redirect::action('SesionController@mostrarPerfil')->with('mensaje', 'Reserva eliminada exitosamente');
            return json_encode(["ok" => 1]);
        } catch (Exception $e) {
            return $this->retornarError($e);
            //return \Redirect::action('SesionController@mostrarPerfil')->with('mensajeError', 'No se pudo eliminar la reserva');
        }
    }
    
    public function cancelarReserva($id){
        $reserva = \App\Contratacion::with(["cliente", "servicio"])->where("id", $id)->first();
        
        if (!sizeof($reserva)){
            return \Redirect::action('Admin\ServicioController@mostrarReservas')->with("mensajeError", "Reserva no encontrada");
        }
        $cliente = \App\Cliente::where("id", $reserva->id_cliente)->first();
        
        $vars = [
                "nombre_cliente" => $reserva->cliente->nombre,
                "direccion_cliente" => $reserva->cliente->direccion,
                "ciudad_cliente" => $reserva->cliente->ciudad,
                "correo_cliente" => $reserva->cliente->email,
                "empresa_cliente" => $reserva->cliente->empresa,
                "creditos" => $reserva->servicio->creditos,
                "nombre_servicio" => $reserva->servicio->nombre,
                "id_sala" => 'Sala '.$reserva->id_sala.' reservada',
                "fecha_inicio_servicio" => 'Fecha de inicio: '.$reserva->fecha_inicio,
                "fecha_fin_servicio" => 'Fecha de fin: '.$reserva->fecha_fin
            ];
            
        $this->enviarMensaje($vars, 'servicio-cowo-eliminado-admin', $reserva->cliente->email, $reserva->cliente->nombre);
        $this->enviarMensaje($vars, 'servicio-eliminado-admin', $this->destino1, 'Cowo.com.co');
        $cliente->creditos += $reserva->servicio->creditos;
        $cliente->save();
        
        $reserva->fecha_inicio = null;
        $reserva->fecha_fin = null;
        
        if ($reserva->save()){
            return \Redirect::action('Admin\ServicioController@mostrarReservas')->with("mensaje", "Reserva cancelada exitosamente");
        }
        else{
            return \Redirect::action('Admin\ServicioController@mostrarReservas')->with("mensajeError", "No se pudo cancelar la reserva");
        }
    }

}
