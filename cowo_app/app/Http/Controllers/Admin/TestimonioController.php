<?php

namespace App\Http\Controllers\Admin;

use \Illuminate\Support\Facades\Input;

class TestimonioController extends AdminController {

    public function mostrarIndex() {
        $tests = \App\Testimonio::paginate(20);
        return \View::make('admin.testimonio.index', array("testimonios" => $tests));
    }

    public function mostrarFormTest($test) {
        if (!sizeof($test)) {
            $test = new \App\Testimonio();
        }
        
        return \View::make("admin.testimonio.form", array("testimonio" => $test));
    }

    public function crearTest() {
        return $this->mostrarFormTest(new \App\Testimonio());
    }

    public function editarTest($id) {
        $test = \App\Testimonio::find($id);
        if (!sizeof($test)) {
            return \Redirect::action('Admin\TestimonioController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el item de testimonios");
        }

        return $this->mostrarFormTest($test);
    }

    public function guardarTest() {
        $id = Input::get("id");

        $test = \App\Testimonio::find($id);
        if (!sizeof($test)) {
            $test = new \App\Testimonio();
        }

        $test->fill(Input::all());
        if (empty($test->activo)){
            $test->activo = 'N';
        }
        
        if (empty($test->titulo)){
            \Illuminate\Support\Facades\Session::flash("mensajeError", "Debe ingresar el nombre del testimonio");
            return $this->mostrarFormTest($test);
        }

        if ($test->save()) {
            if (Input::hasFile('imagen') && Input::file('imagen')->isValid()) {
                Input::file('imagen')->move(public_path('imagenes/testimonios'), $test->id.'.png');
            }
            
            return \Redirect::action('Admin\TestimonioController@mostrarIndex')->with("mensaje", "Testimonio guardado exitosamente");
        } else {
            return \Redirect::action('Admin\TestimonioController@mostrarIndex')->with("mensajeError", "No se pudo guardar el testimonio");
        }
    }
}
