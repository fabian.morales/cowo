<?php

namespace App\Http\Controllers\Admin;

use \Illuminate\Support\Facades\Input;

class PublicidadController extends AdminController {

    public function mostrarIndex() {
        $publicidad = \App\Publicidad::paginate(20);
        return \View::make('admin.publicidad.index', array("publicidad" => $publicidad));
    }

    public function mostrarFormPublicidad($publicidad) {
        if (!sizeof($publicidad)) {
            $publicidad = new \App\Publicidad();
        }
        
        return \View::make("admin.publicidad.form", array("publicidad" => $publicidad));
    }

    public function crearPublicidad() {
        return $this->mostrarFormPublicidad(new \App\Publicidad());
    }

    public function editarPublicidad($id) {
        $publicidad = \App\Publicidad::find($id);
        if (!sizeof($publicidad)) {
            return \Redirect::action('Admin\PublicidadController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el item de publicidad");
        }

        return $this->mostrarFormPublicidad($publicidad);
    }

    public function guardarPublicidad() {
        $id = Input::get("id");

        $publicidad = \App\Publicidad::find($id);
        if (!sizeof($publicidad)) {
            $publicidad = new \App\Publicidad();
        }

        $publicidad->fill(Input::all());
        
        if($publicidad->save()){
            if (Input::has('guardar_permancer')){
                return \Redirect::action("Admin\PublicidadController@editarPublicidad", ["id" => $publicidad->id])->with("mensaje", "Publicidad guardada exitosamente");
            }
            else{
                return \Redirect::action('Admin\PublicidadController@mostrarIndex')->with("mensaje", "Publicidad guardada exitosamente");
            }
            
        }
        else{
            return \Redirect::action('Admin\PublicidadController@mostrarIndex')->with("mensajeError", "No se pudo guardar el item de publicidad");
        }
    }
    
    public function borrarPublicidad($id){
        $publicidad = \App\Publicidad::find($id);
        if (!sizeof($publicidad)) {
            return \Redirect::action('Admin\PublicidadController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el item de publicidad");
        }
        
        if ($publicidad->delete()){
            return \Redirect::action('Admin\PublicidadController@mostrarIndex')->with("mensaje", "Publicidad borrada exitosamente");
        } else {
            return \Redirect::action('Admin\PublicidadController@mostrarIndex')->with("mensajeError", "No se pudo borrar el item de publicidad");
        }
    }
}
