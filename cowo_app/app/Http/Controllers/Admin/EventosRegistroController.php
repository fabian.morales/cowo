<?php

namespace App\Http\Controllers\Admin;
use \Illuminate\Support\Facades\Input;

class EventosRegistroController extends AdminController {
    
    public function index($id, $filtro = '') {
        $filtro = Input::get('filtro');
        
        if( empty($filtro) ){
            $lista = \App\EventoRegistro::where('id_evento', $id)->paginate(10);
        }else{
            $lista = \App\EventoRegistro::where('id_evento', $id)
                        ->where(function($q) use ($filtro){
                            $q->where('nombre', 'LIKE', '%'.$filtro.'%')
                            ->orWhere('email', 'LIKE', '%'.$filtro.'%')
                            ->orWhere('telefono', 'LIKE', '%'.$filtro.'%');
                        })
                        ->paginate(10)->appends($id, $filtro);
        }
        $evento =  \App\Evento::find($id);
        return \View::make("admin.eventos.registro.index", ["evento" => $evento, "lista" => $lista, "filtro" => $filtro]);
    }

    
    function delete($id){
        $registro = \App\EventoRegistro::find($id);
        
        if (!sizeof($registro)){
            return \Redirect::to('administrador/eventos/registro/'.$registro->id_evento)->with("mensajeError", "Registro no encontrado");
        }
        
        if ($registro->delete()){
            return \Redirect::to('administrador/eventos/registro/'.$registro->id_evento)->with("mensaje", "Registro borrado exitosamente");
        }
        else{
            return \Redirect::to('administrador/eventos/registro/'.$registro->id_evento)->with("mensajeError", "No se pudo borrar el Registro");
        }
    }

}
