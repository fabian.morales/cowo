<?php

namespace App\Http\Controllers\Admin;
use \Illuminate\Support\Facades\Input;

class EventosController extends AdminController {
    
    public function index($filtro = '') {
        $filtro = Input::get('filtro');
         if( empty($filtro) ){
            $lista = \App\Evento::with('registros')->orderBy("id", "DESC")->paginate(10);
        }else{
            $lista = \App\Evento::with('registros')->where('evento', 'LIKE', '%'.$filtro.'%')
                        ->orWhere('descripcion', 'LIKE', '%'.$filtro.'%')
                        ->orWhere('fecha', 'LIKE', '%'.$filtro.'%')
                        ->orWhere('lugar', 'LIKE', '%'.$filtro.'%')
                        ->orderBy("id", "DESC")
                        ->paginate(10)->appends($filtro);
        }
        return \View::make("admin.eventos.index", ["lista" => $lista, "filtro" => $filtro]);
    }
    
    
    public function create() {
        return \View::make("admin.eventos.crear");
    }
    
    
    public function store(){
        
        $evento = new \App\Evento;
        $evento->evento = Input::get("evento");
        $evento->descripcion = Input::get("descripcion");
        $evento->fecha = Input::get("fecha");
        $evento->lugar = Input::get("lugar");
        $evento->save();
        
        if(Input::file("imagen") != NULL){
            $imagen = Input::file("imagen")->getClientOriginalName();
            $evento->imagen = $imagen;
            
            $imageSize = getimagesize( Input::file('imagen')->getRealPath() );
        
        
            if(sizeof($imagen) && ($imageSize[0] < 500 || $imageSize[1] < 500 ) ){
                return \Redirect::action('Admin\EventosController@index')->with("mensajeError", "La imagen debe tener un alto y un ancho m&iacute;nimo de 500px ");                
            }else if (sizeof($imagen) && Input::file("imagen")->isValid()){
                $path = public_path('storage/eventos/'.$evento->id.'/');

                if (!is_dir($path)) {
                    $partesPath = ['storage', 'eventos', $evento->id];
                    $subpath = public_path() . '/';
                    foreach ($partesPath as $p) {
                        $subpath .= $p . '/';

                        if (!is_dir($subpath)) {
                            mkdir($subpath);
                        }
                    }
                }

                Input::file("imagen")->move($path, $imagen);  
                
                $evento->save();
                
            }else{
                return \Redirect::action('Admin\EventosController@index')->with("mensajeError", "Debe seleccionar una imagen");    
            }            
        }
        
        return \Redirect::action('Admin\EventosController@index')->with("mensaje", "Evento creado exitosamente");
    }
    
    
    public function edit($id) {
        $evento = \App\Evento::find($id);
        return \View::make("admin.eventos.editar", ["evento" => $evento]);
    }
    
    
    public function update($id){
        
        $evento = \App\Evento::find($id);
        
        if (!sizeof($evento)){
            return \Redirect::action('Admin\EventosController@index')->with("mensajeError", "Evento no encontrado");
        }else{
            $evento->evento = Input::get("evento");
            $evento->descripcion = Input::get("descripcion");
            $evento->fecha = Input::get("fecha");
            $evento->lugar = Input::get("lugar");
            
            if(Input::file("imagen") != NULL){
            
                $imagen = Input::file("imagen")->getClientOriginalName();
                $evento->imagen = $imagen;
                
                $imageSize = getimagesize( Input::file('imagen')->getRealPath() );
                        
                if(sizeof($imagen) && ($imageSize[0] < 500 || $imageSize[1] < 500 ) ){
                    return \Redirect::action('Admin\EventosController@index')->with("mensajeError", "La imagen debe tener un alto y un ancho m&iacute;nimo de 500px ");                
                }else if (sizeof($imagen) && Input::file("imagen")->isValid()){
                    $path = public_path('storage/eventos/'.$evento->id.'/');

                    if (!is_dir($path)) {
                        $partesPath = ['storage', 'eventos', $evento->id];
                        $subpath = public_path() . '/';
                        foreach ($partesPath as $p) {
                            $subpath .= $p . '/';

                            if (!is_dir($subpath)) {
                                mkdir($subpath);
                            }
                        }
                    }

                    Input::file("imagen")->move($path, $imagen);                      
                }else{
                    return \Redirect::action('Admin\EventosController@index')->with("mensajeError", "Debe seleccionar una imagen");    
                }            
            }
            $evento->save();
            
            return \Redirect::action('Admin\EventosController@index')->with("mensaje", "Evento modificado exitosamente");
        }        
    }
    
    
    function delete($id){
        $evento = \App\Evento::find($id);
        
        if (!sizeof($evento)){
            return \Redirect::action('Admin\EventosController@index')->with("mensajeError", "Evento no encontrado");
        }
        
        if ($evento->delete()){
            return \Redirect::action('Admin\EventosController@index')->with("mensaje", "Evento borrado exitosamente");
        }
        else{
            return \Redirect::action('Admin\EventosController@index')->with("mensajeError", "No se pudo borrar el Evento");
        }
    }

}
