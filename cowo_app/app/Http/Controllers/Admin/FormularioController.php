<?php

namespace App\Http\Controllers\Admin;

use \Illuminate\Support\Facades\Input;

class FormularioController extends AdminController {
    
    var $tipos = [
        'texto-corto' => 'Texto corto',
        'texto-largo' => 'Texto largo',
        'numero' => 'N&uacute;mero',
        'email' => 'Correo',
        'calendario' => 'Calendario',
        'lista-desplegable' => 'Lista desplegable',
        'lista-checks' => 'Lista selecci&oacute;n m&uacute;ltiple',
        'lista-radios' => 'Lista selecci&oacute;n &uacute;nica',
        'mapa' => 'Mapa',
        'imagen' => 'Imagen',
        'texto-p' => 'Texto personalizado',
    ];

    public function mostrarIndex() {
        $forms = \App\Formulario::paginate(20);
        return \View::make('admin.formulario.index', array("forms" => $forms));
    }

    public function mostrarForm($form) {
        if (!sizeof($form)) {
            $form = new \App\Formulario();
        }
        
        return \View::make("admin.formulario.form", array("form" => $form, "tipos" => $this->tipos));
    }

    public function crearForm() {
        return $this->mostrarForm(new \App\Formulario(['parametros' => '']));
    }

    public function editarForm($id) {
        $form = \App\Formulario::where("id", $id)->with(["campos" => function($q) {
            $q->orderBy("orden");
        }])->first();
        if (!sizeof($form)) {
            return \Redirect::action('Admin\FormularioController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el fomulario");
        }

        return $this->mostrarForm($form);
    }

    public function guardarForm() {
        $id = Input::get("id");

        $form = \App\Formulario::find($id);
        if (!sizeof($form)) {
            $form = new \App\Formulario();
        }

        $form->fill(Input::all());
        
        $car = ["á", "é", "í", "ó", "ú", "ï", "ü", " ", ",", "/", "?", "¿", "!", "¡", "-"];
        $rep = ["a", "e", "i", "o", "u", "i", "u", "_", "_", "", "", "", "", "", "_"];

        $form->llave = str_replace($car, $rep, strtolower($form->titulo));
                
        if (empty($form->titulo)){
            \Illuminate\Support\Facades\Session::flash("mensajeError", "Debe ingresar el nombre del formulario");
            return $this->mostrarForm($form);
        }

        if ($form->save()) {
            if (Input::has('guardar_permancer')){
                return \Redirect::action('Admin\FormularioController@editarForm', ["id" => $form->id])->with("mensaje", "Formulario guardado exitosamente");    
            }
            else{
                return \Redirect::action('Admin\FormularioController@mostrarIndex')->with("mensaje", "Formulario guardado exitosamente");
            }
            
        } else {
            return \Redirect::action('Admin\FormularioController@mostrarIndex')->with("mensajeError", "No se pudo guardar el formulario");
        }
    }
    
    public function borrarForm($id) {
        $form = \App\Formulario::find($id);
        if (!sizeof($form)) {
            return \Redirect::action('Admin\FormularioController@mostrarIndex')->with("mensajeError", "No se pudo enocntrar el formulario");
        }
        
        foreach ($form->campos()->get() as $c){
            $c->valores()->delete();
        }
        
        $form->respuestas()->delete();
        $form->campos()->delete();

        if ($form->delete()) {
            return \Redirect::action('Admin\FormularioController@mostrarIndex')->with("mensaje", "Formulario eliminado exitosamente");
            
        } else {
            return \Redirect::action('Admin\FormularioController@mostrarIndex')->with("mensajeError", "No se pudo eliminar el formulario");
        }
    }
    
    public function mostrarFormCampo($campo, $form) {
        if (!sizeof($campo)) {
            $campo = new \App\CampoForm(['parametros' => '']);
        }
        
        return \View::make("admin.formulario.form_campo", ["campo" => $campo, "form" => $form, "tipos" => $this->tipos]);
    }

    public function crearFormCampo($idForm) {
        $form = \App\Formulario::find($idForm);
        
        return $this->mostrarFormCampo(new \App\CampoForm(), $form);
    }

    public function editarFormCampo($id) {        
        $campo = \App\CampoForm::where("id", $id)->with("formulario")->first();
        if (!sizeof($campo)) {
            return \Redirect::action('Admin\FormularioController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el campo");
        }

        return $this->mostrarFormCampo($campo, $campo->formulario);
    }

    public function guardarFormCampo(\Illuminate\Http\Request $request) {
        $id = Input::get("id");
        $obligatorio = Input::get("obligatorio", "N");

        $campo = \App\CampoForm::find($id);
        if (!sizeof($campo)) {
            $campo = new \App\CampoForm();
        }

        $campo->fill(Input::all());
        
        $car = ["á", "é", "í", "ó", "ú", "ï", "ü", " ", ",", "/", "?", "¿", "!", "¡","-"];
        $rep = ["a", "e", "i", "o", "u", "i", "u", "-", "-", "", "", "", "", "","_"];

        $campo->llave = str_replace($car, $rep, strtolower($campo->nombre));
        $campo->obligatorio = $obligatorio;
        
        $parametros = [];
        switch ($campo->tipo){
            case 'lista-desplegable':
            case 'lista-checks':
            case 'lista-radios':
                $parametros['lista'] = Input::get('opciones-lista');
                break;
            case 'mapa':
                $parametros['puntos'] = Input::get('puntos-mapa');
                break;
            case 'imagen':
                $imagen = $request->file('imagen-campo');
                if (sizeof($imagen) && $imagen->isValid()){
                    $archivo = uniqid().'.'.$imagen->getClientOriginalExtension();
                    $path = public_path('storage/imagenes/forms/'.$campo->id_form.'/');

                    if (!is_dir($path)) {
                        $partesPath = ['storage', 'imagenes', 'forms',  $campo->id_form];
                        $subpath = public_path() . '/';
                        foreach ($partesPath as $p) {
                            $subpath .= $p . '/';

                            if (!is_dir($subpath)) {
                                mkdir($subpath);
                            }
                        }
                    }

                    $imagen->move($path, $archivo);
                    
                    $parametros['imagen'] = asset('storage/imagenes/forms/'.$campo->id_form.'/'.$archivo);
                }
                break;
            case 'texto-p':
                $parametros['texto'] = Input::get('texto-personalizado');
                break;
        }
        
        $campo->parametros = json_encode($parametros);
        
        if ($campo->save()) {
            $form = \App\Formulario::where("id", $campo->id_form)->with(["campos" => function($q) {
                $q->orderBy("orden");
            }])->first();

            return \View::make("admin.formulario.lista_campos", ["form" => $form, "tipos" => $this->tipos]);
        } else {
            $form = \App\Formulario::where("id", $campo->id_form)->with(["campos" => function($q) {
                $q->orderBy("orden");
            }])->first();

            return \View::make("admin.formulario.lista_campos", ["form" => $form, "tipos" => $this->tipos]);
        }
    }
    
    public function borrarFormCampo($id) {
        $campo = \App\CampoForm::find($id);
        if (!sizeof($campo)) {
            return "";
        }
        
        $idForm = $campo->id_form;
        $campo->valores()->delete();
        
        if ($campo->delete()) {
            $form = \App\Formulario::where("id", $idForm)->with(["campos" => function($q) {
                $q->orderBy("orden");
            }])->first();
        
            return \View::make("admin.formulario.lista_campos", ["form" => $form, "tipos" => $this->tipos]);
        } else {
            $form = \App\Formulario::where("id", $idForm)->with(["campos" => function($q) {
                $q->orderBy("orden");
            }])->first();
            
            return \View::make("admin.formulario.lista_campos", ["form" => $form, "tipos" => $this->tipos]);
        }
    }

    public function organizarCampos(){
        $campos = Input::get("campos");
        foreach ($campos as $i => $c){
            list($campo, $id) = explode("-", $c);
            \App\CampoForm::where("id", $id)->update(["orden" => $i]);
        }
    }
    
    public function visualizarForm($id) {        
        $form = \App\Formulario::where("id", $id)->with(["campos" => function($q) {
            $q->orderBy("orden");
        }])->first();
        
        if (!sizeof($form)) {
            return \Redirect::action('Admin\FormularioController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el fomulario");
        }

        return \View::make("admin.formulario.visualizacion_form", ["form" => $form]);
    }
    
    public function mostrarIndexRespuestas($id=0){
        
        $respuestas = [];
        if ($id > 0){
            $respuestas = \App\RespuestaForm::with("formulario")->whereHas("formulario", function($q) use($id) {
                $q->where("id", $id);
            })->orderBy("fecha", "desc")->paginate(20);
        }
        else{
            $respuestas = \App\RespuestaForm::with("formulario")->orderBy("fecha", "desc")->paginate(20);
        }
        
        return \View::make("admin.formulario.index_respuestas", ["respuestas" => $respuestas]);
    }
    
    public function mostrarDetalleRespuesta($id){
        $respuesta = \App\RespuestaForm::where("id", $id)->with(["formulario", "valores.campo"])->first();
        return \View::make("admin.formulario.detalle_respuesta", ["respuesta" => $respuesta]);
    }
}
