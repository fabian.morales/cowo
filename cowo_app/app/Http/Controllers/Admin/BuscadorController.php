<?php

namespace App\Http\Controllers\Admin;

use \Illuminate\Support\Facades\Input;

class BuscadorController extends AdminController {

    public function mostrarIndex() {
        $categorias = \App\CategoriaBuscador::paginate(20);
        
        return \View::make('admin.categorias_buscador.index', array("categorias" => $categorias));
    }
    
    public function mostrarFormCategoria($categoria) {
        if (!sizeof($categoria)) {
            $categoria = new \App\CategoriaBuscador();
        }

        return \View::make("admin.categorias_buscador.form", array("categoria" => $categoria));
    }

    public function crearCategoria() {
        return $this->mostrarFormCategoria(new \App\CategoriaBuscador());
    }

    public function editarCategoria($id) {
        $categoria = \App\CategoriaBuscador::find($id);
        if (!sizeof($categoria)) {
            return \Redirect::action('Admin\BuscadorController@mostrarIndex')->with("mensajeError", "No se pudo encontrar la categoría");
        }

        return $this->mostrarFormCategoria($categoria);
    }
    
    public function guardarCategoria() {
        $id = Input::get("id");

        $categoria = \App\CategoriaBuscador::find($id);
        if (!sizeof($categoria)) {
            $categoria = new \App\CategoriaBuscador();
        }

        $categoria->fill(Input::all());

        if ($categoria->save()) {
            return \Redirect::action('Admin\BuscadorController@mostrarIndex')->with("mensaje", "Categoría guardada exitosamente");
        } else {
            return \Redirect::action('Admin\BuscadorController@mostrarIndex')->with("mensajeError", "No se pudo guardar la categoría");
        }
    }

}
