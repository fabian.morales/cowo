<?php

namespace App\Http\Controllers\Admin;

use \Illuminate\Support\Facades\Input;

class MenuController extends AdminController {

    public function mostrarIndex() {
        $menus = \App\Menu::paginate(20);
        return \View::make('admin.menu.index', array("menus" => $menus));
    }

    public function mostrarFormMenu($menu) {
        if (!sizeof($menu)) {
            $menu = new \App\Menu();
        }
        
        $secciones = \App\Contenido::where("tipo", "S")->orderBy("titulo")->get();
        return \View::make("admin.menu.form", array("menu" => $menu, "secciones" => $secciones));
    }

    public function crearMenu() {
        return $this->mostrarFormMenu(new \App\Menu());
    }

    public function editarMenu($id) {
        $menu = \App\Menu::find($id);
        if (!sizeof($menu)) {
            return \Redirect::action('Admin\MenuController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el item de menu");
        }

        return $this->mostrarFormMenu($menu);
    }

    public function guardarMenu() {
        $id = Input::get("id");

        $menu = \App\Menu::find($id);
        if (!sizeof($menu)) {
            $menu = new \App\Menu();
        }

        $menu->fill(Input::all());
        $car = ["á", "é", "í", "ó", "ú", "ï", "ü", " ", ",", "/", "?", "¿", "!", "¡"];
        $rep = ["a", "e", "i", "o", "u", "i", "u", "-", "-", "", "", "", "", ""];
        
        if (empty($menu->llave)){
            $menu->llave = str_replace($car, $rep, strtolower($menu->titulo));
        }
        else{
            $menu->llave = str_replace($car, $rep, strtolower($menu->llave));
        }
        
        if (empty($menu->mostrar)){
            $menu->mostrar = 'N';
        }
        
        if (empty($menu->titulo)){
            \Illuminate\Support\Facades\Session::flash("mensajeError", "Debe ingresar el nombre del menu");
            return $this->mostrarFormMenu($menu);
        }

        if ($menu->save()) {
            return \Redirect::action('Admin\MenuController@mostrarIndex')->with("mensaje", "Item de menu guardado exitosamente");
        } else {
            return \Redirect::action('Admin\MenuController@mostrarIndex')->with("mensajeError", "No se pudo guardar el item de menu");
        }
    }
    
    public function borrarMenu($id){
        $menu = \App\Menu::find($id);
        if (!sizeof($menu)) {
            return \Redirect::action('Admin\MenuController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el item de menu");
        }
        
        if ($menu->delete()){
            return \Redirect::action('Admin\MenuController@mostrarIndex')->with("mensaje", "Item de menu borrado exitosamente");
        } else {
            return \Redirect::action('Admin\MenuController@mostrarIndex')->with("mensajeError", "No se pudo borrar el item de menu");
        }
    }
}
