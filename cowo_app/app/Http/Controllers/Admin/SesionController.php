<?php

namespace App\Http\Controllers\Admin;

use \Illuminate\Support\Facades\Input;
use \Illuminate\Support\Facades\Mail;
use \Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use \Riari\Forum\Models as Foro;

class SesionController extends Controller {
    use \App\MailTrait;
    
    public function contratarServicio($id) {
        
        try {
            \Illuminate\Support\Facades\DB::beginTransaction();
            $cliente = \App\Cliente::where("id_usuario", $id)->first();
            $servicio = \App\Servicio::find(Input::get("id_servicio"));
            $con = new \App\Contratacion();
            
            if (sizeof($servicio)){
                $vigencias = [
                    'H' => 'PT1H',
                    'D' => 'P1D',
                    'Q' => 'P15D',
                    'M' => 'P1M',
                    'T' => 'P3M',
                    'S' => 'P6M',
                    'A' => 'P1Y'
                ];

                $fecha = new \DateTime();
                $con->id_cliente = $cliente->id;
                $con->id_servicio = $servicio->id;
                $con->fecha_vence = $fecha->add(new \DateInterval($vigencias[$servicio->periodicidad]));
                $con->fecha_inicio = null;
                $con->fecha_fin = null;
                $con->activo = 'S';

                if (!$con->save()){
                    throw new \Exception('No se pudo contratar el servicio');
                }
            }

            $cliente->creditos -= $servicio->creditos;
            if (!$cliente->save()) {
                throw new \Exception('No se pudo restar los créditos a su cuenta');
            }
            
            if (!in_array($servicio->periodicidad, ['H', 'D'])){
                $vars = [
                    "nombre_cliente" => $cliente->nombre,
                    "direccion_cliente" => $cliente->direccion,
                    "ciudad_cliente" => $cliente->ciudad,
                    "correo_cliente" => $cliente->email,
                    "empresa_cliente" => $cliente->empresa,
                    "creditos" => $servicio->creditos,
                    "nombre_servicio" => $servicio->nombre
                ];
                
                $this->enviarMensaje($vars, 'contratacion_servicio', $cliente->email, $cliente->nombre);
                $this->enviarMensaje($vars, 'contratacion_servicio_adm', $this->destino1, 'Cowo.com.co');
            }

            \Illuminate\Support\Facades\DB::commit();

            return \Redirect::action('Admin\UsuarioController@mostrarPerfil', ['id' => $id])->with('mensaje', 'Servicio contratado exitosamente');
            
        } catch (\Exception $e) {
            \Illuminate\Support\Facades\DB::rollback();
            echo $e->getMessage(); die();
            return \Redirect::action('Admin\ServicioController@mostrarReservas')->with('mensajeError', $e->getMessage());
        }
    }
    
    
    public function obtenerReservas($idSala = ''){
        $fechaInicio = Input::get("start", date('Y-m-01 00:00:00'));
        $fechaFin = Input::get("end", date('Y-m-d 23:59:59'));
       
        if($idSala != NULL){
            $reservas = \App\Contratacion::with("cliente")
                    ->where("id_sala", $idSala)
                    ->where("fecha_inicio", ">=", $fechaInicio)
                    ->where("fecha_inicio", "<=", $fechaFin)
                    ->get();
        }else{
            $reservas = \App\Contratacion::with("cliente")
                    ->where("fecha_inicio", ">=", $fechaInicio)
                    ->where("fecha_inicio", "<=", $fechaFin)
                    ->get();
        }
        
        $_reservas = [];
        foreach ($reservas as $r){
            $reserva = [
                'title' => $r->cliente->nombre.", Sala ".$r->id_sala,
                'start' => $r->fecha_inicio,
                'end' => $r->fecha_fin
            ];
            
            $_reservas[] = $reserva;
        }
        
        return \Psy\Util\Json::encode($_reservas);
    }
    
    
    public function mostrarFormReserva($id){
        $con = \App\Contratacion::with(['servicio', 'cliente.usuario'])->where('id', $id)->first();
        return \View::make('admin.servicio.reserva', ["usuario" =>$con->cliente->usuario, "con" => $con]);
    }
    
    
    public function realizarReserva(){
        try{
            $id = Input::get("id_contratacion");
            $id_usuario = Input::get("id");
            $con = \App\Contratacion::with(["servicio", "cliente"])->where("id", $id)->first();
            
            if (!sizeof($con)){
                return json_encode(["ok" => 0, "msg" => 'Contratación de servicio no encontrada']);
            }
            
            $cliente = \App\Cliente::where("id_usuario", $id_usuario)->first();
            
            if (!sizeof($cliente)){
                return json_encode(["ok" => 0, "msg" => 'Complete su perfil']);
            }
            
            if ($con->id_cliente != $cliente->id){
                return json_encode(["ok" => 0, "msg" => 'La contratación no corresponde con el cliente']);
            }
            
            $fInicio = Input::get("fecha");
            $hInicio = Input::get("hora", "00");
            $mInicio = Input::get("minuto", "00");
            $inicio = $fInicio." ".$hInicio.":".$mInicio.":00";
            
            $fechaInicio = new \DateTime($inicio);
            $fechaFin = new \DateTime($inicio);
            $idSala = Input::get("id_sala");
            
            $hFin = $fechaFin->format("H");

            $vigencias = [
                'H' => 'PT1H',
                'D' => 'P1D',
                'Q' => 'P15D',
                'M' => 'P1M',
                'T' => 'P3M',
                'S' => 'P6M',
                'A' => 'P1Y'
            ];
            
            if ($vigencias[$con->servicio->periodicidad] == 'H' && ($hInicio < 7 || $hInicio > 7 || $hFin < 7 || $hFin > 7)){
                return json_encode(["ok" => 0, "msg" => 'Debe seleccionar una hora entre 7AM y 7PM']);
            }
            
            $dw = $fechaInicio->format("w");
            if ($dw == 6 || $dw == 0){
                return json_encode(["ok" => 0, "msg" => 'Las reservas solo se pueden hacer para días de lunes a viernes '.$dw]);
            }

            $fechaFin->add(new \DateInterval($vigencias[$con->servicio->periodicidad]));
            $cnt = \App\Contratacion::where(function($q) use($fechaInicio, $fechaFin){
                $q->where("fecha_inicio", ">", $fechaInicio)->where("fecha_inicio", "<", $fechaFin);
            })->orWhere(function($q) use($fechaInicio, $fechaFin){
                $q->where("fecha_fin", ">", $fechaInicio)->where("fecha_fin", "<", $fechaFin);
            })->orWhere(function($q) use($fechaInicio, $fechaFin){
                $q->where("fecha_inicio", "=", $fechaInicio)->where("fecha_fin", "=", $fechaFin);
            })->where("id_sala", $idSala)->count();

            if ($cnt > 0){
                return json_encode(["ok" => 0, "msg" => 'La sala '.$idSala.' ya está separada para el periodo seleccionado']);
            }

            $con->id_sala = $idSala;
            $con->fecha_inicio = $fechaInicio;
            $con->fecha_fin = $fechaFin;
            $con->save();
            
            $vars = [
                "nombre_cliente" => $con->cliente->nombre,
                "direccion_cliente" => $con->cliente->direccion,
                "ciudad_cliente" => $con->cliente->ciudad,
                "correo_cliente" => $con->cliente->email,
                "empresa_cliente" => $con->cliente->empresa,
                "creditos" => $con->servicio->creditos,
                "nombre_servicio" => $con->servicio->nombre,
                "id_sala" => 'Sala '.$con->id_sala.' reservada',
                "fecha_inicio_servicio" => 'Fecha de inicio: '.$con->fecha_inicio->format('Y-m-d H:i:s'),
                "fecha_fin_servicio" => 'Fecha de fin: '.$con->fecha_fin->format('Y-m-d H:i:s')
            ];
                
            $this->enviarMensaje($vars, 'contratacion_servicio', $con->cliente->email, $con->cliente->nombre);
            $this->enviarMensaje($vars, 'contratacion_servicio_adm', $this->destino1, 'Cowo.com.co');

            //return \Redirect::action('SesionController@mostrarPerfil')->with('mensaje', 'Reserva realizada exitosamente');
            return json_encode(["ok" => 1]);
        } catch (Exception $e) {
            return $this->retornarError($e);
            //return \Redirect::action('SesionController@mostrarPerfil')->with('mensajeError', 'No se pudo realizar la reserva');
        }
    }
    
}