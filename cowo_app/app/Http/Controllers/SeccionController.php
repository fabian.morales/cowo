<?php

namespace App\Http\Controllers;

use \Illuminate\Support\Facades\Input;
use \Illuminate\Support\Facades\Mail;
use \Illuminate\Support\Facades\Request;

class SeccionController extends Controller {
    use \App\SeccionTrait;
    use \App\MailTrait;
	
    public function mostrarIndex() {        
        /*$destacados = \App\Paquete::all();        
        $servicios = \App\Servicio::take(6)->get();
        $secciones = $this->obtenerSeccionesInicio();
        $menus = \App\Menu::where("id", "<=", 3)->get();

        return \View::make('seccion.index', $vars);*/
        
        $secciones = $this->obtenerSeccionesInicio();
        $vars = [
            "secciones" => $secciones, 
            "inicio" => 1,
            "menu" => null
        ];
        
        return \View::make('seccion.index', $vars);
    }
    
    public function mostrarGracias(){
        return \View::make('contenido.gracias');
    }
    
    public function mostrarSeccion($key, $id = ''){
        if (empty($key) || $key == '{% producto.imagen %}'){
            return \Redirect::action('SeccionController@mostrarIndex');
        }
        
        $seccionObj = \App\Contenido::where("llave", $key)->first();
        
        if (!sizeof($seccionObj)){
            return \Redirect::action('SeccionController@mostrarIndex')->with("mensajeError", "Sección no encontrada (0)");
        }
                
        $seccion = $this->obtenerSeccion($seccionObj, $id);
        
        $template = Request::ajax() ? "seccion.contenido_raw" : "seccion.contenido";
        
        return \View::make($template, ["seccion" => $seccion, "menu" => null, "meta_descripcion" => "", "meta_keywords" => ""]);
    }
    
    public function mostrarSeccionMenu($key){
        if (empty($key) || $key == '{% producto.imagen %}'){
            return \Redirect::action('SeccionController@mostrarIndex');
        }
        
        $menu = \App\Menu::with("seccion")->where("llave", $key)->first();
        
        if (!sizeof($menu)){
            return \Redirect::action('SeccionController@mostrarIndex')->with("mensajeError", "Sección no encontrada (1) - ".$key);
        }
        
        if (!sizeof($menu->seccion)){
            return \Redirect::action('SeccionController@mostrarIndex')->with("mensajeError", "Sección no encontrada (2) - ".$key);
        }
        
        $seccion = $this->obtenerSeccion($menu->seccion);
        
        $template = Request::ajax() ? "seccion.contenido_raw" : "seccion.contenido";
        // dd($seccion);
        return \View::make($template, ["seccion" => $seccion, "menu" => $menu, "meta_descripcion" => $menu->descripcion, "meta_keywords" => $menu->meta, "titulo_seccion" => $menu->titulo]);
    }
    
   public function enviarContacto(){
        try{
            $nombre = Input::get("nombre");
            $email = Input::get("email");
            $mensaje = Input::get("mensaje");
            
            if(empty($nombre)){
                return \Redirect::action('SeccionController@mostrarIndex')->with("mensajeError", "Debe ingresar el nombre");
            }
            
            if(empty($email)){
                return \Redirect::action('SeccionController@mostrarIndex')->with("mensajeError", "Debe ingresar el correo");
            }
            
            if(empty($mensaje)){
                return \Redirect::action('SeccionController@mostrarIndex')->with("mensajeError", "Debe ingresar el mensaje");
            }
            
            $datosEmail = [
                'nombre' => $nombre,
                'email' => $email,
                'mensaje' => $mensaje,
            ];
                        
            Mail::send('emails.contacto.contacto', $datosEmail, function($message) {
                $message->from('info@encarguelo.com', 'Encarguelo.com');
                $message->subject('Solicitud de contacto');
                $message->to('info@encarguelo.com', 'Encarguelo.com');
                $message->bcc('desarrollo@encubo.ws');
            });
            
            return \Redirect::action('SeccionController@mostrarIndex')->with("mensaje", "Hemos recibido tu solicitud de contacto. Pronto nos pondremos en contacto contigo.");
                    
        }
        catch (\Exception $ex) {
            return $this->retornarError($ex);
        }
    }
    
    public function recibirFormulario(){
        /*$resCap = Input::get("g-recaptcha-response");
        if (empty($resCap)){
            throw new \Exception('Debe resolver el captcha para poder continuar');
        }

        $remote = Request::ip();

        $validaCap = \App\Helpers\Helper::validarCaptcha($resCap, $remote);
        if ($validaCap != true){
            throw new \Exception('El captcha no es válido');
        }*/
        
        $idForm = Input::get("id_form");
        $form = \App\Formulario::where("id", $idForm)->with("campos")->first();
        $respuesta = new \App\RespuestaForm();
        $respuesta->fill(Input::all());
        $respuesta->fecha = date('Y-m-d H:i:s');
        $respuesta->save();
        
        foreach ($form->campos as $campo){
            if (!in_array($campo->tipo, ['mapa', 'imagen', 'texto-p'])){
                $llave = $campo->obtenerLlave();
                $valorCampo = Input::get($llave);
                $valorFinal = '';

                if (is_array($valorCampo)){
                    $valorFinal = implode(", ", $valorCampo);
                }
                else{
                    $valorFinal = $valorCampo;
                }

                $valor = new \App\ValorForm();
                $valor->id_respuesta = $respuesta->id;
                $valor->id_form = $idForm;
                $valor->id_campo = $campo->id;
                $valor->valor = $valorFinal;
                $valor->save();
            }
        }
        
        $form = \App\Formulario::where("id", $idForm)->with(["respuestas" => function($q) use ($respuesta) {
            $q->where("id", $respuesta->id);
        }, "respuestas.valores.campo"])->first();
        
        $datosEmail = [
            'form' => $form,
        ];
        
        $destinatarios = [];
        $destTmp = explode(",",  $form->destinatarios);
        foreach ($destTmp as $d){
            $d = trim($d);
            if (filter_var($d, FILTER_VALIDATE_EMAIL)) {
                $destinatarios[] = $d;
            }
        }
        
        Mail::send('emails.contacto.sistema_form_admin', $datosEmail, function($message) use ($form, $destinatarios) {
            $message->from('info@encarguelo.com', 'Encarguelo.com');
            $message->subject('Envío de información - '.$form->titulo);
            foreach ($destinatarios as $d){
                $message->to($d, 'Encarguelo.com');
            }
            
            $message->bcc('desarrollo.myc@gmail.com');
        });
        
        Mail::send('emails.contacto.sistema_form_cliente', $datosEmail, function($message) use ($form, $respuesta) {
            $message->from('info@encarguelo.com', 'Encarguelo.com');
            $message->subject('Envío de información - '.$form->titulo);
            $message->to($respuesta->correo_cliente, $respuesta->nombre_cliente);
            $message->bcc('desarrollo.myc@gmail.com');
        });
        
        return \View::make("contenido.gracias_forms");
    }
	
    function registrarEnevento(){
        $find = \App\EventoRegistro::where("id_evento", Input::get("id") )->where("email", Input::get("email") )->first();
        if(!sizeof($find)){
            $registro = new \App\EventoRegistro();
            $registro->id_evento= Input::get("id");
            $registro->nombre 	= Input::get("nombre");
            $registro->email 	= Input::get("email");
            $registro->telefono = Input::get("telefono");
            $registro->save();

            $evento = \App\Evento::find( Input::get("id") );

            $vars = [
                'nombre_evento' 	=> $evento->evento,
                'descripcion_evento'=> $evento->descripcion,
                'fecha_evento' 		=> $evento->fecha,
                'lugar_evento' 		=> $evento->lugar,
            ];

            $this->enviarMensaje($vars, 'registro-en-evento-cowo', Input::get("email"), Input::get("nombre") );

            $vars = [
                'nombre_cliente' 	=> Input::get("nombre"),
                'correo_cliente' 	=> Input::get("email"),
                'telefono_cliente' 	=> Input::get("telefono"),

                'nombre_evento' 	=> $evento->evento,
                'descripcion_evento'=> $evento->descripcion,
                'fecha_evento' 		=> $evento->fecha,
                'lugar_evento' 		=> $evento->lugar,
            ];

            $this->enviarMensaje($vars, 'registro-en-evento', $this->destino1, 'Cowo.com.co');

            return \Redirect::to('calendario-de-eventos')->with("mensaje", "Tu registro ha sido realizado exitosamente. Pronto nos pondremos en contacto contigo.");
        }
        else{
            return \Redirect::to('calendario-de-eventos')->with("mensajeError", "Ya te has registrado previamente a este evento. Pronto nos pondremos en contacto contigo.");
        }
    }
    
    public function recibirFormularioContactoZoho(\Illuminate\Http\Request $request){
        $ret = [];
        
        try{
            $resCap = Input::get("g-recaptcha-response");
            if (empty($resCap)){
                throw new \Exception('Debe resolver el captcha para poder continuar');
            }

            $remote = Request::ip();

            $validaCap = \App\Helpers\Helper::validarCaptcha($resCap, $remote);
            if ($validaCap != true){
                throw new \Exception('El captcha no es válido');
            }

            //$url = 'https://cowo.com.co/dev/public_html/changos';
            $url = 'https://crm.zoho.com/crm/WebToLeadForm';
            $header = [
                'cache-control: no-cache'
            ];

            $opciones = [
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_ENCODING => '',
                CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1_2,
                CURLOPT_SSL_VERIFYPEER => TRUE,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => $request->all(),
                CURLOPT_HTTPHEADER => $header,
            ];

            $curl = curl_init();
            curl_setopt_array($curl, $opciones);
            $response = curl_exec($curl);
            $err = curl_error($curl);
            $cod = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            curl_close($curl);
            
            $ret = [
                "ok" => 1,
                "response" => $response,
                "error" => $err,
                "cod" => $cod
            ];
        } 
        catch (\Exception $ex) {
            $ret = [
                "ok" => 0,
                "response" => $ex->getMessage()
            ];
        }
        
        return $ret;
    }
}