<?php

namespace App\Http\Controllers;

use \Illuminate\Support\Facades\Input;

class CotizacionController extends Controller {
    
    function ver($id){
        $cotizacion = \App\Cotizacion::with("descuentos")->find($id);
        return view("cotizacion/ver")->with("cotizacion", $cotizacion);
    }
}
