<?php

namespace App\Http\Controllers;

use \Illuminate\Support\Facades\Input;

class CompraController extends Controller {
    use \App\MailTrait;
    
    function obtenerIdSesion(){
        $idSesion = \Session::get("id_sesion");
               
        if (empty($idSesion)){
            $idSesion = uniqid();
            \Session::put("id_sesion", $idSesion);            
        }
        return $idSesion;
    }

    function guardarIdSesion($id){
        \Session::put("id_sesion", $id);
    }
    
    function borrarSesion(){
        \Session::forget('id_sesion');
    }
    
    public function mostrarRespuesta(){
        $ref = Input::get('ref_payco');                
        
        //$ch = curl_init('https://api.secure.payco.co/validation/v1/reference/'.$ref);
        $ch = curl_init('https://secure.epayco.co/validation/v1/reference/'.$ref);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3'); 
        //curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        $res = json_decode(curl_exec($ch));
        
        curl_close($ch);
        
        list($idToken, $idUsuario) = explode('::', $res->data->x_extra1);

        $token = \App\TokenCompra::with([
            'compra.paquete', 
            'compra.cliente', 
            'intentoPago' => function($q) { 
                $q->where('estado', 'A')->take(1);
            }
        ])->where('token', $idToken)->first();
        
        if (!sizeof($token)) {
            $mensaje = empty($ref) ? 'Token no válido' : 'Estamos procesando tu compra.  Una vez se complete el proceso, podrás acceder a todos los servicios que COWO tiene para ti';
            $clase = empty($ref) ? 'mensajeError' : 'mensaje';
            return \Redirect::action('SeccionController@mostrarIndex')->with($clase, $mensaje);
        }        
        
        $p_cust_id_cliente='11093';
        $p_key='b58b8adb82254ce112979b14cba8229e77146461';
        $intento = $token->intentoPago[0];

        $medioPago = $res->data->x_franchise;
        $franquicia = "";
        $respuesta = "";

        switch ($medioPago) {
            case 'VS':
                $franquicia = "Visa";
                break;
            case 'MC':
                $franquicia = "MasterCard";
                break;
            case 'AM':
                $franquicia = "American Express";
                break;
            case 'DN':
                $franquicia = "Diners";
                break;
            case 'CR':
                $franquicia = "Credencial";
                break;
            case 'PSE':
                $franquicia = "PSE (Proveedor de Servicios Electr&oacute;nicos)";
                break;
            case 'DV':
                $franquicia = "Debito Visa";
                break;
            case 'DM':
                $franquicia = "Debito MasterCard";
                break;
            default:
                $franquicia = $medioPago;
        }

        switch ($intento->respuesta) {
            case 1: //Aprobada
                $clase = "texto verde";
                $respuesta = "Aprobada";
                break;
            case 2: //'Rechazada'
                $clase = "texto rojo";
                $respuesta = "Rechazada";
                break;
            case 3: //pendiente
                $clase = "texto amarillo";
                $respuesta = "Pendiente";
                break;
        }

        $imagen = public_path('imagenes/franquicias/' . strtolower($medioPago) . '.png');
        $imagenFranq = is_file($imagen) ? strtolower($medioPago) : "default";

        return \View::make('compra.confirmacion', ["intento" => $intento, "compra" => $token->compra, "franquicia" => $imagenFranq, "claseRespuesta" => $clase, "respuesta" => $respuesta]);
    }
    
    public function procesarCompra() {
            
        try {
            date_default_timezone_set('America/Bogota');
            \Illuminate\Support\Facades\DB::beginTransaction();
            
            $compra = new \App\Compra();
            
            list($idToken, $idUsuario, $horas, $cantidad, $duracion, $idDescuento) = explode('::', Input::get("x_extra1"));
    
            $token = \App\TokenCompra::where('token', $idToken)->first();
            if (sizeof($token)) {
                $compra = $token->compra();
                
                if (sizeof($compra) && $compra->estado == 'P'){
                    throw new \Exception('Token ya usado con anterioridad');
                }
            }
            else{
                $token = new \App\TokenCompra();
                $token->token = $idToken;
                if (!$token->save()) {
                    throw new \Exception('No se pudo registrar el token de compra');
                }
            }

            $p_cust_id_cliente='11093';
            $p_key='b58b8adb82254ce112979b14cba8229e77146461';
            
            $firma = Input::get('x_signature');            
            $medioPago = Input::get('x_franchise');
            $franquicia = "";
            $respuesta = "";

            switch ($medioPago) {
                case 'VS':
                    $franquicia = "Visa";
                    break;
                case 'MC':
                    $franquicia = "MasterCard";
                    break;
                case 'AM':
                    $franquicia = "American Express";
                    break;
                case 'DN':
                    $franquicia = "Diners";
                    break;
                case 'CR':
                    $franquicia = "Credencial";
                    break;
                case 'PSE':
                    $franquicia = "PSE (Proveedor de Servicios Electr&oacute;nicos)";
                    break;
                case 'DV':
                    $franquicia = "Debito Visa";
                    break;
                case 'DM':
                    $franquicia = "Debito MasterCard";
                    break;
                default:
                    $franquicia = $medioPago;
            }

            $intento = new \App\IntentoPago();
            $intento->fecha_transaccion = Input::get('x_fecha_transaccion');
            $intento->respuesta = Input::get('x_cod_response');
            $intento->franquicia = $franquicia;
            $intento->num_transaccion = Input::get('x_transaction_id');
            $intento->cod_aprobacion = Input::get('x_approval_code');
            $intento->ref_payco = Input::get('x_ref_payco');
            $intento->mensaje = Input::get('x_response_reason_text');
            $intento->monto = Input::get('x_amount');
            $intento->moneda = Input::get('x_currency_code');
            $intento->id_token = $token->id;
            
            $firmaGen = hash('sha256',
                $p_cust_id_cliente.'^'
               .$p_key.'^'
               .$intento->ref_payco.'^'
               .$intento->num_transaccion.'^'
               .$intento->monto.'^'
               .$intento->moneda
            );
            
            if ($firma != $firmaGen) {
                $intento->estado = 'R';
                $intento->mensaje .= '. La firma no es válida';
                $intento->save();
                throw new \Exception('La firma no es válida');
            }

            $clase = "";
            $membresia = new \App\Membresia();
            
            if ($intento->respuesta == 1 || $intento->respuesta == 3){
                $cliente = \App\Cliente::where("id_usuario", $idUsuario)->first();
                $compra->id_cliente = $cliente->id;
                $compra->valor = $intento->monto;
                $compra->fecha_pago = date('Y-m-d H:i:s');
            }

            switch ($intento->respuesta) {
                case 1: //Aprobada
                    $intento->estado = 'A';
                    if (!$intento->save()) {
                        throw new \Exception('No se pudo registrar los datos de pago');
                    }
                    
                    $idPaquete = (int)Input::get("x_extra2");
                    $creditos = (int)Input::get("x_extra3");
                    $plan = new \App\Paquete();

                    if ($idPaquete > 0){
                        $compra->id_paquete = $idPaquete;
                        $plan = \App\Paquete::with([
                            'descuentos' =>  function($query) use($idDescuento){
                                $query->where("id", $idDescuento);
                            },
                            'descuentos.cotizacion' => function($query) use($cliente){
                                $query->where("correo", $cliente->email);
                            }
                        ])->find($idPaquete);

                        if (!sizeof($plan)){
                            throw new \Exception('Plan no válido');
                        }
                        
                        if($cantidad <= 1){
                            $periodicidad = sizeof($plan->descuentos) ? $plan->descuentos[0]->meses : 1;
                            $membresia = $cliente->adquirirMembresia($plan, $periodicidad);
                            $servicio = $plan->servicio()->first();
                            if (sizeof($servicio)){
                                $cliente->contratarServicio($servicio);
                            }
                            
                            $cliente->creditos += $plan->creditos;
                            if (!$cliente->save()) {
                                throw new \Exception('No se pudo abonar los créditos a su cuenta');
                            }
                        }
                        else{
                            $cliente->guardarPaquetesPendientes($idPaquete, $cantidad);
                        }
                        
                        if (sizeof($plan->descuentos)){
                            $descuento = $plan->descuentos[0];
                            $descuento->estado = 'N';

                            if (!$descuento->save()) {
                                throw new \Exception('No se pudo desactivar el descuento/precio especial');
                            }
                        }
                    }
                    elseif($creditos > 0){
                        $compra->id_paquete = null;
                        $compra->creditos = $creditos;
                        $cliente->creditos += $creditos;
                        if (!$cliente->save()) {
                            throw new \Exception('No se pudo abonar los créditos a su cuenta');
                        }
                    }
                    else{
                        throw new \Exception('La compra no contiene un plan ni créditos');
                    }
                    
                    $compra->estado = 'P';
                    if (!$compra->save()) {
                        throw new \Exception('No se pudo registrar los datos de la compra');
                    }
                    
                    $token->id_compra = $compra->id;
                    if (!$token->save()) {
                        throw new \Exception('No se pudo registrar los datos de seguridad de la compra');
                    }
                    
                    if ($idPaquete > 0){
                        if ($cantidad > 1){
                            $vars = [
                                "nombre_cliente" => $cliente->nombre,
                                "direccion_cliente" => $cliente->direccion,
                                "ciudad_cliente" => $cliente->ciudad,
                                "correo_cliente" => $cliente->email,
                                "empresa_cliente" => $cliente->empresa,
                                "nombre_plan" => $plan->nombre,
                                "cantidad" => $cantidad,
                                "estado_compra" => 'Aprobada'
                            ];

                            $this->enviarMensaje($vars, 'compra_plan_pendiente', $cliente->email, $cliente->nombre);
                            $this->enviarMensaje($vars, 'compra_plan_pendiente_adm', $this->destino1, 'Cowo.com.co');
                        }
                        else{
                            $vars = [
                                "nombre_cliente" => $cliente->nombre,
                                "direccion_cliente" => $cliente->direccion,
                                "ciudad_cliente" => $cliente->ciudad,
                                "correo_cliente" => $cliente->email,
                                "empresa_cliente" => $cliente->empresa,
                                "nombre_plan" => $plan->nombre,
                                "valor_plan" => $plan->valor,
                                "vencimiento_plan" => $membresia->fecha_vence->format('Y-m-d H:i:s'),
                                "estado_compra" => 'Aprobada'
                            ];

                            $this->enviarMensaje($vars, 'compra_plan', $cliente->email, $cliente->nombre);
                            $this->enviarMensaje($vars, 'compra_plan_adm', $this->destino1, 'Cowo.com.co');                            
                        }                        
                    }
                    else{
                        $vars = [
                            "nombre_cliente" => $cliente->nombre,
                            "direccion_cliente" => $cliente->direccion,
                            "ciudad_cliente" => $cliente->ciudad,
                            "correo_cliente" => $cliente->email,
                            "empresa_cliente" => $cliente->empresa,
                            "valor_compra" => $compra->valor,
                            "creditos" => $compra->creditos,
                            "estado_compra" => 'Aprobada'
                        ];
                    
                        $this->enviarMensaje($vars, 'compra_creditos', $cliente->email, $cliente->nombre);
                    }
                    
                    $clase = "texto verde";
                    $respuesta = "Aprobada";
                    
                    break;
                case 2: //'Rechazada'
                    $intento->estado = 'R';
                    if (!$intento->save()) {
                        throw new \Exception('No se pudo registrar los datos de pago');
                    }
                    
                    $clase = "texto rojo";
                    $respuesta = "Rechazada";
                    break;
                case 3: //pendiente
                    $intento->estado = 'V';
                    if (!$intento->save()) {
                        throw new \Exception('No se pudo registrar los datos de pago');
                    }
                    
                    $compra->estado = 'E';
                    if (!$compra->save()) {
                        throw new \Exception('No se pudo registrar los datos de la compra');
                    }
                    
                    $token->id_compra = $compra->id;
                    if (!$token->save()) {
                        throw new \Exception('No se pudo registrar los datos de seguridad de la compra');
                    }
                    
                    $vars = [
                        "nombre_cliente" => $cliente->nombre,
                        "direccion_cliente" => $cliente->direccion,
                        "ciudad_cliente" => $cliente->ciudad,
                        "correo_cliente" => $cliente->email,
                        "empresa_cliente" => $cliente->empresa,
                        "valor_compra" => $compra->valor,
                        "estado_compra" => 'Pendiente'
                    ];
                    
                    $this->enviarMensaje($vars, 'compra_pendiente', $cliente->email, $cliente->nombre);
                    
                    $clase = "texto amarillo";
                    $respuesta = "Pendiente";
                    break;
            }

            $imagen = public_path('imagenes/franquicias/' . strtolower($medioPago) . '.png');
            $imagenFranq = is_file($imagen) ? strtolower($medioPago) : "default";

            \Illuminate\Support\Facades\DB::commit();

            return \View::make('compra.confirmacion', ["intento" => $intento, "compra" => $compra, "franquicia" => $imagenFranq, "claseRespuesta" => $clase, "respuesta" => $respuesta]);
        } catch (\Exception $e) {
            \Illuminate\Support\Facades\DB::rollback();
            $file = public_path('lel.txt');
            file_put_contents($file, $e->getLine().' - '.$e->getMessage());
            throw new \Exception($e->getMessage());
            die();
            return \Redirect::action('SeccionController@mostrarIndex')->with('mensajeError', $e->getMessage());
        }
    }
    
    public function mostrarBotonCompraCreditos(){
        $creditos = (int)Input::get("creditos");
        $cliente = \App\Cliente::with('membresiaActual')->where("id_usuario", \Auth::user()->id)->first();
        $valorCredito = sizeof($cliente->membresiaActual) ? 3500 : 5000;
        $valorTotal = $creditos * \App\Helpers\Helper::calcularConIva($valorCredito);
        
        return \View::make('compra.boton_creditos', ["creditos" => $creditos, "cliente" => $cliente, "valor" => $valorTotal]);
    }    
    
    public function mostrarBotonCompraPlan($id=null){
        if (empty($id)){
            $id = (int)Input::get("id_plan");
        }

        $plan = \App\Paquete::find($id);
        /*$descuento = $plan->descuentos()->with(['cotizacion' => function($q){ 
            $q->where('par_cotizacion.correo', \Auth::user()->email)->take(1); 
        }])
        ->where('id_cotizacion', $idCotizacion)
        ->orderBy('id', 'desc')
        ->first();*/
        
        $cliente = \App\Cliente::with('membresiaActual')->where("id_usuario", \Auth::user()->id)->first();

        if (!sizeof($cliente)){
            return \Redirect::action('SesionController@mostrarIndex')->with('mensajeError', 'Por favor complete primero su perfil');
        }

        if (sizeof($cliente->membresiaActual) && $cliente->membresiaActual[0]->paquete->valor > $plan->precio){
            return \Redirect::action('SeccionController@mostrarIndex')->with('mensajeError', 'No puede comprar un plan inferior al que ya posee');
        }

        return \View::make('compra.boton_plan', ["plan" => $plan, "descuento" => null, "cliente" => $cliente]);
    }
    
    public function mostrarBotonCompraCotizacion($idCotizacion, $idDescuento){
        if(\Auth::guest()){
            
            $cotizacion = \App\Cotizacion::find($idCotizacion);
            $cliente = \App\Cliente::where("email", $cotizacion->correo)->first();
            
            if(sizeof($cliente)){
                return \Redirect::action('SesionController@mostrarIndex')->with('mensajeError', 'Por favor, inicie sesi&oacute;n');
            }
            else{
                return \Redirect::action('SesionController@crearUsuario')->with('mensajeError', 'Por favor, registrese antes de continuar');
            }
        }
        else{
            
            /*$descuento = $plan->descuentos()->with(['cotizacion' => function($q){ 
                $q->where('par_cotizacion.correo', \Auth::user()->email)->take(1); 
            }])
            ->where('id_cotizacion', $idCotizacion)
            ->orderBy('id', 'desc')
            ->first();*/
            
            $descuento = \App\Descuento::where("id", $idDescuento)->where("fecha_vencimiento", "<=", date('Y-m-d'))->where("estado", "V")->with("paquete")->first();

            $cliente = \App\Cliente::with('membresiaActual')->where("id_usuario", \Auth::user()->id)->first();
            
            if (!sizeof($cliente)){
                return \Redirect::action('SesionController@mostrarIndex')->with('mensajeError', 'Por favor complete primero su perfil');
            }
            
            if ( !sizeof($descuento) ){
                return \Redirect::action('SeccionController@mostrarIndex')->with('mensajeError', 'No existen cotizaciones enviadas a su correo electr&oacute;nico');
            }
            
            if (sizeof($cliente->membresiaActual) && $cliente->membresiaActual[0]->paquete->valor > $descuento->paquete->valor){
                return \Redirect::action('SeccionController@mostrarIndex')->with('mensajeError', 'No puede comprar un plan inferior al que ya posee');
            }
            
            return \View::make('compra.boton_cotizacion', ["plan" => $descuento->paquete, "descuento" => $descuento, "cliente" => $cliente]);
        }
    }
}
