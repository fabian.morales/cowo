<?php

namespace App\Http\Controllers;

use \Illuminate\Support\Facades\Input;

class BuscadorController extends Controller {
    function realizarBusqueda(){
        $tipo = Input::get("buscador");
        $criterio = Input::get("criterio");
        $categoria = Input::get("categoria");
        $resultados = [];
        
        $valorTasa = 1;
        $tasa = \App\TasaMoneda::where("origen", "USD")->where("destino", "COP")->first();
        if (sizeof($tasa)){
            $valorTasa = $tasa->tasa;
        }
        
        try{
            if ($tipo == "A"){
               $resultados = $this->buscarConAmazon($criterio, $categoria, $valorTasa);
            }
            else{
                $resultados = $this->buscarConEbay($criterio, $categoria, $valorTasa);
            }

            return \View::make('buscador.busqueda', ["resultados" => $resultados, "sitio" => $tipo]);
        }
        catch (Exception $ex) {
            return $this->retornarError($ex);
        }
    }
    
    function realizarBusquedaAdmin(){
        $tipo = Input::get("buscador");
        $criterio = Input::get("criterio");
        $resultados = [];
        
        $valorTasa = 1;
        $tasa = \App\TasaMoneda::where("origen", "USD")->where("destino", "COP")->first();
        if (sizeof($tasa)){
            $valorTasa = $tasa->tasa;
        }
        
        try{
            if ($tipo == "A"){
               $resultados = $this->buscarConAmazon($criterio, "All", $valorTasa);
            }
            else{
                $resultados = $this->buscarConEbay($criterio, 0, $valorTasa);
            }

            return \View::make('buscador.busqueda_admin', ["resultados" => $resultados, "sitio" => $tipo]);
        }
        catch (Exception $ex) {
            return $this->retornarError($ex);
        }
    }
    
    function buscarConAmazon($criterio, $categoria, $tasa){
        $ret = [];

        try
        {
            $obj = new \App\Lib\AmazonProductAPI();
            $result = $obj->searchProducts($criterio, $categoria, "TITLE");
            
            foreach ($result->Items->Item as $i){
                $url = $i->DetailPageURL;
                $parteUrl = substr($url, -4);
                
                if ($parteUrl == "null"){
                    $url = substr($url, 0, strlen($url) - 4);
                }
                
                $precio = 0;
                if (isset($i->Offers->Offer->OfferListing->SalePrice)){
                    $precio = $i->Offers->Offer->OfferListing->SalePrice->Amount / 100;
                }
                elseif (isset($i->Offers->Offer->OfferListing->Price)){
                    $precio = $i->Offers->Offer->OfferListing->Price->Amount / 100;
                }
                elseif(isset($i->OfferSummary->LowestNewPrice)){
                    $precio = $i->OfferSummary->LowestNewPrice->Amount / 100;
                }
                elseif(isset($i->OfferSummary->LowestUsedPrice)){
                    $precio = $i->OfferSummary->LowestUsedPrice->Amount / 100;
                }
                
                $peso = 0;
                if (isset($i->ItemAttributes->ItemDimensions->Weight)){
                    $atributos = $i->ItemAttributes->ItemDimensions->Weight->attributes();
                    $factor = 1.00;
                    
                    if (isset($atributos['Units'])){                       
                        switch($atributos['Units']){
                            case 'hundredths-pounds':
                                $factor = 100.00;
                                break;
                            case 'ounces':
                                $factor = 16;
                                break;
                        }
                    }
                    
                    $peso = (float)$i->ItemAttributes->ItemDimensions->Weight / $factor;
                }
                elseif ($i->ItemAttributes->PackageDimensions->Weight){
                    $atributos = $i->ItemAttributes->PackageDimensions->Weight->attributes();
                    $factor = 1.00;
                    
                    if (isset($atributos['Units'])){                       
                        switch($atributos['Units']){
                            case 'hundredths-pounds':
                                $factor = 100.00;
                                break;
                            case 'ounces':
                                $factor = 16;
                                break;
                        }
                    }
                    
                    $peso = (float)$i->ItemAttributes->PackageDimensions->Weight / $factor;
                }
                
                $precioVenta = \App\Helpers\Helper::calcularPrecioVenta($precio, $peso);
                
                $item = [
                    "nombre" => $i->ItemAttributes->Title,
                    "categoria" => $i->ItemAttributes->ProductGroup,
                    "id_categoria" => $i->ItemAttributes->ProductGroup,
                    "codigo" => $i->ASIN,
                    "precio" => round($precioVenta * $tasa, 2),
                    "precio_sitio" => $precio,
                    "peso" => $peso,
                    "imagenes" => (object)[
                        "small" => $i->SmallImage->URL,
                        "medium" => $i->MediumImage->URL,
                        "large" => $i->LargeImage->URL
                    ],
                    "url" => $url,
                    "tipo" =>"A"
                ];
                
                $ret[] = (object)$item;
            }
        }
        catch(Exception $e)
        {
            //echo $e->getMessage();
        }
        return $ret;
    }
    
    function buscarConEbay($criterio, $categoria, $tasa){
        $ret = [];

        try
        {
            $obj = new \App\Lib\EbayProductAPI();
            /*$resul = $obj->getSingleProduct($criterio);
            print_r($resul); die();*/
            $result = $obj->searchProducts($criterio, $categoria);
            
            foreach ($result->searchResult->item as $i){
                $precio = $i->sellingStatus->currentPrice;
                $precioVenta = \App\Helpers\Helper::calcularPrecioVenta($precio, $peso);
                $imgL = $i->pictureURLLarge;
                if (empty($imgL)){
                    $imgL = isset($i->galleryInfoContainer->galleryURL[0]) ? $i->galleryInfoContainer->galleryURL[0] : $i->galleryURL;
                }
                
                $item = [
                    "nombre" => $i->title,
                    "categoria" => $i->primaryCategory->categoryName,
                    "id_categoria" => $i->primaryCategory->categoryId,
                    "codigo" => $i->itemId,
                    "precio" => round($precioVenta * $tasa, 2),
                    "precio_sitio" => $precio,
                    "peso" => 0,
                    "imagenes" => (object)[
                        "small" => isset($i->galleryInfoContainer->galleryURL[1]) ? $i->galleryInfoContainer->galleryURL[1] : $i->galleryURL,
                        "medium" => isset($i->galleryInfoContainer->galleryURL[0]) ? $i->galleryInfoContainer->galleryURL[0] : $i->galleryURL,
                        "large" => $imgL
                    ],
                    "url" => $i->viewItemURL,
                    "tipo" => "E"
                ];
                
                $ret[] = (object)$item;
            }
        }
        catch(Exception $e)
        {
            //echo $e->getMessage();
        }
        return $ret;
    }
    
    function obtenerTasa(){
        $tasa = \App\TasaMoneda::where("origen", "USD")->where("destino", "COP")->first();
        return json_encode($tasa);
    }
}
