<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, \App\SeccionTrait;
    
    public function __construct() {
        \App\Helpers\Helper::cargueInicial("sitio");
        
        $popup = $this->obtenerPopup();
        \View::share('popup', $popup);
    }
    
    public function retornarError($e){
        return \Response::json( [
            'error' => [
                'exception' => class_basename( $e ) . ' in ' . basename( $e->getFile() ) . ' line ' . $e->getLine() . ': ' . $e->getMessage(),
                'message' => $e->getMessage()
            ]
        ], 500 );
    }
}
