<?php

namespace App;

class CampoForm extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'frm_campo';
    protected $fillable = [
        'id_form', 'nombre', 'tipo', 'columnas', 'obligatorio', 'orden'
    ];
    
    public function obtenerParametros(){
        return json_decode($this->parametros);
    }
    
    public function obtenerLlave(){
        return "campo-".$this->id."-".$this->llave;
    }
    
    public function formulario(){
        return $this->belongsTo('\App\Formulario', 'id_form', 'id');
    }
    
    public function valores(){
        return $this->hasMany('\App\ValorForm', 'id_campo');
    }
}
