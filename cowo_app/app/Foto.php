<?php

namespace App;

class Foto extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'con_foto';
    protected $fillable = [
        'id_galeria', 'titulo', 'descripcion', 'meta'
    ];
    
    public function galeria(){
        return $this->belongsTo('\App\GaleriaFoto', 'id_galeria', 'id');
    }
}
