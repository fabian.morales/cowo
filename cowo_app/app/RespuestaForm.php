<?php

namespace App;

class RespuestaForm extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'frm_respuesta';
    protected $fillable = ['id_form', 'nombre_cliente', 'correo_cliente'];
    
    public function formulario(){
        return $this->belongsTo('\App\Formulario', 'id_form', 'id');
    }
    
    public function valores(){
        return $this->hasMany('\App\ValorForm', 'id_respuesta');
    }
}
