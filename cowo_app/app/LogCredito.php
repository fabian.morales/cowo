<?php

namespace App;

class LogCredito extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'log_credito';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_creador', 'id_cliente', 'motivo', 'cantidad'
    ];
    
    public function cliente(){
        return $this->hasOne("\App\Cliente", "id_cliente");
    }
	
	public function usuario(){
        return $this->hasOne("\App\User", "id_creador");
    }
}
