<?php

namespace App;

class Compra extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'ped_compra';
    
    public function cliente(){
        return $this->hasOne('\App\Cliente', 'id', 'id_cliente');
    }
    
    public function paquete(){
        return $this->hasOne('\App\Paquete', 'id', 'id_paquete');
    }
	
    public function descuento(){
        return $this->belongsTo("\App\Descuento", "id_descuento");
    }
    
    public function tokens(){
        return $this->hasMany('\App\TokenPedido', 'id_compra');
    }
    
    public function intentosPago(){
        return $this->hasMany('\App\IntentoPago', 'id_compra');
    }
        
    public static function estados(){
        return [
            "N" => "Nuevo",
            "C" => "Pendiente",
            "P" => "Pagado"
        ];
    }
}
