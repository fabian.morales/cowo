<?php

namespace App;

class GaleriaFoto extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'con_galeria';
    protected $fillable = [
        'titulo', 'llave'
    ];
    
    public function fotos(){
        return $this->hasMany('\App\Foto', 'id_galeria');
    }
}
