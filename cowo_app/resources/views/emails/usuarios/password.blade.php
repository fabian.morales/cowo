@extends('emails')

@section('content')
    <img src="{{ asset('imagenes/logo.png') }}" />
    <h2>Recuperaci&oacute;n de clave</h2>
    <p>       
        Apreciado cliente, <br />

        Para restablecer su clave, por favor complete el formulario que se encuentra en el siguiente v&iacute;nculo: 
        <a href="{{ URL::to('/cliente/clave/reset', array($token)) }}">{{ URL::to('/cliente/clave/reset', array($token)) }}</a><br/>
        Este expirar&aacute; en {{ Config::get('auth.reminder.expire', 60) }} minutos.
    </div>
    </p>
@stop