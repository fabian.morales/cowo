@extends('emails')

@section('content')
    <img src="{{ asset('imagenes/logo.png') }}" />
    <h1>{{ $form->titulo }}</h1>
    <p>{{ $form->descripcion }}</p>
    <p>Se ha recibido una solicitud de contacto con los siguientes datos:</p>
    {{--*/ $respuesta = $form->respuestas[0] /*--}}
    <ul>
        <li>Nombre: {{ $form->nombre_cliente }}</li>
        <li>Correo: {{ $form->correo_cliente }}</li>
        @foreach($respuesta->valores as $valor)
        <li><strong>{{ $valor->campo->nombre }}: </strong>{{ $valor->valor }}</li>
        @endforeach
    </ul>
@stop
