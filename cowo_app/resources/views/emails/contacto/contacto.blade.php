@extends('emails')

@section('content')
    <img src="{{ asset('imagenes/logo.png') }}" />
    <h1>Solicitud de contacto</h1>
    <p>Se ha recibido una solicitud de contacto con los siguientes datos:</p>
    <ul>
        <li><strong>Nombre:</strong> {{ $nombre }}</li>
        <li><strong>Correo:</strong> {{ $email }}</li>
        <li><strong>Mensaje:</strong> {{ $mensaje }}</li>
    </ul>
@stop
