@extends('emails')

@section('content')
    <img src="{{ asset('imagenes/logo.png') }}" />
    <h1>Solicitud de contacto</h1>
    <p>Hemos recibido su solicitud. Le estaremos enviando una respuesta lo mas pronto posible.</p>
@stop
