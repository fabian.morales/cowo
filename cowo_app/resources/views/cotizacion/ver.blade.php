@extends('master')

@section('content')
<div class="container">
    <div class="row">
        <fieldset>
            <legend><h2 class="amarillo">Servicios cotizados</h2></legend>
            
            <div class="row item">
                <div class="col-md-1 small-4 columns"><strong>Plan</strong></div>
                <div class="col-md-1 small-3 columns"><strong>Valor</strong></div>
                <div class="col-md-1 small-3 columns"><strong>Descuento</strong></div>
                <div class="col-md-1 small-3 columns"><strong>Horas</strong></div>
                <div class="col-md-1 small-3 columns"><strong>Cantidad</strong></div>
                <div class="col-md-1 small-3 columns"><strong>Meses</strong></div>
                <div class="col-md-1 small-3 columns"><strong>Subtotal</strong></div>              
                <div class="col-md-1 small-3 columns"><strong>IVA</strong></div>              
                <div class="col-md-1 small-3 columns"><strong>Total a Pagar</strong></div>              
                <div class="col-md-2 small-3 columns"><strong>Vencimiento</strong></div>              
                <div class="col-md-1 small-3 columns"><strong>Comprar</strong></div>              
            </div>
            
            @foreach($cotizacion->descuentos as $descuento)
                <div class="row item">
                    <div class="col-md-1 small-4 columns">{{ $descuento->paquete->nombre }}</div>
                    <div class="col-md-1 small-3 columns">{{ Helper::number_format($descuento->precio) }}</div>
                    <div class="col-md-1 small-3 columns">{{ Helper::number_format($descuento->valor) }}%</div>
                    <div class="col-md-1 small-3 columns">{{ $descuento->horas }}</div>
                    <div class="col-md-1 small-3 columns">{{ $descuento->cantidad }}</div>
                    <div class="col-md-1 small-3 columns">{{ $descuento->meses }}</div>
                    <div class="col-md-1 small-3 columns">{{ 
                        Helper::number_format( 
                            $descuento->precio * $descuento->cantidad * $descuento->meses * ($descuento->horas != 0 ? $descuento->horas : 1), 
                                $descuento->fecha_vencimiento >= date('Y-m-d') ? $descuento->valor : 0
                        ) }}
                    </div>
                    <div class="col-md-1 small-3 columns">{{ 
                        Helper::number_format( 
                            Helper::calcularIva(
                                $descuento->precio * $descuento->cantidad * $descuento->meses * ($descuento->horas != 0 ? $descuento->horas : 1), 
                                $descuento->fecha_vencimiento >= date('Y-m-d') ? $descuento->valor : 0 ) 
                        ) }}
                    </div>
                    <div class="col-md-1 small-3 columns">{{ 
                        Helper::number_format( 
                            Helper::calcularConIva(
                                $descuento->precio * $descuento->cantidad * $descuento->meses * ($descuento->horas != 0 ? $descuento->horas : 1), 
                                $descuento->fecha_vencimiento >= date('Y-m-d') ? $descuento->valor : 0 ) 
                        ) }}
                    </div>
                    <div class="col-md-2 small-3 columns">{{ $descuento->fecha_vencimiento }}</div>
                    <div class="col-md-1 small-3 columns">@if ($descuento->fecha_vencimiento > date("Y-m-d") ) <a href="{{url('compra/paquete/'.$cotizacion->id.'/'.$descuento->id_paquete)}}">Comprar</a> @else -Vencida- @endif</div>
                </div>
            @endforeach
        </fieldset>
    </div>
</div>
<br />
<br />
@stop