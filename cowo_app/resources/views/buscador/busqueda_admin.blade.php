<form id="form_importar" name="form_importar" action="{{ url('administrador/productos/importar') }}" method="post">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" name="sitio" value="{{ $sitio }}" />
    <div class="row">
        <div class="small-12 columns">
            <h3 class="titulo seccion">Resultados</h3>
        </div>
        @foreach($resultados as $r)
        <div class="small-12 columns">
            <div class="row">
                <div class="small-1 columns">
                    <input type="hidden" name="codigo[{{ $r->tipo.$r->codigo }}]" value="{{ $r->codigo }}" />
                    <input type="hidden" name="url[{{ $r->tipo.$r->codigo }}]" value="{{ $r->url }}" />
                    <input type="hidden" name="id_categoria[{{ $r->tipo.$r->codigo }}]" value="{{ $r->id_categoria }}" />
                    <input type="hidden" name="categoria[{{ $r->tipo.$r->codigo }}]" value="{{ $r->categoria }}" />
                    <input type="checkbox" name="importar[{{ $r->tipo.$r->codigo }}]" />
                </div>
                <div class="small-2 columns">
                    <input type="hidden" id="imagen_p_{{ $r->codigo }}" name="imagen_p[{{ $r->tipo.$r->codigo }}]" value="{{ $r->imagenes->small }}" />
                    <input type="hidden" id="imagen_m_{{ $r->codigo }}" name="imagen_m[{{ $r->tipo.$r->codigo }}]" value="{{ $r->imagenes->medium }}" />
                    <input type="hidden" id="imagen_g_{{ $r->codigo }}" name="imagen_g[{{ $r->tipo.$r->codigo }}]" value="{{ $r->imagenes->large }}" />
                    <div class="imagen"><div><img src="{{ $r->imagenes->medium }}" /></div></div>
                </div>
                <div class="small-3 columns">
                    <input type="text" name="nombre[{{ $r->tipo.$r->codigo }}]" value="{{ $r->nombre }}" />
                </div>
                <div class="small-2 columns">
                    <input type="hidden" name="precio_sitio[{{ $r->tipo.$r->codigo }}]" value="{{ round($r->precio_sitio, 2) }}" />
                    <input type="text" name="precio_venta[{{ $r->tipo.$r->codigo }}]" value="{{ round($r->precio, 2) }}" />
                </div>
                <div class="small-2 columns">
                    <input type="text" name="peso[{{ $r->tipo.$r->codigo }}]" value="{{ $r->peso }}" />
                </div>
                <div class="small-2 columns">
                    <label for="destacar_{{ $r->tipo.$r->codigo }}">
                        <input type="checkbox" id="destacar_{{ $r->tipo.$r->codigo }}" name="destacar[{{ $r->tipo.$r->codigo }}]" value="S" />
                        Destacar
                    </label>
                    
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="row">
        <div class="small-12 columns">
            <input type="submit" class="button default" value="Importar" />
        </div>
    </row>    
</form>

