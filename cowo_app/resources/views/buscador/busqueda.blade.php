<div class="row row10 collapse lista-productos">
    <div class="small-12 columns">
        <h3 class="titulo seccion">Resultados</h3>
    </div>
    @if($sitio == 'E')
    <div class="small-12 columns">
        <p class='text-center'><strong>El precio de Ebay es &uacute;nicamente el costo del producto y no incluye los costos de env&iacute;o.
                Agr&eacute;galo al carrito y dale clic a cotizar para obtener el precio total.</strong>
        </p>
    </div>
    @endif
    @foreach($resultados as $r)
    <div class="small-6 medium-4 large-3 columns end item" data-item="{{ $r->tipo.$r->codigo }}">
        <input type="hidden" id="sitio_{{ $r->tipo.$r->codigo }}" name="sitio[{{ $r->tipo.$r->codigo }}]" value="{{ $sitio }}" />
        <input type="hidden" id="codigo_{{ $r->tipo.$r->codigo }}" name="sitio[{{ $r->tipo.$r->codigo }}]" value="{{ $r->codigo }}" />
        <input type="hidden" id="url_{{ $r->tipo.$r->codigo }}" name="url[{{ $r->tipo.$r->codigo }}]" value="{{ $r->url }}" />
        <input type="hidden" id="imagen_p_{{ $r->tipo.$r->codigo }}" name="imagen_p[{{ $r->tipo.$r->codigo }}]" value="{{ $r->imagenes->small }}" />
        <input type="hidden" id="imagen_m_{{ $r->tipo.$r->codigo }}" name="imagen_m[{{ $r->tipo.$r->codigo }}]" value="{{ $r->imagenes->medium }}" />
        <input type="hidden" id="imagen_g_{{ $r->tipo.$r->codigo }}" name="imagen_g[{{ $r->tipo.$r->codigo }}]" value="{{ $r->imagenes->large }}" />
        <input type="hidden" id="nombre_{{ $r->tipo.$r->codigo }}" name="nombre[{{ $r->tipo.$r->codigo }}]" value="{{ $r->nombre }}" />
        <input type="hidden" id="precio_sitio_{{ $r->tipo.$r->codigo }}" name="precio_sitio[{{ $r->tipo.$r->codigo }}]" value="{{ round($r->precio_sitio, 2) }}" />
        <input type="hidden" id="precio_venta_{{ $r->tipo.$r->codigo }}" name="precio_venta[{{ $r->tipo.$r->codigo }}]" value="{{ round($r->precio, 2) }}" />
        <input type="hidden" id="peso_{{ $r->tipo.$r->codigo }}" name="peso[{{ $r->tipo.$r->codigo }}]" value="{{ $r->peso }}" />
        <input type="hidden" id="categoria_{{ $r->tipo.$r->codigo }}" name="categoria[{{ $r->tipo.$r->codigo }}]" value="{{ $r->categoria }}" />
        <input type="hidden" id="id_categoria_{{ $r->tipo.$r->codigo }}" name="id_categoria[{{ $r->tipo.$r->codigo }}]" value="{{ $r->id_categoria }}" />
                    
        <div class="imagen">
            <div>
                <a href="" rel="mas-item" data-item="{{ $r->tipo.$r->codigo }}">
                    <img src="{{ $r->imagenes->medium }}" />
                </a>
            </div>
        </div>
        <p class="precio">$ {{ Helper::number_format($r->precio) }}</p>
        <div class="nombre"><p>{{ strtok(wordwrap($r->nombre, 60, "\n"), "\n") }}</p></div>
        <div class="row collapse">
            <div class="small-6 medium-9 columns">
                <a href="" class="button agregar" rel="agregar-item" data-item="{{ $r->tipo.$r->codigo }}"><img src="{{ asset('/imagenes/template/icono-carrito-blanco.png') }}" /> Agregar al carrito</a>
            </div>
            <div class="small-6 medium-3 columns">
                <a href="" class="boton mas" rel="mas-item" data-item="{{ $r->tipo.$r->codigo }}">M&aacute;s</a>
            </div>
        </div>
    </div>
    @endforeach
</div>
