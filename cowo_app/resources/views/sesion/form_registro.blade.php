@extends('master')

@section('js_header')
<script>
(function (window, $) {
    
    var $buscarCliente = false;
    $(document).ready(function(){        
        
        $("#form_registro #email").change(function(e) {
            $buscarCliente = true;
        });
        
        $("#form_registro #email").focusout(function(e) {
            $("#login").val($("#email").val());
            $("#loader").addClass("loading");
            
            if ($buscarCliente === true){
                $.ajax({
                    url: '{{ url('/cliente/obtener') }}/' + $("#email").val(),
                    method: 'post',
                    dataType: 'json',
                    data: { '_token' : '{!! csrf_token() !!}' }
                })
                .done(function(json) {
                    $("#nombre").val(json.nombre);
                    $("#apellido").val(json.apellido);
                    $("#telefono").val(json.telefono);
                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    alert(jqXHR.responseJSON.error.message);
                }).
                always(function() {
                    $("#loader").removeClass("loading");
                });
            }
        });
        
        $("#form_registro").submit(function(e) {
                        
            if ($("#nombre").val() === ''){
                alert('Debes ingresar tu nombre');
                $("#nombre").focus();
                e.preventDefault();
                return;
            }
            
            if ($("#apellido").val() === ''){
                alert('Debes ingresar tu apellido');
                $("#apellido").focus();
                e.preventDefault();
                return;
            }
            
            if ($("#email").val() === ''){
                alert('Debes ingresar tu dirección de correo');
                $("#email").focus();
                e.preventDefault();
                return;
            }
            
            if (!cotizador.isEmail($("#email").val())){
                alert('La dirección de correo no es válida');
                $("#email").focus();
                e.preventDefault();
                return;
            }
        });
    });
})(window, jQuery);
</script>
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h2 class="amarillo text-center">Registro de usuario</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form id="form_registro" name="form_registro" action="{{ url('cliente/registro') }}" method="post" class="form" enctype="multipart/form-data">
                <input type="hidden" id="id" name="id" value="{{ $usuario->id }}" />
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <input type="hidden" name="login" id="login" value="{{ $usuario->login }}" />
                <div class="row">
                    <div class="col-md-6 columns">
                        <input type="text" name="nombre" id="nombre" value="{{ $usuario->nombre }}" required placeholder="Nombre" />
                    </div>
                    <div class="col-md-6 columns">                    
                        <input type="text" id="apellido" name="apellido" value="@if(sizeof($usuario->cliente)){{ $usuario->cliente->apellido }}@endif" required placeholder="Apellido" />
                    </div> 
                </div>

                <div class="row">
                    <div class="col-md-6 columns">
                        <input type="text" name="email" id="email" value="{{ $usuario->email }}" required placeholder="Correo" />
                    </div>
                    <div class="col-md-6 columns">
                        <input type="text" name="empresa" id="empresa" value="@if(sizeof($usuario->cliente)){{ $usuario->cliente->empresa }}@endif" required placeholder="Empresa" />
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 columns">
                        <select name="tipo_documento" id="tipo_documento" required>
                        @foreach($tipos as $t)
                        <option value="{{ $t }}" @if(sizeof($usuario->cliente) && $usuario->cliente->tipo_documento == $t) selected @endif>{{ $t }}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="col-md-6 columns">
                        <input type="text" id="documento_identidad" name="documento_identidad" value="@if(sizeof($usuario->cliente)){{ $usuario->cliente->documento_identidad }}@endif" required placeholder="Documento identidad" />
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 columns">
                        <input type="text" id="telefono" name="telefono" value="@if(sizeof($usuario->cliente)){{ $usuario->cliente->telefono }}@endif" placeholder="Teléfono" />
                    </div>            
                    <div class="col-md-6 columns">
                        <input type="text" id="direccion" name="direccion" value="@if(sizeof($usuario->cliente)){{ $usuario->cliente->direccion }}@endif" placeholder="Dirección" />
                    </div> 
                </div>

                <div class="row">
                    <div class="col-md-6 columns">
                        <input type="file" name="imagen" id="imagen" placeholder="Imagen" />
                        <small>Tama&ntilde;o m&aacute;ximo: 1MB</small>
                    </div>

                    <div class="col-md-6 columns">
                        <input type="password" name="password" id="password" value="" required placeholder="Clave" />
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 columns">
                        <a class="boton gris" href="{{ url('/') }}" />Cancelar</a>
                        <input type="submit" value="Registrarse" class="boton crema" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop