<div class='row comunidad'>
    <div class='col-sm-12'>
        <div class='item'>
            <img src='{{ url($cliente->usuario->obtenerImagen()) }}?{{ rand() }}' class="img-responsive" />
        </div>
        <br />
        <strong>Nombre:</strong> {{ $cliente->nombre }} {{ $cliente->apellido }}<br />
        <strong>Correo:</strong> {{ $cliente->usuario->email }}<br />
        <strong>Empresa:</strong> {{ $cliente->empresa }}<br />
        <strong>Ciudad:</strong> {{ $cliente->ciudad }}<br />
    </div>
</div>