@extends('master')

@section('js_body')
<script>
(function (window, $) {
    
    var $buscarCliente = false;
    $(document).ready(function(){
        
        /*var $dtOpc = {
            minDate: 0,
            dateFormat: 'yy-mm-dd',
            timeFormat: 'HH:mm:ss',
            dayNames: [ "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado" ],
            dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
            dayNamesShort: [ "Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb" ],
            monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
            monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "Mau", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ],
            currentText: "Hoy",
            closeText: "Aceptar",
            showButtonPanel: true,
            pickerPosition: "bottom-left"
        };
        
        $("a[rel='reserva']").click(function(e) {
            e.preventDefault();
            
            var $href = $(this).attr("href");
            
            $.featherlight($href, {
                afterOpen: function(){

                    var $target = $href + " form";
                    var $targetFecha = $href + " input[rel='fecha']";
                    var $targetFechaHora = $href + " input[rel='fecha_hora']";

                    console.log($targetFecha);

                    $($targetFecha).datepicker("destroy");
                    $($targetFecha).datepicker($dtOpc);

                    $($targetFechaHora).datetimepicker("destroy");
                    $($targetFechaHora).datetimepicker($dtOpc);

                    $($target).submit(function(e) {
                        e.preventDefault();

                        $.ajax({
                            url: $(this).attr("action"),
                            method: 'post',
                            data: $(this).serialize(),
                            dataType: 'json'
                        }).done(function(res) {
                            if (res.ok === 1){
                                alert('Reserva realizada exitosamente');
                                window.location.reload();
                            }
                            else{
                                alert(res.msg);
                            }
                        })
                        .fail(function(jqXHR, textStatus, errorThrown) {
                            alert('Ocurrió un problema al realizar la reserva');
                        });
                    });
                }
            });
        });*/
        
        $("#form_perfil").submit(function(e) {
            console.log($(this).find("#nombre").val());
            if ($(this).find("#nombre").val() === ''){
                alert('Debes ingresar tu nombre');
                $(this).find("#nombre").focus();
                e.preventDefault();
                return;
            }
            
            if ($(this).find("#apellido").val() === ''){
                alert('Debes ingresar tu apellido');
                $(this).find("#apellido").focus();
                e.preventDefault();
                return;
            }
            
            if ($(this).find("#email").val() === ''){
                alert('Debe singresar tu dirección de correo');
                $(this).find("#email").focus();
                e.preventDefault();
                return;
            }
            
            if (!cotizador.isEmail($(this).find("#email").val())){
                alert('La dirección de correo no es válida');
                $(this).find("#email").focus();
                e.preventDefault();
                return;
            }
        });
        
        $("#lnkBuscarComunidad").click(function(e) {
            e.preventDefault();
            $("#div_comunidad").addClass('loading');
            
            $.ajax({
                url: $(this).attr('href'),
                method: 'post',
                data: {
                    criterio: $('#buscador_comunidad').val()
                }
            })
            .done(function(data){
                $("#div_comunidad").html(data);
                $("#div_comunidad").removeClass('loading');
            });
        });
        
        /*$("#lnkEditar").featherlight({
            beforeOpen: function(){
                //$("#div_form_perfil").show();
            }
        });*/
    });
})(window, jQuery);
</script>
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            
            <h2 class="amarillo">Mi perfil</h2>
            <div class="row">
                <div class="col-md-4">
                    <img src="{{ $imagen_perfil }}?{{ rand() }}" class="img-responsive img_fullwidth" />
                </div>
                <div class="col-md-8">
                    @if(sizeof($usuario->cliente))
                    
                    @if(sizeof($usuario->cliente->membresiaActual))
                    Plan: {{ $usuario->cliente->membresiaActual[0]->paquete->nombre }}<br />
                    @endif
                    
                    Nombre: {{ $usuario->cliente->nombre }} {{ $usuario->cliente->apellido }}<br />
                    Correo: {{ $usuario->email }}<br />
                    Documento: {{ $usuario->cliente->tipo_documento }} {{ $usuario->cliente->documento_identidad }}<br />
                    Usuario: {{ $usuario->login }}<br />
                    Empresa: {{ $usuario->cliente->empresa }}<br />
                    
                    @else
                    
                    <p><strong>Debe completar su perfil de usuario</strong></p>
                    
                    @endif
                    <br />
                    <a href="#div_form_perfil" data-featherlight="#div_form_perfil" id="lnkEditar" class="boton mini crema">Editar</a>
                </div>
            </div>
            <div id="div_form_perfil" class="lightbox">
                <h2 class="amarillo">Edici&oacute;n de tu perfil</h2>
                <form id="form_perfil" name="form_perfil" class="form" action="{{ url('cliente/registro') }}" method="post" enctype="multipart/form-data">
                    <input type="hidden" id="id" name="id" value="{{ $usuario->id }}" />
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <input type="hidden" name="login" id="login" value="{{ $usuario->login }}" />
                    
                    <div class="row">
                        <div class="col-md-6 columns">
                            <input type="text" name="nombre" id="nombre" @if(sizeof($usuario->cliente)) value="{{ $usuario->cliente->nombre }}" @endif required placeholder="Nombre" />
                        </div>
                        <div class="col-md-6 columns">
                            <input type="text" id="apellido" name="apellido" @if(sizeof($usuario->cliente)) value="{{ $usuario->cliente->apellido }}" @endif required placeholder="Apellido" />
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6 columns">
                            <select name="tipo_documento" id="tipo_documento" required>
                            @foreach($tipos as $t)
                            <option value="{{ $t }}" @if(sizeof($usuario->cliente) && $usuario->cliente->tipo_documento == $t) selected @endif>{{ $t }}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 columns">
                            <input type="text" id="documento_identidad" name="documento_identidad" @if(sizeof($usuario->cliente)) value="{{ $usuario->cliente->documento_identidad }}" @endif required placeholder="Documento identidad" />
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6 columns">
                            <input type="text" name="email" id="email" value="{{ $usuario->email }}" required placeholder="Correo" />
                        </div>
                        <div class="col-md-6 columns">
                            <input type="text" name="empresa" id="empresa" @if(sizeof($usuario->cliente)) value="{{ $usuario->cliente->empresa }}" @endif required placeholder="Empresa" />
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6 columns">
                            <input type="text" name="direccion" id="direccion" @if(sizeof($usuario->cliente)) value="{{ $usuario->cliente->direccion }}" @endif required placeholder="Dirección" />
                        </div>
                        <div class="col-md-6 columns">
                            <input type="text" name="ciudad" id="ciudad" @if(sizeof($usuario->cliente)) value="{{ $usuario->cliente->ciudad }}" @endif required placeholder="Ciudad" />
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6 columns">
                            Perfil p&uacute;blico: 
                            <label>
                                Si 
                                <input type="radio" name="publico" id="publico_s" @if(sizeof($usuario->cliente) && $usuario->cliente->publico == 'S') checked @endif />
                            </label>
                            <label>
                                No 
                                <input type="radio" name="publico" id="publico_n" @if(sizeof($usuario->cliente) && $usuario->cliente->publico == 'N') checked @endif />
                            </label>
                        </div>
                        <div class="col-md-6 columns">
                            <input type="password" name="password" id="password" value="" placeholder="Clave" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 columns">
                            <input type="file" name="imagen" id="imagen" placeholder="Imagen" />
                            <small>Tama&ntilde;o m&aacute;ximo: 1MB</small>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 columns">
                            <input type="submit" value="Actualizar datos" class="boton crema" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
        <div class="col-md-6">
            <h2 class="text-center amarillo">Mis cr&eacute;ditos</h2>
            <span class="credito grande text-center">
                @if(sizeof($usuario->cliente))
                    {{ $usuario->cliente->creditos }}
                @else
                    0
                @endif
                <br />
            </span>
            <span class="credito text-center">
                @if(sizeof($usuario->cliente) && $usuario->cliente->creditos == 1)
                Cr&eacute;dito
                @else
                Cr&eacute;ditos
                @endif
            </span>
            <div class="text-center">
                <a href="#" class="boton mini crema" data-featherlight="#div_creditos">Comprar cr&eacute;ditos</a>
                <a href="#" class="boton mini gris" data-featherlight="#div_servicios">Acceder a servicios</a>
            </div>
        </div>
        <div id="div_servicios" class="lightbox">
            <form method="post" class="form" action="{{ url('/cliente/contratar/') }}">
                <input type="hidden" id="id_cliente" name="id_cliente" value="@if(sizeof($usuario->cliente)){{ $usuario->cliente->id }}@endif" />
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="row">
                    <div class="col-md-4 columns">Seleccione el servicio</div>
                    <div class="col-md-8 columns">
                        <select id="id_servicio" name="id_servicio">
                            @foreach($servicios as $s)
                            <option value="{{ $s->id }}">{{ $s->nombre }} - {{ $s->creditos }} cr&eacute;dito(s)</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-12 text-center">
                        <input type="submit" class="boton mini crema" value="Adquirir servicio" />
                    </div>
                </div>
            </form>
        </div>
        <div id="div_creditos" class="lightbox">
            <form method="post" class="form" action="{{ url('/compra/creditos/') }}">
                <input type="hidden" id="id_cliente" name="id_cliente" value="@if(sizeof($usuario->cliente)){{ $usuario->cliente->id }}@endif" />
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="row">
                    <div class="col-md-4 columns">Cr&eacute;ditos</div>
                    <div class="col-md-8 columns">
                        <input type="number" id="creditos" name="creditos" />
                    </div>
                    <div class="col-sm-12 text-center">
                        <input type="submit" class="boton mini crema" value="Comprar cr&eacute;ditos" />
                    </div>
                </div>
            </form>
        </div>
    </div>

    @if(sizeof($usuario->cliente))
    
        @if(sizeof($cotizaciones))
            <div class='row'>
                <hr />
                <br />
                <div class='col-sm-12'>
                    <h3 class="amarillo">Cotizaciones realizadas</h3>
                    <p>A continuación se lista las cotizaciones que Cowo le ha realizado.</p>
                </div>
            </div>
            
            <div class="row item">
                <div class="col-md-1 col-sm-4"><strong>#</strong></div>
                <div class="col-md-6 col-sm-4"><strong>Fecha de Creaci&oacute;n</strong></div>
                <div class="col-md-2 col-sm-3"><strong>Estado</strong></div>                
                <div class="col-md-2 col-sm-2"><strong>Ver</strong></div>  
            </div>
            @php($i = 1)
            @foreach($cotizaciones as $cotizacion)
            <div class="row item">
                <div class="col-md-1 col-sm-4">{{ $i }}</div>
                <div class="col-md-6 col-sm-4">{{ $cotizacion->fecha_creacion }}</div>
                <div class="col-md-2 col-sm-3">{{ \App\Helpers\Helper::estados()[$cotizacion->estado] }}</div>
                <div class="col-md-2 col-sm-2"><a href='{{ url('cliente/cotizacion/ver/'.$cotizacion->id) }}'>Ver</a> <br /></div>                
            </div>
            @php($i++)
            @endforeach
        
        @endif
        
        @if(sizeof($pendientes))
            <div class='row'>
                <hr />
                <br />
                <div class='col-sm-12'>
                    <h3 class="amarillo">Servicios pendientes por reservas</h3>
                    <p>A continuación se lista los servicios que requieren seleccionar la fecha y hora de reserva.</p>
                </div>
            </div>
            
            <div class="row item">
                <div class="col-md-6 col-sm-4"><strong>Descripci&oacute;n</strong></div>
                <div class="col-md-2 col-sm-3"><strong>Fecha</strong></div>
                <div class="col-md-2 col-sm-3"><strong>Cr&eacute;ditos</strong></div>                
                <div class="col-md-2 col-sm-2"><strong>Reserva</strong></div>  
            </div>
            
            @foreach($pendientes as $p)
            <div class="row item">
                <div class="col-md-6 col-sm-4">{{ $p->servicio->nombre }}</div>
                <div class="col-md-2 col-sm-3">{{ $p->created_at }}</div>
                <div class="col-md-2 col-sm-3">{{ $p->servicio->creditos }}</div>
                <div class="col-md-2 col-sm-2"><a href='{{ url('cliente/reservar/'.$p->id) }}'>Realizar</a> <br /></div>
                <!--div id='div_reserva_{{ $p->id }}' class='lightbox text-center'>
                    <form method='post' action='{{ url('/cliente/reservar') }}' rel='form-reserva' class='form'>
                        <input type='hidden' name='id_contratacion' rel='id_contratacion' value='{{ $p->id }}' />

                        @if($p->servicio->periodicidad == 'H')
                        <p>Selecciona la fecha y hora de la reserva</p>
                        <input rel='fecha_hora' type='text' name='fecha' readonly="readonly" />
                        @else
                        <p>Selecciona la fecha de la reserva</p>
                        <input rel='fecha' type='text' name='fecha' readonly="readonly" />
                        @endif
                        <p>Seleccione la sala. Tenemos dos salas disponibles</p>
                        <select name='id_sala'>
                            <option value='1'>Sala 1</option>
                            <option value='2'>Sala 2</option>
                        </select>
                        <div class="clearfix"></div>
                        <br />
                        <input type='submit' class='boton mini crema' value='Reservar' />
                    </form>
                </div-->
            </div>
            @endforeach
        
        @endif
    
        <div class="seccion gris">
            <div class="row">
                <div class="col-sm-12 columns">
                   <h3 class="gris">Historial de movimiento de cr&eacute;ditos hechos por el Administrador</h3>
                </div>
            </div>

            <div class="row item">
                <div class="col-md-8 col-sm-6"><strong>Descripci&oacute;n</strong></div>
                <div class="col-md-2 col-sm-3"><strong>Valor</strong></div>                
            </div>

            @forelse($usuario->cliente->logCreditos as $c)
            <div class="row item">
                <div class="col-md-8 col-sm-6">{{ $c->motivo }}</div>
                <div class="col-md-2 col-sm-3">$ {{ Helper::number_format($c->cantidad) }}</div>                
            </div>
            @empty
            <div class="row">
                <div class="col-sm-12"> 
                    <div class="text-center"><strong>No has realizado compras</strong></div>
                </div>
            </div>
            @endforelse
        </div>


        <div class="seccion gris">
            <div class="row">
                <div class="col-sm-12 columns">
                   <h3 class="gris">Historial de compras</h3>
                </div>
            </div>

            <div class="row item">
                <div class="col-md-8 col-sm-6"><strong>Descripci&oacute;n</strong></div>
                <div class="col-md-2 col-sm-3"><strong>Fecha</strong></div>
                <div class="col-md-2 col-sm-3"><strong>Valor</strong></div>                
            </div>

            @forelse($usuario->cliente->compras as $c)
            <div class="row item">
                <div class="col-md-8 col-sm-6">@if(sizeof($c->paquete)) {{ $c->paquete->nombre }} @else {{ $c->creditos }} cr&eacute;ditos @endif</div>
                <div class="col-md-2 col-sm-3">{{ $c->created_at }}</div>
                <div class="col-md-2 col-sm-3">$ {{ Helper::number_format($c->valor) }}</div>                
            </div>
            @empty
            <div class="row">
                <div class="col-sm-12"> 
                    <div class="text-center"><strong>No has realizado compras</strong></div>
                </div>
            </div>
            @endforelse
        </div>

        @if(sizeof($planes_pendientes))
            <div class="seccion gris">
                <div class="row">
                    <div class="col-sm-12 columns">
                       <h3 class="gris">Planes pendientes por asignar</h3>
                    </div>
                </div>

                <div class="row item">
                    <div class="col-md-4 col-sm-4"><strong>Descripci&oacute;n</strong></div>
                    <div class="col-md-2 col-sm-2"><strong>Fecha</strong></div>
                    <div class="col-md-2 col-sm-2"><strong>Cantidad</strong></div>                
                    <div class="col-md-2 col-sm-2"><strong>Valor</strong></div>                
                    <div class="col-md-2 col-sm-2"><strong>Asignar</strong></div>                
                </div>

                @foreach($planes_pendientes as $p)
                    <div class="row item">
                        <div class="col-md-4 col-sm-4">@if(sizeof($p->paquete)) {{ $p->paquete->nombre }} @else {{ $p->creditos }} cr&eacute;ditos @endif</div>
                        <div class="col-md-2 col-sm-2">{{ $p->created_at }}</div>
                        <div class="col-md-2 col-sm-2">{{ $p->cantidad }}</div>
                        <div class="col-md-2 col-sm-2">{{ Helper::number_format($p->paquete->valor) }}</div>                
                        <div class="col-md-2 col-sm-2"> <a href="{{url('cliente/asignar', ["id" => $p->id])}}">Asignar</a> </div>                
                    </div>
                @endforeach
            </div>        
        @endif
        
        @if(sizeof($usuario->cliente->membresiaActual))
        <div class="row">
            <div class="col-sm-12">
                <h3 class="amarillo">Actualiza o mejora tu plan</h3>
                <p>Actualmente cuentas con el plan <strong>{{ $usuario->cliente->membresiaActual[0]->paquete->nombre }}</strong>.
                    Puedes renovarlo o mejorarlo con los botones de abajo. Ten en cuenta que solo puedes tener un
                    plan activo al mismo tiempo y que no puedes adquirir planes inferiores al que ya posees.
                </p>
                <br/>
            </div>
            <div class="row text-center">
                <div class="col-md-4 col-lg-3">
                    <a href="{{ url('/compra/paquete/'.$usuario->cliente->membresiaActual[0]->paquete->id) }}" class="boton mini crema">Renovar</a>
                </div>
                @if(sizeof($planes))
                <div class="col-md-4 col-lg-3">
                    <form method="post" action="{{ url('/compra/paquete/') }}">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <select id="plan_mejora" name="id_plan">
                            @foreach($planes as $p)
                            <option value="{{ $p->id }}">{{ $p->nombre }} - $ {{ Helper::number_format($p->valor) }}</option>
                            @endforeach
                        </select>
                        <input type="submit" class="boton mini gris" value="Mejorar" />                    
                    </form>
                </div>
                @endif
            </div>
        </div>
        <hr />
        @endif
        
        @if(sizeof($temas))
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="amarillo">&Uacute;ltimos temas en el foro</h3>
                    <br/>
                </div>
            </div>
            
            <div class="row item">
                <div class="col-md-8 col-sm-6"><strong>Tema</strong></div>
                <div class="col-md-2 col-sm-3"><strong>Fecha</strong></div>
                <div class="col-md-2 col-sm-3"><strong>Categor&iacute;a</strong></div>              
            </div>

            @forelse($temas as $t)
                <div class="row item">
                    <div class="col-md-8 col-sm-6">{{ $t->title }}</div>
                    <div class="col-md-2 col-sm-3">{{ $t->created_at }}</div>
                    <div class="col-md-2 col-sm-3">{{ $t->category->title }}</div>                
                </div>
            @empty
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center"><strong>No hay temas en el foro</strong></div>
                    </div>
                </div>
            @endforelse
        
        @endif
        
        <div class="seccion gris">
            <div class="row">
                <div class="col-sm-12 columns">
                    <h3 class="gris">Historial de servicios</h3>
                </div>
            </div>
            
            <div class="row item">
                <div class="col-md-8 col-sm-6"><strong>Descripci&oacute;n</strong></div>
                <div class="col-md-2 col-sm-3"><strong>Fecha</strong></div>
                <div class="col-md-2 col-sm-3"><strong>Cr&eacute;ditos</strong></div>              
            </div>

            @forelse($usuario->cliente->contrataciones as $c)
                <div class="row item">
                    <div class="col-md-8 col-sm-6">{{ $c->servicio->nombre }}</div>
                    <div class="col-md-2 col-sm-3">{{ $c->created_at }}</div>
                    <div class="col-md-2 col-sm-3">{{ $c->servicio->creditos }}</div>                
                </div>
            @empty
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center"><strong>No tienes servicios contratados</strong></div>
                    </div>
                </div>
            @endforelse
        </div>
    
    @endif
    
    <div class='row'>
        <div class='col-sm-12'>
            <h3 class='amarillo'>Comunidad</h3>
            <input type='text' id='buscador_comunidad' placeholder='Qui&eacute;n quieres buscar...' />
            <a href='{{ url('/cliente/buscar') }}' id='lnkBuscarComunidad' class='boton mini gris'>Encontrar</a>
        </div>
    </div>
    <br />
    <div id='div_comunidad'>
        @include('sesion.comunidad', array("clientes" => $clientes))
    </div>
</div>
@stop
