@extends('master')

@section('content')
<div class="cforms1">
    <form id="form_login" name="form_login" action="{{ url('/login') }}" method="post" class="form">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="container">

            <div class="row row-centered">
                <div class="col-sm-6 col-md-5 col-centered">
                    <h2 class="amarillo text-center">Inicio de sesi&oacute;n</h2>
                    <br />
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Correo</label>
                        </div>
                        <div class="col-sm-8">
                            <input class="input-group-field" type="text" name="login" id="login" placeholder="Correo" />    
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Nombre de usuario</label>
                        </div>
                        <div class="col-sm-8">
                            <input class="input-group-field" type="password" name="password" id="password" placeholder="Clave" />
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <a href="{{ url('cliente/clave/email') }}">&iquest;No recuerdas tu clave?</a><br />
                            <a href="{{ url('cliente/registro') }}">Quiero registrame</a><br /><br />
                            <input type="submit" value="Iniciar sesi&oacute;n" class="boton crema" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<br />
@stop