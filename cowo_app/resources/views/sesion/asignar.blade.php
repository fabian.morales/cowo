@extends('master')

@section('content')
<div class="container">
    <div class="row">        
        <h2 class="amarillo">Asignaci&oacute;n de Planes</h2>
        <form id="form_perfil" name="form_perfil" class="form" action="{{ url('cliente/guardar_asignacion') }}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="plan" value="{{$plan->paquete->id}}">
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <p>Seleccione el cliente</p>
                <select name='id_cliente' id='id_cliente'>
                    @foreach($clientes as $cliente)
                        <option value='{{$cliente->cliente["id"]}}'>{{$cliente->cliente["nombre"]}} {{$cliente->cliente["apellido"]}}</option>
                    @endforeach
                </select>
            <br />
            <br />
            <input type='submit' class='boton mini crema' value='Asignar' />
        </form>
    </div>
</div>
@stop
