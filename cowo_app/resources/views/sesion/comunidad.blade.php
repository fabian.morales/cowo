<div class='row comunidad'>
    @forelse($clientes as $c)
    <div class='col-md-3'>
        <div class='item'>
            <a rel='mostrar_usuario' href='{{ url('cliente/comunidad/'.$c->id) }}' data-featherlight='ajax'>
                <img src='{{ $c->usuario->obtenerImagen() }}?{{ rand() }}' />
            </a>
            <p class='text-center'>
                <a rel='mostrar_usuario' href='{{ url('cliente/comunidad/'.$c->id) }}' data-featherlight='ajax'>
                    {{ $c->nombre }}
                </a>
            </p>
        </div>
    </div>
    @empty
    <div class='col-sm-12 text-center'>
        No hay usuarios para mostrar
    </div>
    @endforelse
</div>

