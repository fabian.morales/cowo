<style>
    /*body{
        background-image: url("{{asset('template/images/bg-eventos.jpg')}}") !important;
        background-repeat: no-repeat  !important;
        background-position: center center  !important;
        background-size: cover !important;
        background-attachment: fixed !important;
    }
    */
    
    
		
</style>

<div class="container body">
    <div class="row">
        <div class="col-md-12">
            <ul class="eventos">
                @foreach($eventos as $evento)
                    <li>
                        <a href="#" class="popRegistro" data-featherlight="#popRegistro_{{$evento->id}}">
                            <input type="hidden" name="id_evento" class="id_evento" value="{{$evento->id}}">
                            <img alt="" src="{{asset('storage/eventos/'.$evento->id.'/'.$evento->imagen)}}">
                            <span class="eventos_fecha">{{$evento->fecha}}</span>
                            <span class="eventos_lugar">{{$evento->lugar}}</span>
                            <h3 class="eventos_evento">{{$evento->evento}}</h3>
                        </a>
                        
                        <div id="popRegistro_{{$evento->id}}" class="lightbox">
                            <form method="post" class="form" action="{{ url('/calendario/registro') }}">
                                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                <input type="hidden" id="id" name="id" value="{{$evento->id}}">
                                
                                <div class="row">
                                    <div class="col-md-12 columns"> <h1 style="color: #e9b155;">{{$evento->evento}}</h1> </div>
                                </div>
                                
                                <div class="row">
                                    <!--<div class="col-sm-3 columns"><img style="width: 100%" alt="" src="{{asset('storage/eventos/'.$evento->id.'/'.$evento->imagen)}}"></div> -->
                                    
                                    <div class="col-md-12 columns"> <h3>{{$evento->lugar}} <br /> {{$evento->fecha}}</h3> </div>
                                </div>
                                
                                <div class="row">    
                                    <div class="col-md-12 columns"> <h3>{{$evento->descripcion}}</h3> </div>
                                </div>
                                
                                <div class="row">    
                                    <div class="col-sm-12 columns"><h3>Registrate:</h3></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 columns">
                                        <input type="text" placeholder="Nombre completo" id="nombre" name="nombre" value="{{sizeof( \Auth::user() )? \Auth::user()->nombre : ''}}"/>
                                    </div>
                                    <div class="col-sm-6 columns">
                                        <input type="text" placeholder="Telefono" id="telefono" name="telefono" />
                                    </div>
                                </div>
                                
                                <div class="row">    
                                    <div class="col-sm-12 columns">
                                        <input type="email" placeholder="Email" id="email" name="email" value="{{sizeof( \Auth::user() )? \Auth::user()->email : ''}}" />
                                    </div>
                                </div>
                                
                                <div class="row">     
                                    <div class="col-sm-12 text-center">
                                        <input type="submit" class="boton mini crema" value="Enviar" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>
                @endforeach
            </ul>            
        </div>        
    </div> 
</div>