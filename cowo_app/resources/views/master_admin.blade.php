
<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

    <head>
        <title>COWO</title>

        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        
        <!-- Favicon --> 
        <link rel="shortcut icon" href="{{ asset('template/images/favicon.ico') }}">

        <!-- this styles only adds some repairs on idevices  -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Google fonts - witch you want to use - (rest you can just remove) -->
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,600" rel="stylesheet">

        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- ######### CSS STYLES ######### -->
        
        @section ('css_header')
        @show
        <link href="{{ asset('js/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('js/jquery-ui/jquery-ui.theme.min.css') }}" rel="stylesheet" />
        <link rel="stylesheet" href="{{ asset('js/featherlight/featherlight.min.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('js/lightslider/css/lightslider.min.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('template/css/reset.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('template/css/bootstrap.min.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('template/css/style.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('template/css/app.css') }}" type="text/css" />

        <link rel="stylesheet" href="{{ asset('template/css/font-awesome/css/font-awesome.min.css') }}">
        
        <!-- responsive devices styles -->
        <link rel="stylesheet" media="screen" href="{{ asset('template/css/responsive-leyouts.css') }}" type="text/css" />

        <!-- animations -->
        <link href="{{ asset('template/js/animations/css/animations.min.css') }}" rel="stylesheet" type="text/css" media="all" />

        <!-- just remove the below comments witch color skin you want to use -->
        <link rel="stylesheet" href="{{ asset('template/css/colors/slate.css') }}" />
        
        <link rel="alternate" hreflang="es-co" href="https://www.cowo.com.co/" />

        @section ('js_header')
        @show
        
        <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-86493698-1', 'auto');
      ga('send', 'pageview');
    
    </script>
    </head>

    <body>
        <div id="loader"><div></div></div>
        <div class="site_wrapper">
            <div class="parallax_sec3">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class='logo-head'>
                                <a href="{{ url('/') }}"><img src="{{ asset('template/images/logo.png') }}" class="img-responsive"></a>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="panel-header">
                                <a class='item logout' href='{{ url('/logout') }}'>Cerrar sesi&oacute;n</a>
                                @if(Auth::user()->admin == 'Y')
                                    &nbsp;-&nbsp;<a class='item' href='{{ url('/administrador') }}'>Administrador</a>                                
                                @else
                                    <a class='item perfil' href='{{ url('/login') }}'>Iniciar sesi&oacute;n</a>
                                @endif
                            </div>
                            <div class='clearfix'></div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="menu-resp">
                                        <a href="#main-menu">
                                            <img src="{{ asset('imagenes/template/menu_resp.png') }}" />
                                            <span>&nbsp;Menu</span>
                                        </a>
                                    </div>
                                    <nav id="main-menu">
                                        <ul>
                                            <li><a href='{{ url('/administrador') }}'>Inicio</a></li>                                            
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

        <!-- End Header -->  

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mensajes">
                        @if (Session::has('mensajeError'))
                        <div class="alert alert-danger">
                            {{ Session::get('mensajeError') }}
                        </div>
                        @endif
                        @if (Session::has('mensaje'))
                        <div class="alert alert-success">
                            {{ Session::get('mensaje') }}
                        </div>
                        @endif
                        @if (Session::has('mensajeAviso'))
                        <div class="alert alert-info">
                            {{ Session::get('mensajeAviso') }}
                        </div>
                        @endif
                        @if (Session::has('mensajeExt'))
                        <div class="alert alert-info">
                            {{ Session::get('mensajeExt') }}
                        </div>
                        @endif
                        @if (Session::has('status'))
                        <div class="alert alert-info">
                            {{ Session::get('status') }}
                        </div>
                        @endif
                        @if($errors->any())
                        <div class="alert alert-danger">
                            {{$errors->first()}}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        @yield('content')
        
        <div class="copyrights">
            <div class="container">
                <ul>
                    <li class="animate" data-anim-type="zoomIn" data-anim-delay="200"><a href="https://www.facebook.com/cowobogota" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li class="animate" data-anim-type="zoomIn" data-anim-delay="400"><a href="https://twitter.com/cowobogota" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li class="animate" data-anim-type="zoomIn" data-anim-delay="600"><a href="https://www.instagram.com/cowobogota/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                </ul>

                <div class="clearfix"></div>
                <div class="margin_top3"></div>

                © 2017 Cowo.com.co I Todos los derechos reservados.
            </div>
        </div>

        <!-- end copyright info -->


        <a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

    </div>


    <!-- ######### JS FILES ######### -->
    <!-- get jQuery from the google apis -->
    <script type="text/javascript" src="{{ asset('template/js/universal/jquery.js') }}"></script>
    <script src="{{ asset('js/jquery-ui/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/featherlight/featherlight.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/lightslider/js/lightslider.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/script.js') }}"></script>

    <!-- animations -->
    <script src="{{ asset('template/js/animations/js/animations.min.js') }}" type="text/javascript"></script>
    <script>
        (function (window, $) {
            $(document).ready(function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                
                /*$(".aliados").lightSlider({
                    item: 4,
                    responsive : [
                        {
                            breakpoint:800,
                            settings: {
                                item:3,
                                slideMove:1,
                                slideMargin:6,
                              }
                        },
                        {
                            breakpoint:480,
                            settings: {
                                item:2,
                                slideMove:1
                              }
                        }
                    ]
                });
                */
                
                $(".slider_creditos").lightSlider({
                    item: 5,
                    auto:true,
                    speed: 600,
                    loop:true,
                    pauseOnHover: true,
                    pager: false,
                    responsive : [
                        {
                            breakpoint:800,
                            settings: {
                                item:1,
                                slideMove:1,
                                slideMargin:6,
                            }
                        },
                        {
                            breakpoint:480,
                            settings: {
                                item:1,
                                slideMove:1
                              }
                        },
                        {
                            breakpoint:320,
                            settings: {
                                item:1,
                                slideMove:1
                              }
                        }
                    ]
                });
            });
        })(window, jQuery);
    </script>
    @section ('js_body')
    @show
</body>
</html>
