@extends('master')

@section('js_header')

@stop

@section('content')
<div class="container">
    @if(sizeof($menu) && false)

    <div class='row'>
        <div class='col-sm-12'>
            <div class="contenido">
                <h3>{{ $menu->seccion->titulo }}</h3>
            </div>
        </div>
    </div>
    @endif
</div>
<div class='contenidos'>
    {!! $seccion !!}
</div>
@stop