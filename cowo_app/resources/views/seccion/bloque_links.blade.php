<div class="container seccion">
    <div class="row text-center">
        @foreach($menus as $m)
        
        @if(!empty($m->url))
        <div class="col-md-4"><a href=" {{ url($m->url) }}" class="boton blanco">{{ $m->titulo }}</a></div>
        @else
        <div class="col-md-4"><a href=" {{ url('/'.$m->llave) }}" class="boton blanco">{{ $m->titulo }}</a></div>
        @endif
        
        @endforeach
    </div>
</div>
