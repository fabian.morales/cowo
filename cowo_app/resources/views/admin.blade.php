<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>COWO</title>
        <link rel='stylesheet' id='flatsome-googlefonts-css'  href='//fonts.googleapis.com/css?family=Dancing+Script%3A300%2C400%2C700%2C900%7CLato%3A300%2C400%2C700%2C900%7CLato%3A300%2C400%2C700%2C900%7CLato%3A300%2C400%2C700%2C900&#038;subset=latin&#038;ver=4.4.2' type='text/css' media='all' />
        <link href="{{ asset('foundation/css/foundation.css') }}" rel="stylesheet" />
        <link href="{{ asset('foundation/css/app.css') }}" rel="stylesheet" />
        <link href="{{ asset('foundation/motion-ui/motion-ui.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('foundation/fonts/foundation-icons.css') }}" rel="stylesheet" />
        <link href="{{ asset('js/featherlight/featherlight.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('js/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('js/jquery-ui/jquery-ui.theme.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('js/tooltipster/css/tooltipster.bundle.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('js/tooltipster/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-borderless.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('css/estilos.css') }}" rel="stylesheet" />
        <link href="{{ asset('css/tema.css') }}" rel="stylesheet" />
        <link rel="icon" href="{{ asset('/template/images/favicon.ico') }}" sizes="32x32" />
        @section ('css_header')
        @show

        <script src="{{ asset('js/jquery-1.12.1.min.js') }}"></script>
        <script src="{{ asset('foundation/js/vendor/foundation.min.js') }}"></script>
        <script src="{{ asset('foundation/js/app.js') }}"></script>
        <script src="{{ asset('foundation/motion-ui/motion-ui.min.js') }}"></script>
        <script src="{{ asset('js/featherlight/featherlight.min.js') }}"></script>
        <script src="{{ asset('js/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('js/tooltipster/js/tooltipster.bundle.min.js') }}"></script>
        <script src="{{ asset('js/admin.js') }}"></script>
        <script>
        $(document).foundation();
        (function (window, $) {
            $(document).ready(function () {
                $(document).foundation();
            });
        })(window, jQuery);
        </script>
        @section ('js_header')
        @show
    </head>
    <body>
        <div id="loader"><div></div></div>
        <div class="top-bar">
            
                <div class="top-bar-left">
                    <ul class="dropdown menu" data-dropdown-menu>
                        <li class="menu-text"><img src="{{ asset('template/images/logo.png') }}" alt="Cowo" width="90" /></li>
                        @if (Auth::check())
                            <li><a href="{{ url('/') }}">Sitio</a></li>
                            @if (Auth::user()->admin == 'Y')                            
                                <li><a href="#">Usuarios</a>
                                    <ul class="menu vertical">
                                        <li><a href="{{ url('administrador/usuarios') }}">Gestionar</a></li></li>
                                        <li><a href="{{ url('administrador/cotizaciones') }}">Gestionar Cotizaciones</a></li></li>
                                        <!-- <li><a href="{{ url('administrador/cotizaciones/cotizar') }}">Enviar cotizaci&oacute;n</a></li></li> -->
                                    </ul>
                                <li>
                                    <a href="#">Contenidos</a>
                                    <ul class="menu vertical">
                                        <li><a href="{{ url('administrador/menus') }}">Menu</a></li>
                                        <li><a href="{{ url('administrador/contenido/seccion') }}">Secciones</a></li>
                                        <li><a href="{{ url('administrador/formularios') }}">Formularios</a></li>
                                        <li><a href="{{ url('administrador/publicidad') }}">Popups</a></li>
                                        <li><a href="{{ url('forum') }}">Foro</a></li>
                                        <li><a href="{{ url('administrador/galerias') }}">Fotos</a></li>
                                        <li><a href="{{ url('administrador/contenido') }}">Emails</a></li>
                                        <!--li><a href="{{ url('administrador/testimonios') }}">Testimonios</a></li-->
                                        <li><a href="{{ url('administrador/aliados') }}">Aliados</a></li>
                                        <li><a href="{{ url('administrador/faqs') }}">FAQs</a></li>
                                        <li><a href="{{ url('administrador/contacto') }}">Datos de contacto</a></li>
                                        <li><a href="{{ url('administrador/configuracion') }}">Configuraciones</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{ url('administrador/eventos') }}">Eventos</a>                            
                                </li>
                                <li>
                                    <a href="#">Servicios</a>
                                    <ul class="menu vertical">
                                        <li><a href="{{ url('administrador/servicios') }}">Gestionar</a></li>
                                        <li><a href="{{ url('administrador/servicios/reservas') }}">Gestionar Reservas</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{ url('administrador/paquetes') }}">Paquetes</a></li>
                                <!--li><a href="{{ url('administrador/tasas') }}">Tasa</a></li>
                                <li><a href="{{ url('administrador/pedidos') }}">Pedidos</a></li>
                                <li><a href="{{ url('administrador/buscador/categorias') }}">Categor&iacute;as buscador</a></li-->
                            @else
                                <li><a href="{{ url('comercial/cotizaciones') }}">Gestionar Cotizaciones</a></li>
                            @endif
                                <li><a href="{{ url('/logout') }}">Salir</a></li>
                        @endif
                    </ul>
                </div>
            
        </div>
        
        <div class="row separador">
            <div class="small-12 columns">
                <div class="mensajes">
                    @if (Session::has('mensajeError'))
                    <div class="alert callout" data-closable>
                        {{ Session::get('mensajeError') }}
                        <button class="close-button" aria-label="Cerrar" type="button" data-close>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    @if (Session::has('mensaje'))
                    <div class="success callout" data-closable>
                        {{ Session::get('mensaje') }}
                        <button class="close-button" aria-label="Cerrar" type="button" data-close>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    @if (Session::has('mensajeExt'))
                    {{ Session::get('mensajeExt') }}
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns">
                @yield('content')
            </div>
        </div>
        <footer>
            <div class="row">
                Copyright 2016 &copy; <strong>Cowo</strong>
            </div>
        </footer>
    </body>
</html>