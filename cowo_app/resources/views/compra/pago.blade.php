@extends('master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="titulo seccion"><span>Datos de la compra</span></h3>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2"><strong>Compra:</strong></div>
        <div class="col-md-10">{{ $compra->id }}</div>

        <div class="col-md-2"><strong>Fecha:</strong></div>
        <div class="col-md-10">{{ $compra->created_at }}</div>
        
        <div class="col-md-2"><strong>Paquete a comprar:</strong></div>
        <div class="col-md-10">{{ $compra->paquete->nombre }}</div>

        <div class="col-md-2"><strong>Créditos a comprar:</strong></div>
        <div class="col-md-10">{{ $compra->paquete->creditos }}</div>
        
        <div class="col-md-2"><strong>Valor total:</strong></div>
        <div class="col-md-10">$ {{ Helper::number_format($compra->valor) }}</div>
    </div>


    <div class="row separador"></div>
    <form action="https://secure.payco.co/payment.php" method="post" enctype="application/x-www-form-urlencoded" name="paymentForm" id="paymentForm">
        <input type="hidden" name="p_cust_id_cliente" type="text" value="{{ $clienteId }}" />
        <input type="hidden" name="p_key" type="text" value="{{ $firma }}" />
        <input type="hidden" name="p_id_factura" type="text" value="{{ $compra->id }}" />
        <input type="hidden" name="p_currency_code" type="text" value="COP" />
        <input type="hidden" name="p_amount" type="text" value="{{ $compra->valor }}" />
        <input type="hidden" name="p_description" type="text" value="ORDEN DE COMPRA # {{ $compra->id }}" />
        <input type="hidden" name="p_email" type="text" value="{{ $compra->cliente->email }}" />
        <input type="hidden" name="p_url_respuesta" type="text" value="{{ url('/compra/respuesta') }}" />
        <input type="hidden" name="p_url_confirmacion" type="text" value="{{ url('/compra/respuesta') }}" />
        <input type="hidden" name="p_tax" type="text" value="0" />
        <input type="hidden" name="p_amount_base" type="text" value="0" />
        <input type="hidden" name="p_test_request" type="text" value="TRUE" />
        <input type="hidden" name="p_extra1" type="text" value="{{ $token->token }}" />
        <input type="hidden" name="p_extra2" type="text" value="{{ $intento->id }}" />
        <div class="row">
            <div class="col-sm-12"><input type="submit" id="btnPagar" class="button alert float-right" value="Realizar pago" /></div>
        </div>
    </form>
</div>
@stop