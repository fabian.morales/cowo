@extends('master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12 columns">
            
        </div>
    </div>

    <div class="row">
        <div class="col-sm-7 columns">
            <h2 class="titulo seccion"><span>Resultado de la trasacción</span></h2>
            
            <h3 class="{{ $claseRespuesta }}">Transacci&oacute;n {{ $respuesta }}</h3>
            <big><strong>Valor transacci&oacute;n: $ {{ Helper::number_format($compra->valor) }}</strong></big>
            <br />
            <br />
            <h2 class="titulo seccion"><span>Datos de la trasacción</span></h2>
            <div class="row">
                <div class="col-md-6">
                    <img src="{{ asset('/imagenes/franquicias/'.$franquicia.'.png') }}" />
                </div>
                <div class="col-md-6">
                    <ul>
                        <li><strong>Franquicia:</strong> {{ $intento->franquicia }}</li>
                        <li><strong>Fecha transacci&oacute;n:</strong> {{ $intento->fecha_transaccion }}</li>
                        <li><strong>N&uacute;mero de transacci&oacute;n:</strong> {{ $intento->num_transaccion }}</li>
                        <li><strong>C&oacute;digo de aprobaci&oacute;n:</strong> {{ $intento->cod_aprobacion }}</li>
                        <li><strong>Mensaje:</strong> {{ $intento->mensaje }}</li>
                    </ul>
                </div>
            </div>
            <br />
            <br />
        </div>
    </div>    
</div>
@stop