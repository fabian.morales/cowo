<form>
    <!--script src="https://s3-us-west-2.amazonaws.com/epayco/v1.0/checkoutEpayco.js"  -->
    <script src="https://checkout.epayco.co/checkout.js" 
        class="epayco-button" 
        data-epayco-key="8155b1e90b0b6e0181809bd27ba41945" 
        data-epayco-amount="{{ $valor }}" 
        data-epayco-name="{{ $nombre }}" 
        data-epayco-description="{{ $descripcion }}" 
        data-epayco-currency="cop" 
        data-epayco-country="co" 
        data-epayco-test="false"
        data-epayco-extra1="{{ $extra1 }}"
        data-epayco-extra2="{{ $extra2 }}"
        data-epayco-extra3="{{ $extra3 }}"
        data-epayco-response="{{ url('/compra/respuesta') }}" 
        data-epayco-confirmation="{{ url('/compra/confirmar/') }}" >
    </script>
</form>