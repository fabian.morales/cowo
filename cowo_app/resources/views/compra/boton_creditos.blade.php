@extends('master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12 columns">
            <h3 class="titulo seccion"><span>Compra cr&eacute;ditos</span></h3>
            @if(sizeof($cliente->membresiaActual))
            <p>Actualmente tienes un plan {{ $cliente->membresiaActual[0]->paquete->nombre }}, por lo que cada crédito te costará $ {{ Helper::number_format(Helper::calcularConIva(3500)) }}</p>
            @else
            <p>Actualmente no tienes un plan activo, por lo que cada crédito te costará ${{ Helper::number_format(Helper::calcularConIva(5000)) }}</p>
            @endif
            
            <p>
                <big><strong>Total de cr&eacute;ditos: {{ $creditos }}</strong></big>
                <br />
                <big><strong>Total a pagar: $ {{ Helper::number_format($valor) }}</strong></big>
                <br />
                <br />
                Haz clic en este bot&oacute;n para realizar el pago:
            </p>
            
            {{--*/ $extra1 = Helper::generar_token().'::'.Auth::user()->id.'::0::0::0::0' /*--}}
            @include('compra.boton_epayco', ['valor' => $valor, 'nombre' => 'Compra de cr&eacute;ditos', 'descripcion' => 'Compra de cr&eacute;ditos', 'extra1' => $extra1, 'extra2' => 0, 'extra3' => $creditos])
            
            <br />
            <a href="{{ url('/cliente/perfil') }}" class="boton mini crema">Regresar</a>
        </div>
    </div>
</div>
@stop