@extends('master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12 columns">
            <h3 class="titulo seccion"><span>Compra de plan</span></h3>
            @if(sizeof($cliente->membresiaActual))
            <p>Actualmente tienes un plan {{ $cliente->membresiaActual[0]->paquete->nombre }}, el cual podr&aacute; ser renovado o mejorado</p>
            @else
            <p>Est&aacute;s a punto de comprar un plan nuevo</p>
            @endif
            
            <p>
                <big><strong>Plan: {{ $plan->nombre }}</strong></big>
                <br />
                @if($descuento->horas != 0)
                    <big><strong>Horas: {{ $descuento->horas }}</strong></big>
                    <br />
                @else
                    <big><strong>Cantidad: {{ $descuento->cantidad }}</strong></big>
                    <br />
                    <big><strong>Meses: {{ $descuento->meses }}</strong></big>
                    <br />
                @endif
                <big><strong>Total a pagar: $ {{ Helper::number_format( Helper::calcularConIva($descuento->precio * $descuento->cantidad * $descuento->meses * ($descuento->horas != 0 ? $descuento->horas : 1), sizeof($descuento) && $descuento->fecha_vencimiento >= date('Y-m-d') ? $descuento->valor : 0 ) ) }}</strong></big>
                <br />
                <br />
                Haz clic en este bot&oacute;n para realizar el pago:
            </p>
            
            {{--*/ $valor = Helper::calcularConIva( $descuento->precio * $descuento->cantidad * $descuento->meses * ($descuento->horas != 0 ? $descuento->horas : 1), sizeof($descuento) && $descuento->fecha_vencimiento >= date('Y-m-d') ? $descuento->valor : 0 ) /*--}}
            {{--*/ $extra1 = Helper::generar_token().'::'.Auth::user()->id.'::'.$descuento->horas.'::'.$descuento->cantidad.'::'.$descuento->meses.'::'.$descuento->id /*--}}
            @include('compra.boton_epayco', ['valor' => $valor, 'nombre' => $plan->nombre, 'descripcion' => $plan->descripcion_corta, 'extra1' => $extra1, 'extra2' => $plan->id, 'extra3' => 0])
            
            <br />
            <a href="{{ url('/cliente/perfil') }}" class="boton mini crema">Regresar</a>
        </div>
    </div>
</div>
@stop