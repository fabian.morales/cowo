@extends('admin')

@section('content')
<div class="container">

    <div class="content_fullwidth lessmar">
        <div class="clearfix margin_top4"></div>
            <form method="post" action="{{ url('/compra/confirmar') }}" />

            <input type="text" name="x_cust_id_cliente" value="11093" />
            <input type="text" name="x_ref_payco" value="243734" />
            <input type="text" name="x_id_factura" value="r5TF47DdeqoHHtiPG" />
            <input type="text" name="x_id_invoice" value="r5TF47DdeqoHHtiPG" />
            <input type="text" name="x_description" value="Acceso ilimitado" />
            <input type="text" name="x_amount" value="714000" />
            <input type="text" name="x_amount_country" value="714000" />
            <input type="text" name="x_amount_ok" value="714000" />
            <input type="text" name="x_tax" value="0" />
            <input type="text" name="x_amount_base" value="0" />
            <input type="text" name="x_currency_code" value="COP" />
            <input type="text" name="x_bank_name" value="Banco de Pruebas" />
            <input type="text" name="x_cardnumber" value="457562*******0326" />
            <input type="text" name="x_quotas" value="12" />
            <input type="text" name="x_respuesta" value="Aceptada" />
            <input type="text" name="x_response" value="Aceptada" />
            <input type="text" name="x_approval_code" value="000000" />
            <input type="text" name="x_transaction_id" value="243734" />
            <input type="text" name="x_fecha_transaccion" value="2017-02-18 20:23:18" />
            <input type="text" name="x_transaction_date" value="2017-02-18 20:23:18" />
            <input type="text" name="x_cod_respuesta" value="1" />
            <input type="text" name="x_cod_response" value="1" />
            <input type="text" name="x_response_reason_text" value="00-Aprobada" />
            <input type="text" name="x_errorcode" value="00" />
            <input type="text" name="x_franchise" value="VS" />
            <input type="text" name="x_extra1" value="UhJFcKdWSbfvtOJth63sy::7" />
            <input type="text" name="x_extra2" value="5" />
            <input type="text" name="x_extra3:" value="" />
            <input type="text" name="x_business" value="INVERSIONES MMB SAS." />
            <input type="text" name="x_customer_doctype" value="CC" />
            <input type="text" name="x_customer_document" value="123456789" />
            <input type="text" name="x_customer_name" value="Pepito" />
            <input type="text" name="x_customer_lastname" value="Perez" />
            <input type="text" name="x_customer_email" value="pruebas@yopmail.com" />
            <input type="text" name="x_customer_phone" value="0000000" />
            <input type="text" name="x_customer_country" value="CO" />
            <input type="text" name="x_customer_city" value="Cali" />
            <input type="text" name="x_customer_address" value="Calle con carrera" />
            <input type="text" name="x_customer_ip" value="181.143.84.226" />
            <input type="text" name="x_signature" value="0d6a0a680dd7044b7abbc9c572e2edf165ce8f9e82c35fade8fa3d776959bedb" />
            <input type="text" name="x_test_request" value="TRUE" />
            
            <!--input type="text" name="x_cust_id_cliente" value="11093" />
            <input type="text" name="x_ref_payco" value="239219" />
            <input type="text" name="x_id_factura" value="wZBYheTPJ5WhvraTA" />
            <input type="text" name="x_id_invoice" value="wZBYheTPJ5WhvraTA" />
            <input type="text" name="x_description" value="Compra de crÃ©ditos" />
            <input type="text" name="x_amount" value="350000" />
            <input type="text" name="x_amount_country" value="350000" />
            <input type="text" name="x_amount_ok" value="350000" />
            <input type="text" name="x_tax" value="0" />
            <input type="text" name="x_amount_base" value="0" />
            <input type="text" name="x_currency_code" value="COP" />
            <input type="text" name="x_bank_name" value="Banco de Pruebas" />
            <input type="text" name="x_cardnumber" value="457562*******0326" />
            <input type="text" name="x_quotas" value="12" />
            <input type="text" name="x_respuesta" value="Aceptada" />
            <input type="text" name="x_response" value="Aceptada" />
            <input type="text" name="x_approval_code" value="000000" />
            <input type="text" name="x_transaction_id" value="239219" />
            <input type="text" name="x_fecha_transaccion" value="2017-02-09 14:37:52" />
            <input type="text" name="x_transaction_date" value="2017-02-09 14:37:52" />
            <input type="text" name="x_cod_respuesta" value="1" />
            <input type="text" name="x_cod_response" value="1" />
            <input type="text" name="x_response_reason_text" value="00-Aprobada" />
            <input type="text" name="x_errorcode" value="00" />
            <input type="text" name="x_franchise" value="VS" />
            <input type="text" name="x_extra1" value="prJF7k5C5EiwlUUzTslp::1" />
            <input type="text" name="x_extra2:" value="" />
            <input type="text" name="x_extra3" value="100" />
            <input type="text" name="x_business" value="INVERSIONES MMB SAS." />
            <input type="text" name="x_customer_doctype" value="CC" />
            <input type="text" name="x_customer_document" value="123456789" />
            <input type="text" name="x_customer_name" value="Pepito" />
            <input type="text" name="x_customer_lastname" value="Perez" />
            <input type="text" name="x_customer_email" value="pruebas@yopmail.com" />
            <input type="text" name="x_customer_phone" value="0000000" />
            <input type="text" name="x_customer_country" value="CO" />
            <input type="text" name="x_customer_city" value="Cali" />
            <input type="text" name="x_customer_address" value="Calle con carrera" />
            <input type="text" name="x_customer_ip" value="181.143.84.226" />
            <input type="text" name="x_signature" value="099577f76b4ee1506569eb44ce29cc5f151672a1232505d18104cc1ddf69e425" />
            <input type="text" name="x_test_request" value="TRUE" /-->
            
            <input type="submit" value="Probar" />

            </form>

        @if(sizeof($planes))
        <div class="pricing-tables-main">
            <div class="pricing-tables-main">
                <div class="mar_top3"></div>
                <div class="clearfix"></div>
                
                @foreach($planes as $i => $p)

                <div class="@if($i % 2 == 0) pricing-tables-two @else pricing-tables-helight-two @endif">
                    <div class="title">{{ $p->nombre }}</div>
                    <div class="price">${{ Helper::number_format($p->valor) }} <i> Mensuales</i></div>

                    <div class="cont-list">
                        {!! $p->descripcion !!}
                    </div>
                    <div class="ordernow">
                        {{--*/ $extra1 = Helper::generar_token().'::'.Auth::user()->id.'::0::0::0' /*--}}
                        @include('compra.boton_epayco', ['valor' => $p->valor, 'nombre' => $p->nombre, 'descripcion' => $p->descripcion_corta, 'extra1' => $extra1, 'extra2' => $p->id, 'extra3' => 0])
                    </div>
                    
                    <!--div class="ordernow">
                        <a href="{{ url('/compra/paquete/'.$p->id) }}" title="Comprar ahora">
                            <img src="https://369969691f476073508a-60bf0867add971908d4f26a64519c2aa.ssl.cf5.rackcdn.com/btns/boton_carro_de_compras_epayco2.png" alt="Comprar ahora" />
                        </a>
                        <br />
                        <b><a href="contrato-membresia-cowo.pdf" target="_blank" style="font-family:Raleway; font-size:12px; font-weight:normal; text-decoration:underline;">Términos y condiciones</a></b>
                    </div-->

                </div><!-- end section -->
                
                @endforeach
            </div>

        </div><!-- end pricing tables with 4 columns -->
        @endif
    </div>
</div>
@stop