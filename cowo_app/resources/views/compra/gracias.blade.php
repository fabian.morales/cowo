@extends('master')

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3>GRACIAS</h3>
        <p>Gracias por tu cotizaci&oacute;n. Recibir&aacute;s un correo con el detalle en m&aacute;ximo 12 horas h&aacute;biles. 
            Para cualquier duda o comentario puedes comunicarte con el 304-400-0742 o escribiendo a info@encarguelo.com</p>
    </div>
</div>
@stop

@section('js_body')
<!-- Google Code for Cotizaci&oacute;n Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1016498110;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "Ein0COb_6mQQvo_a5AM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1016498110/?label=Ein0COb_6mQQvo_a5AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
@stop