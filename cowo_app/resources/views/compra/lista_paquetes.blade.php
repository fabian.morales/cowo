@extends('master')

@section('js_header')
<script>
(function (window, $) {
    $(document).ready(function(){        
        $("a[rel='agregar-item-producto']").click(function (e) {
            e.preventDefault();
            $("#loader").addClass("loading");
            var $idProd = $(this).attr("data-item");
            
            $.ajax({
                url: '{{ url('/carrito/actualizar/') }}',
                method: 'post',
                data: { id_producto: $idProd, cantidad: 1 },
                dataType: 'json'
            })
            .done(function(res) {
                if (res.resp === 1){
                    window.cotizador.mostrarModalCarrito(res.producto);
                }
                else{
                    alert('No se pudo añadir el producto a su carrito de compras');
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                try {
                    alert(jqXHR.responseJSON.error.message);
                }
                catch(err) {
                    alert('Ha ocurrido un error al agregar el producto, intente nuevamente, porfavor.');
                }
            }).
            always(function() {
                $("#loader").removeClass("loading");
            });
        });
    });
})(window, jQuery);
</script>
@stop

@section('content')
<section id="destacados">
    <div class="row">
        <div class="small-12 medium-9 large-10 columns">
            <div class="row collapse lista-productos">
                <div class="small-12 columns">
                    <h3 class="titulo seccion"><span>Productos</span></h3>
                </div>
                @foreach($productos as $p)
                <div class="small-6 medium-4 large-3 columns end item">

                    <div class="imagen">
                        <a href="{{ url('/producto/'.$p->id) }}">
                            <img src="{{ asset('imagenes/productos/'.$p->id.'_g.png') }}" />
                        </a>
                    </div>
                    <p class="precio">$ {{ Helper::number_format($p->precio_venta) }}</p>
                    <div class="nombre"><p>{{ strtok(wordwrap($p->nombre, 60, "\n"), "\n") }}</p></div>
                    <div class="row collapse">
                        <div class="small-6 medium-9 columns">
                            <a href="" class="button agregar" rel="agregar-item-producto" data-item="{{ $p->id }}"><img src="{{ asset('/imagenes/template/icono-carrito-blanco.png') }}" /> Agregar al carrito</a>
                        </div>
                        <div class="small-6 medium-3 columns">
                            <a href="{{ url('/producto/'.$p->id) }}" class="boton mas">M&aacute;s</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="row collapse lista-productos">
                <div class="small-12 columns">{!! $productos->render() !!}</div>
            </div>
        </div>
        <div class="small-12 medium-3 large-2 columns">
            <h3 class="categoria-prod">Categorías</h3>
            <ul class="lista-categorias">
                <li><a href="{{ url('/productos/') }}">Todo</a></li>
            </ul>
            @if(sizeof($categorias_amazon))
            <h4 class="categoria-prod">Amazon</h4>
            <ul class="lista-categorias">
                @foreach($categorias_amazon as $c)
                <li><a href="{{ url('/productos/'.$c->clave) }}">{{ $c->nombre }}</a></li>
                @endforeach
            </ul>
            @endif
            
            @if(sizeof($categorias_ebay))
            <h4 class="categoria-prod">Ebay</h4>
            <ul class="lista-categorias">
                @foreach($categorias_ebay as $c)
                <li><a href="{{ url('/productos/'.$c->clave) }}">{{ $c->nombre }}</a></li>
                @endforeach
            </ul>
            @endif
            
            @if(sizeof($categorias_otros))
            <h4 class="categoria-prod">Otros</h4>
            <ul class="lista-categorias">
                @foreach($categorias_otros as $c)
                <li><a href="{{ url('/productos/'.$c->clave) }}">{{ $c->nombre }}</a></li>
                @endforeach
            </ul>
            @endif
        </div>
    </div>

</section>
@stop