@extends('master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="animate" data-anim-type="fadeInUp">Gracias por contactarnos,</h2>
            <p class="animate" data-anim-type="fadeInUp" data-anim-delay="200">nos comunicaremos con usted muy pronto.</p>
        </div>
    </div>
</div>
@stop

@section('js_body')
<!-- Google Code for Call to action Cowo Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1016498110;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "lyJ5CL3pwGsQvo_a5AM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1016498110/?label=lyJ5CL3pwGsQvo_a5AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
@stop
