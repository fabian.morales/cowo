<div class="feature_section15">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ul class="slider_creditos">
                    @foreach($servicios as $s)
                    <li>
                        <div class="item_servicio">
                            <strong>{{ $s->creditos }} Créditos</strong>
                            <p>{{ $s->nombre }}</p>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>