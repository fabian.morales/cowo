<div id='div_detalle_plan' class=''>
    <div class="detalle_top">
        <div class="detalle_top_one">
            <div class="image"><img src="{{ asset('storage/imagenes/planes/'.$plan->id).'/'.$plan->imagen }}"></div>            
        </div>
        <div class="detalle_top_two">                
            <div id="title">{{ $plan->nombre }}</div>
             <div class="desde">{{ $plan->desde == 1 ? 'Desde' : '  ' }}</div>
            <div id="price">${{ Helper::number_format($plan->valor, sizeof($plan->descuentos) && $plan->descuentos[0]->fecha_vencimiento >= date("Y-m-d") ? $plan->descuentos[0]->valor : 0) }} {{ Helper::periodicidad($plan->periodicidad) }}</div>
            
            <div id="more_info">&iquest;Desea m&aacute;s informaci&oacute;n? <a href="tel:{{ $cfg_contacto['telefono'] }}">Ll&aacute;manos</a> o <a href="{{ url('contacto') }}">Cont&aacute;ctanos</a></div>
            
        </div>
        <div class="detalle_top_three">
            <form>
                @if (!sizeof($usuario))
                <a href='{{ url('/login') }}'><img src='https://369969691f476073508a-60bf0867add971908d4f26a64519c2aa.ssl.cf5.rackcdn.com/btns/boton_carro_de_compras_epayco2.png' /></a>
                @elseif (!sizeof($usuario->cliente))
                <a href='{{ url('/cliente/perfil') }}'><img src='https://369969691f476073508a-60bf0867add971908d4f26a64519c2aa.ssl.cf5.rackcdn.com/btns/boton_carro_de_compras_epayco2.png' /></a>
                @elseif (!sizeof($usuario->cliente->membresiaActual) || $usuario->cliente->membresiaActual[0]->paquete->valor < $plan->valor)
                
                {{--*/ $valor = Helper::calcularConIva($plan->valor, sizeof($plan->descuentos)  && $plan->descuentos[0]->fecha_vencimiento >= date('Y-m-d') ? $plan->descuentos[0]->valor : 0 ) /*--}}
                {{--*/ $extra1 = Helper::generar_token().'::'.Auth::user()->id.'::0::0::0::'.(sizeof($plan->descuentos)  && $plan->descuentos[0]->fecha_vencimiento >= date('Y-m-d') ? $plan->descuentos[0]->id : 0) /*--}}
                @include('compra.boton_epayco', ['valor' => $valor, 'nombre' => $plan->nombre, 'descripcion' => $plan->descripcion_corta, 'extra1' => $extra1, 'extra2' => $plan->id, 'extra3' => 0])
                
                @else
                
            <img src='https://369969691f476073508a-60bf0867add971908d4f26a64519c2aa.ssl.cf5.rackcdn.com/btns/boton_carro_de_compras_epayco2.png' style='opacity: 0.3;' />
                @endif
            </form>
            
            <div id="cont-adicional">
                Valor cr&eacute;dito adicional: ${{Helper::number_format($plan->credito_adicional)}}                
            </div>
            
        </div>
    </div>
    <div class="detalle_bottom">
        <div class="detalle_bottom_left">
            <b>Beneficios</b>
            <div id="cont-list">
                     {!! $plan->descripcion !!}                
            </div>
        </div>
        <div class="detalle_bottom_right">
            <div id="cont-desc">
                <b>Descripci&oacute;n</b>
                <br />
                {{ $plan->descripcion_corta }}                
            </div>
        </div>
    </div>
</div>
