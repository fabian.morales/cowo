<div class='container'>
    
    <div class="row">
        <div class="col-sm-12">
            <h3 class="titulo seccion"><span>{{ $form->titulo }}</span></h3>
        </div>
    </div>
    <form class='form_sistema' id="form_sistema_{{ $form->id }}" name="form_sistema_{{ $form->id }}" action="{{ url('/formularios/enviar') }}" method="post" enctype="multipart/form-data" @if(!empty($form->clase)) class="{{ $form->clase }}" @endif>
        <input type="hidden" id="id_form" name="id_form" value="{{ $form->id }}" />
        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />

        <div class="row">
            <div class="col-md-{{ ((int)$form->col_email * 3) }} columns">
                <label for="correo_cliente" class='hide'>Nombre y apellido</label>
                <input type="text" name="nombre_cliente" id="nombre _cliente" placeholder="Nombre y apellido" required />
            </div>
            <div class="col-md-{{ ((int)$form->col_nombre * 3) }} columns">
                <label for="correo_cliente" class='hide'>Correo electr&oacute;nico</label>
                <input type="email" name="correo_cliente" id="correo_cliente" placeholder="Correo electr&oacute;nico" required />
            </div>
        
            @foreach($form->campos as $campo)
            {{--*/ $parametros = $campo->obtenerParametros() /*--}}
            {{--*/ $llave = $campo->obtenerLlave() /*--}}

            <div class="col-sm-12 col-md-{{ ((int)$campo->columnas * 3) }}">
                @if($campo->tipo == 'texto-corto')
                <label for="{{ $llave }}" class='hide'>{{ $campo->nombre }}</label>
                <input type='text' name='{{ $llave }}' id='{{ $llave }}' placeholder="{{ $campo->nombre }}" @if($campo->obligatorio == 'S') required @endif />
                @endif

                @if($campo->tipo == 'texto-largo')
                <label for="{{ $llave }}" class='hide'>{{ $campo->nombre }}</label>
                <textarea name='{{ $llave }}' id='{{ $llave }}' placeholder="{{ $campo->nombre }}" @if($campo->obligatorio == 'S') required @endif rows="5"></textarea>
                @endif

                @if($campo->tipo == 'numero')
                <label for="{{ $llave }}" class='hide'>{{ $campo->nombre }}</label>
                <input type='number' name='{{ $llave }}' id='{{ $llave }}' placeholder="{{ $campo->nombre }}" @if($campo->obligatorio == 'S') required @endif />
                @endif

                @if($campo->tipo == 'email')
                <label for="{{ $llave }}" class='hide'>{{ $campo->nombre }}</label>
                <input type='email' name='{{ $llave }}' id='{{ $llave }}' placeholder="{{ $campo->nombre }}" @if($campo->obligatorio == 'S') required @endif />
                @endif

                @if($campo->tipo == 'calendario')
                <label for="{{ $llave }}" class='hide'>{{ $campo->nombre }}</label>
                <input type='text' name='{{ $llave }}' id='{{ $llave }}' placeholder="{{ $campo->nombre }}" readonly="readonly" rel="date-picker" @if($campo->obligatorio == 'S') required @endif />
                @endif

                @if($campo->tipo == 'lista-desplegable' && (sizeof($parametros) && isset($parametros->lista) && sizeof($parametros->lista)))
                <label for="{{ $llave }}">{{ $campo->nombre }}</label>
                <select name='{{ $llave }}' id='{{ $llave }}' @if($campo->obligatorio == 'S') required @endif>
                @foreach($parametros->lista as $l)
                    <option value="{{ $l }}">{{ $l }}</option>
                @endforeach
                </select>
                @endif

                @if($campo->tipo == 'lista-checks' && (sizeof($parametros) && isset($parametros->lista) && sizeof($parametros->lista)))
                <fieldset>
                    <legend>{{ $campo->nombre }}</legend>
                    @foreach($parametros->lista as $k => $l)
                    <label for="{{ $llave }}-{{ $k }}">
                        <input type='checkbox' name='{{ $llave }}[]' id='{{ $llave }}-{{ $k }}' value="{{ $l }}" @if($campo->obligatorio == 'S') required @endif />
                        &nbsp;
                        {{ $l }}
                    </label>
                    @endforeach
                </fieldset>
                @endif

                @if($campo->tipo == 'lista-radios' && (sizeof($parametros) && isset($parametros->lista) && sizeof($parametros->lista)))
                <fieldset>
                    <legend>{{ $campo->nombre }}</legend>
                    @foreach($parametros->lista as $k => $l)
                    <label for="{{ $llave }}-{{ $k }}">
                        <input type='radio' name='{{ $llave }}[]' id='{{ $llave }}-{{ $k }}' value="{{ $l }}"  @if($campo->obligatorio == 'S') required @endif />
                        &nbsp;
                        {{ $l }}
                    </label>
                    @endforeach
                </fieldset>
                @endif

                @if($campo->tipo == 'mapa')
                <div id="mapa-{{ $campo->id }}-{{ $campo->llave }}" rel="mapa-form"></div>
                @endif

                @if($campo->tipo == 'imagen' && (sizeof($parametros) && isset($parametros->imagen)))
                <img class="img-responsive" id="imagen-{{ $campo->id }}-{{ $campo->llave }}" src="{{ $parametros->imagen }}" rel="imagen-form" title="{{ $campo->nombre }}" alt="{{ $campo->nombre }}" />
                @endif

                @if($campo->tipo == 'texto-p' && (sizeof($parametros) && isset($parametros->texto)))
                <p id="texto-{{ $campo->id }}-{{ $campo->llave }}">
                    @if(!empty($campo->nombre))<strong>{{ $campo->nombre }}: </strong>@endif {{ $parametros->texto }}
                </p>
                @endif
            </div>

        @endforeach
        </div>
        <br />
        <div class="row">
            <div class="col-sm-12">
                <!--div class="g-recaptcha" data-sitekey="6Lec9RgUAAAAAHeEGbx_UOIcGRCCDW8wLGUjgknU"></div-->
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-sm-12">
                <input type="submit" value="Enviar" class="boton crema" />
            </div>
        </div>
    </form>
</div>