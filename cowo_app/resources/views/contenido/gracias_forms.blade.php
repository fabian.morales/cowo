@extends('master')

@section('content')
<div class='container'>
    <div class="row">
        <div class="col-sm-12">
            <h3>GRACIAS</h3>
            <p>Hemos recibido tu solicitud. Te estaremos enviando una respuesta lo mas pronto posible.</p>
        </div>
    </div>
</div>
@stop