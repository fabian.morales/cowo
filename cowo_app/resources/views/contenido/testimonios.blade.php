<ul class="testimonios">
    @foreach($testimonios as $t)
    <li>
        <div class="imagen">
            <img src="{{ asset('imagenes/testimonios/'.$t->id.'.png') }}" />
        </div>
        <h3>{{ $t->titulo }}</h3>
        <p>{{ $t->descripcion }}</p>
    </li>
    @endforeach
</ul>
