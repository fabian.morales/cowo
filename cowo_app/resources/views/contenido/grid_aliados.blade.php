<ul class="aliados">
    @foreach($aliados as $a)
    <li>
        <div class="item">
            <a href="{{ $a->url}}"><img class="fullWidth" src="{{ asset('imagenes/aliados/'.$a->id.'.png') }}" /></a>
            <div class="texto">
                @if(!empty($a->descripcion)) 
                    {{ $a->descripcion }}
                @else
                    {{ $a->titulo }}
                @endif
            </div>
        </div>
    </li>
    @endforeach
</ul>
