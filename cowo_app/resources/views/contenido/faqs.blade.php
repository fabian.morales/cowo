<ul class="accordion" data-accordion data-allow-all-closed="true" data-multi-expand="true">
    @foreach ($faqs as $f)
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">{{ $f->titulo }}</a>
        <div class="accordion-content" data-tab-content>
            {{ $f->descripcion }}
        </div>
    </li>
    @endforeach
</ul>