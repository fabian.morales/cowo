<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="cforms animate" data-anim-type="fadeInUp" data-anim-delay="200">
                <form action='{{ url('/contacto/trabaja') }}' method='POST'>
                    <!-- Do not remove this code. -->                    

                    <fieldset>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre">
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="telefono" id="telefono" placeholder="Teléfono">
                            </div>
                            <div class="col-md-6">
                                <input type="email" name="email" id="email" placeholder="Email">
                            </div>
                        </div>
                    </fieldset>
                    <footer>
                        <button type="submit" class="boton crema">Enviar</button>
                    </footer>
                </form>
            </div>
        </div>
    </div>
</div>
