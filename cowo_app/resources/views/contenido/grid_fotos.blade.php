<div class="container_full">
    <div class="row no-gutter">
        @if(sizeof($fotos))
        
        @foreach($fotos as $f)
        <div class="col-sm-6 col-md-3">
            <div class="item foto">
                <a href="{{ asset('storage/imagenes/galeria/'.$f->id_galeria.'/'.$f->id.'.jpg') }}" data-featherlight>
                    <img src="{{ asset('storage/imagenes/galeria/'.$f->id_galeria.'/'.$f->id.'.jpg') }}" title="{{ $f->descripcion }}" alt="{{ $f->titulo }}" />
                </a>
            </div>
        </div>
        @endforeach
        
        @endif
    </div>
</div>