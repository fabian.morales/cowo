<div class="container" id="contacto">
    <div class="row">
        <div class="col-md-8">
            <div class="cforms animate" data-anim-type="fadeInUp" data-anim-delay="200">
                <div id='crmWebToEntityForm' >    
                    <form action='{{ url('/contacto/zoho/enviar') }}' rel='form-contacto-zoho' name='WebToLeads2173064000000113047' method='POST' accept-charset='UTF-8'>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <!-- Do not remove this code. -->
                        <input type='text' style='display:none;' name='xnQsjsdp' value='29c537be617247df6279159f406b7135ef836b27e0b9026a4a36b8f43b865efb'/>
                        <input type='hidden' name='zc_gad' id='zc_gad' value=''/>
                        <input type='text' style='display:none;' name='xmIwtLD' value='57307e5a25b762d2e1a623c2ca5af0356f428e5c18aeeab862b465e0483d291d'/>
                        <input type='text' style='display:none;'  name='actionType' value='TGVhZHM='/>

                        <input type='text' style='display:none;' name='returnURL' value='http&#x3a;&#x2f;&#x2f;cowo.com.co&#x2f;gracias' /> 
                        <!-- Do not remove this code. -->

                        <div class="row">
                            <div class="col-sm-12"><input type='text' maxlength='40' name='First Name' placeholder="Nombre" /></div>
                            <div class="col-sm-12"><input type='text' maxlength='80' name='Last Name' placeholder="Apellido" /></div>
                            <div class="col-sm-12"><input type='text' maxlength='100' name='Company' placeholder='Compañía' /></div>
                            <div class="col-sm-12"><input type='text' maxlength='100' name='Email' placeholder='Email' /></div>
                            <div class="col-sm-12"><input type='text' maxlength='30' name='Phone' placeholder='Teléfono o móvil' /></div>
                        </div>
                        <div class="row">
                            <div class='col-sm-12'>
                                <h4>&nbsp;&nbsp;¿Cuál de nuestros planes te interesa?</h4>
                                <br />
                            </div>
                            <div class="col-md-4">
                                <label><input type='checkbox' name='LEADCF106' /> Full Time Comunity</label>
                                <br />
                                <label><input type='checkbox' name='LEADCF110' /> Sala de Juntas</label>
                            </div>
                            <div class="col-md-4">
                                <label><input type='checkbox'  name='LEADCF102' /> Virtual</label>
                                <br />
                                <label><input type='checkbox'  name='LEADCF104' /> Semi Private</label>
                            </div>
                            <div class="col-md-4">
                                <label><input type='checkbox'  name='LEADCF103' /> Private</label>
                                <br />
                                <label><input type='checkbox'  name='LEADCF109' /> Part Time</label>
                            </div>
                        </div>
                        <div class='row'>
                            <div class='col-sm-12'>
                                <textarea name='Description' rows='10' maxlength='1000' placeholder='Quieres contarnos algo más...'></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="g-recaptcha" data-sitekey="6Lec9RgUAAAAAHeEGbx_UOIcGRCCDW8wLGUjgknU"></div>
                            </div>
                        </div>
                        <br />
                        <div class='row'>
                            <div class='col-sm-12'>
                                <input type='submit' class='boton crema' value='Enviar' onClick='javascript:document.charset = "UTF-8"; return checkMandatory()' />
                                <input type='reset' class='boton gris' value='Limpiar' />
                            </div>
                        </div>        

                        <script>
                            var mndFileds = new Array('Company', 'Last Name', 'Email');
                            var fldLangVal = new Array('Company', 'Last Name', 'Email');
                            var name = '';
                            var email = '';

                            function checkMandatory() {
                                for (i = 0; i < mndFileds.length; i++) {
                                    var fieldObj = document.forms['WebToLeads2173064000000113047'][mndFileds[i]];
                                    if (fieldObj) {
                                        if (((fieldObj.value).replace(/^\s+|\s+$/g, '')).length == 0) {
                                            if (fieldObj.type == 'file')
                                            {
                                                alert('Please select a file to upload.');
                                                fieldObj.focus();
                                                return false;
                                            }
                                            alert(fldLangVal[i] + ' cannot be empty.');
                                            fieldObj.focus();
                                            return false;
                                        } else if (fieldObj.nodeName == 'SELECT') {
                                            if (fieldObj.options[fieldObj.selectedIndex].value == '-None-') {
                                                alert(fldLangVal[i] + ' cannot be none.');
                                                fieldObj.focus();
                                                return false;
                                            }
                                        } else if (fieldObj.type == 'checkbox') {
                                            if (fieldObj.checked == false) {
                                                alert('Please accept  ' + fldLangVal[i]);
                                                fieldObj.focus();
                                                return false;
                                            }
                                        }
                                        try {
                                            if (fieldObj.name == 'Last Name') {
                                                name = fieldObj.value;
                                            }
                                        } catch (e) {
                                        }
                                    }
                                }
                            }
                        </script>
                    </form>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="col-md-4">
            <div class="addressinfo animate" data-anim-type="fadeInUp" data-anim-delay="400">
                <h3 class="informacion">Info</h3>
                C. {{ $cfg_contacto['telefono'] }}<br />
                D. {{ $cfg_contacto['direccion'] }}<br />
                {{ $cfg_contacto['correo'] }}<br />
                {{ $cfg_contacto['ciudad'] }}
            </div><!-- end section -->
            <div class="googglemap animate" data-anim-type="fadeInUp" data-anim-delay="500">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3976.5193948141386!2d{{ $cfg_contacto['latitud'] }}!3d{{ $cfg_contacto['longitud'] }}!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e3f9a9a6bba7359%3A0xdc439c872aadd2a!2sCl.+98+%238-37%2C+Bogot%C3%A1%2C+Colombia!5e0!3m2!1ses!2sus!4v1478191622155" width="100%" height="340" frameborder="0" style="border:0" allowfullscreen></iframe>        
            </div><!-- end section -->
        </div>        
    </div>
</div>
