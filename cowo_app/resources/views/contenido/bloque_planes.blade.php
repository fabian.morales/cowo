<div class="container">
    <div class="content_fullwidth lessmar">
        <div class="clearfix margin_top4"></div>
        @if(sizeof($planes))
        <div class="pricing-tables-main">
            <div class="pricing-tables-main">
                <div class="mar_top3"></div>
                <div class="clearfix"></div>
                
                @foreach($planes as $i => $p)

                <div class="bloque-plan">
                    <div class="pricing-tables-two">
                        <div class="image"><img src="{{ asset('storage/imagenes/planes/'.$p->id).'/'.$p->imagen }}"></div>
                        <div class="title">{{ $p->nombre }}</div>
                        <div class="desde">{{ $p->desde == 1 ? 'Desde' : '  ' }}</div>
                        <div class="price">${{ Helper::number_format($p->valor, sizeof($p->descuentos) && $p->descuentos[0]->fecha_vencimiento >= date("Y-m-d") ? $p->descuentos[0]->valor : 0) }} {{ Helper::periodicidad($p->periodicidad) }} </div>

                        <div class="cont-desc">
                            {{ strlen($p->descripcion_corta) > 80 ? substr($p->descripcion_corta, 0, 80).' ...' : $p->descripcion_corta }}
                        </div>
                        <div class="cont-list">
                            <b>Beneficios</b>
                            {!! $p->descripcion !!}
                        </div>
                        <div class="ordernow">
                            <a href="{{url('planes', ['detalle-plan', $p->id])}}" class="detalle-plan">M&Aacute;S INFORMACI&Oacute;N</a>
                        </div>
                    </div><!-- end section -->
                </div>
                
                @endforeach
            </div>

        </div><!-- end pricing tables with 4 columns -->
        @endif
    </div>
    
</div>