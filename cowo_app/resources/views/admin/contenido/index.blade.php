@extends('admin')

@section('content')
<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('administrador/contenido/crear') }}" class="button alert">Email nuevo <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">Editar Emails</div>
</div>
<div class="row item lista head">
    <div class="small-10 columns">Nombre</div>
    <div class="small-2 columns">Editar</div>
</div>
@foreach($emails as $e)
<div class="row item lista">
    <div class="small-10 columns">{{ $e->titulo }}</div>
    <div class="small-2 columns">
        <a class="tooltip-x" title='Editar email' href="{{ url('administrador/contenido/editar/'.$e->llave) }}"><i class="fi-pencil"></i></a>
        <!--<a class="tooltip-x" title='Eliminar email' href="{{ url('administrador/contenido/eliminar/'.$e->id) }}" onclick="return confirm('Desea eliminar este email?')"><i class="fi-trash"></i></a>-->
    </div>
</div>
@endforeach

@stop