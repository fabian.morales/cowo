@extends('admin')

@section('js_header')
<script src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>
<script>
    (function(window, $){
        $(document).ready(function() {
            tinymce.init({
                selector: "textarea#cuerpo",
                height: 300,
                theme: 'modern',
                language: 'es',
                relative_urls: false,
                document_base_url: 'http://encarguelo.frontiersoft.info/',
                remove_script_host: false,
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor varemails",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste filemanager textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | forecolor backcolor | bold italic fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | visualblocks",
                image_advtab: true
            });
            
            $("#btnAgregarVar").click(function(e) {
                e.preventDefault();
                tinymce.activeEditor.execCommand('mceInsertContent', false, $("#variable").val());
            });
        });
    })(window, jQuery);

</script>
@stop

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Crear cuerpo email</span></h3>
    </div>
</div>
<form id="form_email" name="form_email" action="{{ url('administrador/contenido/guardar') }}" method="post" enctype="multipart/form-data">
    <input type="hidden" id="tipo" name="tipo" value="E" />
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="small-12 columns">
            <label for="titulo">T&iacute;tulo</label>
            <input type='text' name='titulo' id='titulo'/>
        </div>
        <div class="small-12 columns">
            <label for="nombre">Contenido del email</label>
        </div>
        <div class="small-12 columns">
            <textarea id='cuerpo' name='cuerpo'></textarea>
        </div>
    </div>
    
    <div class="row">
        <div class="small-12 columns">
            <a class="button alert" href="{{ url('/administrador/contenido/') }}" />Cancelar</a>
            <input type="submit" value="Guardar" class="button default" />
        </div>
    </div>
</form>
@stop