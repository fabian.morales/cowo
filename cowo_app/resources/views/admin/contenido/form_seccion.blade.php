@extends('admin')

@section('js_header')
<script src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>
<script>
    (function(window, $){
        $(document).ready(function() {
            tinymce.init({
                selector: "textarea#cuerpo",
                content_css : 'css/tema.css',
                height: 300,
                theme: 'modern',
                language: 'es',
                relative_urls: false,
                document_base_url: '{{ url('/') }}',
                remove_script_host: false,
                extended_valid_elements: 'span,iframe[src|frameborder|style|scrolling|class|width|height|name|align]',
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor varsecciones contenedores",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime table contextmenu paste filemanager textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | forecolor backcolor | bold italic fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | visualblocks | table",
                image_advtab: true
            });
            
            $("#btnAgregarVar").click(function(e) {
                e.preventDefault();
                tinymce.activeEditor.execCommand('mceInsertContent', false, $("#variable").val());
            });
        });
    })(window, jQuery);

</script>
@stop

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Editar cuerpo secci&oacute;n</span></h3>
    </div>
</div>
<form id="form_email" name="form_email" action="{{ url('administrador/contenido/guardar') }}" method="post" enctype="multipart/form-data">
    <input type="hidden" id="id" name="id" value="{{ $contenido->id }}" />
    <input type="hidden" id="tipo" name="tipo" value="S" />
    <input type="hidden" id="llave" name="llave" value="{{ $contenido->id }}" />
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="small-12 columns">
            <label for="titulo">T&iacute;tulo</label>
            <input type='text' name='titulo' id='titulo' value='{{ $contenido->titulo }}' />
        </div>
        <div class="small-12 columns">
            <label for="peso">Peso (orden)</label>
            <input type='text' name='peso' id='peso' value='{{ $contenido->peso }}' />
        </div>
        <div class="small-12 columns">
            <label for="inicio">
                Mostrar en el home
                &nbsp;
                <label for='inicio_S'><input type='radio' name='inicio' id='inicio_S' value='S' @if($contenido->inicio == 'S') checked="selected" @endif /> Si</label>
                <label for='inicio_N'><input type='radio' name='inicio' id='inicio_N' value='N' @if($contenido->inicio == 'N') checked="selected" @endif /> No</label>
            </label>
        </div>
        <div class="small-12 columns">
            <label for="nombre">Contenido de la secci&oacute;n</label>
        </div>
        <div class="small-12 columns">
            <textarea id='cuerpo' name='cuerpo'>{{ $contenido->cuerpo }}</textarea>
        </div>
    </div>
    
    <div class="row">
        <div class="small-12 columns">
            <a class="button alert" href="{{ url('/administrador/contenido/seccion') }}" />Cancelar</a>
            <input type="submit" name='guardar' value="Guardar" class="button default" />
            <input type="submit" name='guardar_permancer' value="Guardar y permanecer" class="button default" />
        </div>
    </div>
</form>
@stop