@extends('admin')

@section('content')
<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('administrador/contenido/seccion/crear') }}" class="button alert">Secci&oacute;n nueva <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">Editar Secciones</div>
</div>
<div class="row item lista head">
    <div class="small-7 columns">Nombre</div>
    <div class="small-1 columns">Home</div>
    <div class="small-1 columns">Peso</div>
    <div class="small-3 columns">Editar</div>
</div>
@foreach($secciones as $s)
<div class="row item lista">
    <div class="small-7 columns">{{ $s->titulo }}</div>
    <div class="small-1 columns">@if($s->inicio == 'S') Si @else No @endif</div>
    <div class="small-1 columns">{{ $s->peso }}</div>
    <div class="small-3 columns">
        <a class="tooltip-x" title='Editar contenido' href="{{ url('administrador/contenido/seccion/editar/'.$s->id) }}"><i class="fi-pencil"></i></a>
        <a class="tooltip-x" title='Eliminar contenido' href="{{ url('administrador/contenido/seccion/borrar/'.$s->id) }}" onclick="return confirm('Desea eliminar esta seccion?')"><i class="fi-trash"></i></a>
	</div>
</div>
@endforeach

@stop