@extends('admin')

@section('css_header')
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" href="{{ asset('/js/jquery-ui/jquery-ui-timepicker-addon.css') }}" />
@stop

@section('js_header')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script>
    (function($){
        
        $(document).ready(function() {
            var $dtOpc = {
                minDate: -60,
                dateFormat: 'yy-mm-dd',
                timeFormat: 'HH:mm:ss',
                dayNames: [ "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado" ],
                dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
                dayNamesShort: [ "Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb" ],
                monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
                monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "Mau", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ],
                currentText: "Hoy",
                closeText: "Aceptar",
                showButtonPanel: true,
                pickerPosition: "bottom-left",
            };
            
            $("#lnkSumar").click(function(e) {
                e.preventDefault();
                $.ajax({
                    url: $(this).attr("href"),
                    method: 'post',
                    data: {
                        '_token': '{!! csrf_token() !!}',
                        'id_usuario': '{{ $usuario->id }}',
                        'motivo': $("#descripcion").val(),
                        'cantidad': $("#cantidad_creditos").val()
                    }
                })
                .done(function() {
                    alert('Créditos sumados exitosamente');
                    window.location.reload();
                });
            });
            
            $("#lnkRestar").click(function(e) {
                e.preventDefault();
                $.ajax({
                    url: $(this).attr("href"),
                    method: 'post',
                    data: {
                        '_token': '{!! csrf_token() !!}',
                        'id_usuario': '{{ $usuario->id }}',
                        'motivo': $("#descripcion").val(),
                        'cantidad': -parseInt($("#cantidad_creditos").val())
                    }
                })
                .done(function() {
                    alert('Créditos restados exitosamente');
                    window.location.reload();
                });
            });
            
            $("#fecha").datepicker($dtOpc);
            
            $("#lnkAsignarPlan").click(function(e) {
                e.preventDefault();
                
                if ($("#id_plan").val() !== ''){
                    $.ajax({
                        url: $(this).attr("href"),
                        method: 'post',
                        data: {
                            '_token': '{!! csrf_token() !!}',
                            'id_usuario': '{{ $usuario->id }}',
                            'id_plan': $("#id_plan").val(),
                            'fecha': $("#fecha").val()
                        }
                    })
                    .done(function() {
                        alert('Membresía asignada exitosamente');
                        window.location.reload();
                    })
                    .fail(function(qXHR, textStatus, errorThrown) {
                        alert(qXHR.responseText);
                    });
                }
            });
        });
    })(jQuery);
</script>
@stop

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Datos del usuario</span></h3>
    </div>
</div>
<form id="form_usuario" name="form_usuario" action="{{ url('administrador/usuarios/guardar') }}" method="post">
    <input type="hidden" id="id" name="id" value="{{ $usuario->id }}" />
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="login">Login</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="login" id="login" value="{{ $usuario->login }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="password">Clave</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="password" name="password" id="password" value="" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="email">Correo electr&oacute;nico</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="email" id="email" value="{{ $usuario->email }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label>Tipo</label>
        </div>
        <div class="medium-8 small-12 columns columns">
            <select name="tipo" id="tipo">
                <option value="">Cliente</option>
                <option value="A" @if($usuario->admin == "Y") selected @endif >Administrador</option>
                <option value="C" @if($usuario->comercial == "S") selected @endif>Comercial</option>
            </select>            
        </div>
    </div>
    <hr />
    <fieldset>
        <legend>Datos de cliente</legend>
        <br />
        <div class="row">
            <div class="medium-4 small-12 columns">
                <label for="nombre">Nombre</label>
            </div>
            <div class="medium-8 small-12 columns">
                <input type="text" name="nombre" id="nombre" value="{{ $usuario->nombre }}" />
            </div>
        </div>
        <div class="row">
            <div class="medium-4 small-12 columns">
                <label>Apellido</label>
            </div>
            <div class="medium-8 small-12 columns">
                <input type="text" id="apellido" name="apellido" value="@if(sizeof($usuario->cliente)){{ $usuario->cliente->apellido }}@endif" required placeholder="Apellido" />
            </div>
        </div>

        <div class="row">
            <div class="medium-4 small-12 columns">
                <label>Tipo de documento</label>
            </div>
            <div class="medium-8 small-12 columns">
                <select name="tipo_documento" id="tipo_documento" required>
                @foreach($tipos as $t)
                <option value="{{ $t }}" @if(sizeof($usuario->cliente) && $usuario->cliente->tipo_documento == $t) selected @endif>{{ $t }}</option>
                @endforeach
                </select>
            </div>
        </div>

        <div class="row">
            <div class="medium-4 small-12 columns">
                <label>Documento de identidad</label>
            </div>
            <div class="medium-8 small-12 columns">
                <input type="text" id="documento_identidad" name="documento_identidad" value="@if(sizeof($usuario->cliente)){{ $usuario->cliente->documento_identidad }}@endif" required placeholder="Documento identidad" />
            </div>
        </div>

        <div class="row">
            <div class="medium-4 small-12 columns">
                <label>Empresa</label>
            </div>
            <div class="medium-8 small-12 columns">
                <input type="text" name="empresa" id="empresa" value="@if(sizeof($usuario->cliente)){{ $usuario->cliente->empresa }}@endif" required placeholder="Empresa" />
            </div>
        </div>

        <div class="row">
            <div class="medium-4 small-12 columns">
                <label>Dirección</label>
            </div>
            <div class="medium-8 small-12 columns">
                <input type="text" name="direccion" id="direccion" value="@if(sizeof($usuario->cliente)){{ $usuario->cliente->direccion }}@endif" required placeholder="Dirección" />
            </div>
        </div>

        <div class="row">
            <div class="medium-4 small-12 columns">
                <label>Ciudad</label>
            </div>
            <div class="medium-8 small-12 columns">
                <input type="text" name="ciudad" id="ciudad" value="@if(sizeof($usuario->cliente)){{ $usuario->cliente->ciudad }}@endif" required placeholder="Ciudad" />
            </div>
        </div>        
    </fieldset>

    <!--div class="row">
        <div class="medium-4 small-12 columns">
            <label>Recibe alertas</label>
        </div>
        <div class="medium-8 small-12 columns columns">
            <input type="radio" name="alerta_edicion" id="alerta_edicion_si" value="Y" @if($usuario->alerta_edicion == "Y") checked @endif /><label for="alerta_edicion_si">Si</label>
            <input type="radio" name="alerta_edicion" id="alerta_edicion_no" value="N" @if($usuario->alerta_edicion == "N") checked @endif /><label for="alerta_edicion_no">No</label>
        </div>
    </div--> 
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label>Mostrar perfil</label>
        </div>
        <div class="medium-8 small-12 columns columns">
            <input type="radio" name="publico" id="publico_si" value="Y" @if($usuario->admin == "S") checked @endif /><label for="publico_si">Si</label>
            <input type="radio" name="publico" id="publico_no" value="N" @if($usuario->admin == "N") checked @endif /><label for="publico_no">No</label>
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label>Activo</label>
        </div>
        <div class="medium-8 small-12 columns columns">
            <input type="radio" name="activo" id="activo_si" value="Y" @if($usuario->activo == "Y") checked @endif /><label for="activo_si">Si</label>
            <input type="radio" name="activo" id="activo_no" value="N" @if($usuario->activo == "N") checked @endif /><label for="activo_no">No</label>
        </div>
    </div>
    
    @if(sizeof($usuario->cliente))
    <hr />
    <fieldset>
        <legend>Membres&iacute;a y Cr&eacute;ditos</legend>
        <br />
        @if(sizeof($usuario->cliente->membresiaActual))
        <div class="row">
            <div class="small-12 columns">
                <div class="callout">
                    <strong>Plan: {{ $usuario->cliente->membresiaActual[0]->paquete->nombre }}</strong><br />
                    <strong>Inicio: {{ $usuario->cliente->membresiaActual[0]->fecha_inicio }}</strong><br />
                    <strong>Vencimiento: {{ $usuario->cliente->membresiaActual[0]->fecha_vence }}</strong><br />
                </div>
            </div>
        </div>
        <br />
        @endif
        
        <div class="row">
            <div class="medium-4 small-6 columns">
                Asignar membres&iacute;a
            </div>
            <div class="medium-4 small-6 columns end">
                <select id="id_plan" name="id_plan">
                    <option>Seleccione un plan</option>
                    @foreach($planes as $p)
                    <option value="{{ $p->id }}">{{ $p->nombre }}</option>
                    @endforeach
                </select>
                <label>Seleccione la fecha</label>
                <input type="text" name="fecha" id="fecha" @if(sizeof($usuario->cliente->membresiaActual)) value="{{ $usuario->cliente->membresiaActual[0]->fecha_inicio }}" @endif />
                <a id="lnkAsignarPlan" href="{{ url('/administrador/usuarios/membresia') }}" class="button">Asignar</a>
            </div>
        </div>
		
		<div class="row">
            <div class="medium-4 small-6 columns">
                Cr&eacute;ditos Actuales
            </div>
            <div class="medium-4 small-6 columns end">
                <input type="text" name="creditos" id="creditos" value="{{ $usuario->cliente->creditos }}" placeholder="Creditos" readonly="readonly" />
            </div>
        </div>
        
        <div class="row">
            <div class="medium-4 small-12 columns">
                Modificar Cr&eacute;ditos
            </div>
            <div class="medium-4 small-6 columns">
                <input type="text" name="cantidad_creditos" id="cantidad_creditos" value="" />                
            </div>
            <div class="medium-4 small-12 columns">
                <a id="lnkSumar" href="{{ url('/administrador/usuarios/creditos/modificar') }}" class="button">Sumar</a>
                <a id="lnkRestar" href="{{ url('/administrador/usuarios/creditos/modificar') }}" class="button alert">Restar</a>
            </div>
        </div>
        
		<div class="row">
            <div class="medium-4 small-12 columns">
                Descripci&oacuten de la modificaci&oacute;n:
            </div>
            <div class="medium-4 small-6 columns">
                <input type="text" name="descripcion" id="descripcion" value="" />                
            </div>
			<div class="medium-4 small-12 columns">
                
            </div>            
        </div>
		
    </fieldset>
    
    <hr />
    <fieldset>
        <legend><b>&Uacute;ltimos movimientos de cr&eacute;ditos</b></legend>
        <div class="row item">
            <div class="medium-8 small-6 columns"><strong>Descripci&oacute;n</strong></div>
            <div class="medium-2 small-3 columns"><strong>Cantidad</strong></div>             
        </div>

        @forelse($usuario->cliente->logCreditos as $c)
		<div class="row item">
            <div class="medium-8 small-6 columns">{{ $c->motivo }}</div>
            <div class="medium-2 small-3 columns">$ {{ Helper::number_format($c->cantidad) }}</div>                
        </div>
        @empty
        <div class="row">
            <div class="small-12 columns"> 
                <div class="text-center"><strong>No tiene movimiento de creditos realizados</strong></div>
            </div>
        </div>
        @endforelse
    </fieldset>

    <hr />
    <fieldset>
        <legend><b>&Uacute;ltimas compras</b></legend>
        <div class="row item">
            <div class="medium-8 small-6 columns"><strong>Descripci&oacute;n</strong></div>
            <div class="medium-2 small-3 columns"><strong>Fecha</strong></div>
            <div class="medium-2 small-3 columns"><strong>Valor</strong></div>                
        </div>

        @forelse($usuario->cliente->compras as $c)
        <div class="row item">
            <div class="medium-8 small-6 columns">@if(sizeof($c->paquete)) {{ $c->paquete->nombre }} @else {{ $c->creditos }} cr&eacute;ditos @endif</div>
            <div class="medium-2 small-3 columns">{{ $c->created_at }}</div>
            <div class="medium-2 small-3 columns">$ {{ Helper::number_format($c->valor) }}</div>                
        </div>
        @empty
        <div class="row">
            <div class="small-12 columns"> 
                <div class="text-center"><strong>No tiene compras realizadas</strong></div>
            </div>
        </div>
        @endforelse
    </fieldset>
    
    <hr />
    <fieldset>
        <legend><b>&Uacute;ltimos servicios contratados</b></legend>
        
        <div class="row item">
            <div class="medium-6 small-6 columns"><strong>Descripci&oacute;n</strong></div>
            <div class="medium-2 small-3 columns"><strong>Fecha</strong></div>
            <div class="medium-2 small-3 columns"><strong>Fecha reserva</strong></div>
            <div class="medium-2 small-3 columns"><strong>Cr&eacute;ditos</strong></div>              
        </div>

        @forelse($usuario->cliente->contrataciones as $c)
        <div class="row item">
            <div class="medium-6 small-6 columns">{{ $c->servicio->nombre }}</div>
            <div class="medium-2 small-3 columns">{{ $c->created_at }}</div>
            <div class="medium-2 small-3 columns">@if(!empty($c->fecha_inicio)) {{ $c->fecha_inicio }} @else - @endif</div>
            <div class="medium-2 small-3 columns">{{ $c->servicio->creditos }}</div>                
        </div>
        @empty
        <div class="row">
            <div class="small-12 columns">
                <div class="text-center"><strong>No ha contratado servicios</strong></div>
            </div>
        </div>
        @endforelse
    </fieldset>
    @endif
    <hr />
    
    <div class="row">
        <div class="small-12 columns">
            <a class="button gris" href="{{ url('/administrador/usuarios/') }}" />Cancelar</a>
            <input type="submit" value="Guardar" class="button default" />
        </div>
    </div>
</form>
@stop