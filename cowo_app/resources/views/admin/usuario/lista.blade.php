<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('administrador/usuarios/crear') }}" class="button alert">Usuario nuevo <i class="fi-plus"></i></a>
    </div>
</div>

<form id="form_usuario" name="form_usuario" action="{{ url('administrador/usuarios') }}" method="get">
    <div class="row">
        <div class="medium-4 small-12 columns">
            <input type="text" name="filtro" id="login" placeholder="Buscar" value="{{ $filtro }}" />
            <input type="submit" value="Buscar" class="button default" />
        </div>
    </div>
</form>

<div class="row titulo lista">
    <div class="small-12 columns">Lista de usuarios</div>
</div>
<div class="row item lista head">    
    <div class="medium-3 columns">Nombre</div>
    <div class="medium-3 columns">Correo</div>
    <div class="medium-2 columns">Vence plan</div>
    <div class="medium-1 columns">Admin</div>
    <div class="medium-1 columns">Comercial</div>
    <div class="medium-1 columns">Activo</div>
    <div class="medium-1 columns">Editar</div>
</div>
@foreach($usuarios as $u)
<div class="row item lista">
    <div class="medium-3 columns">{{ $u->id }}. {{ $u->nombre }}@if(sizeof($u->cliente)) {{ $u->cliente->apellido }}@endif</div>
    <div class="medium-3 columns">{{ $u->email }}</div>
    <div class="medium-2 columns">@if(sizeof($u->cliente) && sizeof($u->cliente->membresiaActual)) {{ $u->cliente->membresiaActual[0]->fecha_vence }} @else - @endif</div>
    <div class="medium-1 columns">@if($u->admin == 'Y') Si @else No @endif</div>
    <div class="medium-1 columns">@if($u->comercial == 'S') Si @else No @endif</div>
    <div class="medium-1 columns">@if($u->activo == 'Y') Si @else No @endif</div>
    <div class="medium-1 columns">
        <a class="tooltip-x" title='Editar usuario' href="{{ url('administrador/usuarios/editar/'.$u->id) }}"><i class="fi-pencil"></i></a>
        <a rel="borrar-usuario" class="tooltip-x" title='Borrar usuario' href="{{ url('administrador/usuarios/borrar/'.$u->id) }}"><i class="fi-trash"></i></a>
    </div>
</div>
@endforeach
<div class="row">
    <div class="small-12 columns text-center">
        {!! $usuarios->render() !!}
    </div>
</div>