@extends('admin')

@section('content')
@include('admin.usuario.lista', array("filtro" => $filtro, "usuarios" => $usuarios))
@stop