@extends('admin')

@section('content')
    
    <form id="form_usuario" name="form_usuario" action="{{ url('administrador/eventos/registro', ['id' => $evento->id]) }}" method="get">
        <div class="row">
            <div class="medium-4 small-12 columns">
                <input type="text" name="filtro" id="login" placeholder="Buscar" value="{{ $filtro }}" />
                <input type="submit" value="Buscar" class="button default" />
            </div>
        </div>
    </form>

    <div class="row titulo lista">
        <div class="small-12 columns">Lista de personas registradas al evento {{$evento->evento}}</div>
    </div>
    <div class="row item lista head">    
        <div class="medium-4 columns">Nombre</div>
        <div class="medium-4 columns">Email</div>
        <div class="medium-2 columns">Telefono</div>
        <div class="medium-1 columns">Acciones</div>
    </div>
    @foreach($lista as $registro)
    <div class="row item lista">
        <div class="medium-4 columns">{{ $registro->id }}. {{ $registro->nombre }}</div>
        <div class="medium-4 columns">{{ $registro->email }}</div>
        <div class="medium-2 columns">{{ $registro->telefono }}</div>
        <div class="medium-1 columns">
            <a rel="borrar-registro" class="tooltip-x" title='Borrar registro' href="{{ url('administrador/eventos/registro/eliminar/'.$registro->id) }}" onclick="return confirm('Desea eliminar este item?')"><i class="fi-trash"></i></a>
        </div>
    </div>
    @endforeach
    <div class="row">
        <div class="small-12 columns text-center">
            {!! $lista->render() !!}
        </div>
    </div>
@stop