@extends('admin')

@section('content')
    <div class="row">
        <div class="small-12 columns">
            <a href="{{ url('administrador/eventos/crear') }}" class="button alert">Evento nuevo <i class="fi-plus"></i></a>
        </div>
    </div>

    <form id="form_usuario" name="form_usuario" action="{{ url('administrador/eventos') }}" method="get">
        <div class="row">
            <div class="medium-4 small-12 columns">
                <input type="text" name="filtro" id="login" placeholder="Buscar" value="{{ $filtro }}" />
                <input type="submit" value="Buscar" class="button default" />
            </div>
        </div>
    </form>

    <div class="row titulo lista">
        <div class="small-12 columns">Lista de eventos</div>
    </div>
    <div class="row item lista head">    
        <div class="medium-2 columns">Nombre</div>
        <div class="medium-6 columns">Descripci&oacute;n</div>
        <div class="medium-2 columns">Fecha</div>
        <div class="medium-1 columns">Lugar</div>
        <div class="medium-1 columns">Acciones</div>
    </div>
    @foreach($lista as $evento)
    <div class="row item lista">
        <div class="medium-2 columns">{{ $evento->id }}. {{ $evento->evento }}</div>
        <div class="medium-6 columns">{{ $evento->descripcion }}</div>
        <div class="medium-2 columns">{{ $evento->fecha }}</div>
        <div class="medium-1 columns">{{ $evento->lugar }}</div>
        <div class="medium-1 columns">
            @if(sizeof( $evento->registros ) )
                <a class="tooltip-x" title='Personas registradas' href="{{ url('administrador/eventos/registro/'.$evento->id) }}"><i class="fi-list"></i></a>
            @endif
            <a class="tooltip-x" title='Editar evento' href="{{ url('administrador/eventos/editar/'.$evento->id) }}"><i class="fi-pencil"></i></a>
            
            <a rel="borrar-evento" class="tooltip-x" title='Borrar evento' href="{{ url('administrador/eventos/eliminar/'.$evento->id) }}" onclick="return confirm('Desea eliminar este item?')"><i class="fi-trash"></i></a>
        </div>
    </div>
    @endforeach
    <div class="row">
        <div class="small-12 columns text-center">
            {!! $lista->render() !!}
        </div>
    </div>
@stop