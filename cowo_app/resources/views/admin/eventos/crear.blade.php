@extends('admin')

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Crear Evento</span></h3>
    </div>
</div>
<form id="form_cotizacion" name="form_cotizacion" action="{{ url('administrador/eventos/guardar') }}" method="post" enctype="multipart/form-data">
    
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="small-12 columns">
            <label for="nombre">Evento</label>
        </div>
        <div class="small-12 columns">
           <input type="text" name="evento" id="evento"/>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="nombre">Descripci&oacute;n</label>
        </div>
        <div class="small-12 columns">
            <textarea name="descripcion" rows="3" id="descripcion"></textarea>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="correo">Fecha del evento</label>
        </div>
        <div class="small-12 columns">
            <input type="date" name="fecha" class="fecha"/>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="empresa">Lugar del evento</label>
        </div>
        <div class="small-12 columns">
            <input type="text" name="lugar" id="lugar"/>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="archivo">Adjuntar imagen</label>
        </div>
        <div class="small-12 columns">
            <input type="file" name="imagen" id="imagen" />
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <input type="submit" value="Enviar" class="button default" />
        </div>
    </div>
</form>
@stop