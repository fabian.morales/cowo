@extends('admin')

@section('js_header')
<script>
    (function (window, $){
        $(document).ready(function() {
            $("a[rel='borrar-paquete']").click(function(e) {
                if (!confirm('¿Realmente desea borrar este producto?')){
                    e.preventDefault();
                }
            });
            
            /*$("#btnLimpiar").click(function(e) {
                e.preventDefault();
                $("#fecha_inicio").val('');
                $("#fecha_fin").val('');
                $("#cliente").val('');
                $("#estado").val('');
                $("#id").val('');
            });
            
            $("#lnkFiltro").click(function(e) {
                e.preventDefault();
                $("#form_filtro").toggle("slow", function() {
                    if ($("#form_filtro").is(":visible")){
                        $("#lnkFiltro i").removeClass("fi-arrow-down").addClass("fi-arrow-up");
                    }
                    else{
                        $("#lnkFiltro i").removeClass("fi-arrow-up").addClass("fi-arrow-down");
                    }
                });
            });*/
        });
    })(window, jQuery);
</script>
@stop

@section('content')
<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('administrador/paquetes/crear') }}" class="button alert">Paquete nuevo <i class="fi-plus"></i></a>
    </div>
</div>

@include('admin.paquete.lista', array("paquetes" => $paquetes))
@stop