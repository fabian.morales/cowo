<div class="row titulo lista">
    <div class="small-12 columns">Lista de paquetes</div>
</div>
<div class="row item lista head">
    <div class="small-1 columns">N&uacute;m</div>
    <div class="small-1 columns">Tipo</div>
    <div class="small-3 columns">Nombre</div>
    <div class="small-2 columns">Valor</div>
    <div class="small-2 columns">Cr&eacute;ditos</div>
    <div class="small-1 columns">Orden</div>
    <div class="small-2 columns end">Editar</div>
</div>
@foreach($paquetes as $p)
<div class="row item lista">
    <div class="small-1 columns">{{ $p->id }}</div>
    <div class="small-1 columns">{{ $p->tipo == "T" ? "Publico" : "Privado" }}</div>
    <div class="small-3 columns">{{ $p->nombre }}</div>
    <div class="small-2 columns">{{ $p->valor }}</div>
    <div class="small-2 columns">{{ $p->creditos }}</div>
    <div class="small-1 columns">{{ $p->orden }}</div>
    <div class="small-2 columns end">
        <a class="tooltip-x" title='Editar paquete' href="{{ url('administrador/paquetes/editar/'.$p->id) }}"><i class="fi-pencil"></i></a>
        <a rel="borrar-paquete" class="tooltip-x" title='Borrar paquete' href="{{ url('administrador/paquetes/borrar/'.$p->id) }}"><i class="fi-trash"></i></a>
    </div>
</div>
@endforeach
<div class="row">
    <div class="small-12 columns text-center">
        {!! $paquetes->render() !!}
    </div>
</div>