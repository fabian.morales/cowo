@extends('admin')

@section('js_header')
<script src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>
<script>
    (function(window, $){
        $(document).ready(function() {
            tinymce.init({
                selector: "textarea#descripcion",
                height: 300,
                theme: 'modern',
                language: 'es',
                relative_urls: false,
                document_base_url: 'http://encarguelo.frontiersoft.info/',
                remove_script_host: false,
                extended_valid_elements: 'span,iframe[src|frameborder|style|scrolling|class|width|height|name|align]',
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor varsecciones",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste filemanager textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | forecolor backcolor | bold italic fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | visualblocks",
                image_advtab: true
            });
            
            $("#btnAgregarVar").click(function(e) {
                e.preventDefault();
                tinymce.activeEditor.execCommand('mceInsertContent', false, $("#variable").val());
            });
        });
    })(window, jQuery);

</script>
@stop

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Datos del paquete</span></h3>
    </div>
</div>
<form id="form_paquete" name="form_paquete" action="{{ url('administrador/paquetes/guardar') }}" method="post" enctype="multipart/form-data">
    <input type="hidden" id="id" name="id" value="{{ $paquete->id }}" />
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="tipo">Tipo</label>
        </div>
        <div class="medium-8 small-12 columns">
            <select name="tipo" id="tipo" value="{{ $paquete->tipo }}" />
                <option value="T" {{ $paquete->tipo == "T" ? "selected": ""}}> Publico </option>
                <option value="P" {{ $paquete->tipo == "P" ? "selected": ""}}> Privado </option>                
            </select>
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="nombre">Nombre</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="nombre" id="nombre" value="{{ $paquete->nombre }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="valor">Valor</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="valor" id="valor" value="{{ $paquete->valor }}" />
        </div>
    </div>
	
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="descripcion">A&ntilde;adir texto "desde" antes del precio</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="checkbox" name="desde" id="desde" rows="5" value="1" {{ $paquete->desde == 1 ? 'checked' : '' }}> S&iacute;
        </div>
    </div>
    
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="creditos">Créditos</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="creditos" id="creditos" value="{{ $paquete->creditos }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="credito_adicional">Valor Crédito adicional</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="credito_adicional" id="credito_adicional" value="{{ $paquete->credito_adicional }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="periodicidad">Periodicidad</label>
        </div>
        <div class="medium-8 small-12 columns">
            <select name="periodicidad" id="periodicidad">
                @foreach($periodos as $key => $p)
                <option value="{{ $key }}" @if($paquete->periodicidad == $key) selected @endif>{{ $p }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="vigencia">Vigencia</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="vigencia" id="vigencia" value="{{ $paquete->vigencia }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="peso">Orden</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="number" name="orden" id="orden" value="{{ $paquete->orden }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="creditos">Servicios</label>
        </div>
        <div class="medium-8 small-12 columns">
            <select id="id_servicio" name="id_servicio">
                <option value="">Sin servicio asociado</option>
                @foreach($servicios as $s)
                <option value="{{ $s->id }}" @if($s->id == $paquete->id_servicio) selected @endif>{{ $s->nombre }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="imagen">Imagen</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="file" name="imagen" id="imagen" rows="5">
        </div>
    </div>
	
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="descripcion_corta">Descripción corta</label>
        </div>
        <div class="medium-8 small-12 columns">
            <textarea name="descripcion_corta" id="descripcion_corta" rows="5">{{ $paquete->descripcion_corta }}</textarea>
        </div>
    </div>
    
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="descripcion">Beneficios</label>
        </div>
        <div class="medium-8 small-12 columns">
            <textarea name="descripcion" id="descripcion" rows="5">{{ $paquete->descripcion }}</textarea>
        </div>
    </div>
    
    <div class="row">
        <div class="small-12 columns">
            <a class="button gris" href="{{ url('/administrador/paquetes/') }}" />Cancelar</a>
            <input type="submit" value="Guardar" class="button default" />
        </div>
    </div>
</form>
@stop