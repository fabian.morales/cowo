@extends('admin')

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Datos de la galer&iacute;a</span></h3>
    </div>
</div>
<form id="form_galeria" name="form_galeria" action="{{ url('administrador/galerias/guardar') }}" method="post" enctype="multipart/form-data">
    <input type="hidden" id="id" name="id" value="{{ $galeria->id }}" />
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="small-12 columns">
            <label for="titulo">Nombre</label>
        </div>
        <div class="small-12 columns">
            <input type="text" name="titulo" id="titulo" value="{{ $galeria->titulo }}" />
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="llave">Llave</label>
        </div>
        <div class="small-12 columns">
            <input type="text" name="llave" id="llave" value="{{ $galeria->llave }}" />
        </div>
    </div>
    
    <div class="row">
        <div class="small-12 columns">
            <a class="button alert" href="{{ url('/administrador/galerias/') }}" />Cancelar</a>
            <input type="submit" value="Guardar" class="button default" />
        </div>
    </div>
</form>

@if($galeria->id)
<div class="row">
    <div class="small-12 columns">
        <a class='button alert' rel='agregar-foto'><i class='fi-plus'></i> Agregar foto</a>
    </div>
</div>

<div id='div_form_foto' class='featherlight relativo'>
    <form id="form_foto" name="form_foto" action="{{ url('administrador/fotos/guardar') }}" method="post" enctype="multipart/form-data">
        <input type="hidden" id="id_foto" name="id" value="" />
        <input type="hidden" id="id_galeria" name="id_galeria" value="{{ $galeria->id }}" />
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="row">
            <div class="small-12 columns">
                <label for="titulo">Nombre</label>
            </div>
            <div class="small-12 columns">
                <input type="text" name="titulo" id="titulo_foto" value="" />
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <label for="descripcion_foto">Descripci&oacute;n</label>
            </div>
            <div class="small-12 columns">
                <textarea id='descripcion_foto' name='descripcion'></textarea>
            </div>        
        </div>
        <div class="row">
            <div class="small-12 columns">
                <label for="meta_foto">Etiquetas</label>
            </div>
            <div class="small-12 columns">
                <textarea id='meta_foto' name='meta' rows='4'></textarea>
            </div>        
        </div>
        <div class="row">
            <div class="small-12 columns">
                <label for="meta">Archivo de imagen</label>
            </div>
            <div class="small-12 columns">
                <input type="file" id="imagen" name="imagen" />
            </div>        
        </div>


        <div class="row">
            <div class="small-12 columns">
                <input type="submit" value="Guardar" class="button default" />
            </div>
        </div>
    </form>
</div>

<div class="row">
@foreach($galeria->fotos as $f)
    <div class="medium-3 columns end">
        <div class='item-galeria relativo'>
            <img src="{{ url('storage/imagenes/galeria/'.$f->id_galeria.'/'.$f->id.'.jpg') }}" alt="{{ $f->titulo }}" title="{{ $f->titulo }}" />
            <div class='acciones'>
                <a rel='borrar-foto' href='{{ url('administrador/fotos/borrar/'.$f->id) }}'><i class='fi-trash'></i> Borrar</a>
                <a rel='editar-foto' href='{{ url('administrador/fotos/editar/'.$f->id) }}'><i class='fi-pencil'></i> Editar</a>
            </div>
        </div>
    </div>
@endforeach
</div>
@endif

@stop