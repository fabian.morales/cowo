@extends('admin')

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Datos del servicio</span></h3>
    </div>
</div>
<form id="form_servicio" name="form_servicio" action="{{ url('administrador/servicios/guardar') }}" method="post" enctype="multipart/form-data">
    <input type="hidden" id="id" name="id" value="{{ $servicio->id }}" />
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="nombre">Nombre</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="nombre" id="nombre" value="{{ $servicio->nombre }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="creditos">Créditos</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="creditos" id="creditos" value="{{ $servicio->creditos }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="periodicidad">Periodicidad</label>
        </div>
        <div class="medium-8 small-12 columns">
            <select name="periodicidad" id="periodicidad">
                @foreach($periodos as $key => $p)
                <option value="{{ $key }}" @if($servicio->periodicidad == $key) selected @endif>{{ $p }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="vigencia">Vigencia</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="vigencia" id="vigencia" value="{{ $servicio->vigencia }}" />
        </div>
    </div>
    
    <div class="row">
        <div class="small-12 columns">
            <a class="button gris" href="{{ url('/administrador/servicios/') }}" />Cancelar</a>
            <input type="submit" value="Guardar" class="button default" />
        </div>
    </div>
</form>
@stop