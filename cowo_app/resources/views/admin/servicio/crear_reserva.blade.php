@extends('admin')

@section('content')
<form id="form_usuario" name="form_usuario" action="{{ url('administrador/servicios/crear_reserva') }}" method="get">
    <div class="row">
        <div class="medium-4 small-12 columns">
            <input type="text" name="filtro" id="login" placeholder="Buscar" value="{{ $filtro }}" />
            <input type="submit" value="Buscar" class="button default" />
        </div>
    </div>
</form>

<div class="row titulo lista">
    <div class="small-12 columns">Nueva reserva</div>
</div>
<div class="row item lista head">    
    <div class="medium-3 columns">Nombre</div>
    <div class="medium-4 columns">Correo</div>
    <div class="medium-2 columns">Activo</div>
    <div class="medium-3 columns">Crear reserva</div>
</div>

@foreach($usuarios as $u)
    <div class="row item lista">
        <div class="medium-3 columns">{{ $u->nombre }}@if(sizeof($u->cliente)) {{ $u->cliente->apellido }}@endif</div>
        <div class="medium-4 columns">{{ $u->email }}</div>
        <div class="medium-2 columns">@if($u->activo == 'Y') Si @else No @endif</div>
        <div class="medium-3 columns">
            <a class="tooltip-x" title='Editar usuario' href="{{ url('administrador/usuarios/perfil/'.$u->id) }}"><i class="fi-pencil"></i></a>            
        </div>
    </div>
@endforeach

<div class="row">
    <div class="small-12 columns text-center">
        {!! $usuarios->render() !!}
    </div>
</div>
@stop