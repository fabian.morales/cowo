@extends('admin')

@section('css_header')
<link rel="stylesheet" href="{{ asset('js/fullcalendar/fullcalendar.min.css') }}" type="text/css" />
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" />
@stop

@section('js_header')
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/fullcalendar/fullcalendar.min.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script>
    (function (window, $){
        $(document).ready(function() {
            $("a[rel='cancelar-servicio']").click(function(e) {
                if (!confirm('¿Realmente desea cancelar esta reserva?')){
                    e.preventDefault();
                }
            });     
            
            $("#form_reserva").css("display", "none");
			$(".horas").css("display", "none");
			
            $("#cancelar").click(function(){
                $("#form_reserva").css("display", "none");
            })
            
            $(".editar").click(function(){
                fila  = $(this).parent().parent();
                id    = $(fila).find( $(".id") ).html();
                id_sala = $(fila).find( $(".sala") ).html();
                fecha = $(fila).find( $(".inicio") ).html().split(" ");
                hora  = fecha[1].split(":");
                
				$.post( '{{url("administrador/servicios/inforeserva")}}', {id : id, '_token' : $("input[name='_token']").val()}, function(data){
					info = $.parseJSON(data);
					if(info.servicio.periodicidad == 'H')
						$(".horas").css("display", "inline");
					else
						$(".horas").css("display", "none");
				})
			
                $("#id").val(id);
                $("#id_sala").val(parseInt(id_sala));
                $("#fecha").val(fecha[0]);
				$("#hora").val( parseInt( hora[0] ) );
                $("#minuto").val(hora[1]);
                
                $("#form_reserva").css("display", "block");            
            })
            
            var $dtOpc = {
                minDate: 0,
                dateFormat: 'yy-mm-dd',
                timeFormat: 'HH:mm:ss',
                dayNames: [ "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado" ],
                dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
                dayNamesShort: [ "Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb" ],
                monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
                monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "Mau", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ],
                currentText: "Hoy",
                closeText: "Aceptar",
                showButtonPanel: true,
                pickerPosition: "bottom-left"
            };
            
            $("#calendario").fullCalendar({
                events: {
                    url: '{{ url('administrador/servicios/get_reservas') }}',
                    refetchResourcesOnNavigate: true,
                    type: 'GET'
                },
                header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay,listWeek'
                },
                navLinks: true, // can click day/week names to navigate views
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                defaultDate: '{{ date('Y-m-d') }}'
            });
            
            $("#fecha").datepicker($dtOpc);
            
            $("#form_reserva").submit(function(e) {
                e.preventDefault();

                $("#loader").addClass("loading");
                
                $.ajax({
                    url: $(this).attr("action"),
                    method: 'post',
                    data: $(this).serialize(),
                    dataType: 'json'
                }).done(function(res) {
                    if (res.ok === 1){
                        alert('Reserva modificada exitosamente');
                        window.location.href = '{{ url('administrador/servicios/reservas') }}';
                    }
                    else{
                        alert(res.msg);
                    }
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    alert('Ocurrió un problema al realizar la reserva');
                })
                .always(function() {
                    $("#loader").removeClass("loading");
                });
            });
        
        });
    })(window, jQuery);
</script>
@stop

@section('content')
<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('administrador/servicios/crear_reserva') }}" class="button alert">Nueva Reserva<i class="fi-plus"></i></a>
    </div>
</div>
<div class="small-5 columns">
    <div class="row">
        <form id="form_reserva" name="form_reserva" action="{{ url('administrador/servicios/editar_reserva') }}" method="post" enctype="multipart/form-data">
            <div class="row titulo lista">
                <div class="small-12 columns">Editar reservas</div>
            </div>
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <input type="hidden" name="id" id="id">
            
            <div class="row">
                <div class="small-12 columns">
                    <label for="nombre">Seleccione la sala</label>
                </div>
                <div class="small-12 columns">
                    <select name='id_sala' id='id_sala'>
                        <option value='1'>Sala 1</option>
                        <option value='2'>Sala 2</option>
                    </select>
                </div>
            </div>
            
            <div class="row">
                <div class="small-12 columns">
                    <label for="nombre">Selecciona la fecha <span class="horas"> y hora de la reserva </span></label>
                </div>				
                <div class="small-4 columns">
                    <input id='fecha' type='text' name='fecha' readonly="readonly" />
                </div>
                <div class="small-3 columns horas">
                    <select id="hora" name="hora">
                        <option value="0">00</option>
                        <option value="1">01</option>
                        <option value="2">02</option>
                        <option value="3">03</option>
                        <option value="4">04</option>
                        <option value="5">05</option>
                        <option value="6">06</option>
                        <option value="7">07</option>
                        <option value="8">08</option>
                        <option value="9">09</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                    </select>
                </div>
                <div class="small-1 horas columns">:</div>
                <div class="small-3 horas columns">
                    <select id="minuto" name="minuto">
                        <option value="00">00</option>
                        <option value="05">05</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                        <option value="25">25</option>
                        <option value="30">30</option>
                        <option value="35">35</option>
                        <option value="40">40</option>
                        <option value="45">45</option>
                        <option value="50">50</option>
                        <option value="55">55</option>
                    </select>
                </div>
            </div>                    
            <div class="row">
                <div class="small-12 columns">
                    <input type="submit" value="Editar" class="button default" />
                    <a class='button' id="cancelar">Cancelar</a>
                </div>
            </div>
        </form>
    </div>
    <div class="row titulo lista">
        <div class="small-12 columns">Lista de reservas</div>
    </div>
    <table class="table table-hover table-striped table-condensed">
        <thead>
            <tr>
                <th>No.</th>
                <th>Cliente</th>
                <th>Fecha</th>
                <th>Sala</th>
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
        @foreach($reservas as $r)
            <tr>
                <td class="id">{{ $r->id }}</td>
                <td class="cliente">{{ $r->cliente->nombre }} {{ $r->cliente->apellido }}</div>
                <td class="inicio">{{ $r->fecha_inicio }}</div>
                <td class="sala">{{ $r->id_sala }}</div>
                <td>
                    <a  rel="editar-servicio" class="tooltip-x editar" title='Editar reserva'><i class="fi fi-pencil"></i></a>
                 </td><td>            
                    <a rel="cancelar-servicio" class="tooltip-x" title='Cancelar reserva' href="{{ url('administrador/servicios/cancelar/'.$r->id) }}"><i class="fi-trash"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>                
    </table>
</div>    
<div class="small-7 columns">
    <div class="col-md-7">
        <div id="calendario"></div>
    </div>
</div>
@stop