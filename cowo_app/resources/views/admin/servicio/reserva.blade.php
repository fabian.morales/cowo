@extends('master_admin')

@section('css_header')
<link rel="stylesheet" href="{{ asset('js/fullcalendar/fullcalendar.min.css') }}" type="text/css" />
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" />
@stop

@section('js_body')
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/fullcalendar/fullcalendar.min.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script>
(function (window, $) {
    
    $(document).ready(function(){
        var $dtOpc = {
            minDate: 0,
            dateFormat: 'yy-mm-dd',
            timeFormat: 'HH:mm:ss',
            dayNames: [ "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado" ],
            dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
            dayNamesShort: [ "Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb" ],
            monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
            monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "Mau", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ],
            currentText: "Hoy",
            closeText: "Aceptar",
            showButtonPanel: true,
            pickerPosition: "bottom-left"
        };
        
        $("#id_sala").change(function(e) {
            e.preventDefault();
            
            var $valor = $(this).val();
            
            if ($valor === ''){
                return;
            }
            
            $("#calendario").fullCalendar('destroy');
            
            $("#calendario").fullCalendar({
                events: {
                    url: '{{ url('/cliente/reservas') }}/' + $valor ,
                    refetchResourcesOnNavigate: true,
                    type: 'GET'
                },
                header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay,listWeek'
                },
                navLinks: true, // can click day/week names to navigate views
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                defaultDate: '{{ date('Y-m-d') }}'
            });
        });
        
        $("#id_sala").change();
        
        $("#fecha").datepicker($dtOpc);
        
        $("#form_reserva").submit(function(e) {
            e.preventDefault();

            $("#loader").addClass("loading");
            
            $.ajax({
                url: $(this).attr("action"),
                method: 'post',
                data: $(this).serialize(),
                dataType: 'json'
            }).done(function(res) {
                if (res.ok === 1){
                    alert('Reserva realizada exitosamente');
                    window.location.href = '{{ url('administrador/usuarios/perfil', ["id" => $usuario->id]) }}';
                }
                else{
                    alert(res.msg);
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                alert('Ocurrió un problema al realizar la reserva');
            })
            .always(function() {
                $("#loader").removeClass("loading");
            });
        });
    });
})(window, jQuery);
</script>
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h2 class="amarillo text-center">Reserva de salas</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <form id="form_reserva" name="form_reserva" action="{{ url('administrador/cliente/reservar') }}" method="post" class="form" enctype="multipart/form-data">
                <input type="hidden" id="id" name="id" value="{{ $usuario->id }}" />
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <input type='hidden' name='id_contratacion' rel='id_contratacion' value='{{ $con->id }}' />
                
                <p>Seleccione la sala. Tenemos dos salas disponibles</p>
                <select name='id_sala' id='id_sala'>
                    <option value='1'>Sala 1</option>
                    <option value='2'>Sala 2</option>
                </select>
                
                <p>Selecciona la fecha @if($con->servicio->periodicidad == 'H') y hora @endif de la reserva</p>
                <div class="row">
                    <div class="col-md-5">
                        <input id='fecha' type='text' name='fecha' readonly="readonly" />
                    </div>
                    
                    @if($con->servicio->periodicidad == 'H')
                    <div class="col-sm-3">
                        <select id="hora" name="hora">
                            <option value="0">00</option>
                            <option value="1">01</option>
                            <option value="2">02</option>
                            <option value="3">03</option>
                            <option value="4">04</option>
                            <option value="5">05</option>
                            <option value="6">06</option>
                            <option value="7">07</option>
                            <option value="8">08</option>
                            <option value="9">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                        </select>
                    </div>
                    <div class="col-sm-1 text-center">:</div>
                    <div class="col-sm-3">
                        <select id="minuto" name="minuto">
                            <option value="00">00</option>
                            <option value="05">05</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                            <option value="30">30</option>
                            <option value="35">35</option>
                            <option value="40">40</option>
                            <option value="45">45</option>
                            <option value="50">50</option>
                            <option value="55">55</option>
                        </select>
                    </div>
                    @endif
                </div>
                <div class="clearfix"></div>
                <br />
                <input type='submit' class='boton mini crema' value='Reservar' />
                <a class='boton mini crema' href="{{ url('administrador/usuarios/perfil', ['id' => $usuario->id]) }}">Regresar</a>
            </form>
        </div>
        <div class="col-md-8">
            <div id="calendario"></div>
        </div>
    </div>
</div>
@stop