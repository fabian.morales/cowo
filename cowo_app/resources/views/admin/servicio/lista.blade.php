<div class="row titulo lista">
    <div class="small-12 columns">Lista de servicios</div>
</div>
<div class="row item lista head">
    <div class="small-1 columns">N&uacute;m</div>
    <div class="small-5 columns">Nombre</div>
    <div class="small-2 columns">Créditos</div>
    <div class="small-2 columns end">Editar</div>
</div>
@foreach($servicios as $s)
<div class="row item lista">
    <div class="small-1 columns">{{ $s->id }}</div>
    <div class="small-5 columns">{{ $s->nombre }}</div>
    <div class="small-2 columns">{{ $s->creditos }}</div>
    <div class="small-2 columns end">
        <a class="tooltip-x" title='Editar servicio' href="{{ url('administrador/servicios/editar/'.$s->id) }}"><i class="fi-pencil"></i></a>
        <a rel="borrar-servicio" class="tooltip-x" title='Borrar servicio' href="{{ url('administrador/servicios/borrar/'.$s->id) }}"><i class="fi-trash"></i></a>
    </div>
</div>
@endforeach
<div class="row">
    <div class="small-12 columns text-center">
        {!! $servicios->render() !!}
    </div>
</div>