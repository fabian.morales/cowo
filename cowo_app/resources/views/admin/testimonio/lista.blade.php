<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('administrador/testimonios/crear') }}" class="button alert">Testimonio nuevo <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">Lista de testimonios</div>
</div>
<div class="row item lista head">
    <div class="small-2 columns">N&uacute;m</div>
    <div class="small-7 columns">Nombre</div>
    <div class="small-3 columns">Editar</div>
</div>
@foreach($testimonios as $t)
<div class="row item lista">
    <div class="small-2 columns">{{ $t->id }}</div>
    <div class="small-7 columns">{{ $t->titulo }}</div>
    <div class="small-3 columns"><a class="tooltip-x" title='Editar testimonio' href="{{ url('administrador/testimonios/editar/'.$t->id) }}"><i class="fi-pencil"></i></a></div>
</div>
@endforeach
<div class="row">
    <div class="small-12 columns text-center">
        {!! $testimonios->render() !!}
    </div>
</div>