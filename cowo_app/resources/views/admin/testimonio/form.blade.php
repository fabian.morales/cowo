@extends('admin')

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Datos del testimonio</span></h3>
    </div>
</div>
<form id="form_test" name="form_test" action="{{ url('administrador/testimonios/guardar') }}" method="post" enctype="multipart/form-data">
    <input type="hidden" id="id" name="id" value="{{ $testimonio->id }}" />
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="small-12 columns">
            <label for="titulo">Nombre</label>
        </div>
        <div class="small-12 columns">
            <input type="text" name="titulo" id="titulo" value="{{ $testimonio->titulo }}" />
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="nombre">Contenido</label>
        </div>
        <div class="small-12 columns">
            <textarea id='descripcion' name='descripcion'>{{ $testimonio->descripcion }}</textarea>
        </div>        
    </div>
    <div class="small-12 columns">
        <label for="activo">
            Activo
            <input type='checkbox' name='activo' id='activo' value='S' @if($testimonio->activo == 'S') checked="checked" @endif />
        </label>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="imagen">Imagen</label>
        </div>
        <div class="small-12 columns end">
            <input type="file" name="imagen" id="imagen" />
        </div>
    </div>
    
    <div class="row">
        <div class="small-12 columns">
            <a class="button alert" href="{{ url('/administrador/testimonios/') }}" />Cancelar</a>
            <input type="submit" value="Guardar" class="button default" />
        </div>
    </div>
</form>
@stop