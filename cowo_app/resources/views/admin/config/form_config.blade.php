@extends('admin')

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Editar datos de configuraci&oacute;n</span></h3>
    </div>
</div>
<form id="form_config" name="form_config" action="{{ url('administrador/configuracion/guardar') }}" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    
    @foreach($config as $c)
    <div class="row">
        <div class="small-3 columns">
            <label>{{ $c->titulo }}</label>
        </div>
        <div class="small-9 columns">
            <input type='text' name='cfg[{{ $c->id }}]' id='cfg_{{ $c->id }}' value='{{ $c->contenido }}' />
        </div>
    </div>
    @endforeach
    
    <div class="row">
        <div class="small-12 columns">
            <input type="submit" value="Guardar" class="button default" />
        </div>
    </div>
</form>
@stop

