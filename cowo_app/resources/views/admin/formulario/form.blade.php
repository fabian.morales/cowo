@extends('admin')

@section('css_header')
<style>
    .ui-state-highlight {
        height: 1.5em; 
        line-height: 1.2em; 
        width: 100%; 
        margin-left: -0.9375rem;
        margin-right: -0.9375rem;
        background-color: #99ff99;
        border: 1px dashed #009966;
    }
</style>
@stop

@section('js_header')
<script>
    
(function($, window){
    $(document).ready(function() {
        
        function hookListaCampos(){
            $("#lnkAdCampo, a[rel='editar-campo']").click(function(e) {
                e.preventDefault();
                $.featherlight($(this).attr('href'), {
                    afterOpen: function(){
                        hookFormCampo();
                        $("#tipo").change();
                    }
                });
            });
            
            $("a[rel='borrar-campo']").click(function(e) {
                e.preventDefault();
                if(!confirm('¿Está seguro de borrar este campo?')){
                    return;
                }
                
                $.ajax({
                    url: $(this).attr("href"),
                    method: 'get',
                    cache: false,
                    contentType: false,
                    processData: false
                })
                .done(function(res) {
                    $("#campos_form").html(res);
                    
                    hookFormCampo();
                    $("#tipo").change();
                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    alert(jqXHR.responseJSON.error.message);
                });
            });
        }
        
        function hookFormCampo() {
            
            $("#tipo").change(function(e) {
                e.preventDefault();
                
                $(".parametro").each(function(i, o) {
                    $(o).addClass("hide");
                });

                if (($(this).val() === 'lista-desplegable') || ($(this).val() === 'lista-checks') || ($(this).val() === 'lista-radios')){
                    $("#div-lista-opciones").removeClass("hide");
                }

                if ($(this).val() === 'mapa'){
                    $("#div-puntos-mapa").removeClass("hide");
                }

                if ($(this).val() === 'imagen'){
                    $("#div-imagen-campo").removeClass("hide");
                }

                if ($(this).val() === 'texto-p'){
                    $("#div-texto-p").removeClass("hide");
                }
            });
            
            $("#adicion-lista").click(function(e) {
                e.preventDefault();
                
                if ($("#opcion-lista").val() === ''){
                    alert('Debe ingresar el valor');
                    return;
                }
                
                $("#opciones-lista").append('<option value="' + $("#opcion-lista").val() + '">' + $("#opcion-lista").val() + '</option>');
                $("#opcion-lista").val('');
                $("#opcion-lista").focus();
            });
            
            $("#quitar-lista").click(function(e) {
                e.preventDefault();
                $("#opciones-lista option:selected").remove();
            });
            
            $("#adicion-punto").click(function(e) {
                e.preventDefault();
                
                if ($("#punto-mapa").val() === ''){
                    alert('Debe ingresar el valor');
                    return;
                }
                
                $("#puntos-mapa").append('<option value="' + $("#punto-mapa").val() + '">' + $("#punto-mapa").val() + '</option>');
                $("#punto-mapa").val('');
                $("#punto-mapa").focus();
            });
            
            $("#quitar-punto").click(function(e) {
                e.preventDefault();
                $("#puntos-mapa option:selected").remove();
            });
            
            $("#lnkCampoCerrar").click(function(e) {
                e.preventDefault();
                $.featherlight.close();
            });
            
            $("#form_campo").submit(function(e) {
                e.preventDefault();
                
                $("#loader").addClass("loading");
                
                $("#opciones-lista > option, #puntos-mapa > option").each(function(i, o) {
                    $(o).attr('selected', true);
                });
                
                var $form = new FormData($('#form_campo')[0]);
                $.ajax({
                    url: $("#form_campo").attr("action"),
                    method: 'post',
                    data: $form,
                    cache: false,
                    contentType: false,
                    processData: false
                })
                .done(function(res) {
                    $("#loader").removeClass("loading");
                    $("#campos_form").html(res);
                    
                    hookListaCampos();
                    
                    $.featherlight.close();
                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    $("#loader").removeClass("loading");
                    alert(jqXHR.responseJSON.error.message);
                });
                
            });
        }
        
        $("#campos_form").sortable({
            placeholder: "ui-state-highlight"
        });
        $("#campos_form").disableSelection();
        
        $("#btnGuardarOrden").click(function(e) {
            e.preventDefault();
            var sortedIDs = $( "#campos_form" ).sortable( "toArray" );
            $.ajax({
                url: "{{ url('administrador/formularios/organizar') }}",
                method: "post",
                data: {
                    'id_form': $("#id").val(),
                    'campos[]': sortedIDs,
                    '_token': '{!! csrf_token() !!}'
                }
            })
            .done(function(res) {
                alert('Se ha guardado el orden de los campos');
            })
            .fail(function (jqXHR, textStatus, errorThrown) {

                alert(jqXHR.responseJSON.error.message);
            });
        });
        
        hookListaCampos();
    });
})(jQuery, window);
</script>
@stop

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Datos del formulario</span></h3>
    </div>
</div>
<form id="form_form" name="form_form" action="{{ url('administrador/formularios/guardar') }}" method="post" enctype="multipart/form-data">
    <input type="hidden" id="id" name="id" value="{{ $form->id }}" />
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="small-12 columns">
            <label for="titulo">Nombre</label>
        </div>
        <div class="small-12 columns">
            <input type="text" name="titulo" id="titulo" value="{{ $form->titulo }}" />
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="llave">Llave</label>
        </div>
        <div class="small-12 columns">
            <input type="text" name="llave" id="llave" value="{{ $form->llave }}" readonly="readonly" />
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="nombre">Descripci&oacute;n</label>
        </div>
        <div class="small-12 columns">
            <textarea id='descripcion' name='descripcion'>{{ $form->descripcion }}</textarea>
        </div>        
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="destinatarios">Destinatarios</label>
        </div>
        <div class="small-12 columns">
            <input type="text" name="destinatarios" id="destinatarios" value="{{ $form->destinatarios }}" />
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="clase">Clase CSS</label>
        </div>
        <div class="small-12 columns">
            <input type="text" name="clase" id="clase" value="{{ $form->clase }}" />
        </div>
    </div>
    
    <div class="row">
        <div class="small-12 columns">
            <label for="col_nombre">Columnas para el campo nombre</label>
        </div>
        <div class="small-12 columns">
            <select id="col_nombre" name="col_nombre">
            @for($i=1; $i<=4; $i++)
                <option value="{{ $i }}" @if($form->col_nombre == $i) selected @endif>{{ $i }}</option>
            @endfor
            </select>
        </div>
    </div>
    
    <div class="row">
        <div class="small-12 columns">
            <label for="col_correo">Columnas para el campo correo</label>
        </div>
        <div class="small-12 columns">
            <select id="col_email" name="col_email">
            @for($i=1; $i<=4; $i++)
                <option value="{{ $i }}" @if($form->col_email == $i) selected @endif>{{ $i }}</option>
            @endfor
            </select>
        </div>
    </div>
    
    <div class="row">
        <div class="small-12 columns">            
            <input type="submit" value="Guardar" class="button default" />
            <input type="submit" name='guardar_permancer' value="Guardar y permanecer" class="button default warning" />
            <a class="button" href="{{ url('/administrador/formularios/visualizar/'.$form->id) }}" target='_blnak'>Visualizar</a>
            <a class="button alert" href="{{ url('/administrador/formularios/') }}" />Cancelar</a>
        </div>
    </div>
</form>

<div class="row">
    <div class="small-12 columns">
        <h4 class="titulo seccion"><span>Campos del formulario</span></h4>
    </div>
</div>
@if($form->id > 0)
<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('administrador/formularios/campos/crear/'.$form->id) }}" class="button alert small" id="lnkAdCampo">Campo nuevo <i class="fi-plus"></i></a>
        <button id='btnGuardarOrden' class='button default small'>Guardar orden <i class='fi-save'></i></button>
    </div>
</div>
<div class="row item lista head">
    <div class="small-2 columns">N&uacute;m</div>
    <div class="small-3 columns">Nombre</div>
    <div class="small-2 columns">Columnas</div>
    <div class="small-3 columns">Tipo</div>
    <div class="small-2 columns">Acciones</div>
</div>
<div id="campos_form">
@include('admin.formulario.lista_campos', array("form" => $form))
</div>
@else
<h4 class="text-center">Para agregar campos, primero debe guardar el formulario</h4>
@endif

@stop