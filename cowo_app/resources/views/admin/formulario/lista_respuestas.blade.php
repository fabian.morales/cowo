<div class="row titulo lista">
    <div class="small-12 columns">Lista de respuestas</div>
</div>
<div class="row item lista head">
    <div class="small-1 columns">N&uacute;m</div>
    <div class="small-2 columns">Formulario</div>
    <div class="small-2 columns">Fecha</div>
    <div class="small-3 columns">Cliente</div>
    <div class="small-3 columns">Correo</div>
    <div class="small-1 columns">Ver</div>
</div>
@forelse($respuestas as $r)
<div class="row item lista">
    <div class="small-1 columns">{{ $r->id }}</div>
    <div class="small-2 columns">{{ $r->formulario->titulo }}</div>
    <div class="small-2 columns">{{ $r->fecha }}</div>
    <div class="small-3 columns">{{ $r->nombre_cliente }}</div>
    <div class="small-3 columns">{{ $r->correo_cliente }}</div>
    <div class="small-1 columns">
        <a class="tooltip-x" title='Ver datos' href="{{ url('administrador/formularios/respuestas/detalle/'.$r->id) }}" data-featherlight><i class="fi-list"></i></a>
    </div>
</div>
@empty
<h4 class='text-center'>No hay respuestas para este formulario</h4>
@endforelse
<div class="row">
    <div class="small-12 columns text-center">
        {!! $respuestas->render() !!}
    </div>
</div>