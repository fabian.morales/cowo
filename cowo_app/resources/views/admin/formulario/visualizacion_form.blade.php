@extends('admin')

@section('js_header')
<script>
    
(function($, window){
    $(document).ready(function() {
        var $datePickerOpc = {
            dateFormat: "yy-mm-dd",
            dayNames: [ "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado" ],
            dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
            dayNamesShort: [ "Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb" ],
            monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
            monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ],
            currentText: "Hoy",
            closeText: "Aceptar",
            showButtonPanel: true
        };
        
        $("input[rel='date-picker']").datepicker($datePickerOpc);
    });
})(jQuery, window);
</script>
@stop

@section('content')

@include('contenido.formulario', array("form" => $form))

@stop