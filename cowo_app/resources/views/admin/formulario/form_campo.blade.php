<form id="form_campo" name="form_campo" action="{{ url('administrador/formularios/campos/guardar') }}" method="post" enctype="multipart/form-data" style="min-width: 600px">
    <input type="hidden" id="id" name="id" value="{{ $campo->id }}" />
    <input type="hidden" id="id_form" name="id_form" value="{{ $form->id }}" />
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    
    <div class="row">
        <div class="large-6 columns">
            <h5>Datos</h5>
            <div class="row">
                <div class="small-12 columns">
                    <label for="nombre">Nombre</label>
                </div>
                <div class="small-12 columns">
                    <input type="text" name="nombre" id="nombre" value="{{ $campo->nombre }}" />
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <label for="nombre">Llave</label>
                </div>
                <div class="small-12 columns">
                    <input type="text" id='llave' name='llave' value="{{ $campo->llave }}" readonly="readonly" />
                </div>        
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <label for="tipo">Tipo</label>
                </div>
                <div class="small-12 columns">
                    <select id="tipo" name="tipo">
                    @foreach($tipos as $k => $t)
                        <option value="{{ $k }}" @if($campo->tipo == $k) selected @endif>{{ $t }}</option>
                    @endforeach
                    </select>
                </div>        
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <label for="obligatorio">Obligatorio</label>
                </div>
                <div class="small-12 columns">
                    <input type="checkbox" name="obligatorio" id="obligatorio" value="S" @if($campo->obligatorio == 'S') checked="checked" @endif />
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <label for="orden">Orden</label>
                </div>
                <div class="small-12 columns">
                    <input type="text" name="orden" id="orden" value="{{ $campo->orden }}" />
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <label for="columnas">&iquest;Cu&aacute;ntas columnas debe ocupar?</label>
                </div>
                <div class="small-12 columns">
                    <select id="columnas" name="columnas">
                    @for($i=1; $i<=4; $i++)
                        <option value="{{ $i }}" @if($campo->columnas == $i) selected @endif>{{ $i }}</option>
                    @endfor
                    </select>
                </div>        
            </div>
        </div>
        <div class="large-6 columns">
            {{--*/ $parametros = $campo->obtenerParametros() /*--}}
            
            <h5>Par&aacute;metros</h5>
            <div id="div-lista-opciones" class="parametro hide">
                <p>A&ntilde;ada las opciones para la lista</p>
                <input type="text" id="opcion-lista" />
                <input type="button" id="adicion-lista" class="small button" value="A&ntilde;adir" />
                <select id="opciones-lista" name="opciones-lista[]" size="5" style="height: 200px;" multiple>
                    @if(sizeof($parametros) && isset($parametros->lista) && sizeof($parametros->lista))

                    @foreach($parametros->lista as $l)
                    <option value="{{ $l }}">{{ $l }}</option>
                    @endforeach

                    @endif
                </select>
                <input type="button" id="quitar-lista" class="small button alert" value="Quitar" />
            </div>

            <div id="div-puntos-mapa" class="parametro hide">
                <p>A&ntilde;ada las coordenadas para el mapa. Ingrese la latitud y la longitud separadas por comas. Use el punto (.) como s&iacute;mbolo de decimales.</p>
                <input type="text" id="punto-mapa" />
                <input type="button" id="adicion-punto" class="small button" value="A&ntilde;adir" />
                <select id="puntos-mapa" name="puntos-mapa[]" size="5" style="height: 200px;" multiple>
                    @if(sizeof($parametros) && isset($parametros->puntos) && sizeof($parametros->puntos))

                    @foreach($parametros->puntos as $p)
                    <option value="{{ $p }}">{{ $p }}</option>
                    @endforeach

                    @endif
                </select>
                <input type="button" id="quitar-punto" class="small button alert" value="Quitar" />
            </div>
                
            <div id="div-imagen-campo" class="parametro hide">
                <p>Seleccione la imagen</p>
                <input type="file" id="imagen-campo" name="imagen-campo" />
            </div>
                
            <div id="div-texto-p" class="parametro hide">
                <p>Escriba el texto personalizado</p>
                <textarea id="texto-personalizado" name="texto-personalizado" row="6">@if(sizeof($parametros) && isset($parametros->texto)) {{ $parametros->texto }} @endif</textarea>
            </div>

        </div>
    </div>
    
    <div class="row">
        <div class="small-12 columns text-right">
            <a class="button alert" href="{{ url('/administrador/formularios/editar/'.$form->id) }}" id="lnkCampoCerrar">Cancelar</a>
            <input type="submit" value="Guardar" class="button default" />
        </div>
    </div>
</form>