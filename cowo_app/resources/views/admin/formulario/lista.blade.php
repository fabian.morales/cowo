<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('administrador/formularios/crear') }}" class="button alert">Formulario nuevo <i class="fi-plus"></i></a>
        <a href="{{ url('administrador/formularios/respuestas') }}" class="button warning">Ver respuestas <i class="fi-list"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">Lista de formularios</div>
</div>
<div class="row item lista head">
    <div class="small-1 columns">N&uacute;m</div>
    <div class="small-3 columns">Nombre</div>
    <div class="small-3 columns">Destinatarios</div>
    <div class="small-3 columns">Token</div>
    <div class="small-2 columns">Acciones</div>
</div>
@foreach($forms as $f)
<div class="row item lista">
    <div class="small-1 columns">{{ $f->id }}</div>
    <div class="small-3 columns">{{ $f->titulo }}</div>
    <div class="small-3 columns">{{ $f->destinatarios }}</div>
    <div class="small-3 columns">{{ str_replace('\\', '', '\\{!\\! \\$\\formulario|'.$f->llave.' \\!\\!}') }}</div>
    <div class="small-2 columns">
        <a class="tooltip-x" title='Editar formulario' href="{{ url('administrador/formularios/editar/'.$f->id) }}"><i class="fi-pencil"></i></a>
        <a class="tooltip-x" title='Visualizar formulario' href="{{ url('administrador/formularios/visualizar/'.$f->id) }}" target="_blank"><i class="fi-web"></i></a>
        <a class="tooltip-x" title='Ver respuestas' href="{{ url('administrador/formularios/respuestas/'.$f->id) }}"><i class="fi-list"></i></a>
        <a class="tooltip-x" title='Borrar formulario' href="{{ url('administrador/formularios/borrar/'.$f->id) }}"><i class="fi-trash"></i></a>
    </div>
</div>
@endforeach
<div class="row">
    <div class="small-12 columns text-center">
        {!! $forms->render() !!}
    </div>
</div>