<h3>{{ $respuesta->formulario->titulo }}</h3>
<p>{{ $respuesta->formulario->descripcion }}</p>
<ul>
    @foreach($respuesta->valores as $valor)
    <li><strong>{{ $valor->campo->nombre }}: </strong>{{ $valor->valor }}</li>
    @endforeach
</ul>