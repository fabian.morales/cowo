@foreach($form->campos as $c)
<div class="row item lista ui-state-default" style='cursor: move;' id='campo-{{ $c->id }}'>
    <div class="small-2 columns">{{ $c->id }}</div>
    <div class="small-3 columns">{{ $c->nombre }}</div>
    <div class="small-2 columns">{{ $c->columnas }}</div>
    <div class="small-3 columns">{{ $tipos[$c->tipo] }}</div>
    <div class="small-2 columns">
        <a class="tooltip-x" title='Editar campo' href="{{ url('administrador/formularios/campos/editar/'.$c->id) }}" rel='editar-campo'><i class="fi-pencil"></i></a>
        <a class="tooltip-x" title='Borrar campo' href="{{ url('administrador/formularios/campos/borrar/'.$c->id) }}" rel='borrar-campo'><i class="fi-trash"></i></a>
    </div>
</div>
@endforeach
