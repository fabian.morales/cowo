@extends('admin')

@section('content')
<a href='{{ url("/administrador/formularios") }}' class='button alert'>&lt; Regresar</a>
@include('admin.formulario.lista_respuestas', array("respuestas" => $respuestas))
@stop