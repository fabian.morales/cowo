@extends('admin')

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Datos del aliado</span></h3>
    </div>
</div>
<form id="form_tienda" name="form_tienda" action="{{ url('administrador/aliados/guardar') }}" method="post" enctype="multipart/form-data">
    <input type="hidden" id="id" name="id" value="{{ $aliado->id }}" />
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="small-12 columns">
            <label for="titulo">Nombre</label>
        </div>
        <div class="small-12 columns">
            <input type="text" name="titulo" id="titulo" value="{{ $aliado->titulo }}" />
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="nombre">Contenido</label>
        </div>
        <div class="small-12 columns">
            <textarea id='descripcion' name='descripcion'>{{ $aliado->descripcion }}</textarea>
        </div>        
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="url">URL</label>
        </div>
        <div class="small-12 columns">
            <input type="text" name="url" id="url" value="{{ $aliado->url }}" />
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="imagen">Imagen</label>
        </div>
        <div class="small-12 columns end">
            <input type="file" name="imagen" id="imagen" />
        </div>
    </div>
    <div class="small-12 columns">
        <label for="activo">
            Activo
            <input type='checkbox' name='activo' id='activo' value='S' @if($aliado->activo == 'S') checked="checked" @endif />
        </label>
    </div>
    
    <div class="row">
        <div class="small-12 columns">
            <a class="button alert" href="{{ url('/administrador/aliados/') }}" />Cancelar</a>
            <input type="submit" value="Guardar" class="button default" />
        </div>
    </div>
</form>
@stop