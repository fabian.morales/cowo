<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('administrador/aliados/crear') }}" class="button alert">Aliado nuevo <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">Lista de aliados</div>
</div>
<div class="row item lista head">
    <div class="small-2 columns">N&uacute;m</div>
    <div class="small-7 columns">Nombre</div>
    <div class="small-3 columns">Editar</div>
</div>
@foreach($aliados as $a)
<div class="row item lista">
    <div class="small-2 columns">{{ $a->id }}</div>
    <div class="small-7 columns">{{ $a->titulo }}</div>
    <div class="small-3 columns">
        <a class="tooltip-x" title='Editar aliado' href="{{ url('administrador/aliados/editar/'.$a->id) }}"><i class="fi-pencil"></i></a>
        <a class="tooltip-x" title='Borrar aliado' href="{{ url('administrador/aliados/borrar/'.$a->id) }}"><i class="fi-trash"></i></a>
    </div>
</div>
@endforeach
<div class="row">
    <div class="small-12 columns text-center">
        {!! $aliados->render() !!}
    </div>
</div>