@extends('admin')

@section('content')
<fieldset>
    <legend><b>Informaci&oacute;n de Cotizaci&oacute;n</b></legend>

    <div class="row item">    
        <div class="medium-3 columns">Cliente</div>
        <div class="medium-3 columns">Comercial</div>
        <div class="medium-2 columns">Fecha</div>
        <div class="medium-1 columns">Estado</div>
    </div>
    <div class="row item">
    
        <div class="medium-3 columns">{{ $cotizacion->cliente !== NULL ? $cotizacion->cliente->nombre .' '. $cotizacion->cliente->apellido : $cotizacion->correo }}</div>
        <div class="medium-3 columns">{{ $cotizacion->usuario->nombre }}</div>
        <div class="medium-2 columns">{{ $cotizacion->fecha_creacion }}</div>
        <div class="medium-1 columns">{{ \App\Helpers\Helper::estados()[$cotizacion->estado]}}</div>                            
    </div>
        
</fieldset>
<hr></hr>
<fieldset>
    <legend><b>Servicios cotizados</b></legend>
    
    <div class="row item">
        <div class="medium-2 small-6 columns"><strong>Plan</strong></div>
        <div class="medium-1 small-3 columns"><strong>Valor</strong></div>
        <div class="medium-1 small-3 columns"><strong>Descuento</strong></div>
        <div class="medium-1 small-3 columns"><strong>Horas</strong></div>
        <div class="medium-1 small-3 columns"><strong>Cantidad</strong></div>
        <div class="medium-1 small-3 columns"><strong>Meses</strong></div>
        <div class="medium-1 small-3 columns"><strong>Subtotal</strong></div>              
        <div class="medium-1 small-3 columns"><strong>IVA</strong></div>              
        <div class="medium-1 small-3 columns"><strong>Total a Pagar</strong></div>              
        <div class="medium-2 small-3 columns"><strong>Vencimiento</strong></div>              
    </div>
    
    @foreach($cotizacion->descuentos as $descuento)
        <div class="row item">
            <div class="medium-2 small-6 columns">{{ $descuento->paquete->nombre }}</div>
            <div class="medium-1 small-3 columns">{{ Helper::number_format($descuento->precio) }}</div>
            <div class="medium-1 small-3 columns">{{ Helper::number_format($descuento->valor) }}%</div>
            <div class="medium-1 small-3 columns">{{ $descuento->horas }}</div>
            <div class="medium-1 small-3 columns">{{ $descuento->cantidad }}</div>
            <div class="medium-1 small-3 columns">{{ $descuento->meses }}</div>
            <div class="medium-1 small-3 columns">{{ 
                Helper::number_format( 
                    $descuento->precio * $descuento->cantidad * $descuento->meses * ($descuento->horas != 0 ? $descuento->horas : 1), 
                        $descuento->fecha_vencimiento >= date('Y-m-d') ? $descuento->valor : 0
                ) }}
            </div>
            <div class="medium-1 small-3 columns">{{ 
                Helper::number_format( 
                    Helper::calcularIva(
                        $descuento->precio * $descuento->cantidad * $descuento->meses * ($descuento->horas != 0 ? $descuento->horas : 1), 
                        $descuento->fecha_vencimiento >= date('Y-m-d') ? $descuento->valor : 0 ) 
                ) }}
            </div>
            <div class="medium-1 small-3 columns">{{ 
                Helper::number_format( 
                    Helper::calcularConIva(
                        $descuento->precio * $descuento->cantidad * $descuento->meses * ($descuento->horas != 0 ? $descuento->horas : 1), 
                        $descuento->fecha_vencimiento >= date('Y-m-d') ? $descuento->valor : 0 ) 
                ) }}
            </div>
            <div class="medium-2 small-3 columns">{{ $descuento->fecha_vencimiento }}</div>
        </div>
    @endforeach
</fieldset>

<hr></hr>

<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('administrador/cotizaciones/estados', ['id' => $cotizacion->id ]) }}" class="button alert">Cambiar estado <i class="fi-pencil"></i></a>
    </div>
</div>

@stop