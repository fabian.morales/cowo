@extends('admin')

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Envío de cotización</span></h3>
    </div>
</div>
<form id="form_cotizacion" name="form_cotizacion" action="{{ url('administrador/cotizaciones/enviar') }}" method="post" enctype="multipart/form-data">
    
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="small-12 columns">
            <label for="nombre">Nombre del comercial</label>
        </div>
        <div class="small-12 columns">
            <select name="comercial" id="comercial">
                <option value="">-Seleccione-</option>
                @foreach($comerciales as $comercial)
                    <option value="{{$comercial->id}}">{{$comercial->nombre}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="nombre">Nombre del cliente</label>
        </div>
        <div class="small-12 columns">
            <input type="text" name="nombre" id="nombre" value="" />
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="correo">Correo del cliente</label>
        </div>
        <div class="small-12 columns">
            <input type="text" name="correo" id="correo" value="" />
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="empresa">Empresa del cliente</label>
        </div>
        <div class="small-12 columns">
            <input type="text" name="empresa" id="empresa" value="" />
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="titulo">Ciudad del cliente</label>
        </div>
        <div class="small-12 columns">
            <input type="text" name="ciudad" id="ciudad" value="" />
        </div>
    </div>
    <table class="planes table table-striped table-condensed">
        <thead>
            <tr>    
                <th>Plan</th>
                <th>Valor</th>
                <th>Dcto(%)</th>
                <th>Horas</th>
                <th>Cantidad</th>
                <th>Periodos</th>
                <th>Subtotal</th>
                <th>IVA</th>
                <th>Valor Total</th>
                <th>Vence</th>
                <th>Acci&oacute;n</th>
            </tr>
        <thead>
        <tbody class="lista body">
            <tr rel="tr_planes">
                <td>
                    <select name="id_plan[]" class="plan" onchange="buscarPlan(this)">
                        <option value="">-Seleccione-</option>
                        @foreach($planes as $p)
                            <option value="{{ $p->id }}">{{ $p->nombre }}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <input type="number" name="valor[]" class="valor"/>
                </td>
                <td>
                    <input type="number" min="0" max="100" name="descuento[]" class="descuento" />
                </td>
                <td>
                    <input type="number" min="0" max="100" name="horas[]" class="horas" />
                </td>
                <td>
                    <input type="number" min="1" max="100" name="cantidad[]" class="cantidad" />
                </td>
                <td>
                    <input type="number" min="1" max="100" name="meses[]" class="meses" />
                </td>
                <td>
                    <input type="number" name="subtotal[]" class="subtotal"  readonly="readonly"/>
                </td>
                <td> 
                    <input type="number" name="iva[]" class="iva" readonly="readonly"/> 
                </td>
                <td> 
                    <input type="number" name="total[]" class="total" readonly="readonly"/> 
                </td>
                <td> 
                    <input type="date" name="vence[]" class="vence" /> 
                </td>
                <td>
                    <a rel="agregar-fila" class="agregar" onclick="agregarPlan(this)"><i class="fi-plus"></i></a>
                    <a rel="borrar-fila" class="suprimir"  onclick="removerPlan(this)"><i class="fi-trash"></i></a>
                </td>        
            </tr>
        </tbody>
    </table>
    <!--
    <div class="row planes">
        <div class="row item lista head">    
            <div class="small-2 columns">Plan</div>
            <div class="small-2 columns">Valor</div>
            <div class="small-1 columns">Descuento</div>
            <div class="small-2 columns">Subtotal</div>
            <div class="small-2 columns">IVA</div>
            <div class="small-2 columns">Total con IVA</div>
            <div class="small-2 columns">Vence</div>
            <div class="medium-1 columns">Acciones</div>
        </div>
        <div class="row item lista body">
            <div class="small-2 columns">
                <select name="id_plan[]" class="plan" onchange="buscarPlan(this)">
                    <option value="">-Seleccione-</option>
                    @foreach($planes as $p)
                        <option value="{{ $p->id }}">{{ $p->nombre }}</option>
                    @endforeach
                </select>
            </div>
            <div class="small-2 columns">
                <input type="number" name="valor[]" class="valor" readonly="readonly" />
            </div>
            <div class="small-1 columns">
                <input type="number" min="0" max="100" name="descuento[]" class="descuento" onkeyup="calcularTotal(this)" />
            </div>
            <div class="small-2 columns">
                <input type="number" name="subtotal[]" class="subtotal"  readonly="readonly"/>
            </div>
            <div class="small-2 columns"> 
                <input type="number" name="iva[]" class="iva" readonly="readonly"/> 
            </div>
            <div class="small-2 columns"> 
                <input type="number" name="total[]" class="total" readonly="readonly"/> 
            </div>
            <div class="small-2 columns"> 
                <input type="date" name="vence[]" class="vence" /> 
            </div>
            <div class="medium-1 columns">
                <a rel="agregar-fila" class="agregar" onclick="agregarPlan(this)"><i class="fi-plus"></i></a>
                <a rel="borrar-fila" class="suprimir"  onclick="removerPlan(this)"><i class="fi-trash"></i></a>
            </div>
        </div>        
    </div>
    -->
    <div class="row">
        <div class="small-12 columns">
            <label for="archivo">Adjuntar archivo</label>
        </div>
        <div class="small-12 columns">
            <input type="file" name="archivos[]" id="archivos" />
        </div>
    </div>

    <div class="row">
        <div class="small-12 columns">
            <label for="mensaje">Mensaje adicional</label>
        </div>
        <div class="small-12 columns">
            <textarea name="mensaje" id="mensaje" value="" /></textarea>
        </div>
    </div>
       
    <div class="row">
        <div class="small-12 columns">
            <input type="submit" value="Enviar" class="button default" />
        </div>
    </div>
</form>
@stop