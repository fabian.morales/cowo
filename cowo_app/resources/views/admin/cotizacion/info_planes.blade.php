@foreach($paquetes as $key => $p)
    <p>Plan</p>
    <h1 style="color: #e9b155">{{ $p->nombre }}</h1>
    <p>{!! $p->descripcion !!}</p>
    
    <table style="border: 1px solid #fff; font-family: verdana; width: 100%; font-size: 13px;">
        <thead>
            <tr style="background: #fff; color: #000">
                <td style="padding: 10px;">Valor</td>
                <td>Dcto</td>
                <td>Horas</td>
                <td>Cant.</td>
                <td>Periodos</td>
                <td>Subtotal</td>
                <td>IVA</td>
                <td>TOTAL</td>
                <td>Vence</td>
                <td>Comprar</td>
            </tr>
        </thead>
         <tbody>
            @foreach($p->descuentos as $d)
            @php
                $cantidad = (int)$d->cantidad > 0 ? (int)$d->cantidad : 1;
                $meses = (int)$d->meses > 0 ? (int)$d->meses : 1;
                $horas = (int)$d->horas > 0 ? (int)$d->horas : 1;
                $subtotal = ($d->precio - (($d->precio * $d->valor) / 100)) * $cantidad * $meses * $horas;
                $iva = ($subtotal * 19 / 100);
                $total = ($iva + $subtotal);
            @endphp
            <tr style="background: #eee; color: #000">
                <td style="text-align: center; padding: 10px;">${{number_format( $d->precio != NULL ? $d->precio : $p->valor )}}</td>
                <td style="text-align: center;">{{ $d->valor != NULL ? $d->valor : 'N.A.' }}%</td>
                <td style="text-align: center;">{{ $d->horas != NULL ? $d->horas : 'N.A.' }}</td>
                <td style="text-align: center;">{{ $d->cantidad != NULL ? $d->cantidad : 'N.A.' }}</td>
                <td style="text-align: center;">{{ $d->meses != NULL ? $d->meses : 'N.A.' }}</td>
                <td style="text-align: center;">${{ Helper::number_format($subtotal) }}</td>
                <td style="text-align: center;">${{ Helper::number_format($iva) }}</td>
                <td style="text-align: center;">${{ Helper::number_format($total) }}</td>
                <td style="text-align: center;">{{ $d->fecha_vencimiento }}</td>
                <td style="text-align: center;"><a href="{{url('/compra/paquete', [$idCotizacion, $d->id])}}" style='background-color: #e9b155; color: #4f130c; font-weight: bold; padding: 10px; display: inline-block; font-size: .8rem; border-radius: 3px; text-align: center; text-decoration: none;'>Comprar aqu&iacute;</a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <br />
    <hr style="border: 1px dashed #eee;" />
    <br />
@endforeach

@if($mensaje != '')
    <div style="padding: 15px; background: #eee; font-size: 13px;">
        <b>Observaciones:</b> <br />
        {{$mensaje}}
    </div>
@endif

<br /><br />