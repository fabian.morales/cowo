@extends('admin')

@section('js_header')
<script>
(function($, window){
    $(document).ready(function() {
        $("#btnLimpiar").click(function(e) {
            e.preventDefault();
            $("#titulo").val('');
        });
        
        $("#lnkFiltro").click(function(e) {
            e.preventDefault();
            $("#form_filtro").toggle("slow", function() {
                if ($("#form_filtro").is(":visible")){
                    $("#lnkFiltro i").removeClass("fi-arrow-down").addClass("fi-arrow-up");
                }
                else{
                    $("#lnkFiltro i").removeClass("fi-arrow-up").addClass("fi-arrow-down");
                }
            });
        });
    });
})(jQuery, window);
</script>
@stop

@section('content')
<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('administrador/publicidad/crear') }}" class="button alert">Publicidad nueva <i class="fi-plus"></i></a>
    </div>
</div>

<div class="row titulo lista">
    <div class="small-12 columns">
        Filtro 
        <a id="lnkFiltro" href="#">
            <i class="fi-arrow-up">&nbsp;</i>
        </a>
    </div>
</div>

<form id="form_filtro" method='post' action='{{ url('/administrador/publicidad') }}'>
    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
    <div class="row">
        <div class="medium-4 columns">
            <div>Nombre</div>
            <input type="text" name="titulo" id="titulo" value='' />
        </div>
    </div>
    <div class="row">
        <div class="medium-12 columns text-right">
            <input type="button" value="Limpiar" class="tiny button warning" id='btnLimpiar' />
            <input type="submit" value="Filtrar" class="tiny button default" />
        </div>
    </div>
    <hr />
</form>
<br />

@include('admin.publicidad.lista', array("publicidad" => $publicidad))
@stop