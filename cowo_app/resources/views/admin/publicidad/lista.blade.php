
<div class="row titulo lista">
    <div class="small-12 columns">Editar Publicidad</div>
</div>
<div class="row item lista head">
    <div class="small-10 columns">Nombre</div>
    <div class="small-2 columns">Acciones</div>
</div>
@foreach($publicidad as $p)
<div class="row item lista">
    <div class="small-10 columns">{{ $p->nombre }}</div>
    <div class="small-2 columns">
        <a class="tooltip-x" title='Editar publicidad' href="{{ url('administrador/publicidad/editar/'.$p->id) }}"><i class="fi-pencil"></i></a>
        <a class="tooltip-x" title='Borrar publicidad' href="{{ url('administrador/publicidad/borrar/'.$p->id) }}"><i class="fi-trash"></i></a>
    </div>
</div>
@endforeach
