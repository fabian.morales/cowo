@extends('admin')

@section('js_header')
<script src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>
<script>
    (function(window, $){
        $(document).ready(function() {
            var $datePickerOpc = {
                dateFormat: "yy-mm-dd",
                dayNames: [ "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado" ],
                dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
                dayNamesShort: [ "Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb" ],
                monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
                monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ],
                currentText: "Hoy",
                closeText: "Aceptar",
                showButtonPanel: true
            };
        
            tinymce.init({
                selector: "textarea#cuerpo",
                height: 300,
                theme: 'modern',
                language: 'es',
                relative_urls: false,
                document_base_url: 'http://encarguelo.com/',
                remove_script_host: false,
                extended_valid_elements: 'span,iframe[src|frameborder|style|scrolling|class|width|height|name|align]',
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor varsecciones",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste filemanager textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | forecolor backcolor | bold italic fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | visualblocks | table ",
                image_advtab: true
            });
            
            $("#btnAgregarVar").click(function(e) {
                e.preventDefault();
                tinymce.activeEditor.execCommand('mceInsertContent', false, $("#variable").val());
            });
            
            $("#fecha_inicio, #fecha_fin").datepicker($datePickerOpc);
        });
    })(window, jQuery);

</script>
@stop

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Editar publicidad</span></h3>
    </div>
</div>
<form id="form_publicidad" name="form_publicidad" action="{{ url('administrador/publicidad/guardar') }}" method="post" enctype="multipart/form-data">
    <input type="hidden" id="id" name="id" value="{{ $publicidad->id }}" />
    <input type="hidden" id="tipo" name="tipo" value="P" />
    <input type="hidden" id="llave" name="llave" value="{{ $publicidad->id }}" />
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="small-12 columns">
            <label for="titulo">Nombre</label>
            <input type='text' name='nombre' id='nombre' value='{{ $publicidad->nombre }}' />
        </div>
        <div class="small-12 columns">
            <label for="titulo">Fecha inicio</label>
            <input type='text' name='fecha_inicio' id='fecha_inicio' value='{{ $publicidad->fecha_inicio }}' />
        </div>
        <div class="small-12 columns">
            <label for="titulo">Fecha fin</label>
            <input type='text' name='fecha_fin' id='fecha_fin' value='{{ $publicidad->fecha_fin }}' />
        </div>
        <div class="small-12 columns">
            <label for="mostrar_logueado">
                Mostrar para usuarios logueados
                <input type='checkbox' name='mostrar_logueado' id='mostrar_logueado' value='S' @if($publicidad->mostrar_logueado == 'S') checked="checked" @endif />
            </label>
        </div>
        <div class="small-12 columns">
            <label for="activo">
                Activo
                <input type='checkbox' name='activo' id='activo' value='S' @if($publicidad->activo == 'S') checked="checked" @endif />
            </label>
        </div>
        <div class="small-12 columns">
            <label for="cuerpo">Contenido de la publicidad</label>
        </div>
        <div class="small-12 columns">
            <textarea id='cuerpo' name='cuerpo'>{{ $publicidad->cuerpo }}</textarea>
        </div>
    </div>
    
    <div class="row">
        <div class="small-12 columns">
            <input type="submit" value="Guardar" class="button default" />
            <input type="submit" name='guardar_permancer' value="Guardar y permanecer" class="button default warning" />
            <a class="button alert" href="{{ url('/administrador/publicidad') }}" />Cancelar</a>
        </div>
    </div>
</form>
@stop