@extends('emails')

@section('content')
    <br /><br /> Apreciado usuario, <br /><br />    
    
    En respuesta a tu solicitud de Recuperación de Clave, te invitamos a presionar el siguiente enlace para continuar con el proceso:  <br /><br />    
    <a href="{{ $link = url('cliente/clave/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a>    
    
    <br /><br /> En caso de no tener conocimiento de este hecho, por favor has caso omiso de este mensaje.<br /><br />
    Cordialmente,
@stop
        