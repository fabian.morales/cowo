@extends('master')

@section('js_header')
@stop

@section('content')                    
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="amarillo">Recuperar clave</h3>
            <br />
            <p>Ingresa tu direcci&oacute;n de correo</p>
            <form method="post" action="{{ url('/cliente/clave/email') }}" class="form">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="row">
                    <div class="col-md-3 columns"><label class="inline" for="email">Email</label></div>
                    <div class="col-md-9 columns"><input type="text" name="email" value="" /></div>
                    <div class="col-sm-12 columns"><input type="submit" value="Enviar" class="boton crema" /></div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
