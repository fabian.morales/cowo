@extends('master')

@section('js_header')
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="small-6col-sm-12 columns">
            <h3 class="amarillo">Cambia tu clave</h3>
            <div class="clearfix"></div>
            <br />
            <p>Diligencia este formulario para cambiar tu clave</p>
            <form method="post" action="{{ url('/cliente/clave/reset') }}" class="form">
                <input type="hidden" name="token" value="{{ $token }}" />
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="row">
                    <div class="col-md-3 columns"><label class="inline" for="email">Email</label></div>
                    <div class="col-md-9 columns"><input type="text" name="email" value="" class="form-control" /></div>
                    <div class="col-md-3 columns"><label class="inline" for="password_confirmation">Clave nueva</label></div>
                    <div class="col-md-9 columns"><input type="password" name="password" value="" class="form-control" /></div>
                    <div class="col-md-3 columns"><label class="inline" for="password_confirmation">Confirmar clave</label></div>
                    <div class="col-md-9 columns"><input type="password" name="password_confirmation" value="" class="form-control" /></div>
                    <div class="col-sm-12 columns"><input type="submit" value="Cambiar clave" class="boton crema" /></div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop