@extends('admin')

@section('content')
<fieldset>
    <legend><b>Informaci&oacute;n de Cotizaci&oacute;n</b></legend>

    <div class="row item">    
        <div class="medium-3 columns">Cliente</div>
        <div class="medium-3 columns">Comercial</div>
        <div class="medium-2 columns">Fecha</div>
        <div class="medium-1 columns">Estado</div>
    </div>
    <div class="row item">
    
        <div class="medium-3 columns">{{ $cotizacion->cliente !== NULL ? $cotizacion->cliente->nombre .' '. $cotizacion->cliente->apellido : $cotizacion->correo }}</div>
        <div class="medium-3 columns">{{ $cotizacion->usuario->nombre }}</div>
        <div class="medium-2 columns">{{ $cotizacion->fecha_creacion }}</div>
        <div class="medium-1 columns">{{ \App\Helpers\Helper::estados()[$cotizacion->estado]}}</div>                            
    </div>
        
</fieldset>
<hr></hr>
<fieldset>
    <legend><b>Cambiar estado</b></legend>
    
    <form id="form_cotizacion" name="form_cotizacion" action="{{ url('comercial/cotizaciones/estados/cambiar', ['id' => $cotizacion->id]) }}" method="post" enctype="multipart/form-data">
    
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="row">
            <div class="small-12 columns">
                <label for="nombre">Estado</label>
            </div>
            <div class="small-12 columns">
                <select name="estado" id="estado">
                    <option value="">-Seleccione-</option>
                    @foreach($estados as $key => $estado)
                        <option value="{{$key}}" {{$key == $cotizacion->estado ? 'selected="Selected"' : ''}} >{{$estado}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        
        <div class="row">
            <div class="small-12 columns">
                <input type="submit" value="Enviar" class="button default" />
            </div>
        </div>
    </form>
    
</fieldset>
@stop