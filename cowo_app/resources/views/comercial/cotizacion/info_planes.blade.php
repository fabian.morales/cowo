@foreach($planes as $key => $plan)
    <p>Plan</p>
	<h1 style="color: #e9b155">{{$plan->nombre}}</h1>
    <p>{!!$plan->descripcion!!}</p>
    
    <table style="border: 1px solid #fff; font-family: verdana; width: 100%; font-size: 13px;">
        <thead><tr style="background: #fff; color: #000">
            <td style="padding: 10px;">Valor</td>
            <td>Dcto</td>
            <td>Horas</td>
            <td>Cant.</td>
            <td>Periodos</td>
            <td>Subtotal</td>
            <td>IVA</td>
            <td>TOTAL</td>
            <td>Vence</td>
         </tr></thead>
         <tbody>                
                <tr style="background: #e9b155; color: #000">
                    <td style="text-align: center; padding: 10px;">${{number_format( $valores[$key] != NULL ? $valores[$key] : $plan->valor )}}</td>
                    <td style="text-align: center;">{{$descuento[$key] != NULL ? $descuento[$key] : 'N.A.'}}%</td>
                    <td style="text-align: center;">{{$horas[$key] != NULL ? $horas[$key] : 'N.A.'}}</td>
                    <td style="text-align: center;">{{$cantidad[$key] != NULL ? $cantidad[$key] : 'N.A.' }}</td>
                    <td style="text-align: center;">{{$meses[$key] != NULL ? $meses[$key] : 'N.A.'}}</td>
                    <td style="text-align: center;">${{Helper::number_format($subtotal[$key])}}</td>
                    <td style="text-align: center;">${{Helper::number_format($iva[$key])}}</td>
                    <td style="text-align: center;">${{Helper::number_format($total[$key])}}</td>
                    <td style="text-align: center;">{{$vence[$key]}}</td>
                </tr>
                
            </tbody>
    </table>
    <br />
    <a href="{{url('/compra/paquete', [$idCotizacion, $plan->id])}}" style='background-color: #e9b155; color: #4f130c; font-weight: bold; padding: 10px; display: inline-block; font-size: .8rem; border-radius: 3px; text-align: center; text-decoration: none;'>Comprar aquí</a>
    <br />
    <hr style="border: 1px dashed #eee;" />
	<br />
@endforeach

@if($mensaje != '')
    <div style="padding: 15px; background: #eee; font-size: 13px;">
        <b>Observaciones:</b> <br />
        {{$mensaje}}
    </div>
@endif

<br /><br />