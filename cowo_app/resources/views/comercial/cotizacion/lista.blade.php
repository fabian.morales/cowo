@extends('admin')

@section('content')
<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('comercial/cotizaciones/cotizar') }}" class="button alert">Enviar cotizaci&oacute;n <i class="fi-plus"></i></a>
    </div>
</div>

<form id="form_usuario" name="form_usuario" action="{{ url('comercial/cotizaciones/') }}" method="get">
    <div class="row">
        <div class="medium-4 small-12 columns">
            <input type="text" name="filtro" id="login" placeholder="Buscar" value="{{ $filtro }}" />
            <input type="submit" value="Buscar" class="button default" />
        </div>
    </div>
</form>

<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Lista de cotizaciones</span></h3>
    </div>
</div>

<div class="row planes">
    <div class="row item lista head">    
        <div class="medium-3 columns">Cliente</div>
        <div class="medium-3 columns">Comercial</div>
        <div class="medium-2 columns">Fecha</div>
        <div class="medium-1 columns">Estado</div>
        <div class="medium-1 columns">Acciones</div>
    </div>
    @foreach($lista as $cotizacion)
        <div class="row item lista body">
        
            <div class="medium-3 columns">{{ $cotizacion->cliente !== NULL ? $cotizacion->cliente->nombre .' '. $cotizacion->cliente->apellido : $cotizacion->correo }}</div>
            <div class="medium-3 columns">{{ $cotizacion->usuario->nombre }}</div>
            <div class="medium-2 columns">{{ $cotizacion->fecha_creacion }}</div>
            <div class="medium-1 columns">{{ \App\Helpers\Helper::estados()[$cotizacion->estado]}}</div>                
            <div class="medium-1 columns">
                <a href="{{ url('comercial/cotizaciones/ver', ['id' => $cotizacion->id] ) }}"><i class="fi-magnifying-glass"></i></a>
                @if($cotizacion->estado == 'P')
                    <a href="{{ url('comercial/cotizaciones/eliminar', ['id' => $cotizacion->id] ) }}" onclick="return confirm('desea eliminar este item?')"><i class="fi-trash"></i></a>
                @endif
            </div>
        </div>       
    @endforeach
</div>

<div class="row">
    <div class="small-12 columns text-center">
        {!! $lista->render() !!}
    </div>
</div>
@stop