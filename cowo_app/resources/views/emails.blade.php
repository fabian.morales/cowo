<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
        <style>
            body{
                font-family: 'verdana', 'sans-serif';
                font-size: 11px;
                text-align: justify;
                padding: 20px;
            }
            
            body p{
                font-size: 11px;
            }
            
            table thead{
                text-align: center;
                background: #336699;
                color: #fff;
                font-weight: bold;
            }
            
        </style>
    </head>
    <body>
        @yield('content')
        <br />
        <table style="width: 100%; display: block; padding: 10px 0;">
            <tr>
                <td valign="top"><img src="{{ asset('/template/images/cowo-shine.jpg') }}" style="float: left; margin-right: 5px;" /></td>
                <td style="padding-left: 10px; border-left: 2px solid #000; font-size: 0.9rem;">Daniela Alvarez Costa <br />
                    Comercial<br />
                    danielac@cowo.com.co <br />
                    319-499-6064 <br />
                    Calle 98 #8-37, Bogotá - Colombia <br />
                    <a href="https://www.facebook.com/cowobogota" target="_blank"><img style="width: 36px; height: 36px; float: left; margin-right: 5px;" src="{{ asset('imagenes/facebook_blanco_email.png') }}" /></a>
                    <a href="https://www.instagram.com/cowobogota/" target="_blank"><img style="width: 36px; height: 36px; float: left; margin-right: 5px;" src="{{ asset('imagenes/instagram_blanco_email.png') }}" /></a>
                    <a href="https://www.linkedin.com/in/cowo-bog-admin/" target="_blank"><img style="width: 36px; height: 36px; float: left; margin-right: 5px;" src="{{ asset('imagenes/linkedin_blanco_email.png') }}" /></a>
                    <br />
                    <a style="clear: both; display: block;" href="http://www.cowo.com.co" target="_blank">www.cowo.com.co</a>
                </td>
            </tr>
        </table>
    </body>
</html>