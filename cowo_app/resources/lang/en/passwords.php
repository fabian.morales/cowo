<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Las claves deben tener al menos 6 caracteres y debe coincidir conla confirmación.',
    'reset' => 'Tu clave ha sido restablecida.',
    'sent' => 'Se ha enviado a tu correo las instrucciones para restablecer tu clave.',
    'token' => 'El token no es válido.',
    'user' => "No hay un usuario registrado con el correo ingresado.",

];
